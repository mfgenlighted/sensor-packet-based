﻿namespace SensorManufSY2
{
    partial class Config_SU_SY2_Fixture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Config_SU_SY2_Fixture));
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.bAutoFindFlasher = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cbFlasherComPort = new System.Windows.Forms.ComboBox();
            this.vAutoFindDUTComPort = new System.Windows.Forms.Button();
            this.bAutofindLEDComPort = new System.Windows.Forms.Button();
            this.bAutoFindSMComPort = new System.Windows.Forms.Button();
            this.bAutoFindTestRadioComPort = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.cbSwitchMateCom = new System.Windows.Forms.ComboBox();
            this.nudControlVoltageGain = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxTSBlueRange = new System.Windows.Forms.TextBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.numericUpDownTSPIRDLSOffDelay = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.numericUpDownTSPIRDLSOnDelay = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.numericUpDownTSAmbDLSOffDelay = new System.Windows.Forms.NumericUpDown();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.numericUpDownTSAmbDLSOnDelay = new System.Windows.Forms.NumericUpDown();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.numericUpDownTSAmbDLSLEDV = new System.Windows.Forms.NumericUpDown();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.textBoxTSGreenRange = new System.Windows.Forms.TextBox();
            this.textBoxTSRedRange = new System.Windows.Forms.TextBox();
            this.comboBoxTSTestRadioComPort = new System.Windows.Forms.ComboBox();
            this.comboBoxTSDUTComPort = new System.Windows.Forms.ComboBox();
            this.comboBoxTSLEDDetectorComPort = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.comboBoxTSProductPrinter = new System.Windows.Forms.ComboBox();
            this.comboBoxTSPCBAPrinter = new System.Windows.Forms.ComboBox();
            this.numericUpDownTSTestChannel = new System.Windows.Forms.NumericUpDown();
            this.comboBoxTSTempProbe = new System.Windows.Forms.ComboBox();
            this.textBoxTSStationOperation = new System.Windows.Forms.TextBox();
            this.textBoxTSStationTitle = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbStationSection = new System.Windows.Forms.Label();
            this.lbStationDB = new System.Windows.Forms.Label();
            this.lbFixtureType = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudControlVoltageGain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSPIRDLSOffDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSPIRDLSOnDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSAmbDLSOffDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSAmbDLSOnDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSAmbDLSLEDV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSTestChannel)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.groupBox4.Controls.Add(this.bAutoFindFlasher);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.cbFlasherComPort);
            this.groupBox4.Controls.Add(this.vAutoFindDUTComPort);
            this.groupBox4.Controls.Add(this.bAutofindLEDComPort);
            this.groupBox4.Controls.Add(this.bAutoFindSMComPort);
            this.groupBox4.Controls.Add(this.bAutoFindTestRadioComPort);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.cbSwitchMateCom);
            this.groupBox4.Controls.Add(this.nudControlVoltageGain);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.textBoxTSBlueRange);
            this.groupBox4.Controls.Add(this.buttonCancel);
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Controls.Add(this.numericUpDownTSPIRDLSOffDelay);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.numericUpDownTSPIRDLSOnDelay);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.label46);
            this.groupBox4.Controls.Add(this.label43);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.label35);
            this.groupBox4.Controls.Add(this.numericUpDownTSAmbDLSOffDelay);
            this.groupBox4.Controls.Add(this.label36);
            this.groupBox4.Controls.Add(this.label37);
            this.groupBox4.Controls.Add(this.label42);
            this.groupBox4.Controls.Add(this.numericUpDownTSAmbDLSOnDelay);
            this.groupBox4.Controls.Add(this.label40);
            this.groupBox4.Controls.Add(this.label41);
            this.groupBox4.Controls.Add(this.numericUpDownTSAmbDLSLEDV);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.label38);
            this.groupBox4.Controls.Add(this.textBoxTSGreenRange);
            this.groupBox4.Controls.Add(this.textBoxTSRedRange);
            this.groupBox4.Controls.Add(this.comboBoxTSTestRadioComPort);
            this.groupBox4.Controls.Add(this.comboBoxTSDUTComPort);
            this.groupBox4.Controls.Add(this.comboBoxTSLEDDetectorComPort);
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Controls.Add(this.comboBoxTSProductPrinter);
            this.groupBox4.Controls.Add(this.comboBoxTSPCBAPrinter);
            this.groupBox4.Controls.Add(this.numericUpDownTSTestChannel);
            this.groupBox4.Controls.Add(this.comboBoxTSTempProbe);
            this.groupBox4.Controls.Add(this.textBoxTSStationOperation);
            this.groupBox4.Controls.Add(this.textBoxTSStationTitle);
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // bAutoFindFlasher
            // 
            resources.ApplyResources(this.bAutoFindFlasher, "bAutoFindFlasher");
            this.bAutoFindFlasher.Name = "bAutoFindFlasher";
            this.toolTip1.SetToolTip(this.bAutoFindFlasher, resources.GetString("bAutoFindFlasher.ToolTip"));
            this.bAutoFindFlasher.UseVisualStyleBackColor = true;
            this.bAutoFindFlasher.Click += new System.EventHandler(this.bAutofindFlasher_Click);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // cbFlasherComPort
            // 
            this.cbFlasherComPort.DropDownWidth = 300;
            this.cbFlasherComPort.FormattingEnabled = true;
            resources.ApplyResources(this.cbFlasherComPort, "cbFlasherComPort");
            this.cbFlasherComPort.Name = "cbFlasherComPort";
            this.toolTip1.SetToolTip(this.cbFlasherComPort, resources.GetString("cbFlasherComPort.ToolTip"));
            this.cbFlasherComPort.DropDown += new System.EventHandler(this.DutComPort_DropDown);
            this.cbFlasherComPort.SelectionChangeCommitted += new System.EventHandler(this.cbFlasherComPort_SelectionChangeCommitted);
            // 
            // vAutoFindDUTComPort
            // 
            resources.ApplyResources(this.vAutoFindDUTComPort, "vAutoFindDUTComPort");
            this.vAutoFindDUTComPort.Name = "vAutoFindDUTComPort";
            this.toolTip1.SetToolTip(this.vAutoFindDUTComPort, resources.GetString("vAutoFindDUTComPort.ToolTip"));
            this.vAutoFindDUTComPort.UseVisualStyleBackColor = true;
            this.vAutoFindDUTComPort.Click += new System.EventHandler(this.bAutoFindDUTComPort_Click);
            // 
            // bAutofindLEDComPort
            // 
            resources.ApplyResources(this.bAutofindLEDComPort, "bAutofindLEDComPort");
            this.bAutofindLEDComPort.Name = "bAutofindLEDComPort";
            this.toolTip1.SetToolTip(this.bAutofindLEDComPort, resources.GetString("bAutofindLEDComPort.ToolTip"));
            this.bAutofindLEDComPort.UseVisualStyleBackColor = true;
            this.bAutofindLEDComPort.Click += new System.EventHandler(this.autofindLED_Click);
            // 
            // bAutoFindSMComPort
            // 
            this.bAutoFindSMComPort.BackColor = System.Drawing.SystemColors.ButtonFace;
            resources.ApplyResources(this.bAutoFindSMComPort, "bAutoFindSMComPort");
            this.bAutoFindSMComPort.Name = "bAutoFindSMComPort";
            this.toolTip1.SetToolTip(this.bAutoFindSMComPort, resources.GetString("bAutoFindSMComPort.ToolTip"));
            this.bAutoFindSMComPort.UseVisualStyleBackColor = true;
            this.bAutoFindSMComPort.Click += new System.EventHandler(this.autofindSM_Click);
            // 
            // bAutoFindTestRadioComPort
            // 
            resources.ApplyResources(this.bAutoFindTestRadioComPort, "bAutoFindTestRadioComPort");
            this.bAutoFindTestRadioComPort.Name = "bAutoFindTestRadioComPort";
            this.toolTip1.SetToolTip(this.bAutoFindTestRadioComPort, resources.GetString("bAutoFindTestRadioComPort.ToolTip"));
            this.bAutoFindTestRadioComPort.UseVisualStyleBackColor = true;
            this.bAutoFindTestRadioComPort.Click += new System.EventHandler(this.bAutofindRefRadio_Click);
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // cbSwitchMateCom
            // 
            this.cbSwitchMateCom.DropDownWidth = 300;
            this.cbSwitchMateCom.FormattingEnabled = true;
            resources.ApplyResources(this.cbSwitchMateCom, "cbSwitchMateCom");
            this.cbSwitchMateCom.Name = "cbSwitchMateCom";
            this.toolTip1.SetToolTip(this.cbSwitchMateCom, resources.GetString("cbSwitchMateCom.ToolTip"));
            this.cbSwitchMateCom.DropDown += new System.EventHandler(this.DutComPort_DropDown);
            this.cbSwitchMateCom.SelectionChangeCommitted += new System.EventHandler(this.comboBoxTSSwitchMateComPort_SelectionChangeCommitted);
            // 
            // nudControlVoltageGain
            // 
            this.nudControlVoltageGain.DecimalPlaces = 1;
            this.nudControlVoltageGain.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            resources.ApplyResources(this.nudControlVoltageGain, "nudControlVoltageGain");
            this.nudControlVoltageGain.Name = "nudControlVoltageGain";
            this.toolTip1.SetToolTip(this.nudControlVoltageGain, resources.GetString("nudControlVoltageGain.ToolTip"));
            this.nudControlVoltageGain.Value = new decimal(new int[] {
            30,
            0,
            0,
            65536});
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // textBoxTSBlueRange
            // 
            resources.ApplyResources(this.textBoxTSBlueRange, "textBoxTSBlueRange");
            this.textBoxTSBlueRange.Name = "textBoxTSBlueRange";
            this.toolTip1.SetToolTip(this.textBoxTSBlueRange, resources.GetString("textBoxTSBlueRange.ToolTip"));
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.Name = "buttonCancel";
            this.toolTip1.SetToolTip(this.buttonCancel, resources.GetString("buttonCancel.ToolTip"));
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.toolTip1.SetToolTip(this.button1, resources.GetString("button1.ToolTip"));
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // numericUpDownTSPIRDLSOffDelay
            // 
            resources.ApplyResources(this.numericUpDownTSPIRDLSOffDelay, "numericUpDownTSPIRDLSOffDelay");
            this.numericUpDownTSPIRDLSOffDelay.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDownTSPIRDLSOffDelay.Name = "numericUpDownTSPIRDLSOffDelay";
            this.toolTip1.SetToolTip(this.numericUpDownTSPIRDLSOffDelay, resources.GetString("numericUpDownTSPIRDLSOffDelay.ToolTip"));
            this.numericUpDownTSPIRDLSOffDelay.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.Name = "label24";
            // 
            // label45
            // 
            resources.ApplyResources(this.label45, "label45");
            this.label45.Name = "label45";
            // 
            // numericUpDownTSPIRDLSOnDelay
            // 
            resources.ApplyResources(this.numericUpDownTSPIRDLSOnDelay, "numericUpDownTSPIRDLSOnDelay");
            this.numericUpDownTSPIRDLSOnDelay.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDownTSPIRDLSOnDelay.Name = "numericUpDownTSPIRDLSOnDelay";
            this.toolTip1.SetToolTip(this.numericUpDownTSPIRDLSOnDelay, resources.GetString("numericUpDownTSPIRDLSOnDelay.ToolTip"));
            this.numericUpDownTSPIRDLSOnDelay.Value = new decimal(new int[] {
            2500,
            0,
            0,
            0});
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // label46
            // 
            resources.ApplyResources(this.label46, "label46");
            this.label46.Name = "label46";
            // 
            // label43
            // 
            resources.ApplyResources(this.label43, "label43");
            this.label43.Name = "label43";
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.Name = "label23";
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.Name = "label25";
            // 
            // label35
            // 
            resources.ApplyResources(this.label35, "label35");
            this.label35.Name = "label35";
            // 
            // numericUpDownTSAmbDLSOffDelay
            // 
            resources.ApplyResources(this.numericUpDownTSAmbDLSOffDelay, "numericUpDownTSAmbDLSOffDelay");
            this.numericUpDownTSAmbDLSOffDelay.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDownTSAmbDLSOffDelay.Name = "numericUpDownTSAmbDLSOffDelay";
            this.toolTip1.SetToolTip(this.numericUpDownTSAmbDLSOffDelay, resources.GetString("numericUpDownTSAmbDLSOffDelay.ToolTip"));
            this.numericUpDownTSAmbDLSOffDelay.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // label36
            // 
            resources.ApplyResources(this.label36, "label36");
            this.label36.Name = "label36";
            // 
            // label37
            // 
            resources.ApplyResources(this.label37, "label37");
            this.label37.Name = "label37";
            // 
            // label42
            // 
            resources.ApplyResources(this.label42, "label42");
            this.label42.Name = "label42";
            // 
            // numericUpDownTSAmbDLSOnDelay
            // 
            resources.ApplyResources(this.numericUpDownTSAmbDLSOnDelay, "numericUpDownTSAmbDLSOnDelay");
            this.numericUpDownTSAmbDLSOnDelay.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDownTSAmbDLSOnDelay.Name = "numericUpDownTSAmbDLSOnDelay";
            this.toolTip1.SetToolTip(this.numericUpDownTSAmbDLSOnDelay, resources.GetString("numericUpDownTSAmbDLSOnDelay.ToolTip"));
            this.numericUpDownTSAmbDLSOnDelay.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // label40
            // 
            resources.ApplyResources(this.label40, "label40");
            this.label40.Name = "label40";
            // 
            // label41
            // 
            resources.ApplyResources(this.label41, "label41");
            this.label41.Name = "label41";
            // 
            // numericUpDownTSAmbDLSLEDV
            // 
            this.numericUpDownTSAmbDLSLEDV.DecimalPlaces = 1;
            resources.ApplyResources(this.numericUpDownTSAmbDLSLEDV, "numericUpDownTSAmbDLSLEDV");
            this.numericUpDownTSAmbDLSLEDV.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownTSAmbDLSLEDV.Name = "numericUpDownTSAmbDLSLEDV";
            this.toolTip1.SetToolTip(this.numericUpDownTSAmbDLSLEDV, resources.GetString("numericUpDownTSAmbDLSLEDV.ToolTip"));
            this.numericUpDownTSAmbDLSLEDV.Value = new decimal(new int[] {
            20,
            0,
            0,
            65536});
            // 
            // label29
            // 
            resources.ApplyResources(this.label29, "label29");
            this.label29.Name = "label29";
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.Name = "label30";
            // 
            // label38
            // 
            resources.ApplyResources(this.label38, "label38");
            this.label38.Name = "label38";
            // 
            // textBoxTSGreenRange
            // 
            resources.ApplyResources(this.textBoxTSGreenRange, "textBoxTSGreenRange");
            this.textBoxTSGreenRange.Name = "textBoxTSGreenRange";
            this.toolTip1.SetToolTip(this.textBoxTSGreenRange, resources.GetString("textBoxTSGreenRange.ToolTip"));
            // 
            // textBoxTSRedRange
            // 
            resources.ApplyResources(this.textBoxTSRedRange, "textBoxTSRedRange");
            this.textBoxTSRedRange.Name = "textBoxTSRedRange";
            this.toolTip1.SetToolTip(this.textBoxTSRedRange, resources.GetString("textBoxTSRedRange.ToolTip"));
            // 
            // comboBoxTSTestRadioComPort
            // 
            this.comboBoxTSTestRadioComPort.DropDownWidth = 300;
            this.comboBoxTSTestRadioComPort.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxTSTestRadioComPort, "comboBoxTSTestRadioComPort");
            this.comboBoxTSTestRadioComPort.Name = "comboBoxTSTestRadioComPort";
            this.toolTip1.SetToolTip(this.comboBoxTSTestRadioComPort, resources.GetString("comboBoxTSTestRadioComPort.ToolTip"));
            this.comboBoxTSTestRadioComPort.DropDown += new System.EventHandler(this.DutComPort_DropDown);
            this.comboBoxTSTestRadioComPort.SelectionChangeCommitted += new System.EventHandler(this.cbRefRadioComPort_SelectionChangeCommitted);
            // 
            // comboBoxTSDUTComPort
            // 
            this.comboBoxTSDUTComPort.DropDownWidth = 300;
            this.comboBoxTSDUTComPort.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxTSDUTComPort, "comboBoxTSDUTComPort");
            this.comboBoxTSDUTComPort.Name = "comboBoxTSDUTComPort";
            this.toolTip1.SetToolTip(this.comboBoxTSDUTComPort, resources.GetString("comboBoxTSDUTComPort.ToolTip"));
            this.comboBoxTSDUTComPort.DropDown += new System.EventHandler(this.DutComPort_DropDown);
            this.comboBoxTSDUTComPort.SelectionChangeCommitted += new System.EventHandler(this.comboBoxTSDUTComPort_SelectionChangeCommitted);
            // 
            // comboBoxTSLEDDetectorComPort
            // 
            this.comboBoxTSLEDDetectorComPort.DropDownWidth = 300;
            this.comboBoxTSLEDDetectorComPort.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxTSLEDDetectorComPort, "comboBoxTSLEDDetectorComPort");
            this.comboBoxTSLEDDetectorComPort.Name = "comboBoxTSLEDDetectorComPort";
            this.toolTip1.SetToolTip(this.comboBoxTSLEDDetectorComPort, resources.GetString("comboBoxTSLEDDetectorComPort.ToolTip"));
            this.comboBoxTSLEDDetectorComPort.DropDown += new System.EventHandler(this.DutComPort_DropDown);
            this.comboBoxTSLEDDetectorComPort.SelectionChangeCommitted += new System.EventHandler(this.cbLEDSensorComPort_SelectionChangeCommitted);
            // 
            // label33
            // 
            resources.ApplyResources(this.label33, "label33");
            this.label33.Name = "label33";
            // 
            // comboBoxTSProductPrinter
            // 
            this.comboBoxTSProductPrinter.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxTSProductPrinter, "comboBoxTSProductPrinter");
            this.comboBoxTSProductPrinter.Name = "comboBoxTSProductPrinter";
            this.toolTip1.SetToolTip(this.comboBoxTSProductPrinter, resources.GetString("comboBoxTSProductPrinter.ToolTip"));
            this.comboBoxTSProductPrinter.DropDown += new System.EventHandler(this.buttonFindBarcodeFile_Click);
            // 
            // comboBoxTSPCBAPrinter
            // 
            this.comboBoxTSPCBAPrinter.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxTSPCBAPrinter, "comboBoxTSPCBAPrinter");
            this.comboBoxTSPCBAPrinter.Name = "comboBoxTSPCBAPrinter";
            this.toolTip1.SetToolTip(this.comboBoxTSPCBAPrinter, resources.GetString("comboBoxTSPCBAPrinter.ToolTip"));
            this.comboBoxTSPCBAPrinter.DropDown += new System.EventHandler(this.buttonFindBarcodeFile_Click);
            // 
            // numericUpDownTSTestChannel
            // 
            resources.ApplyResources(this.numericUpDownTSTestChannel, "numericUpDownTSTestChannel");
            this.numericUpDownTSTestChannel.Maximum = new decimal(new int[] {
            26,
            0,
            0,
            0});
            this.numericUpDownTSTestChannel.Minimum = new decimal(new int[] {
            11,
            0,
            0,
            0});
            this.numericUpDownTSTestChannel.Name = "numericUpDownTSTestChannel";
            this.toolTip1.SetToolTip(this.numericUpDownTSTestChannel, resources.GetString("numericUpDownTSTestChannel.ToolTip"));
            this.numericUpDownTSTestChannel.Value = new decimal(new int[] {
            11,
            0,
            0,
            0});
            // 
            // comboBoxTSTempProbe
            // 
            this.comboBoxTSTempProbe.FormattingEnabled = true;
            this.comboBoxTSTempProbe.Items.AddRange(new object[] {
            resources.GetString("comboBoxTSTempProbe.Items"),
            resources.GetString("comboBoxTSTempProbe.Items1"),
            resources.GetString("comboBoxTSTempProbe.Items2")});
            resources.ApplyResources(this.comboBoxTSTempProbe, "comboBoxTSTempProbe");
            this.comboBoxTSTempProbe.Name = "comboBoxTSTempProbe";
            this.toolTip1.SetToolTip(this.comboBoxTSTempProbe, resources.GetString("comboBoxTSTempProbe.ToolTip"));
            // 
            // textBoxTSStationOperation
            // 
            resources.ApplyResources(this.textBoxTSStationOperation, "textBoxTSStationOperation");
            this.textBoxTSStationOperation.Name = "textBoxTSStationOperation";
            this.toolTip1.SetToolTip(this.textBoxTSStationOperation, resources.GetString("textBoxTSStationOperation.ToolTip"));
            // 
            // textBoxTSStationTitle
            // 
            resources.ApplyResources(this.textBoxTSStationTitle, "textBoxTSStationTitle");
            this.textBoxTSStationTitle.Name = "textBoxTSStationTitle";
            this.toolTip1.SetToolTip(this.textBoxTSStationTitle, resources.GetString("textBoxTSStationTitle.ToolTip"));
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // lbStationSection
            // 
            resources.ApplyResources(this.lbStationSection, "lbStationSection");
            this.lbStationSection.Name = "lbStationSection";
            // 
            // lbStationDB
            // 
            resources.ApplyResources(this.lbStationDB, "lbStationDB");
            this.lbStationDB.Name = "lbStationDB";
            // 
            // lbFixtureType
            // 
            resources.ApplyResources(this.lbFixtureType, "lbFixtureType");
            this.lbFixtureType.Name = "lbFixtureType";
            // 
            // Config_SU_SY2_Fixture
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ControlBox = false;
            this.Controls.Add(this.lbFixtureType);
            this.Controls.Add(this.lbStationDB);
            this.Controls.Add(this.lbStationSection);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.label18);
            this.Name = "Config_SU_SY2_Fixture";
            this.Load += new System.EventHandler(this.SetupForm);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudControlVoltageGain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSPIRDLSOffDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSPIRDLSOnDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSAmbDLSOffDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSAmbDLSOnDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSAmbDLSLEDV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSTestChannel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.NumericUpDown numericUpDownTSPIRDLSOffDelay;
        private System.Windows.Forms.NumericUpDown numericUpDownTSPIRDLSOnDelay;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.NumericUpDown numericUpDownTSAmbDLSOffDelay;
        private System.Windows.Forms.NumericUpDown numericUpDownTSAmbDLSOnDelay;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.NumericUpDown numericUpDownTSAmbDLSLEDV;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBoxTSGreenRange;
        private System.Windows.Forms.TextBox textBoxTSRedRange;
        private System.Windows.Forms.ComboBox comboBoxTSTestRadioComPort;
        private System.Windows.Forms.ComboBox comboBoxTSDUTComPort;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox comboBoxTSLEDDetectorComPort;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox comboBoxTSProductPrinter;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox comboBoxTSPCBAPrinter;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.NumericUpDown numericUpDownTSTestChannel;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox comboBoxTSTempProbe;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxTSStationOperation;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxTSStationTitle;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbStationSection;
        private System.Windows.Forms.Label lbStationDB;
        private System.Windows.Forms.Label lbFixtureType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxTSBlueRange;
        private System.Windows.Forms.NumericUpDown nudControlVoltageGain;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbSwitchMateCom;
        private System.Windows.Forms.Button bAutoFindTestRadioComPort;
        private System.Windows.Forms.Button bAutofindLEDComPort;
        private System.Windows.Forms.Button bAutoFindSMComPort;
        private System.Windows.Forms.Button vAutoFindDUTComPort;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbFlasherComPort;
        private System.Windows.Forms.Button bAutoFindFlasher;
    }
}