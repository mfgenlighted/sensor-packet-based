﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorManufSY2
{
    public partial class GetHWInfo : Form
    {
        public string Data;

        public GetHWInfo()
        {
            InitializeComponent();
        }

        private void bSet_Click(object sender, EventArgs e)
        {
            Data = tbData.Text;
            DialogResult = DialogResult.OK;
            return;
        }
    }
}
