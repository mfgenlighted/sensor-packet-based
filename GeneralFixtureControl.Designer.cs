﻿namespace SensorManufSY2
{
    partial class GeneralFixtureControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bCalTemp = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.nudRebootWaitMS = new System.Windows.Forms.NumericUpDown();
            this.bReboot = new System.Windows.Forms.Button();
            this.bPOST = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.tbTempx = new System.Windows.Forms.Label();
            this.tbPIR = new System.Windows.Forms.TextBox();
            this.tbTemp = new System.Windows.Forms.TextBox();
            this.tbAmbient = new System.Windows.Forms.TextBox();
            this.bReadSensors = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.nudDim2Voltage = new System.Windows.Forms.NumericUpDown();
            this.nudDim1Voltage = new System.Windows.Forms.NumericUpDown();
            this.bSetDim1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbNone = new System.Windows.Forms.RadioButton();
            this.rbBlue = new System.Windows.Forms.RadioButton();
            this.rbGreen = new System.Windows.Forms.RadioButton();
            this.rbRed = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bDUTtoRef = new System.Windows.Forms.Button();
            this.nBTtimeout = new System.Windows.Forms.NumericUpDown();
            this.bReftoDUT = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.rtStatusWindow = new System.Windows.Forms.RichTextBox();
            this.bHwConfig = new System.Windows.Forms.Button();
            this.cbSetRefRadio = new System.Windows.Forms.CheckBox();
            this.bClearInputBuffers = new System.Windows.Forms.Button();
            this.cbDisplayData = new System.Windows.Forms.CheckBox();
            this.bReadTestRadioParams = new System.Windows.Forms.Button();
            this.bReadDUTRadioParams = new System.Windows.Forms.Button();
            this.bSetTestRadioParams = new System.Windows.Forms.Button();
            this.bSetRadioParams = new System.Windows.Forms.Button();
            this.bClearManData = new System.Windows.Forms.Button();
            this.bReadManData = new System.Windows.Forms.Button();
            this.bMACConfig = new System.Windows.Forms.Button();
            this.bGetVersions = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.bHLAConfig = new System.Windows.Forms.Button();
            this.bPCBAConfig = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbOscStatus = new System.Windows.Forms.Label();
            this.lbPIRMin = new System.Windows.Forms.Label();
            this.lbPIRMax = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.bPIROscTest = new System.Windows.Forms.Button();
            this.RadioTest = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.nudPowerLevel = new System.Windows.Forms.NumericUpDown();
            this.lbTestChannel = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.nudLQIT = new System.Windows.Forms.NumericUpDown();
            this.nudLQIM = new System.Windows.Forms.NumericUpDown();
            this.bTestRaio = new System.Windows.Forms.Button();
            this.LEDColor = new System.Windows.Forms.TextBox();
            this.textLEDIntest = new System.Windows.Forms.Label();
            this.textLEDFreq = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBoxTestIndicator = new System.Windows.Forms.CheckBox();
            this.buttonExit = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDownLEDVoltage = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.cbMeasurementToRead = new System.Windows.Forms.ComboBox();
            this.channelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dgChannelData = new System.Windows.Forms.DataGridView();
            this.cName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFunction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cChannel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bWriteValue = new System.Windows.Forms.Button();
            this.nudSingleValue = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.bLEDDetectorRead = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.bProgramCode = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudRebootWaitMS)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDim2Voltage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDim1Voltage)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nBTtimeout)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.RadioTest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPowerLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQIM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLEDVoltage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgChannelData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSingleValue)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // bCalTemp
            // 
            this.bCalTemp.Location = new System.Drawing.Point(6, 113);
            this.bCalTemp.Name = "bCalTemp";
            this.bCalTemp.Size = new System.Drawing.Size(75, 23);
            this.bCalTemp.TabIndex = 157;
            this.bCalTemp.Text = "Cal Temp";
            this.bCalTemp.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 85);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 23);
            this.button1.TabIndex = 156;
            this.button1.Text = "Change Over";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(82, 11);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(86, 13);
            this.label22.TabIndex = 155;
            this.label22.Text = "max wait time ms";
            // 
            // nudRebootWaitMS
            // 
            this.nudRebootWaitMS.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudRebootWaitMS.Location = new System.Drawing.Point(94, 27);
            this.nudRebootWaitMS.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudRebootWaitMS.Name = "nudRebootWaitMS";
            this.nudRebootWaitMS.Size = new System.Drawing.Size(56, 20);
            this.nudRebootWaitMS.TabIndex = 154;
            this.nudRebootWaitMS.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            // 
            // bReboot
            // 
            this.bReboot.Location = new System.Drawing.Point(12, 25);
            this.bReboot.Name = "bReboot";
            this.bReboot.Size = new System.Drawing.Size(75, 23);
            this.bReboot.TabIndex = 153;
            this.bReboot.Text = "Reboot";
            this.bReboot.UseVisualStyleBackColor = true;
            // 
            // bPOST
            // 
            this.bPOST.Location = new System.Drawing.Point(3, 102);
            this.bPOST.Name = "bPOST";
            this.bPOST.Size = new System.Drawing.Size(75, 23);
            this.bPOST.TabIndex = 152;
            this.bPOST.Text = "POST";
            this.bPOST.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(17, 26);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(25, 13);
            this.label25.TabIndex = 151;
            this.label25.Text = "PIR";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(17, 45);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 13);
            this.label24.TabIndex = 150;
            this.label24.Text = "Ambient";
            // 
            // tbTempx
            // 
            this.tbTempx.AutoSize = true;
            this.tbTempx.Location = new System.Drawing.Point(17, 66);
            this.tbTempx.Name = "tbTempx";
            this.tbTempx.Size = new System.Drawing.Size(34, 13);
            this.tbTempx.TabIndex = 149;
            this.tbTempx.Text = "Temp";
            // 
            // tbPIR
            // 
            this.tbPIR.Location = new System.Drawing.Point(68, 20);
            this.tbPIR.Name = "tbPIR";
            this.tbPIR.Size = new System.Drawing.Size(100, 20);
            this.tbPIR.TabIndex = 148;
            // 
            // tbTemp
            // 
            this.tbTemp.Location = new System.Drawing.Point(68, 64);
            this.tbTemp.Name = "tbTemp";
            this.tbTemp.Size = new System.Drawing.Size(100, 20);
            this.tbTemp.TabIndex = 147;
            // 
            // tbAmbient
            // 
            this.tbAmbient.Location = new System.Drawing.Point(68, 42);
            this.tbAmbient.Name = "tbAmbient";
            this.tbAmbient.Size = new System.Drawing.Size(100, 20);
            this.tbAmbient.TabIndex = 146;
            // 
            // bReadSensors
            // 
            this.bReadSensors.Location = new System.Drawing.Point(29, 91);
            this.bReadSensors.Name = "bReadSensors";
            this.bReadSensors.Size = new System.Drawing.Size(95, 23);
            this.bReadSensors.TabIndex = 145;
            this.bReadSensors.Text = "Read Sensors";
            this.bReadSensors.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.nudDim2Voltage);
            this.groupBox4.Controls.Add(this.nudDim1Voltage);
            this.groupBox4.Controls.Add(this.bSetDim1);
            this.groupBox4.Location = new System.Drawing.Point(17, 451);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(118, 100);
            this.groupBox4.TabIndex = 144;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Dim outputs";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(15, 48);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(31, 13);
            this.label21.TabIndex = 85;
            this.label21.Text = "Dim2";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(16, 24);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(31, 13);
            this.label20.TabIndex = 84;
            this.label20.Text = "Dim1";
            // 
            // nudDim2Voltage
            // 
            this.nudDim2Voltage.Location = new System.Drawing.Point(52, 45);
            this.nudDim2Voltage.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudDim2Voltage.Name = "nudDim2Voltage";
            this.nudDim2Voltage.Size = new System.Drawing.Size(41, 20);
            this.nudDim2Voltage.TabIndex = 83;
            // 
            // nudDim1Voltage
            // 
            this.nudDim1Voltage.Location = new System.Drawing.Point(52, 19);
            this.nudDim1Voltage.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudDim1Voltage.Name = "nudDim1Voltage";
            this.nudDim1Voltage.Size = new System.Drawing.Size(41, 20);
            this.nudDim1Voltage.TabIndex = 82;
            // 
            // bSetDim1
            // 
            this.bSetDim1.Location = new System.Drawing.Point(18, 71);
            this.bSetDim1.Name = "bSetDim1";
            this.bSetDim1.Size = new System.Drawing.Size(75, 23);
            this.bSetDim1.TabIndex = 80;
            this.bSetDim1.Text = "Set Dim";
            this.bSetDim1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox2.Controls.Add(this.rbNone);
            this.groupBox2.Controls.Add(this.rbBlue);
            this.groupBox2.Controls.Add(this.rbGreen);
            this.groupBox2.Controls.Add(this.rbRed);
            this.groupBox2.Location = new System.Drawing.Point(149, 452);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(94, 99);
            this.groupBox2.TabIndex = 143;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "LED Control";
            // 
            // rbNone
            // 
            this.rbNone.AutoSize = true;
            this.rbNone.Location = new System.Drawing.Point(7, 80);
            this.rbNone.Name = "rbNone";
            this.rbNone.Size = new System.Drawing.Size(51, 17);
            this.rbNone.TabIndex = 3;
            this.rbNone.TabStop = true;
            this.rbNone.Text = "None";
            this.rbNone.UseVisualStyleBackColor = true;
            // 
            // rbBlue
            // 
            this.rbBlue.AutoSize = true;
            this.rbBlue.Location = new System.Drawing.Point(7, 60);
            this.rbBlue.Name = "rbBlue";
            this.rbBlue.Size = new System.Drawing.Size(46, 17);
            this.rbBlue.TabIndex = 2;
            this.rbBlue.TabStop = true;
            this.rbBlue.Text = "Blue";
            this.rbBlue.UseVisualStyleBackColor = true;
            // 
            // rbGreen
            // 
            this.rbGreen.AutoSize = true;
            this.rbGreen.Location = new System.Drawing.Point(7, 40);
            this.rbGreen.Name = "rbGreen";
            this.rbGreen.Size = new System.Drawing.Size(54, 17);
            this.rbGreen.TabIndex = 1;
            this.rbGreen.TabStop = true;
            this.rbGreen.Text = "Green";
            this.rbGreen.UseVisualStyleBackColor = true;
            // 
            // rbRed
            // 
            this.rbRed.AutoSize = true;
            this.rbRed.Location = new System.Drawing.Point(7, 20);
            this.rbRed.Name = "rbRed";
            this.rbRed.Size = new System.Drawing.Size(45, 17);
            this.rbRed.TabIndex = 0;
            this.rbRed.TabStop = true;
            this.rbRed.Text = "Red";
            this.rbRed.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bDUTtoRef);
            this.groupBox1.Controls.Add(this.nBTtimeout);
            this.groupBox1.Controls.Add(this.bReftoDUT);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Location = new System.Drawing.Point(589, 541);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 110);
            this.groupBox1.TabIndex = 142;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bluetooth Test";
            // 
            // bDUTtoRef
            // 
            this.bDUTtoRef.Location = new System.Drawing.Point(51, 14);
            this.bDUTtoRef.Name = "bDUTtoRef";
            this.bDUTtoRef.Size = new System.Drawing.Size(75, 23);
            this.bDUTtoRef.TabIndex = 39;
            this.bDUTtoRef.Text = "DUT to Ref";
            this.bDUTtoRef.UseVisualStyleBackColor = true;
            // 
            // nBTtimeout
            // 
            this.nBTtimeout.Location = new System.Drawing.Point(63, 84);
            this.nBTtimeout.Name = "nBTtimeout";
            this.nBTtimeout.Size = new System.Drawing.Size(52, 20);
            this.nBTtimeout.TabIndex = 37;
            this.nBTtimeout.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // bReftoDUT
            // 
            this.bReftoDUT.Location = new System.Drawing.Point(51, 37);
            this.bReftoDUT.Name = "bReftoDUT";
            this.bReftoDUT.Size = new System.Drawing.Size(75, 23);
            this.bReftoDUT.TabIndex = 40;
            this.bReftoDUT.Text = "Ref to DUT";
            this.bReftoDUT.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(33, 64);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 13);
            this.label10.TabIndex = 38;
            this.label10.Text = "Scan Timeout(Seconds)";
            // 
            // rtStatusWindow
            // 
            this.rtStatusWindow.Location = new System.Drawing.Point(488, 45);
            this.rtStatusWindow.Name = "rtStatusWindow";
            this.rtStatusWindow.Size = new System.Drawing.Size(635, 265);
            this.rtStatusWindow.TabIndex = 117;
            this.rtStatusWindow.Text = "";
            // 
            // bHwConfig
            // 
            this.bHwConfig.Location = new System.Drawing.Point(6, 87);
            this.bHwConfig.Name = "bHwConfig";
            this.bHwConfig.Size = new System.Drawing.Size(75, 23);
            this.bHwConfig.TabIndex = 141;
            this.bHwConfig.Text = "HW Config";
            this.bHwConfig.UseVisualStyleBackColor = true;
            // 
            // cbSetRefRadio
            // 
            this.cbSetRefRadio.AutoSize = true;
            this.cbSetRefRadio.Location = new System.Drawing.Point(982, 353);
            this.cbSetRefRadio.Name = "cbSetRefRadio";
            this.cbSetRefRadio.Size = new System.Drawing.Size(93, 17);
            this.cbSetRefRadio.TabIndex = 140;
            this.cbSetRefRadio.Text = "Set Ref Radio";
            this.cbSetRefRadio.UseVisualStyleBackColor = true;
            // 
            // bClearInputBuffers
            // 
            this.bClearInputBuffers.Location = new System.Drawing.Point(3, 19);
            this.bClearInputBuffers.Name = "bClearInputBuffers";
            this.bClearInputBuffers.Size = new System.Drawing.Size(116, 23);
            this.bClearInputBuffers.TabIndex = 139;
            this.bClearInputBuffers.Text = "Clear Input Buffers";
            this.bClearInputBuffers.UseVisualStyleBackColor = true;
            // 
            // cbDisplayData
            // 
            this.cbDisplayData.AutoSize = true;
            this.cbDisplayData.Location = new System.Drawing.Point(892, 353);
            this.cbDisplayData.Name = "cbDisplayData";
            this.cbDisplayData.Size = new System.Drawing.Size(84, 17);
            this.cbDisplayData.TabIndex = 138;
            this.cbDisplayData.Text = "Display data";
            this.cbDisplayData.UseVisualStyleBackColor = true;
            // 
            // bReadTestRadioParams
            // 
            this.bReadTestRadioParams.Location = new System.Drawing.Point(732, 400);
            this.bReadTestRadioParams.Name = "bReadTestRadioParams";
            this.bReadTestRadioParams.Size = new System.Drawing.Size(75, 23);
            this.bReadTestRadioParams.TabIndex = 137;
            this.bReadTestRadioParams.Text = "Read";
            this.bReadTestRadioParams.UseVisualStyleBackColor = true;
            // 
            // bReadDUTRadioParams
            // 
            this.bReadDUTRadioParams.Location = new System.Drawing.Point(732, 377);
            this.bReadDUTRadioParams.Name = "bReadDUTRadioParams";
            this.bReadDUTRadioParams.Size = new System.Drawing.Size(75, 23);
            this.bReadDUTRadioParams.TabIndex = 136;
            this.bReadDUTRadioParams.Text = "Read";
            this.bReadDUTRadioParams.UseVisualStyleBackColor = true;
            // 
            // bSetTestRadioParams
            // 
            this.bSetTestRadioParams.Location = new System.Drawing.Point(595, 400);
            this.bSetTestRadioParams.Name = "bSetTestRadioParams";
            this.bSetTestRadioParams.Size = new System.Drawing.Size(131, 23);
            this.bSetTestRadioParams.TabIndex = 135;
            this.bSetTestRadioParams.Text = "Set Test Radio Params";
            this.bSetTestRadioParams.UseVisualStyleBackColor = true;
            // 
            // bSetRadioParams
            // 
            this.bSetRadioParams.Location = new System.Drawing.Point(595, 377);
            this.bSetRadioParams.Name = "bSetRadioParams";
            this.bSetRadioParams.Size = new System.Drawing.Size(131, 23);
            this.bSetRadioParams.TabIndex = 134;
            this.bSetRadioParams.Text = "Set DUT Radio Params";
            this.bSetRadioParams.UseVisualStyleBackColor = true;
            // 
            // bClearManData
            // 
            this.bClearManData.Location = new System.Drawing.Point(12, 56);
            this.bClearManData.Name = "bClearManData";
            this.bClearManData.Size = new System.Drawing.Size(95, 23);
            this.bClearManData.TabIndex = 133;
            this.bClearManData.Text = "Clear Man Data";
            this.bClearManData.UseVisualStyleBackColor = true;
            // 
            // bReadManData
            // 
            this.bReadManData.Location = new System.Drawing.Point(3, 47);
            this.bReadManData.Name = "bReadManData";
            this.bReadManData.Size = new System.Drawing.Size(95, 23);
            this.bReadManData.TabIndex = 132;
            this.bReadManData.Text = "Read Man Data";
            this.bReadManData.UseVisualStyleBackColor = true;
            // 
            // bMACConfig
            // 
            this.bMACConfig.Location = new System.Drawing.Point(6, 65);
            this.bMACConfig.Name = "bMACConfig";
            this.bMACConfig.Size = new System.Drawing.Size(75, 23);
            this.bMACConfig.TabIndex = 131;
            this.bMACConfig.Text = "MAC Config";
            this.bMACConfig.UseVisualStyleBackColor = true;
            // 
            // bGetVersions
            // 
            this.bGetVersions.Location = new System.Drawing.Point(3, 72);
            this.bGetVersions.Name = "bGetVersions";
            this.bGetVersions.Size = new System.Drawing.Size(90, 23);
            this.bGetVersions.TabIndex = 130;
            this.bGetVersions.Text = "Get Versions";
            this.bGetVersions.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(883, 340);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 129;
            this.label3.Text = "DUT Command";
            // 
            // bHLAConfig
            // 
            this.bHLAConfig.Location = new System.Drawing.Point(6, 42);
            this.bHLAConfig.Name = "bHLAConfig";
            this.bHLAConfig.Size = new System.Drawing.Size(90, 23);
            this.bHLAConfig.TabIndex = 125;
            this.bHLAConfig.Text = "HLA Config";
            this.bHLAConfig.UseVisualStyleBackColor = true;
            // 
            // bPCBAConfig
            // 
            this.bPCBAConfig.Location = new System.Drawing.Point(6, 19);
            this.bPCBAConfig.Name = "bPCBAConfig";
            this.bPCBAConfig.Size = new System.Drawing.Size(90, 23);
            this.bPCBAConfig.TabIndex = 124;
            this.bPCBAConfig.Text = "PCBA Config";
            this.bPCBAConfig.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbOscStatus);
            this.groupBox3.Controls.Add(this.lbPIRMin);
            this.groupBox3.Controls.Add(this.lbPIRMax);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.bPIROscTest);
            this.groupBox3.Location = new System.Drawing.Point(289, 451);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(272, 68);
            this.groupBox3.TabIndex = 123;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "PIR Test";
            // 
            // lbOscStatus
            // 
            this.lbOscStatus.AutoSize = true;
            this.lbOscStatus.Location = new System.Drawing.Point(223, 19);
            this.lbOscStatus.Name = "lbOscStatus";
            this.lbOscStatus.Size = new System.Drawing.Size(16, 13);
            this.lbOscStatus.TabIndex = 5;
            this.lbOscStatus.Text = "---";
            // 
            // lbPIRMin
            // 
            this.lbPIRMin.AutoSize = true;
            this.lbPIRMin.Location = new System.Drawing.Point(171, 40);
            this.lbPIRMin.Name = "lbPIRMin";
            this.lbPIRMin.Size = new System.Drawing.Size(16, 13);
            this.lbPIRMin.TabIndex = 4;
            this.lbPIRMin.Text = "---";
            // 
            // lbPIRMax
            // 
            this.lbPIRMax.AutoSize = true;
            this.lbPIRMax.Location = new System.Drawing.Point(170, 20);
            this.lbPIRMax.Name = "lbPIRMax";
            this.lbPIRMax.Size = new System.Drawing.Size(16, 13);
            this.lbPIRMax.TabIndex = 3;
            this.lbPIRMax.Text = "---";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(140, 40);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(24, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Min";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(137, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(27, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Max";
            // 
            // bPIROscTest
            // 
            this.bPIROscTest.Location = new System.Drawing.Point(23, 31);
            this.bPIROscTest.Name = "bPIROscTest";
            this.bPIROscTest.Size = new System.Drawing.Size(90, 23);
            this.bPIROscTest.TabIndex = 0;
            this.bPIROscTest.Text = "Oscillation Test";
            this.bPIROscTest.UseVisualStyleBackColor = true;
            // 
            // RadioTest
            // 
            this.RadioTest.Controls.Add(this.label12);
            this.RadioTest.Controls.Add(this.nudPowerLevel);
            this.RadioTest.Controls.Add(this.lbTestChannel);
            this.RadioTest.Controls.Add(this.label15);
            this.RadioTest.Controls.Add(this.label14);
            this.RadioTest.Controls.Add(this.label13);
            this.RadioTest.Controls.Add(this.nudLQIT);
            this.RadioTest.Controls.Add(this.nudLQIM);
            this.RadioTest.Controls.Add(this.bTestRaio);
            this.RadioTest.Location = new System.Drawing.Point(589, 429);
            this.RadioTest.Name = "RadioTest";
            this.RadioTest.Size = new System.Drawing.Size(269, 98);
            this.RadioTest.TabIndex = 122;
            this.RadioTest.TabStop = false;
            this.RadioTest.Text = "Radio Test";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(171, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 13);
            this.label12.TabIndex = 54;
            this.label12.Text = "Power Level";
            // 
            // nudPowerLevel
            // 
            this.nudPowerLevel.Location = new System.Drawing.Point(171, 41);
            this.nudPowerLevel.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudPowerLevel.Name = "nudPowerLevel";
            this.nudPowerLevel.Size = new System.Drawing.Size(65, 20);
            this.nudPowerLevel.TabIndex = 53;
            // 
            // lbTestChannel
            // 
            this.lbTestChannel.AutoSize = true;
            this.lbTestChannel.Location = new System.Drawing.Point(78, 12);
            this.lbTestChannel.Name = "lbTestChannel";
            this.lbTestChannel.Size = new System.Drawing.Size(16, 13);
            this.lbTestChannel.TabIndex = 52;
            this.lbTestChannel.Text = "---";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(27, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(45, 13);
            this.label15.TabIndex = 51;
            this.label15.Text = "channel";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(41, 62);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 49;
            this.label14.Text = "LQIT";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(39, 36);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 13);
            this.label13.TabIndex = 48;
            this.label13.Text = "LQIM";
            // 
            // nudLQIT
            // 
            this.nudLQIT.Location = new System.Drawing.Point(78, 59);
            this.nudLQIT.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLQIT.Name = "nudLQIT";
            this.nudLQIT.Size = new System.Drawing.Size(59, 20);
            this.nudLQIT.TabIndex = 47;
            this.nudLQIT.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // nudLQIM
            // 
            this.nudLQIM.Location = new System.Drawing.Point(78, 34);
            this.nudLQIM.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLQIM.Name = "nudLQIM";
            this.nudLQIM.Size = new System.Drawing.Size(59, 20);
            this.nudLQIM.TabIndex = 46;
            this.nudLQIM.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // bTestRaio
            // 
            this.bTestRaio.Location = new System.Drawing.Point(171, 69);
            this.bTestRaio.Name = "bTestRaio";
            this.bTestRaio.Size = new System.Drawing.Size(75, 23);
            this.bTestRaio.TabIndex = 45;
            this.bTestRaio.Text = "Test Radio";
            this.bTestRaio.UseVisualStyleBackColor = true;
            // 
            // LEDColor
            // 
            this.LEDColor.Location = new System.Drawing.Point(702, 650);
            this.LEDColor.Name = "LEDColor";
            this.LEDColor.ReadOnly = true;
            this.LEDColor.Size = new System.Drawing.Size(22, 20);
            this.LEDColor.TabIndex = 116;
            this.LEDColor.TabStop = false;
            // 
            // textLEDIntest
            // 
            this.textLEDIntest.AutoSize = true;
            this.textLEDIntest.Location = new System.Drawing.Point(57, 38);
            this.textLEDIntest.Name = "textLEDIntest";
            this.textLEDIntest.Size = new System.Drawing.Size(16, 13);
            this.textLEDIntest.TabIndex = 115;
            this.textLEDIntest.Text = "---";
            // 
            // textLEDFreq
            // 
            this.textLEDFreq.AutoSize = true;
            this.textLEDFreq.Location = new System.Drawing.Point(57, 25);
            this.textLEDFreq.Name = "textLEDFreq";
            this.textLEDFreq.Size = new System.Drawing.Size(16, 13);
            this.textLEDFreq.TabIndex = 114;
            this.textLEDFreq.Text = "---";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 113;
            this.label9.Text = "Intest";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 13);
            this.label7.TabIndex = 112;
            this.label7.Text = "Freq";
            // 
            // checkBoxTestIndicator
            // 
            this.checkBoxTestIndicator.AutoSize = true;
            this.checkBoxTestIndicator.Location = new System.Drawing.Point(466, 402);
            this.checkBoxTestIndicator.Name = "checkBoxTestIndicator";
            this.checkBoxTestIndicator.Size = new System.Drawing.Size(82, 17);
            this.checkBoxTestIndicator.TabIndex = 110;
            this.checkBoxTestIndicator.Text = "DUT Power";
            this.checkBoxTestIndicator.UseVisualStyleBackColor = true;
            // 
            // buttonExit
            // 
            this.buttonExit.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExit.Location = new System.Drawing.Point(17, 616);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(75, 23);
            this.buttonExit.TabIndex = 109;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(485, 382);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 108;
            this.label8.Text = "LED Voltage";
            // 
            // numericUpDownLEDVoltage
            // 
            this.numericUpDownLEDVoltage.DecimalPlaces = 1;
            this.numericUpDownLEDVoltage.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownLEDVoltage.Location = new System.Drawing.Point(437, 376);
            this.numericUpDownLEDVoltage.Maximum = new decimal(new int[] {
            48,
            0,
            0,
            65536});
            this.numericUpDownLEDVoltage.Name = "numericUpDownLEDVoltage";
            this.numericUpDownLEDVoltage.Size = new System.Drawing.Size(41, 20);
            this.numericUpDownLEDVoltage.TabIndex = 107;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 335);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(108, 23);
            this.button2.TabIndex = 203;
            this.button2.Text = "Read All AC/DC inputs";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.readACDCValues_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(12, 365);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 204;
            this.button3.Text = "Read Single";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.readDCValue_Click);
            // 
            // cbMeasurementToRead
            // 
            this.cbMeasurementToRead.FormattingEnabled = true;
            this.cbMeasurementToRead.Location = new System.Drawing.Point(93, 382);
            this.cbMeasurementToRead.Name = "cbMeasurementToRead";
            this.cbMeasurementToRead.Size = new System.Drawing.Size(150, 21);
            this.cbMeasurementToRead.TabIndex = 217;
            // 
            // dgChannelData
            // 
            this.dgChannelData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgChannelData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cName,
            this.cFunction,
            this.cChannel,
            this.cValue});
            this.dgChannelData.Location = new System.Drawing.Point(12, 12);
            this.dgChannelData.Name = "dgChannelData";
            this.dgChannelData.Size = new System.Drawing.Size(449, 298);
            this.dgChannelData.TabIndex = 218;
            // 
            // cName
            // 
            this.cName.HeaderText = "Name";
            this.cName.Name = "cName";
            this.cName.Width = 200;
            // 
            // cFunction
            // 
            this.cFunction.HeaderText = "Function";
            this.cFunction.Name = "cFunction";
            this.cFunction.Width = 50;
            // 
            // cChannel
            // 
            this.cChannel.HeaderText = "Channel";
            this.cChannel.Name = "cChannel";
            this.cChannel.Width = 50;
            // 
            // cValue
            // 
            this.cValue.HeaderText = "Value";
            this.cValue.Name = "cValue";
            // 
            // bWriteValue
            // 
            this.bWriteValue.Location = new System.Drawing.Point(12, 401);
            this.bWriteValue.Name = "bWriteValue";
            this.bWriteValue.Size = new System.Drawing.Size(75, 23);
            this.bWriteValue.TabIndex = 219;
            this.bWriteValue.Text = "Write value";
            this.bWriteValue.UseVisualStyleBackColor = true;
            this.bWriteValue.Click += new System.EventHandler(this.bWriteValue_Click);
            // 
            // nudSingleValue
            // 
            this.nudSingleValue.DecimalPlaces = 3;
            this.nudSingleValue.Location = new System.Drawing.Point(282, 382);
            this.nudSingleValue.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudSingleValue.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.nudSingleValue.Name = "nudSingleValue";
            this.nudSingleValue.Size = new System.Drawing.Size(120, 20);
            this.nudSingleValue.TabIndex = 220;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tbPIR);
            this.groupBox5.Controls.Add(this.bReadSensors);
            this.groupBox5.Controls.Add(this.tbAmbient);
            this.groupBox5.Controls.Add(this.tbTemp);
            this.groupBox5.Controls.Add(this.tbTempx);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Location = new System.Drawing.Point(289, 525);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(189, 132);
            this.groupBox5.TabIndex = 221;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "PCBA Sensors";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.bLEDDetectorRead);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.textLEDFreq);
            this.groupBox6.Controls.Add(this.textLEDIntest);
            this.groupBox6.Location = new System.Drawing.Point(149, 557);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(94, 100);
            this.groupBox6.TabIndex = 222;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "LED Detector";
            // 
            // bLEDDetectorRead
            // 
            this.bLEDDetectorRead.Location = new System.Drawing.Point(6, 68);
            this.bLEDDetectorRead.Name = "bLEDDetectorRead";
            this.bLEDDetectorRead.Size = new System.Drawing.Size(75, 23);
            this.bLEDDetectorRead.TabIndex = 116;
            this.bLEDDetectorRead.Text = "Read";
            this.bLEDDetectorRead.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.bMACConfig);
            this.groupBox7.Controls.Add(this.bPCBAConfig);
            this.groupBox7.Controls.Add(this.bHLAConfig);
            this.groupBox7.Controls.Add(this.bHwConfig);
            this.groupBox7.Controls.Add(this.bCalTemp);
            this.groupBox7.Location = new System.Drawing.Point(888, 376);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(123, 153);
            this.groupBox7.TabIndex = 223;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Configure";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.bReadManData);
            this.groupBox8.Controls.Add(this.bClearInputBuffers);
            this.groupBox8.Controls.Add(this.bGetVersions);
            this.groupBox8.Controls.Add(this.bPOST);
            this.groupBox8.Location = new System.Drawing.Point(1030, 380);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(125, 141);
            this.groupBox8.TabIndex = 224;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Display";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.bProgramCode);
            this.groupBox9.Controls.Add(this.bReboot);
            this.groupBox9.Controls.Add(this.nudRebootWaitMS);
            this.groupBox9.Controls.Add(this.label22);
            this.groupBox9.Controls.Add(this.bClearManData);
            this.groupBox9.Controls.Add(this.button1);
            this.groupBox9.Location = new System.Drawing.Point(897, 535);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(293, 122);
            this.groupBox9.TabIndex = 225;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Actions";
            // 
            // bProgramCode
            // 
            this.bProgramCode.Location = new System.Drawing.Point(151, 81);
            this.bProgramCode.Name = "bProgramCode";
            this.bProgramCode.Size = new System.Drawing.Size(107, 23);
            this.bProgramCode.TabIndex = 157;
            this.bProgramCode.Text = "Program Code";
            this.bProgramCode.UseVisualStyleBackColor = true;
            this.bProgramCode.Click += new System.EventHandler(this.bProgramCode_Click);
            // 
            // GeneralFixtureControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1309, 663);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.nudSingleValue);
            this.Controls.Add(this.bWriteValue);
            this.Controls.Add(this.dgChannelData);
            this.Controls.Add(this.cbMeasurementToRead);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.rtStatusWindow);
            this.Controls.Add(this.cbSetRefRadio);
            this.Controls.Add(this.cbDisplayData);
            this.Controls.Add(this.bReadTestRadioParams);
            this.Controls.Add(this.bReadDUTRadioParams);
            this.Controls.Add(this.bSetTestRadioParams);
            this.Controls.Add(this.bSetRadioParams);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.RadioTest);
            this.Controls.Add(this.LEDColor);
            this.Controls.Add(this.checkBoxTestIndicator);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.numericUpDownLEDVoltage);
            this.Name = "GeneralFixtureControl";
            ((System.ComponentModel.ISupportInitialize)(this.nudRebootWaitMS)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDim2Voltage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDim1Voltage)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nBTtimeout)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.RadioTest.ResumeLayout(false);
            this.RadioTest.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPowerLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQIM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLEDVoltage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgChannelData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSingleValue)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bCalTemp;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.NumericUpDown nudRebootWaitMS;
        private System.Windows.Forms.Button bReboot;
        private System.Windows.Forms.Button bPOST;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label tbTempx;
        private System.Windows.Forms.TextBox tbPIR;
        private System.Windows.Forms.TextBox tbTemp;
        private System.Windows.Forms.TextBox tbAmbient;
        private System.Windows.Forms.Button bReadSensors;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown nudDim2Voltage;
        private System.Windows.Forms.NumericUpDown nudDim1Voltage;
        private System.Windows.Forms.Button bSetDim1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbNone;
        private System.Windows.Forms.RadioButton rbBlue;
        private System.Windows.Forms.RadioButton rbGreen;
        private System.Windows.Forms.RadioButton rbRed;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bDUTtoRef;
        private System.Windows.Forms.NumericUpDown nBTtimeout;
        private System.Windows.Forms.Button bReftoDUT;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox rtStatusWindow;
        private System.Windows.Forms.Button bHwConfig;
        private System.Windows.Forms.CheckBox cbSetRefRadio;
        private System.Windows.Forms.Button bClearInputBuffers;
        private System.Windows.Forms.CheckBox cbDisplayData;
        private System.Windows.Forms.Button bReadTestRadioParams;
        private System.Windows.Forms.Button bReadDUTRadioParams;
        private System.Windows.Forms.Button bSetTestRadioParams;
        private System.Windows.Forms.Button bSetRadioParams;
        private System.Windows.Forms.Button bClearManData;
        private System.Windows.Forms.Button bReadManData;
        private System.Windows.Forms.Button bMACConfig;
        private System.Windows.Forms.Button bGetVersions;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bHLAConfig;
        private System.Windows.Forms.Button bPCBAConfig;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbOscStatus;
        private System.Windows.Forms.Label lbPIRMin;
        private System.Windows.Forms.Label lbPIRMax;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button bPIROscTest;
        private System.Windows.Forms.GroupBox RadioTest;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nudPowerLevel;
        private System.Windows.Forms.Label lbTestChannel;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown nudLQIT;
        private System.Windows.Forms.NumericUpDown nudLQIM;
        private System.Windows.Forms.Button bTestRaio;
        private System.Windows.Forms.TextBox LEDColor;
        private System.Windows.Forms.Label textLEDIntest;
        private System.Windows.Forms.Label textLEDFreq;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBoxTestIndicator;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDownLEDVoltage;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox cbMeasurementToRead;
        private System.Windows.Forms.BindingSource channelBindingSource;
        private System.Windows.Forms.DataGridView dgChannelData;
        private System.Windows.Forms.DataGridViewTextBoxColumn cName;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFunction;
        private System.Windows.Forms.DataGridViewTextBoxColumn cChannel;
        private System.Windows.Forms.DataGridViewTextBoxColumn cValue;
        private System.Windows.Forms.Button bWriteValue;
        private System.Windows.Forms.NumericUpDown nudSingleValue;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button bLEDDetectorRead;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button bProgramCode;
    }
}