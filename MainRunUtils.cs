﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

using Seagull.BarTender.Print;

using FixtureDatabase;
using TestStationErrorCodes;

namespace SensorManufSY2
{
    public partial class MainWindow
    {

        /// <summary>
        /// Will ask the operator for the PCBA SN. Returns all manufacturing data.
        /// If it existed in the database, it is use. Otherwise new data is made.
        /// </summary>
        /// <remarks>
        /// If in debug mode, and it dose not exist in the database, it will not get a real HLA serial number. It will be a generated debug number
        /// which will not be recorded in the database. It will also return debug mac addresses. It will not
        /// get them from the MAC pools.
        /// </remarks>
        /// <param name="uutData"></param>
        /// <param name="testparams"></param>
        /// <param name="outmsg"></param>
        /// <returns>
        /// 6    ErrorCodes.Program.PCBADataMismatch
        /// -7   ErrorCodes.System.OperatorCancel, operator canceled when asked for sn
        /// -17  ErrorCodes.System.DatabaseSetupError, product code was not found database product table
        /// -3   ErrorCodes.System.MacDBFailure, Error reading or generating a MAC
        /// -4   ErrorCodes.System.SNDBFailure, Error reading or generating serial numbers
        /// </returns>
        public int GetPCBASN(FixtureFunctions fixtureFunctions, 
                               ProductDatabase.Product_data productCodeData, ref uutdata uutData, ref string outmsg)
        {
            int status = 0;

            // special debug function. If the first character is # then give the operator the 
            //  option of using this or enter a new one.
            if (uutData.pcbaSN.Length > 1)
            {
                if (uutData.pcbaSN.Substring(0, 1) == "#")
                {
                    if (MessageBox.Show("Do you want to use the last sn(" + uutData.pcbaSN.Substring(1) + ")?", "SN reuse", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        uutData.pcbaSN = uutData.pcbaSN.Substring(1);
                    else
                        uutData.pcbaSN = string.Empty;
                }
                else
                    uutData.pcbaSN = string.Empty;
            }

            // get the PCBA SN 
            if ((uutData.pcbaSN == string.Empty) | (uutData.pcbaSN == null))
            {
                GetPCBASerialNumber iForm = new GetPCBASerialNumber(fixtureFunctions);
                if (iForm.ShowDialog() == DialogResult.OK)
                {
                    uutData.pcbaSN = iForm.PcaSN.ToUpper();
                    iForm.Dispose();
                }
                else   // operator canceled
                {
                    uutData.pcbaSN = string.Empty;
                    iForm.Dispose();
                    return (int)ErrorCodes.System.OperatorCancel;
                }
            }
            
            using (MACDatabase macDB = new MACDatabase(stationConfig.SQLServer, stationConfig.SQLMacDatabaseName, stationConfig.SQLOperatorName, stationConfig.SQLOperatorPassword))
            using (SerialNumberDatabase snDB = new SerialNumberDatabase(stationConfig.SQLServer, stationConfig.SQLSerialNumberDatabaseName, stationConfig.SQLOperatorName,
                                                                            stationConfig.SQLOperatorPassword, ""))
            {
                string newMac = string.Empty;
                string newMac2 = string.Empty;
                List<string> MacList = new List<string>();
                int numberOfMacs = 0;
                string familyCode = string.Empty;
                string pcbaPN = string.Empty;
                string pcbaSN = string.Empty;
                string hlaPN = string.Empty;
                string hlaSN = string.Empty;


                // if the pcba product code matches what is database data
                if (uutData.pcbaSN.Substring(1, 5) == productCodeData.pcba_product_code)
                {
                    uutData.pcbaPN = productCodeData.pcba_used;     // get the pcba pn from database data
                    uutData.mac = string.Empty;

                    // if the pcba serial number does not exists in the MAC data base
                    if (!macDB.DoesPcaSnExist(ref MacList, ref numberOfMacs, productCodeData.family_code, uutData.pcbaSN))
                    {
                        if (macDB.LastErrorCode != 0)       // if there was a database error
                        {
                            outmsg = "MAC DB Error " + macDB.LastErrorCode + " looking for serial number. " + macDB.LastErrorMessage;
                            return (int)ErrorCodes.SystemGroups.MACDatabaseErrors - macDB.LastErrorCode;
                        }
                        if (stationConfig.ConfigDebug)      // if in debug mode, make fake macs
                        {
                            MacList.Clear();
                            MacList.Add("6854F5000000");
                            if (numberOfMacs == 2)
                                MacList.Add("6854F5000001");
                        }
                        else        // get a record real macs
                        {
                            macDB.GetAndRecordNextMac(productCodeData.family_code, uutData.pcbaSN, stationConfig.ConfigStationName, ref MacList);
                            if (macDB.LastErrorCode != 0)       // if there was a error
                            {
                                outmsg = string.Format("Error recording MACs. Error code = {0}. Error message = {1}", macDB.LastErrorCode, macDB.LastErrorMessage);
                                return (int)ErrorCodes.SystemGroups.MACDatabaseErrors - macDB.LastErrorCode;
                            }
                        }
                    }
                    uutData.mac = MacList.ElementAt(0);         // save data in stucture
                    if (MacList.Count == 2)                     // if there are 2 macs for this unit
                        uutData.mac2 = MacList.ElementAt(1);
                    else
                        uutData.mac2 = string.Empty;
                }
                else    // pcba PN does not match what the HLA needs
                {
                    outmsg = string.Format("Error: The pcba PN {0} is wrong for {1}. Expects {2}", pcbaPN, productCodeData.product_code, productCodeData.pcba_used);
                    MessageBox.Show(outmsg);
                    return (int)ErrorCodes.Program.PCBADataMismatch;
                }
                // at this point we have a valid pcba SN and MAC
                // see if this MAC has a HLA SN assigned to it yet

                    // look for the mac. only the first mac is looked for
                if (!snDB.LookForMac(productCodeData.product_code.Substring(0, 3), uutData.mac, ref uutData.topSN, ref uutData.topPN, ref pcbaSN))
                {
                    if (snDB.LastErrorCode != 0)        // if some error
                    {
                        outmsg = string.Format("Error looking for macs. Error code = {0}. Error message = {1}", snDB.LastErrorCode, snDB.LastErrorMessage);
                        return (int)ErrorCodes.SystemGroups.SNDatabaseErrors - snDB.LastErrorCode;
                    }
                    // if in debug mode, use the Factory code of X and a fake SN. This indicates that this not a shipible unit.
                    if (stationConfig.ConfigDebug)
                    {
                        uutData.topSN = "X" + productCodeData.product_code.Substring(0, 5) + "999900001";
                    }
                    else
                    {
                        uutData.topSN = snDB.GetAndRecordNextSN(stationConfig.ConfigFactoryCode, productCodeData.product_code.Substring(0, 3), productCodeData.product_code.Substring(3, 2),
                                                productCodeData.pn, uutData.pcbaSN, MacList);
                        if (snDB.LastErrorCode  != 0)
                        {
                            outmsg = string.Format("Error getting and recording next SN. Error code = {0}. Error message = {1}", snDB.LastErrorCode, snDB.LastErrorMessage);
                            return (int)ErrorCodes.SystemGroups.SNDatabaseErrors - snDB.LastErrorCode;
                        }
                    }
                    uutData.topPN = productCodeData.pn;
                }
           }   // end of using block
            uutData.model = productCodeData.model;

            if (uutData.mac2 == string.Empty)
                addTextToStatusWindow(string.Format("Using\n PCBA SN: {0} PN: {1} MAC: {2}\nHLA SN: {3} PN: {4}\nModel: {5} \n", uutData.pcbaSN, uutData.pcbaPN, uutData.mac,
                                                    uutData.topSN, uutData.topPN, uutData.model));
            else
                addTextToStatusWindow(string.Format("Using\n PCBA SN: {0} PN: {1} MAC: {2}:{6}\nHLA SN: {3} PN: {4}\nModel: {5} \n", uutData.pcbaSN, uutData.pcbaPN, uutData.mac,
                                                    uutData.topSN, uutData.topPN, uutData.model, uutData.mac2));

            if (stationConfig.ConfigEnablePFS & !stationConfig.ConfigDebug) // do not do if in debug mode
            {
                uutData.pfsAssemblyNumber = productCodeData.pfs_assembly_number;
                pfsInterface.PSFQuery(uutData.pfsAssemblyNumber, uutData.pcbaSN, stationConfig.ConfigStationOperation, ref outmsg);
                if (!outmsg.Contains("OK"))     // if sn was rejected
                {
                    addTextToStatusWindow("This SN was refused by PFS.\n" + outmsg + "\n", StatusType.failed);
                    MessageBox.Show("This SN was refused by PFS.\n" + outmsg);
                    status = (int)ErrorCodes.Program.RefusedByPFS;
                }
            }


            return status;

        }


        /// <summary>
        /// Will ask the operator for the HLA SN. Returns all manufacturing data.
        /// </summary>
        /// <remarks>
        /// Gets from stations config:
        ///     product code
        ///     model
        ///     hla pn
        ///     pcba pn
        ///  Gets from serialnumbersused table based on scaned in HLA SN
        ///     MAC
        ///     pcba sn
        /// </remarks>
        /// <param name="uutData"></param>
        /// <param name="testparams"></param>
        /// <param name="outmsg"></param>
        /// <returns>
        /// 14  ErrorCodes.Program.RefusedByPFS
        /// -3  ErrorCodes.System.MacDBFailure
        /// -4  ErrorCodes.System.SNDBFailure
        /// -7  ErrorCodes.System.OperatorCancel
        /// </returns>
        public int GetHLASN(FixtureFunctions fixtureFunctions, ProductDatabase.Product_data productCodeData, ref uutdata uutData, ref string outmsg)
        {
            int status = 0;
            // get the HLA SN 
            GetHLASerialNumber iForm = new GetHLASerialNumber(fixtureFunctions);
            if (iForm.ShowDialog() == DialogResult.OK)
            {
                uutData.topSN = iForm.HlaSN.ToUpper();
                iForm.Dispose();
            }
            else   // operator canceled
            {
                uutData.topSN = string.Empty;
                iForm.Dispose();
                return (int)ErrorCodes.System.OperatorCancel;
            }

            // get the stuff from the station configuration
            uutData.model = productCodeData.model;
            uutData.topPN = productCodeData.pn;
            uutData.pcbaPN = productCodeData.pcba_used;

            using (SerialNumberDatabase snDB = new SerialNumberDatabase(stationConfig.SQLServer, stationConfig.SQLSerialNumberDatabaseName, stationConfig.SQLOperatorName,
                                                                            stationConfig.SQLOperatorPassword, ""))
            {
                string newMac = string.Empty;
                string familyCode = string.Empty;
                string pcbaPN = string.Empty;
                string pcbaSN = string.Empty;
                string hlaPN = string.Empty;
                string hlaSN = string.Empty;



                if (!snDB.LookForHLASN(productCodeData.product_code.Substring(0, 3), uutData.topSN, ref uutData.pcbaSN, ref uutData.mac))
                {
                    // if it did not find the serial number, big issues
                    outmsg = "HLA Serial Number " + uutData.topSN + " was not found in the serialnumbersused table.";
                    return (int)ErrorCodes.SystemGroups.SNDatabaseErrors - snDB.LastErrorCode;
                }
            }   // end of using block

            addTextToStatusWindow(string.Format("Using\n PCBA SN: {0} PN: {1} MAC: {2}\nHLA SN: {3} PN: {4}\nModel: {5} \n", uutData.pcbaSN, uutData.pcbaPN, uutData.mac,
                                                uutData.topSN, uutData.topPN, uutData.model));
            if (stationConfig.ConfigEnablePFS)
            {
                uutData.pfsAssemblyNumber = productCodeData.pfs_assembly_number;
                pfsInterface.PSFQuery(uutData.pfsAssemblyNumber, uutData.pcbaSN, stationConfig.ConfigStationOperation, ref outmsg);
                if (!outmsg.Contains("OK"))     // if sn was rejected
                {
                    addTextToStatusWindow("This SN was refused by PFS.\n" + outmsg + "\n", StatusType.failed);
                    MessageBox.Show("This SN was refused by PFS.\n" + outmsg);
                    status = (int)ErrorCodes.Program.RefusedByPFS;
                }
            }


            return status;

        }

        /// <summary>
        /// Will ask the operator for the MAC. Returns all manufacturing data.
        /// If it existed in the database, it is use. Otherwise test fails.
        /// </summary>
        /// <remarks>
        /// Will verify that it matches the configured product. Retrives all the manufacturing
        /// data from the database.
        /// </remarks>
        /// <param name="uutData"></param>
        /// <param name="testparams"></param>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        public int GetMAC(FixtureFunctions fixtureFunctions, List<ProductDatabase.ProductInfo> productList,
                               ProductDatabase.Product_data productCodeData, ref uutdata uutData, ref string outmsg)
        {
            int status = 0;
            // get the MAC 
            GetMAC iForm = new GetMAC();
            if (iForm.ShowDialog() == DialogResult.OK)
            {
                uutData.mac = iForm.MAC1.ToUpper();
                iForm.Dispose();
            }
            else   // operator canceled
            {
                uutData.pcbaSN = string.Empty;
                iForm.Dispose();
                return (int)ErrorCodes.System.OperatorCancel;
            }

            using (SerialNumberDatabase snDB = new SerialNumberDatabase(stationConfig.SQLServer, stationConfig.SQLSerialNumberDatabaseName, stationConfig.SQLOperatorName,
                                                                            stationConfig.SQLOperatorPassword, ""))
            {
                string newMac = string.Empty;
                string familyCode = string.Empty;
                string pcbaPN = string.Empty;
                string pcbaSN = string.Empty;
                string hlaPN = string.Empty;
                string hlaSN = string.Empty;


                // at this point we have a valid  MAC
                // get the data for this mac

                if (!snDB.LookForMac(productCodeData.product_code.Substring(0, 3), uutData.mac, ref uutData.topSN, ref uutData.topPN, ref pcbaSN))
                {
                    // no mac found
                    outmsg = "No data found for MAC " + uutData.mac;
                    addTextToStatusWindow(outmsg + "\n");
                    status =  (int)ErrorCodes.Program.ValueOutsideOfLimit;
                }
                else
                { 
                    outmsg = "Error looking for mac. " + snDB.LastErrorMessage;
                    status = (int)ErrorCodes.SystemGroups.SNDatabaseErrors - snDB.LastErrorCode;
                }

            }   // end of using block

            if (status == 0)
                addTextToStatusWindow(string.Format("Data Found\n PCBA SN: {0} PN: {1} MAC: {2}\nHLA SN: {3} PN: {4}\n", uutData.pcbaSN, uutData.pcbaPN, uutData.mac,
                                                uutData.topSN, uutData.topPN));

            return status;

        }

        /// <summary>
        /// Read the manufacturing data from the unit. It is assumed that there is not power control
        /// other than pluging and unpluging in the unit to a power sorce.
        /// </summary>
        /// <param name="fixture">FixtureFunctions</param>
        /// <param name="uutData">ref uutdata</param>
        /// <param name="resultmessage">ref string</param>
        /// <returns></returns>
        public int ReadPCBASN(FixtureFunctions fixture, ProductDatabase.Product_data productCodeData, ref uutdata uutData, ref string resultmessage)
        {
            int status = 0;
            int readversion = 0;
            string outmsg = string.Empty;

            if (fixture == null)
            {
                resultmessage = "Fixture not initialized";
                return (int)ErrorCodes.System.InstrumentNotConnected;
            }

            if (fixture.SensorConsole == null)
            {
                resultmessage = "Sensor port not initialzied";
                return (int)ErrorCodes.System.InstrumentNotConnected;
            }

            fixture.SensorConsole.ClearBuffers();           // junk from previous runs
//            var yn = MessageBox.Show("Connect Unit to cable and click OK when ready", "Get Serial number", MessageBoxButtons.OKCancel);
            var yn = MessageBox.Show(Properties.Resources.ConnectDUTClickOK, "Get Serial number", MessageBoxButtons.OKCancel);
            if (yn == DialogResult.Cancel)
            {
                resultmessage = "Operator cancel";
                return (int)ErrorCodes.System.OperatorCancel;
            }

            for (int i = 0; i < 2; i++)
            {

                // look for the DVTMAN message
                for (int j = 0; j < 5; j++)             // try mulitiple times just in case trash when turned on
                {
                    if (j == 0)
                        addTextToStatusWindow("  Looking for DVTMSG.");
                    else
                        addTextToStatusWindow(".");
                    status = 0;
                    if (!fixture.SensorConsole.WaitForMsgVersion(ref readversion, 1000))       // if not good
                    {
                        resultmessage = "Did not find DVTMAN version message";                      // save results
                        status = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                    }
                    else        // found msg version response
                    {
                        fixture.SensorConsole.GetPCBAData(ref uutData.pcbaPN, ref uutData.pcbaPN);
                        break;
                    }
                }
                addTextToStatusWindow("\n");
                if (status != 0)      // if did not find the DVTMAN version message
                {
                    MessageBox.Show(Properties.Resources.UnitUnplugReplug);
                    //addPromptToStatusWindow("Unplug and reconnect the sensor. Click OK when ready.\n");
                    addPromptToStatusWindow(Properties.Resources.UnitUnplugReplug + "\n");
                }
                else
                {
                    break;
                }
            }

            fixture.SensorConsole.ClearBuffers();
            if (!fixture.SensorConsole.GetPCBAData(ref uutData.pcbaPN, ref uutData.pcbaSN))
            {
                resultmessage = fixture.SensorConsole.LastErrorMessage;
                return fixture.SensorConsole.LastErrorCode;
            }

            if (!fixture.SensorConsole.GetHLAData(ref uutData.topPN, ref uutData.topSN, ref uutData.model))
            {
                resultmessage = fixture.SensorConsole.LastErrorMessage;
                return fixture.SensorConsole.LastErrorCode;
            }

            if (!fixture.SensorConsole.GetMACData(ref uutData.mac, ref uutData.mac2))
            {
                resultmessage = fixture.SensorConsole.LastErrorMessage;
                return fixture.SensorConsole.LastErrorCode;
            }
            if (stationConfig.ConfigEnablePFS & !stationConfig.ConfigDebug) // do not do if in debug mode
            {
                uutData.pfsAssemblyNumber = productCodeData.pfs_assembly_number;
                pfsInterface.PSFQuery(uutData.pfsAssemblyNumber, uutData.pcbaSN, stationConfig.ConfigStationOperation, ref outmsg);
                if (!outmsg.Contains("OK"))     // if sn was rejected
                {
                    //addTextToStatusWindow("This SN was refused by PFS.\n" + outmsg + "\n", StatusType.failed);
                    //MessageBox.Show("This SN was refused by PFS.\n" + outmsg);
                    addTextToStatusWindow(Properties.Resources.SNRefusedByPFS + "\n" + outmsg + "\n", StatusType.failed);
                    MessageBox.Show(Properties.Resources.SNRefusedByPFS + outmsg);
                    status = (int)ErrorCodes.Program.RefusedByPFS;
                    return status;
                }
            }

            return 0;
        }


    }
}