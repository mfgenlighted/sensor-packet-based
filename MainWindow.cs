﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.Configuration;
using System.Runtime.Remoting.Messaging;
using System.IO;
using System.Diagnostics;
using System.Globalization;
using System.Resources;

using TestStationErrorCodes;

using Seagull.BarTender.Print;

using FixtureDatabase;
using PFS_System;
using ShopFloorSystem;

namespace SensorManufSY2
{

    //--------------------
    //  events
    //--------------------
    public enum StatusType
    {
        text,
        passed,
        failed,
        serialSend,
        serialRcv,
        prompt,
        testHeader,
        testFooter,
        cuSend,
        cuRcv,
        snifferSend,
        snifferRcv,
    }




    public partial class MainWindow : Form
    {

        private bool abortButtonPushed = false;
        private List<ProductDatabase.Product_data> ProductInfoList;     // list of all products defined on this station
        private ProductDatabase.Product_data ProductInfo;               // config data for selected product
        private List<ProductDatabase.Station_data> StationInfoList;     // list of all stations in database
        private ProductDatabase.Station_data StationInfo;               // database info for the configured station

        private string operatorName = string.Empty;
        private string stationType = string.Empty;
        private string stationTitle = string.Empty;
        private string fixtureName = string.Empty;
        private string fixtureType = string.Empty;

        public delegate void ButtonExec();

        public StationConfigure stationConfig;
        public string productCodeToTest = string.Empty;

        private PFSInterface pfsInterface;
        private ShopFloorInterface sfSystem;

        private StatusWindowFileDump statusWindowFileDump = new StatusWindowFileDump();

        public delegate void SetTextCallback(string text);
        public delegate void SetTextTypeCallback(string text, StatusType type);
        public delegate void SetTextResultCallback(string text, bool pass);
        public delegate void SetHeadersCallback(int step, string name);
        public delegate void SetFooterCallback(int testResultCode, int testNumber, string errormsg);
        public delegate void SetUnitCounters(int run, int pass, int fail);

        //---------------------------------------------------------
        // this is used with other classes to send data to the status screen.
        // set up the following in the call class:
        //    public event EventHandler<StatusUpdatedEventArgs>
        //      StatusUpdated;

        //    protected virtual void
        //        OnStatusUpdated(StatusUpdatedEventArgs e)
        //    {
        //        if (StatusUpdated != null)
        //            StatusUpdated(this, e);
        //    }

        //    private void UpdateStatusWindow(string msg)
        //    {
        //        string newStatus = msg;
        //        StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
        //        nuea.statusText = msg;
        //        OnStatusUpdated(nuea);
        //    }
        //
        //  When a class is intiaited, add the following event handler
        //      sc.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);
        // where sc is the class instance.

        public void sc_StatusUpdated(object sender, StatusUpdatedEventArgs e)
        {
            switch (e.statusType)
            {
                case StatusType.testHeader:
                    this.addTestHeaderToScreen(e.stepNumber, e.statusText);
                    break;

                case StatusType.testFooter:
                    this.addTestFooterToScreen(e.resultCode, e.testNumber, e.statusText);
                    break;

                default:
                    this.addTextToStatusWindow(e.statusText, e.statusType);
                    break;
            }
        }        

        //-----------------
        // constructor
        //-------------------
        public MainWindow()
        {

            InitializeComponent();
            buttonAbortTest.Enabled = false;
            buttonStartTest.Enabled = false;
            textProgramVersion.Text = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            this.Text = "Sensor Test System  " + ConfigurationManager.AppSettings["ProductToTest"];
        }

        //---------------------
        // Status box routines
        //--------------------
        private int stepNumber;
        private string testName;

        public void addUnitsRunCounter(int run, int pass, int fail)
        {
            if (this.testStatusBox.InvokeRequired)
            {
                SetUnitCounters d = new SetUnitCounters(addUnitsRunCounter);
                this.Invoke(d, new object[] { run, pass, fail });
            }
            else
            {
                this.lUnitsRun.Text = run.ToString();
                this.lUnitsPassed.Text = pass.ToString();
                this.lUnitsFailed.Text = fail.ToString();
            }
        }



        /// <summary>
        /// Adds text to the status screen in black;
        /// </summary>
        /// <param name="msg"></param>
        public void addTextToStatusWindow(string msg)
        {
            addTextToStatusWindow(msg, StatusType.text);
            //if (this.testStatusBox.InvokeRequired)
            //{
            //    SetTextCallback d = new SetTextCallback(addTextToStatusWindow);
            //    this.Invoke(d, new object[] { msg });
            //}
            //else
            //{
            //    statusWindowFileDump.DumpData(msg);
            //    this.testStatusBox.SelectionColor = Color.Black;
            //    this.testStatusBox.AppendText(msg);
            //    this.testStatusBox.ScrollToCaret();
            //    this.testStatusBox.Update();
            //}
        }

        /// <summary>
        /// Adds text to the status screen;
        /// </summary>
        /// <param name="msg"></param>
        public void addTextToStatusWindow(string msg, StatusType type)
        {
            if (this.testStatusBox.InvokeRequired)
            {
                SetTextTypeCallback d = new SetTextTypeCallback(addTextToStatusWindow);
                this.Invoke(d, new object[] { msg, type });
            }
            else
            {
                statusWindowFileDump.DumpData(msg);
                switch (type)
                {
                    case StatusType.text:
                        this.testStatusBox.SelectionColor = Color.Black;
                        break;
                    case StatusType.passed:
                        this.testStatusBox.SelectionColor = Color.Green;
                        break;
                    case StatusType.failed:
                        this.testStatusBox.SelectionColor = Color.Red;
                        break;
                    case StatusType.serialSend:
                        this.testStatusBox.SelectionColor = Color.Blue;
                        break;
                    case StatusType.serialRcv:
                        this.testStatusBox.SelectionColor = Color.Gray;
                        break;
                    case StatusType.prompt:
                        this.testStatusBox.SelectionColor = Color.Purple;
                        break;
                    case StatusType.cuSend:
                        this.testStatusBox.SelectionColor = Color.Aqua;
                        break;
                    case StatusType.cuRcv:
                        this.testStatusBox.SelectionColor = Color.RosyBrown;
                        break;
                    case StatusType.snifferSend:
                        this.testStatusBox.SelectionColor = Color.Aqua;
                        break;
                    case StatusType.snifferRcv:
                         this.testStatusBox.SelectionColor = Color.RosyBrown;
                        break;
                    default:
                        this.testStatusBox.SelectionColor = Color.Black;
                        break;
                }
                this.testStatusBox.AppendText(msg);
                this.testStatusBox.ScrollToCaret();
                this.testStatusBox.Update();
            }
        }


        /// <summary>
        /// Adds the text to the test status screen. Will display green if pass and red
        /// if !pass.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="pass"></param>
        protected void addTextToStatusWindow(string msg, bool pass)
        {
            addTextToStatusWindow(msg, pass ? StatusType.passed : StatusType.failed);
            //if (this.testStatusBox.InvokeRequired)
            //{
            //    SetTextResultCallback d = new SetTextResultCallback(addTextToStatusWindow);
            //    this.Invoke(d, new object[] { msg, pass });
            //}
            //else
            //{
            //    statusWindowFileDump.DumpData(msg);
            //    if (pass)
            //        this.testStatusBox.SelectionColor = Color.Green;
            //    else
            //        this.testStatusBox.SelectionColor = Color.Red;
            //    this.testStatusBox.AppendText(msg);
            //    this.testStatusBox.ScrollToCaret();
            //    this.testStatusBox.Update();
            //}
        }

        /// <summary>
        /// Adds the text to the test result screen. Will display green if pass and red
        /// if !pass.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="pass"></param>
        protected void addTextToResultsWindow(string msg, bool pass)
        {
            if (this.testResults.InvokeRequired)
            {
                SetTextResultCallback d = new SetTextResultCallback(addTextToResultsWindow);
                this.Invoke(d, new object[] { msg, pass });
            }
            else
            {
                if (pass)
                    this.testResults.SelectionColor = Color.Green;
                else
                    this.testResults.SelectionColor = Color.Red;
                this.testResults.AppendText(msg);
                this.testResults.ScrollToCaret();
                this.testResults.Update();
            }
        }

        /// <summary>
        /// Adds the text to the test result screen. Will display green if pass and red
        /// if !pass.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="pass"></param>
        protected void addHeaderResultsWindow(string serialNumber)
        {
            if (this.testResults.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(addHeaderResultsWindow);
                this.Invoke(d, new object[] { serialNumber});
            }
            else
            {
                this.testResults.SelectionColor = Color.Black;
                this.testResults.AppendText(string.Format("{0} - {1}\n", serialNumber, DateTime.Now));
                this.testResults.ScrollToCaret();
                this.testResults.Update();
            }
        }

        /// <summary>
        /// Adds text to the status screen in purple.
        /// </summary>
        /// <param name="msg"></param>
        protected void addPromptToStatusWindow(string msg)
        {
            if (this.testStatusBox.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(addPromptToStatusWindow);
                this.Invoke(d, new object[] { msg });
            }
            else
            {
                statusWindowFileDump.DumpData(msg);
                this.testStatusBox.SelectionColor = Color.Purple;
                this.testStatusBox.AppendText(msg);
                this.testStatusBox.ScrollToCaret();
                this.testStatusBox.Update();
            }
        }


        /// <summary>
        /// Adds text to the status screen in gray
        /// </summary>
        /// <param name="msg"></param>
        protected void addRcvToStatusWindow(string msg)
        {
            if (this.testStatusBox.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(addRcvToStatusWindow);
                this.Invoke(d, new object[] { msg });
            }
            else
            {
                testStatusBox.SelectionColor = Color.Gray;
                testStatusBox.AppendText(msg);
                testStatusBox.ScrollToCaret();
                testStatusBox.Update();
            }
        }
        /// <summary>
        /// Adds text to the status screen in gray
        /// </summary>
        /// <param name="msg"></param>
        protected void addSendToStatusWindow(string msg)
        {
            statusWindowFileDump.DumpData(msg);
            if (this.testStatusBox.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(addSendToStatusWindow);
                this.Invoke(d, new object[] { msg });
            }
            else
            {
                testStatusBox.SelectionColor = Color.Blue;
                testStatusBox.AppendText(msg);
                testStatusBox.ScrollToCaret();
                testStatusBox.Update();
            }
        }


        protected void addLimitFileVersionScreen(string msg)
        {
            if (this.testStatusBox.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(addLimitFileVersionScreen);
                this.Invoke(d, new object[] { msg });
            }
            else
            {
                this.textLimitFileVersion.Text = msg;
            }
        }

        protected void addLimitFileNameScreen(string msg)
        {
            if (this.testStatusBox.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(addLimitFileNameScreen);
                this.Invoke(d, new object[] { msg });
            }
            else
            {
                this.textLimitFileName.Text = msg;
            }
        }

        protected void addStationFunctionScreen(string tag)
        {
            if (this.testStatusBox.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(addStationFunctionScreen);
                this.Invoke(d, new object[] { tag });
            }
            else
            {
                this.lbStationFunction.Text = tag;
            }
        }

        protected void addResultDBNameScreen(string msg)
        {
            if (this.testStatusBox.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(addResultDBNameScreen);
                this.Invoke(d, new object[] { msg });
            }
            else
            {
                this.textResultDatabase.Text = msg;
            }
        }



        /// <summary>
        /// Adds the test header on the status screen. stepNumber  will be saved.
        /// </summary>
        /// <param name="testNumber"></param>
        /// <param name="testName"></param>
        protected void addTestHeaderToScreen(int step, string name)
        {
            if (this.testStatusBox.InvokeRequired)
            {
                SetHeadersCallback d = new SetHeadersCallback(addTestHeaderToScreen);
                this.Invoke(d, new object[] { step, name });
            }
            else
            {
                stepNumber = step;      // save for the addTestFooterToScreen function
                testName = name;

                addTextToStatusWindow("--------------------------------------------------\n");
                addTextToStatusWindow(string.Format("-->  Step {0}: Start Test: {1}\n", stepNumber, testName));
            }
        }


        /// <summary>
        /// Adds the text to the status window and result window. Uses test data from addTestHeaderToScreen call.
        /// </summary>
        /// <param name="testNumber"></param>
        /// <param name="testResult"></param>
        /// <param name="testResultCode"></param>
        /// <param name="errormsg"></param>
        protected void addTestFooterToScreen(int testResultCode, int testNumber, string errormsg)
        {
            if (this.testStatusBox.InvokeRequired)
            {
                SetFooterCallback d = new SetFooterCallback(addTestFooterToScreen);
                this.Invoke(d, new object[] { testResultCode, testNumber, errormsg });
            }
            else
            {
                if (testResultCode != 0)            // if there is a test code
                {
                    addTextToStatusWindow("--> Test Error Code: " + testResultCode.ToString() + "\n", false);
                    addTextToStatusWindow("--> Test Error Msg: " + errormsg + "\n", false);
                }
                addTextToStatusWindow(string.Format("--> End Test #{0}: {1}\n", testNumber, (testResultCode == 0) ? "PASS" : "FAIL"), (testResultCode == 0));
                addTextToStatusWindow("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n\n", (testResultCode == 0));
                addTextToResultsWindow(string.Format("{0,3}\t{1,3}\t{3} - {2}\n", stepNumber, testNumber, testName, (testResultCode == 0) ? "PASS" : "FAIL  "),
                                        (testResultCode == 0));
            }
        }

        //----------------------------------------
        /// <summary>
        /// Initialize the test station.
        /// </summary>
        private void InitStation(object sender, EventArgs e)
        {
            bool initializeGood = true;
            string outmsg = string.Empty;
            DialogResult okCancel = System.Windows.Forms.DialogResult.None;
            string lang = string.Empty;
            List<string> sectionHeadings = new List<string>();
            List<string> sectionNames = new List<string>();
            List<string> fixtureTypes = new List<string>();
            List<string> fixtureNames = new List<string>();
            int status;


            // get operator name
            while (okCancel != System.Windows.Forms.DialogResult.OK)
            {
                StationSignIn iForm = new StationSignIn();
                okCancel = iForm.ShowDialog();
                operatorName = iForm.UserName;
                lang = iForm.Language;
                iForm.Dispose();
                if (okCancel == System.Windows.Forms.DialogResult.Cancel)
                    System.Environment.Exit(1);
                if (operatorName == string.Empty)
                {
                    MessageBox.Show(Properties.Resources.NameMustBeEntered);
                    //MessageBox.Show("Name must be entered");
                    okCancel = System.Windows.Forms.DialogResult.None;
                }
            }


            if (Properties.Settings.Default.Language != lang)
            {
                Properties.Settings.Default.Language = lang;
                Properties.Settings.Default.Save();
                Environment.Exit(0);
            }

            // verify that the config file exists
            stationConfig = new StationConfigure();
            if (stationConfig.CheckForConfigFile(ref outmsg) == -1)       // if failed
            {
                MessageBox.Show("Problem opeing the station config file. \n" + outmsg);
                initializeGood = false;
                return;
            }

            // read the station types in the config file
            if ((status = stationConfig.ReadStationFixtureTypes(ref sectionHeadings, ref sectionNames, ref fixtureTypes, ref fixtureNames)) != 0)
            {
                addTextToStatusWindow("\nProblem reading the Station Configuration file. Station Initialization failed.\n" , false);
                initializeGood = false;
                return;                 // no good config so don't try doing anything else
            }

            // if there are no stations defined, delete the config file and start all over
            if (sectionHeadings.Count == 0)
            {
                stationConfig.ResetConfigFile();
                // "You must restart the program and reconfigure the stations"
                MessageBox.Show(Properties.Resources.RestartAfterStationConfigReset);
                Environment.Exit(0);
            }

            if (sectionHeadings.Count != 1)      // if more than one station is defined in the config file
            {                       // then let the operator select which one to use
                SelectStation sForm = new SelectStation(fixtureNames);
                sForm.ShowDialog();
                // find the fixture name index
                int x = 0;
                foreach (var item in fixtureNames)
                {
                    if (item.ToString() == sForm.Station)
                        break;
                    x++;
                }
                stationType = sectionHeadings[x].ToString();
                fixtureName = fixtureNames[x];
                fixtureType = fixtureTypes[x];
                sForm.Dispose();
            }
            else
            {
                stationType = sectionHeadings[0];
                fixtureName = fixtureNames[0];
            }
            this.Text = fixtureName;

            // if the fixture has changed since last run
            if (Properties.Settings.Default.FixtureName == fixtureName)
            {
                // then get the product from the app config file
                productCodeToTest = Properties.Settings.Default.ProductToTest;
                if (productCodeToTest != string.Empty)                      // if there is something there
                    productCodeToTest = productCodeToTest.Substring(0, 5);  // make sure it is just productcode + version
            }
            else        // else the fixture has changed, so wipe out the product to force reconfig
            {
                productCodeToTest = string.Empty;
                Properties.Settings.Default.FixtureName = string.Empty;
                Properties.Settings.Default.ProductToTest = "unknown";
                Properties.Settings.Default.Save();
            }

            // now read the station configuraton for the fixture
            if (stationConfig.ReadStationConfigFile(stationType, ref outmsg) != 0)                                // read the station config file
            {
                MessageBox.Show("Problem reading the Station Configuration file.\n" + outmsg);
                addTextToStatusWindow("\nProblem reading the Station Configuration file. Station Initialization failed.\n" + outmsg, false);
                initializeGood = false;
                return;
            }
            else
            {
                addTextToStatusWindow("Station Configure file read\n", true);
                stationTitle = stationConfig.ConfigStationOperation + " - " + stationConfig.StationTitle + " : " + Properties.Settings.Default.ProductToTest;
                this.Text = stationTitle;
                this.lbStation.Text = stationConfig.ConfigStationToUse;
                stationConfig.StationOperator = operatorName;
            }

            if (stationConfig.ConfigDebug)
            {
                this.BackColor = Color.Yellow;
                this.lDebugMode.Text = Properties.Resources.StationInDebugMode;         // "Station in Debug Mode";
                this.lbDebugOptions.Text = stationConfig.ConfigDebugOptions;
            }
            else
            {
                this.lDebugMode.Text = "";
                this.lbDebugOptions.Text = string.Empty;
            }

                // setup database server stuff
                this.textDatabaseServer.Text = stationConfig.SQLServer;

            this.textResultDatabase.Text = stationConfig.SQLResultsDatabaseName;
            this.textTestChannel.Text = stationConfig.ConfigTestRadioChannel;
            this.lblComputerName.Text = stationConfig.ConfigStationName;

            // try to init the fixture
            if (stationConfig.ConfigDUTComPort.ToUpper() == "COM0")
            {
                MessageBox.Show("Looks like this fixture is not configured. Configure the fixture before testing.");
                addTextToStatusWindow("Configure fixture before testing.\n", false);
                initializeGood = false;
            }
            else
            {
                FixtureFunctions fixture = new FixtureFunctions();
                fixture.StationConfigure = stationConfig;
                fixture.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);
                if (fixture.InitFixture())
                {
                    addTextToStatusWindow("Fixture setup OK\n", true);
                    fixture.Dispose();
                }
                else
                {
                    MessageBox.Show("Error inititalizing the fixture. " + fixture.LastFunctionErrorMessage);
                    addTextToStatusWindow("Error setting up fixture. " + fixture.LastFunctionErrorMessage + "\n", false);
                    initializeGood = false;
                }
            }

            // try to open the product database to get the data for the station defined in the config file
            using (ProductDatabase prodDB = new FixtureDatabase.ProductDatabase())
            {
                try
                {
                    prodDB.Open(stationConfig.SQLServer, stationConfig.SQLProductDatabaseName, stationConfig.SQLOperatorName,
                    stationConfig.SQLOperatorPassword);

                }
                catch (Exception ex)    // problem opening database
                {
                    MessageBox.Show("Error reading product database.\n" + ex.Message);
                    addTextToStatusWindow("Error reading product database.  Station Initialization failed.\n", false);
                    initializeGood = false;
                }
                // product database is open
                if (prodDB != null)
                {
                    // get list of stations in the database
                    if (!prodDB.GetListOfStations(ref StationInfoList, out outmsg))
                    {
                        MessageBox.Show("Error getting station data.\n" + outmsg);
                        addTextToStatusWindow("Error getting station data.\n" + outmsg + "\n", false);
                        initializeGood = false;
                    }
                    else
                    {
                        // see if the configured station is in the database
                        if (!StationInfoList.Exists(x => x.name.ToUpper() == stationConfig.ConfigStationToUse.ToUpper()))
                        {
                            MessageBox.Show("Configured station data(" + stationConfig.ConfigStationToUse + ") is not in the database.\n");
                            addTextToStatusWindow("Configured station data(" + stationConfig.ConfigStationToUse + ") is not in the database.\n", false);
                            initializeGood = false;
                        }
                        else
                        {
                            // everything is good now.
                            StationInfo = StationInfoList.Find(x => x.name.ToUpper() == stationConfig.ConfigStationToUse.ToUpper());
                            // make sure this station good to run at this cm
                            if (!StationInfo.cm_code.ToUpper().Contains(stationConfig.ConfigFactoryCode.ToUpper()))
                            {
                                MessageBox.Show("Station not configured for this factory." );
                                addTextToStatusWindow("Station not configured for this factory.", false);
                                initializeGood = false;
                            }
                            else
                            {
                                if (ProductInfoList == null)
                                    ProductInfoList = new List<ProductDatabase.Product_data>();
                                ProductInfoList.Clear();
                                // get the products that are defined for this station type
                                if (!prodDB.GetStationProductData(stationConfig.ConfigStationToUse, out ProductInfoList, out outmsg))
                                {
                                    MessageBox.Show("Error getting product data.\n" + outmsg);
                                    addTextToStatusWindow("Error getting product data.\n" + outmsg + "\n", false);
                                    initializeGood = false;
                                }
                                else
                                {
                                    foreach (var item in ProductInfoList)
                                    {
                                        // only list products that are valid for this factory
                                        if (item.cm_codes.ToUpper().Contains(stationConfig.ConfigFactoryCode.ToUpper()))
                                            productSelection.Items.Add(item.product_code + " - " + item.product_desc);
                                    }
                                    addTextToStatusWindow("Production information read.\n", true);
                                    lbStationFunction.Text = StationInfo.function;
                                }
                            }
                        }
                    }
                }
            }

            // try to open the serial number database
            using (SerialNumberDatabase snDB = new SerialNumberDatabase(stationConfig.SQLServer, stationConfig.SQLSerialNumberDatabaseName, stationConfig.SQLOperatorName,
                                                                            stationConfig.SQLOperatorPassword, ""))
            {
                bool sndbCheck = true;
                if (snDB.CheckForDatabase())
                {
                    outmsg = "Serial Number Database is good.\n";
                }
                else
                { 
                    outmsg = "Error reading serial number database.\n SN Database error=" + snDB.LastErrorCode + " Message= " + snDB.LastErrorMessage + "\n" ;
                    initializeGood = false;
                    sndbCheck = false;
                }
                addTextToStatusWindow(outmsg, sndbCheck);
            }

            // try to open the mac database
            using (MACDatabase macDB = new MACDatabase(stationConfig.SQLServer, stationConfig.SQLMacDatabaseName, stationConfig.SQLOperatorName,
                                            stationConfig.SQLOperatorPassword))
            {
                bool macCheck = true;
                if (macDB.CheckForDatabase())
                {
                    outmsg = "MAC database good.\n";
                }
                else
                {
                    outmsg = "Error checking mac database.\n Database error code=" + macDB.LastErrorCode + " message= " + macDB.LastErrorMessage + "\n";
                    initializeGood = false;
                    macCheck = false;
                }
                addTextToStatusWindow(outmsg, macCheck);
            }


            // verify the configured printer is avialible.
            // A station might have both printers in its list, but might only use one of the printers.
            // If this is so, then the fixture config will just leave that field blank.
            if (stationConfig.FixtureEquipment.hasHLAPrinter | stationConfig.FixtureEquipment.hasPCBAPrinter)
            {
                Printers printers = new Printers();
                bool pcbaprintervalid = false;
                bool productprintervalid = false;
                foreach (Printer printer in printers)
                {
                    if (stationConfig.FixtureEquipment.hasPCBAPrinter & (stationConfig.ConfigPCBALabelPrinter != ""))
                    {
                        if (printer.PrinterName == stationConfig.ConfigPCBALabelPrinter)
                        {
                            pcbaprintervalid = true;
                        }
                    }
                    if (stationConfig.FixtureEquipment.hasHLAPrinter & (stationConfig.ConfigProductLabelPrinter != ""))
                    {
                        if (printer.PrinterName == stationConfig.ConfigProductLabelPrinter)
                        {
                            productprintervalid = true;
                        }
                    }
                }
                if (stationConfig.FixtureEquipment.hasHLAPrinter & !productprintervalid & (stationConfig.ConfigPCBALabelPrinter != ""))
                {
                    MessageBox.Show("Product Printer " + stationConfig.ConfigProductLabelPrinter + " was not found on this system.");
                    addTextToStatusWindow("Product printer initialization failed.  Station Initialization failed.\n", false);
                    initializeGood = false;
                }
                else
                    addTextToStatusWindow("Product Printer found.\n", true);
                if (stationConfig.FixtureEquipment.hasPCBAPrinter & !pcbaprintervalid & (stationConfig.ConfigProductLabelPrinter != ""))
                {
                    MessageBox.Show("PCBA Printer " + stationConfig.ConfigPCBALabelPrinter + " was not found on this system.");
                    addTextToStatusWindow("PCBA printer initialization failed.  Station Initialization failed.\n", false);
                    initializeGood = false;
                }
                else
                    addTextToStatusWindow("PCBA Printer found.\n", true);

            }


            // make sure Bartender can startup
            Engine btengine = null;
            try
            {
                btengine = new Engine(true);
                btengine.Dispose();
                addTextToStatusWindow("Bartender good.\n", true);
            }
            catch (Exception ex)
            {
                addTextToStatusWindow("Loading Bartender problem.\n" + ex.Message + "\n", false);
                testStatusBox.Update();
                initializeGood = false;
            }


            ////--------------------------------------------
            // initialize PFS(Benchmark)
            //---------------------------------------------
            if (stationConfig.ConfigEnablePFS)
            {
                pfsInterface = new PFSInterface(stationConfig.PFSServerIPAddress, stationConfig.PFSDatabaseName, stationConfig.ConfigStationOperation);
                if ((stationConfig.PFSDatabaseName == string.Empty) |
                    (stationConfig.PFSServerIPAddress == string.Empty) |
                    (stationConfig.PFSWorkCenter == string.Empty))
                {
                    addTextToStatusWindow("PFS System error: One or more configurations are missing.\n", false);
                    addTextToStatusWindow(outmsg);
                    initializeGood = false;
                }
                else
                {
                    if (!pfsInterface.GetUserInfo(ref outmsg))
                    {
                        addTextToStatusWindow(outmsg, false);
                        initializeGood = false;
                    }
                    pfsInterface.PSFLogin(ref outmsg);
                    if (outmsg.Contains("OK"))
                    {
                        addTextToStatusWindow("PFS System logged in\n", true);
                    }
                    else
                    {
                        addTextToStatusWindow("PFS System error:\n", false);
                        addTextToStatusWindow(outmsg);
                        initializeGood = false;
                    }
                }
            }

            ////--------------------------------------------
            // initialize shop floor system(Foxlink)
            //---------------------------------------------
            if (stationConfig.ConfigEnableSFS)
            {
                addTextToStatusWindow("Setting up Shop Floor System\n");
                sfSystem = new ShopFloorInterface(stationConfig.ConfigShopFloorDirectory, stationConfig.ConfigShopFloorFileName,
                        stationConfig.ConfigStationName, stationConfig.StationOperator);
            }


            if (!stationConfig.ConfigDebug)
            {
                toggleSerialDisplayToolStripMenuItem.Visible = false;
                debugToolStripMenuItem.Visible = false;
            }


            // all done with checks
            if (initializeGood)
            {
                buttonStartTest.Enabled = true;
                buttonStartTest.Visible = true;
                buttonStartTest.Focus();
            }
        }

        private void StartTestButton_Click(object sender, EventArgs e)
        {
            buttonStartTest.Enabled = false;
            debugToolStripMenuItem.Enabled = false;     // can not do debug because MainRun opens ports
            if (stationConfig.ConfigDebug) { buttonAbortTest.Enabled = true; buttonAbortTest.Visible = true; }
            ButtonExec b = new ButtonExec(MainRun);
//            IAsyncResult iftAR = b.BeginInvoke(new AsyncCallback(TestDone), null);
            IAsyncResult iftAR = b.BeginInvoke(result =>
                                {
                                    this.Invoke((Action)delegate
                                    {
                                        this.buttonStartTest.Enabled = true;
                                        this.debugToolStripMenuItem.Enabled = true;
                                        //this.AbortTestButton.Enabled = false;
                                    });
                                    try
                                    {
                                        b.EndInvoke(result);

                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show("Unhandled exception in test excution." + ex.Message + "\n" + ex.StackTrace);
                                    }
                                },
                            null);
        }

        //private void TestDone(IAsyncResult itfAR)
        //{
        //    this.Invoke((Action)delegate
        //    {
        //        this.StartTestButton.Enabled = true;
        //        this.AbortTestButton.Enabled = false;
        //    }
        //    );
        //}
        private void TestDone(IAsyncResult itfAR)
        {
            AsyncResult delegateResult = (AsyncResult)itfAR;
            ButtonExec delegateInstance = (ButtonExec)delegateResult.AsyncDelegate;

            try
            {
                delegateInstance.EndInvoke(itfAR);
            }
            catch (Exception)
            {
                
                throw;
            }

            this.Invoke((Action)delegate
            {
                this.buttonStartTest.Enabled = true;
                //this.AbortTestButton.Enabled = false;
            }
            );
        }


        //--------------------------------
        //  Events
        //-----------------------------------

        public static void MyExceptionHandler(object sender, ThreadExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message + "\n" + e.Exception.StackTrace, "Program Error. Please report this error to Enlighted.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            Application.Exit();
        }


        /// <summary>
        /// Select which product to test
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void productSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            productCodeToTest = productSelection.SelectedItem.ToString();
            Properties.Settings.Default.ProductToTest = productCodeToTest;
            Properties.Settings.Default.FixtureName = fixtureName;
            Properties.Settings.Default.Save();
            productToTestToolStripMenuItem.Owner.Hide();
            this.Text = stationConfig.ConfigStationOperation + " - " + stationConfig.StationTitle + " : " + productCodeToTest;
        }

        /// <summary>
        /// Set abort button status if button is pushed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AbortTestButton_Click(object sender, EventArgs e)
        {
            abortButtonPushed = true;
        }

        private void toggleDUTDisplayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            stationConfig.ConfigDisplayRcvChar = !stationConfig.ConfigDisplayRcvChar;
            stationConfig.ConfigDisplaySendChar = !stationConfig.ConfigDisplaySendChar;

        }


        private void pIRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FixtureFunctions fixture = new FixtureFunctions();;
            fixture.StationConfigure = stationConfig;
            if (!fixture.InitFixture())
            {
                MessageBox.Show("Error initializing the fixture. " + fixture.LastFunctionErrorMessage);
            }
            else
            {
                PacketBasedConsole senConsole = new PacketBasedConsole();
                senConsole.Open(stationConfig.ConfigDUTComPort, 115200);
                senConsole.DisplayRcvChar = stationConfig.ConfigDisplayRcvChar;
                senConsole.DisplaySendChar = stationConfig.ConfigDisplaySendChar;
                senConsole.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);

                SensorDVTMANTests sensorDVTMANTests = new SensorDVTMANTests();
                sensorDVTMANTests.fixture = fixture;
                //sensorDVTMANTests.sensorPacketConsole = senConsole;
                sensorDVTMANTests.stationConfig = stationConfig;
                sensorDVTMANTests.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);


                pirgraph pform = new pirgraph(stationConfig, senConsole, sensorDVTMANTests, fixture);
                pform.ShowDialog();
                pform.Dispose();
                senConsole.Close();
                fixture.Dispose();
            }
        }

        private void fixtureControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string outmsg = string.Empty;

            //using (SensorConsole senConsole = new SensorConsole())
            {
                //SensorPacketConsole senConsole = new SensorPacketConsole();
                //senConsole.DisplayRcvChar = true;
                //senConsole.DisplaySendChar = true;
                //senConsole.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);

                //testcodeClasses = new codeClasses();
                //testcodeClasses.fixtureFunctions = fixture;
                //testcodeClasses.sensorConsole = senConsole;

                if (stationConfig.ConfigFixtureType.ToUpper().Equals("JB_SY2"))
                {
                    //try
                    //{
                    //    senConsole.Open(stationConfig.ConfigDUTComPort, 115200);
                    //}
                    //catch (Exception ex)
                    //{
                    //    MessageBox.Show("Error opening sensor console port. " + ex.Message);
                    //}
                    //JBUFixtureControlWindow controlForm = new JBUFixtureControlWindow(this, senConsole);
                    //controlForm.ShowDialog();
                    //controlForm.Dispose();
                    //senConsole.Close();
                }
                else if (stationConfig.ConfigFixtureType.ToUpper().Equals("SNCK"))
                {
                    SNCKFixtureControl snckControlForm = new SNCKFixtureControl(stationConfig);
                    snckControlForm.ShowDialog();
                    snckControlForm.Dispose();
                }
                else if (stationConfig.ConfigFixtureType.ToUpper().Equals("GW-3-FCT"))
                {
                    GeneralFixtureControl fdsControlForm = new GeneralFixtureControl(this);
                    fdsControlForm.ShowDialog();
                    fdsControlForm.Dispose();
                    //senConsole.Close();
                }
                else
                {
                    FDSFixtureControl fdsControlForm = new FDSFixtureControl(this);
                    fdsControlForm.ShowDialog();
                    fdsControlForm.Dispose();
                    //senConsole.Close();
                }
            }
        }

        private void configureStationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string outmsg = string.Empty;
            ConfigStation csform = new ConfigStation(stationType);
           csform.ShowDialog();
           csform.Dispose();

            // now re-read the station configuraton for the fixture
            if (stationConfig.ReadStationConfigFile(stationType, ref outmsg) != 0)                                // read the station config file
            {
                MessageBox.Show("Problem reading the Station Configuration file.");
                addTextToStatusWindow("\nProblem reading the Station Configuration file. Station Initialization failed.\n" + outmsg, false);
            }
            else
            {
                addTextToStatusWindow("Station Configure file read\n", true);
                stationTitle = stationConfig.ConfigStationOperation + " - " + stationConfig.StationTitle + " : " + productCodeToTest;
                this.Text = stationTitle;
            }

            this.textDatabaseServer.Text = stationConfig.SQLServer;

            this.textResultDatabase.Text = stationConfig.SQLResultsDatabaseName;
            this.textTestChannel.Text = stationConfig.ConfigTestRadioChannel;
            if (stationConfig.ConfigDebug)
            {
                this.BackColor = Color.Yellow;
                this.lDebugMode.Text = Properties.Resources.StationInDebugMode;         // "Station in Debug Mode";
                this.lbDebugOptions.Text = stationConfig.ConfigDebugOptions;
            }
            else
            {
                this.lDebugMode.Text = "";
                this.lbDebugOptions.Text = string.Empty;
            }

        }

        private void configureFixtureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string outmsg = string.Empty;

            ConfigFixtures cfform = new ConfigFixtures(stationType);
            cfform.ShowDialog();
            cfform.Dispose();

            // now re-read the station configuraton for the fixture
            if (stationConfig.ReadStationConfigFile(stationType, ref outmsg) != 0)                                // read the station config file
            {
                MessageBox.Show("Problem reading the Station Configuration file.");
                addTextToStatusWindow("\nProblem reading the Station Configuration file. Station Initialization failed.\n" + outmsg, false);
            }
            else
            {
                addTextToStatusWindow("Station Configure file read\n", true);
                stationTitle = stationConfig.ConfigStationOperation + " - " + stationConfig.StationTitle + " : " + productCodeToTest;
                this.Text = stationTitle;
            }


        }

        private void TurnOnDUT(object sender, EventArgs e)
        {
            FixtureFunctions fixture = new FixtureFunctions();
            fixture.StationConfigure = stationConfig;
            fixture.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);
            if (!fixture.InitFixture())
            {
                MessageBox.Show("Error inititalizing the fixture. " + fixture.LastFunctionErrorMessage);
                addTextToStatusWindow("Error setting up fixture. " + fixture.LastFunctionErrorMessage + "\n", false);
                return;
            }
            fixture.ControlDUTPower(FixtureFunctions.PowerControl.ON, 0);
            MessageBox.Show("DUT power is on. Click OK to turn off");
            fixture.ControlDUTPower(FixtureFunctions.PowerControl.OFF, 0);
            fixture.Dispose();

        }

        private void clearResultWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            testResults.Clear();
        }

        private void clearStatusWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            testStatusBox.Clear();
        }

        private void CloseStation(object sender, FormClosedEventArgs e)
        {

        }

        private void resetStationConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // "Are you sure you want to reset the Station Configuration?"
            if (MessageBox.Show(Properties.Resources.StationConfigResetYesNo, "Station Configuration Reset", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                stationConfig.ResetConfigFile();
                // "You must restart the program and reconfigure the stations"
                MessageBox.Show(Properties.Resources.RestartAfterStationConfigReset);
            }
        }

        private void testStatusBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                testStatusBox.Clear();
        }

        private void testResults_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                testResults.Clear();
        }
    }



    //-------------------------------------------------------------------
    // event to enable printing in the status window from other classes
    //-------------------------------------------------------------------
    public class StatusUpdatedEventArgs : EventArgs
    {
        public string statusText { get; set; }
        public int testNumber { get; set; }
        public int stepNumber { get; set; }
        public int resultCode { get; set; }
        public StatusType statusType { get; set; }
    }


}
