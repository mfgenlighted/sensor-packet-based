﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.IO.Ports;
using System.Xml.Linq;

using TestStationErrorCodes;
using FixtureInstrumentBase;

namespace SensorManufSY2
{
    
    public class StationConfigure
    {
        public enum DBRecordType { OnTheFly, NONE, END };       // this must match what is in FixtureDatabase.cs
        private enum IsItemRequired { NO, YES };                // used for the UpdateItem method

        //--------------------------------------------------        
        // values optional contained in the settings
        //--------------------------------------------------
        // Full path and file name of the station configuration file. Optional
        //  If no path, it will expect in same directory
        //  as the test program. If no entry, defaults to
        //  c:\ProgramData\enlighted\SensorManufSY2\StationConfig.xml
        protected string configConfigFileName = "StationConfig.xml";
        protected string configConfigFileBakName = "StationConfig.xml.bak";
        protected string configConfigFileTemplateName = "StationConfigTemplate.xml";

        //-----------------------------
        // data not in config file but used
        //---------------------------
        protected string stationOperator = string.Empty;

        public string StationOperator { get { return stationOperator; } set { stationOperator = value; } }
        
        //------------------------------------
        // Config file read values
        //------------------------------------
        protected string configToUse = string.Empty;          // this is passed to the config read routine

        public string ConfigToUse { get { return configToUse; } }

        // <Station> Section
        protected string configStationName = string.Empty;            // name of this test station
        protected string configFactoryCode = string.Empty;            // station factory code for PN

        public string ConfigStationName { get { return configStationName; } }
        public string ConfigFactoryCode { get { return configFactoryCode; } set { configFactoryCode = value; } }


        // Test Options
        protected string configNextStepAfterFail =  "END";
        protected string configNextStepAfterPass =  "NEXT";
        protected string configDisplaySendChar =    "NO";
        protected string configDisplayRcvChar =     "NO";
        protected string configDebug =              "NO";
        protected string configDebugOptions =       string.Empty;
        protected string configEnablePFS =          "NO";
        protected string configPFSAskIfFail =       "NO";
        protected string configEnableSFS =          "NO";
        protected string configEnableConsoleDump =  "NO";

        public string ConfigNextStepAfterFail
        {   get { return configNextStepAfterFail.ToUpper(); }
            set { configNextStepAfterFail = value.ToUpper(); }
        }
        public string ConfigNextStepAfterPass 
        {   get { return configNextStepAfterPass.ToUpper(); }
            set { configNextStepAfterPass = value.ToUpper(); }
        }
        public bool ConfigDisplaySendChar
        {
            get { return configDisplaySendChar.Equals("Yes", StringComparison.OrdinalIgnoreCase); }
            set { configDisplaySendChar = (value ? "YES":"NO");}
        }
        public bool ConfigDisplayRcvChar
        {
            get { return configDisplayRcvChar.Equals("Yes", StringComparison.OrdinalIgnoreCase); }
            set { configDisplayRcvChar = (value ? "YES":"NO");}
        }
        public bool ConfigDebug
        {   get { return configDebug.ToUpper().Equals("YES"); }
            set { configDebug = (value ? "YES" : "NO"); }
        }
        public string ConfigDebugOptions
        {
            get { return configDebugOptions; }
            set { configDebugOptions = value; }
        }
        public bool ConfigEnablePFS
        {
            get { return configEnablePFS.ToUpper().Equals("YES"); }
            set { configEnablePFS = (value ? "YES" : "NO"); }
        }
        public bool ConfigPFSAskIfFail
        {
            get { return configPFSAskIfFail.ToUpper().Equals("YES"); }
            set { configPFSAskIfFail = (value ? "YES" : "NO"); }
        }
        public bool ConfigEnableSFS
        {
            get { return configEnableSFS.ToUpper().Equals("YES"); }
            set { configEnableSFS = (value ? "YES" : "NO"); }
        }
        public bool ConfigEnableConsoleDump
        {
            get { return configEnableConsoleDump.ToUpper().Equals("YES"); }
            set { configEnableConsoleDump = (value ? "YES" : "NO"); }
        }


        // Test Station section
        protected FixtureInstrument.fixtureTypes fixtureEquipment;   // equipment list of the fixture
        protected string configStationToUse = string.Empty;         // station that is defined in product map
        protected string configStationOperation = string.Empty;
        protected string configStationTitle = string.Empty;
        protected string configTempSensorOutput = "K";
        protected double configFixtureLoadA = 0;
        protected string configFixtureType = string.Empty;
        protected string configFixtureName = string.Empty;          // Fixture name to display and record

        public FixtureInstrument.fixtureTypes FixtureEquipment { get { return fixtureEquipment; } }
        public string ConfigStationToUse { get { return configStationToUse; }   set { configStationToUse = value; } }
        public string ConfigStationOperation { get { return configStationOperation; } set { configStationOperation = value; } }
        public string StationTitle { get { return configStationTitle; }         set { configStationTitle = value; } }
        public string ConfigTempSensorOutput { get { return configTempSensorOutput; } set { configTempSensorOutput = value.ToUpper(); } }
        public double ConfigFixtureLoadA { get { return configFixtureLoadA; }   set { configFixtureLoadA = value; } }
        public string ConfigFixtureType { get { return configFixtureType; } set { configFixtureType = value.ToUpper(); } }
        public string ConfigFixtureName { get { return configFixtureName; } set { configFixtureName = value; } }

        // DUTRadio subsection
        protected string configDUTComPort = string.Empty;              // com port to talk to DUT. format: COM<x>
        protected Dictionary<string, string> configDUTQCs = new Dictionary<string, string>();               // quick commands for the debug window
         
        public string ConfigDUTComPort { get { return configDUTComPort; } set { configDUTComPort = value.ToUpper(); } }
        public Dictionary<string, string> ConfigDUTQCs {  get { return configDUTQCs; } }


            // Test Radio subsection
        protected string configTestRadioComPort = string.Empty;              // com port to talk to Ref Radio. format: COM<x>
        protected string configTestRadioChannel = string.Empty;               // channel sniffer will operate on once radio is found
        public string ConfigTestRadioChannel
        {   get {   return configTestRadioChannel; }
            set {   if ((Int32.Parse(value) < 11) | (Int32.Parse(value) > 26))
                        configTestRadioChannel = "15";
                    else
                        configTestRadioChannel = value;}
                  }
        public string ConfigTestRadioComPort { get { return configTestRadioComPort; } set { configTestRadioComPort = value; } }

        // Switch-Mate subsection
        protected string configSwitchMateComPort = string.Empty;        // com port for the Switch-Mate. format: COM<x>
        public string ConfigSwitchMateComPort { get { return configSwitchMateComPort; } set { configSwitchMateComPort = value; } }

        // Programmer subsection
        protected string configFlasherComPort = string.Empty;
        public string ConfigFlasherComPort { get { return configFlasherComPort; } set { configFlasherComPort = value; } }

            // Label printer subsection
        protected string configPCBALabelPrinter = string.Empty;
        public string ConfigPCBALabelPrinter { get { return configPCBALabelPrinter; } set { configPCBALabelPrinter = value; } }
        protected string configProductLabelPrinter = string.Empty;
        public string ConfigProductLabelPrinter { get { return configProductLabelPrinter; } set { configProductLabelPrinter = value; } }

            // gains subsection
        protected double configLjChannelContolVGain = 2;          // default to 2
        public double ConfigLjChannelControlVGain { get { return configLjChannelContolVGain; } set { configLjChannelContolVGain = value; } }

            // LED detector subsection - default to the USB type
        public class LEDDetector
        {
            protected string comPort = "COM1";
                // the ledColors are not used for testing. They are only used to display the
                // color box on the fixture debug screen.
            protected string colorRED = "6100,6500";
            protected string colorGREEN = "5200,5800";
            protected string colorBLUE = "4200,4800";

            public string ComPort { get { return comPort; } set { comPort = value.ToUpper(); } }
            public string ColorRED { get { return colorRED; } set { colorRED = value; } }
            public string ColorGREEN { get { return colorGREEN; } set { colorGREEN = value; } }
            public string ColorBLUE { get { return colorBLUE; } set { colorBLUE = value; } }
        }

        protected LEDDetector configLedDetector = new LEDDetector();
        public LEDDetector ConfigLedDetector { get { return configLedDetector; } set { configLedDetector = value; } }

            // Ambient Detector subsection - default Type=RedLED, Voltage=2.0, OnDelayMs=100
        public class AmbientDetectorLightSource
        {
            protected double voltage = 2.0;
            protected int onDelayMs = 100;
            protected int offDelayMs = 0;

            public double Voltage { get { return voltage; } set { voltage = value; } }
            public int OnDelayMs { get { return onDelayMs; } set { onDelayMs = value; } }
            public int OffDelayMs { get { return offDelayMs; } set { offDelayMs = value; } }
        }
        protected AmbientDetectorLightSource configAmbientDetectorLightSource = new AmbientDetectorLightSource();
        public AmbientDetectorLightSource ConfigAmbientDetectorLightSource { get { return configAmbientDetectorLightSource;}}

            // PIR Detector subsection - default Type=Lamp, OnDelayMs=2500
        public class PIRDetectorLightSource
        {
            protected int onDelayMs = 2500;
            protected int offDelayMs = 0;

            public int OnDelayMs { get { return onDelayMs; } set { onDelayMs = value; } }
            public int OffDelayMs { get { return offDelayMs; } set { offDelayMs = value; } }
        }
        protected PIRDetectorLightSource configPIRDetectorLightSource = new PIRDetectorLightSource();
        public PIRDetectorLightSource ConfigPIRDetectorLightSource { get { return configPIRDetectorLightSource; } }


        // Directory Section
        protected string configReportDirectory = string.Empty;          // directory to put the test results text files
        protected string configLimitFileDirectory = string.Empty;       // directory to get the limits from
        protected string configLabelFileDirectory = string.Empty;       // directory to get the labels from

        public string ConfigReportDirectory { get { return configReportDirectory; } set { configReportDirectory = value; } }
        public string ConfigLabelFileDirectory { get { return configLabelFileDirectory; } set { configLabelFileDirectory = value; } }

        // SQL Section
        protected string configSQLServer = string.Empty;                            // name of the sql server to record the test results
        protected string configSQLProductDatabaseName = "productmap";               // product data info. If in debug mode, can be overriden by config file.
        protected string configSQLMacDatabaseName = "macsused";                     // name of the sql database to retrive and store MAC address. If in debug mode, can be overriden by config file.
        protected string configSQLTestLimitsDataName = "production_tests";          // database test limits are in
        protected string configSQLSerialNumberDatabaseName = "serialnumbersused";   // serial number database. If in debug mode, can be overriden by config file.
        protected string configSQLResultsDatabaseName = "production_results";       // results database
        protected string configSQLUser = "teststation";                             // database user. If in debug mode, can be overriden by config file.
        protected string configSQLPassword = "teston";                              // database password. If in debug mode, can be overriden by config file.
        protected string configSQLRecordOption = "END";                             // OnTheFly, None, End
        protected string configRecordTextFile = "NO";                               // Yes=Record the results in a text file.

        public string SQLServer { get { return configSQLServer; } set { configSQLServer = value; } }
        public string SQLResultsDatabaseName { get { return configSQLResultsDatabaseName;} set { configSQLResultsDatabaseName = value; }}
        public string SQLProductDatabaseName { get { return configSQLProductDatabaseName;  } set { configSQLProductDatabaseName = value; } }
        public string SQLMacDatabaseName { get { return configSQLMacDatabaseName;  } set { configSQLMacDatabaseName = value; } }
        public string SQLTestLimitsDataName { get { return configSQLTestLimitsDataName; } set { configSQLTestLimitsDataName = value; } }
        public string SQLSerialNumberDatabaseName { get { return configSQLSerialNumberDatabaseName; } set { configSQLSerialNumberDatabaseName = value; } }
        public string SQLOperatorName { get { return configSQLUser; } set { configSQLUser = value; } }
        public string SQLOperatorPassword { get { return configSQLPassword; } set { configSQLPassword = value; } }
        public DBRecordType SQLRecordOption
        {
            get
            {
                if (configSQLRecordOption.Equals("ONTHEFLY", StringComparison.InvariantCultureIgnoreCase))
                    return DBRecordType.OnTheFly;
                if (configSQLRecordOption.Equals("NONE", StringComparison.InvariantCultureIgnoreCase))
                    return DBRecordType.NONE;
                if (configSQLRecordOption.Equals("END", StringComparison.InvariantCultureIgnoreCase))
                    return DBRecordType.END;
                return DBRecordType.END;
            }
            set
            {
                if (value == DBRecordType.OnTheFly)
                    configSQLRecordOption = "onTheFly";
                else if (value == DBRecordType.NONE)
                    configSQLRecordOption = "NONE";
                else if (value == DBRecordType.END)
                    configSQLRecordOption = "END";
                else
                    configSQLRecordOption = "END";
            }
        }
        public bool ConfigRecordTextFile
        {
            get { return configRecordTextFile.Equals("Yes", StringComparison.OrdinalIgnoreCase); }
            set { configRecordTextFile = value ? "YES" : "NO"; }
        }

        // shopfloor section
        protected string configShopFloorDirectory = string.Empty;
        protected string configShopFloorFileName = string.Empty;

        public string ConfigShopFloorDirectory { get { return configShopFloorDirectory; } set { configShopFloorDirectory = value; } }
        public string ConfigShopFloorFileName { get { return configShopFloorFileName; } set { configShopFloorFileName = value; } }

        // PFS section
        protected string configPFSDatabaseName = string.Empty;
        protected string configPFSSeverIPAddress = string.Empty;
        protected string configPFSWorkCenter = string.Empty;

        public string PFSDatabaseName { get { return configPFSDatabaseName; } set { configPFSDatabaseName = value; } }
        public string PFSServerIPAddress { get { return configPFSSeverIPAddress; } set { configPFSSeverIPAddress = value; } }
        public string PFSWorkCenter { get { return configPFSWorkCenter; } set { configPFSWorkCenter = value; } }



        //==============//
        //==============//
        // constructor  //
        //==============//
        //==============//
        public StationConfigure()
        {
            string prgName = Path.GetFileName(Application.ExecutablePath);      // get the executile name
            prgName = prgName.Substring(0, prgName.LastIndexOf('.'));           // get rid of ext


            if (Properties.Settings.Default.StationConfigFileName != string.Empty)
                configConfigFileName = Properties.Settings.Default.StationConfigFileName;
            else
                configConfigFileName = @"c:\programdata\enlighted\" + prgName + "\\" + configConfigFileName;

            if (Properties.Settings.Default.StationConfigFileBakName != string.Empty)
                configConfigFileBakName = Properties.Settings.Default.StationConfigFileBakName;
            else
                configConfigFileBakName = @"c:\programdata\enlighted\" + prgName + "\\" + configConfigFileBakName;
        }

        //==================//
        //==================//
        //  Config routines //
        //==================//
        //==================//

        /// <summary>
        /// Will look for the config file. If none exits, will try to make one from StationConfigTemplate.xml.
        /// </summary>
        /// <returns>0 = found
        /// 1 = copyied
        /// -1 = failed to copy</returns>
        public int CheckForConfigFile(ref string outmsg)
        {
            int status = 0;
            outmsg = string.Empty;
            XDocument configdocTemplate;
            string configPath = string.Empty;

            if (!File.Exists(configConfigFileName))
            {
                MessageBox.Show("Config file does not exits. Creating from " + configConfigFileTemplateName);
                try
                {
                    if (!Directory.Exists(Path.GetDirectoryName(configConfigFileName)))
                        Directory.CreateDirectory(outmsg = Path.GetDirectoryName(configConfigFileName));
                    //File.Copy("StationConfigTemplate.xml", configConfigFileName);

                    // Read the template
                    try     // read the file
                    {
                        string path;
                        path = System.IO.Path.GetDirectoryName(
                           System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                        path = Path.Combine(path, configConfigFileTemplateName);
                        configdocTemplate = XDocument.Load(path);
                    }
                    catch (Exception e)
                    {
                        outmsg = "Problem opening the config file " + configConfigFileTemplateName + "\n" + e.ToString();
                        MessageBox.Show(outmsg);
                        return -1;
                    }
                    // make new doc
                    configdocTemplate.Element("StationConfig").Element("TestStations").Remove();
                    // copy Station, TestOptions, Directory, SQL, Shopfloor, and PFS sections
                    // save new doc
                    configdocTemplate.Save(configConfigFileName);

                }
                catch (Exception ex)
                {
                    outmsg = "Error copying file. " + ex.Message;
                    status = -1;
                }
            }

            return status;
        }

        public void ResetConfigFile()
        {
            if (File.Exists(configConfigFileName))
            {
                if (File.Exists(configConfigFileBakName))
                    File.Delete(configConfigFileBakName);
                File.Move(configConfigFileName, configConfigFileBakName);
            }
            File.Delete(configConfigFileName);
        }

        //-------------------------------------------
        // Read the station configuration file
        //  return - 0 = success
        //           -1 = did not find the file
        //           -2 = error reading values
        public short ReadStationConfigFile(string configToUse, ref string outmsg)
        {
            XDocument configdoc;
            List<string> missingItems = new List<string>();


            try     // read the file
            {
                configdoc = XDocument.Load(configConfigFileName);
            }
            catch (Exception e)
            {
                outmsg = "Problem opening the config file " + configConfigFileName + "\n" + e.ToString();
                MessageBox.Show(outmsg);
                return -1;
            }

            
            try
            {
                // -- Station section
                XElement genElement = configdoc.Element("StationConfig").Element("Station");
                if (genElement == null)
                    missingItems.Add("Missing StationConfig/Station section");
                else
                {
                    configStationName = Environment.MachineName; ;
                    if (genElement.Element("FactoryCode") == null)
                        missingItems.Add("StationConfig/Station/FactoryCode");
                    else
                        configFactoryCode = genElement.Element("FactoryCode").Value;
                }

                // -- Test Options Section
                //      All the items in this section are optional
                //configResendBadCmdEcho = "YES";
                configDisplaySendChar =     "NO";
                configDisplayRcvChar =      "NO";
                configDebug =               "NO";
                ConfigDebugOptions =        string.Empty;
                configEnablePFS =           "NO";
                configPFSAskIfFail =        "NO";
                configEnableSFS =           "NO";
                configEnableConsoleDump =   "NO";

                genElement = configdoc.Element("StationConfig").Element("TestOptions");
                if (genElement != null)
                {
                    if (genElement.Element("DisplaySendChar") != null)
                        configDisplaySendChar = genElement.Element("DisplaySendChar").Value;
                    if (genElement.Element("DisplayRcvChar") != null)
                        configDisplayRcvChar = genElement.Element("DisplayRcvChar").Value;
                    if (genElement.Element("Debug") != null)
                        configDebug = genElement.Element("Debug").Value;
                    if (genElement.Element("DebugOptions") != null)
                        configDebugOptions = genElement.Element("DebugOptions").Value;
                    if (genElement.Element("NextStepAfterPass") != null)
                        configNextStepAfterPass = genElement.Element("NextStepAfterPass").Value;
                    if (genElement.Element("NextStepAfterFail") != null)
                        configNextStepAfterFail = genElement.Element("NextStepAfterFail").Value;
                    if (genElement.Element("EnablePFS") != null)     // optional
                        configEnablePFS = genElement.Element("EnablePFS").Value.ToUpper();
                    if (genElement.Element("EnableSFS") != null)     // optional
                        configEnableSFS = genElement.Element("EnableSFS").Value.ToUpper();
                    if (genElement.Element("EnableConsoleDump") != null)     // optional
                        configEnableConsoleDump = genElement.Element("EnableConsoleDump").Value.ToUpper();
                    if (genElement.Element("PFSAskIfFail") != null)    // optional, default NO
                        configPFSAskIfFail = genElement.Element("PFSAskIfFail").Value;
                }

                ReadStationConfigStationSection(configToUse, configdoc, missingItems);

                // -- Directory Section
                //      If not are defined, then make one in Public area
                if (configdoc.Element("StationConfig").Element("Directory") == null)
                {
                    configReportDirectory = System.Environment.SpecialFolder.CommonApplicationData + @"\" + configToUse;
                    configLimitFileDirectory = string.Empty;     // will assume in application directory
                    configLabelFileDirectory = string.Empty;    // will assume in application directory
                }
                else
                {
                    if (configdoc.Element("StationConfig").Element("Directory").Element("Report") != null)
                        configReportDirectory = configdoc.Element("StationConfig").Element("Directory").Element("Report").Value;
                    if (configdoc.Element("StationConfig").Element("Directory").Element("Limits") != null)
                        configLimitFileDirectory = configdoc.Element("StationConfig").Element("Directory").Element("Limits").Value;
                    if (configdoc.Element("StationConfig").Element("Directory").Element("Labels") != null)
                        configLabelFileDirectory = configdoc.Element("StationConfig").Element("Directory").Element("Labels").Value;
                }
                if (!Directory.Exists(configReportDirectory))           // create the report directory if it does not exist
                    Directory.CreateDirectory(configReportDirectory);

                // -- SQL Section
                // - everything execpt the server address, is now optional.
                // - if the result database is not defined, then one will be defined at run time as:
                //      <product code>_testresults.
                if (configdoc.Element("StationConfig").Element("SQL") == null)
                    missingItems.Add("StationConfig/SQL section");
                else
                {
                    genElement = configdoc.Element("StationConfig").Element("SQL");
                    if (genElement.Element("Server") == null)
                        missingItems.Add("SQL/Server");
                    else
                        configSQLServer = genElement.Element("Server").Value;

                    if (configDebug == "YES")   // these item are predefined if not in debug mode
                    {
                        if (genElement.Element("ResultsDatabase") != null)
                            configSQLResultsDatabaseName = genElement.Element("ResultsDatabase").Value;

                        if (genElement.Element("ProductDatabase") != null)
                            configSQLProductDatabaseName = genElement.Element("ProductDatabase").Value;

                        if (genElement.Element("MACDatabase") != null)
                            configSQLMacDatabaseName = genElement.Element("MACDatabase").Value;

                        if (genElement.Element("SerialNumberDatabase") != null)
                            configSQLSerialNumberDatabaseName = genElement.Element("SerialNumberDatabase").Value;

                        if (genElement.Element("TestLimitsDatabase") != null)
                            configSQLTestLimitsDataName = genElement.Element("TestLimitsDatabase").Value;

                        if (genElement.Element("User") != null)
                            configSQLUser = genElement.Element("User").Value;

                        if (genElement.Element("Password") != null)
                            configSQLPassword = genElement.Element("Password").Value;

                        if (genElement.Element("DebugOptions") != null)
                            configDebugOptions = genElement.Element("DebugOptions").Value;
                    }


                    if (genElement.Element("RecordTextFileResults") != null)
                        configRecordTextFile = genElement.Element("RecordTextFileResults").Value;

                    if (genElement.Element("RecordSQLOption") != null)
                        if ((genElement.Element("RecordSQLOption").Value.ToUpper().Equals("ONTHEFLY") |
                            genElement.Element("RecordSQLOption").Value.ToUpper().Equals("NONE") |
                            genElement.Element("RecordSQLOption").Value.ToUpper().Equals("END")))
                        {
                            configSQLRecordOption = genElement.Element("RecordSQLOption").Value;
                        }
                        else
                        {
                            MessageBox.Show("RecordSQLOption can only be OnTheFly, None, or End.");
                            return -2;
                        }
                }

                // ---- the PFS items are optional. ----
                configPFSDatabaseName = string.Empty;
                configPFSSeverIPAddress = string.Empty;
                configPFSWorkCenter = string.Empty;
                genElement = configdoc.Element("StationConfig").Element("PFS");
                if (genElement != null)
                {
                    try
                    {
                        configPFSDatabaseName = genElement.Element("DatabaseName").Value;
                        configPFSSeverIPAddress = genElement.Element("ServerIPAddress").Value;
                        configPFSWorkCenter = genElement.Element("WorkCenter").Value;
                    }
                    catch (Exception)
                    {
                        missingItems.Add("One or more of the PFS items are missing");
                    }
                }

                // ---- the Shopfloor items are optional
                configShopFloorDirectory = string.Empty;
                configShopFloorFileName = string.Empty;
                genElement = configdoc.Element("StationConfig").Element("ShopFloor");
                if (genElement != null)
                {
                    try
                    {
                        configShopFloorFileName = genElement.Element("FileName").Value;
                        configShopFloorDirectory = genElement.Element("Directory").Value;
                    }
                    catch (Exception)
                    {
                        missingItems.Add("One or more of the ShopFloor items are missing");
                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("One or more of the Station Configuration items are missing\n" + e.ToString());
                return 2;
            }

            if (missingItems.Count != 0)
            {
                outmsg = "Missing\n";
                foreach (var item in missingItems)
                    outmsg = outmsg + item + "\n";
                return 2;
            }
            return 0;
        }

        /// <summary>
        /// Read the Test Station section of the config
        /// </summary>
        /// <param name="configToUse">name of the section to read</param>
        /// <param name="configdoc">XDocument to use</param>
        /// <param name="missingItems">List of missing items</param>
        public void ReadStationConfigStationSection(string configToUse, XDocument configdoc, List<string> missingItems)
        {
            XElement genElement;
            FixtureFunctions fixture = new FixtureFunctions();          // used to get the fixture definitions
            fixtureEquipment = null; 

            // -- Test Station Config Section
            genElement = configdoc.Element("StationConfig").Element("TestStations");
            if (genElement == null) // if the TestStations section does not exist
            {
                return;
            }
            genElement = configdoc.Element("StationConfig").Element("TestStations").Element(configToUse);
            if (genElement == null)
                missingItems.Add("StationConfig/TestStations/" + configToUse + " Section");
            else
            {
                if (genElement.Element("FixtureType") == null) // this is needed to do anything
                    missingItems.Add("FixtureType");
                else
                {
                    configFixtureType = genElement.Element("FixtureType").Value;
                    int x = 0;
                    foreach (var item in fixture.fixtureTypeList)           // get the equipment list for fixture
                    {
                        if (item.fixtureType.ToUpper() == ConfigFixtureType.ToUpper())
                            fixtureEquipment = item;
                        x++;
                    }
                    if (FixtureEquipment == null)
                    {
                        missingItems.Add("Invalid fixture type: " + ConfigFixtureType);
                        return;
                    }
                }


                if (genElement.Element("Station") == null)
                    missingItems.Add("Station");
                else
                    configStationToUse = genElement.Element("Station").Value;

                if (genElement.Element("Fixture") == null)
                    missingItems.Add("Fixture");
                else
                    configFixtureName = genElement.Element("Fixture").Value;

                if (genElement.Element("StationOperation") == null)
                    missingItems.Add("StationOperation");
                else
                    configStationOperation = genElement.Element("StationOperation").Value;

                if (genElement.Element("StationTitle") != null)
                    configStationTitle = genElement.Element("StationTitle").Value;

                if (genElement.Element("TempSensorOutput") != null)
                    configTempSensorOutput = genElement.Element("TempSensorOutput").Value;

                if (genElement.Element("FixtureLoadA") != null)
                    configFixtureLoadA = Convert.ToDouble(genElement.Element("FixtureLoadA").Value);

                // Label Printer subsection
                genElement = configdoc.Element("StationConfig").Element("TestStations").Element(configToUse).Element("LabelPrinters");
                if (fixtureEquipment.hasPCBAPrinter)
                {
                    if (genElement == null)
                        missingItems.Add("StationConfig/TestStations/" + configToUse + "/LabelPrinters");
                    else
                    {
                        if (genElement.Element("PCBA") == null)
                            missingItems.Add("StationConfig/TestStations/" + configToUse + "/LabelPrinters/PCBA");
                        else        // has something
                            configPCBALabelPrinter = genElement.Element("PCBA").Value;
                    }
                }
                if (fixtureEquipment.hasHLAPrinter)
                {
                    if (genElement == null)
                        missingItems.Add("StationConfig/TestStations/" + configToUse + "/LabelPrinters");
                    else
                    {
                        if (genElement.Element("Product") == null)
                            missingItems.Add("StationConfig/TestStations/" + configToUse + "/LabelPrinters/Product");
                        else        // has something
                            configProductLabelPrinter = genElement.Element("Product").Value;
                    }
                }

                // Test Radio subsection
                genElement = configdoc.Element("StationConfig").Element("TestStations").Element(configToUse).Element("TestRadio");
                if (FixtureEquipment.hasTestRadio)
                {
                    if (genElement == null)
                        missingItems.Add("StationConfig/TestStations/" + configToUse + "/TestRadio section");
                    else
                    {
                        if (genElement.Element("ComPort") == null)
                            missingItems.Add("StationConfig/TestStations/" + configToUse + "/TestRadio/ComPort");
                        else
                            configTestRadioComPort = genElement.Element("ComPort").Value;
                        if (genElement.Element("Channel") == null)
                            missingItems.Add("StationConfig/TestStations/" + configToUse + "/TestRadio/Channel");
                        else
                            configTestRadioChannel = genElement.Element("Channel").Value;
                    }
                }


                // DUT subsection
                if (fixtureEquipment.hasConsolePort)
                {
                    genElement = configdoc.Element("StationConfig").Element("TestStations").Element(configToUse).Element("DUT");
                    if (genElement == null)
                        missingItems.Add("StationConfig/TestStations/" + configToUse + "/DUT section");
                    else
                    {
                        if (genElement.Element("ComPort") == null)
                            missingItems.Add("StationConfig/TestStations/" + configToUse + "/DUT/ComPort");
                        else
                            configDUTComPort = genElement.Element("ComPort").Value;
                        if (genElement.Element("QuickCommands") != null)        // this is a optional section
                        {
                            string display, cmd;
                            configDUTQCs.Clear();
                            foreach (var llstep in genElement.Descendants("QuickCommands"))
                            {
                                foreach (var lstep in llstep.Descendants("CMD"))
                                {
                                    display = lstep.Attribute("display").Value;
                                    cmd = lstep.Attribute("command").Value;
                                    configDUTQCs.Add(display, cmd);
                                }
                            }

                        }
                    }

                }

                // Switch-Mate subsection
                if (fixtureEquipment.channelDefs != null)
                {
                    if (fixtureEquipment.channelDefs.Exists(x => x.instrumentID == FixtureInstrument.InstrumentID.SwitchMate))
                    {
                        genElement = configdoc.Element("StationConfig").Element("TestStations").Element(configToUse).Element("Switch-Mate");
                        if (genElement == null)
                            missingItems.Add("StationConfig/TestStations/" + configToUse + "/Switch-Mate");
                        else
                        {
                            genElement.Element("ComPort");
                            if (genElement == null)
                                missingItems.Add("StationConfig/TestStations/" + configToUse + "/Switch-Mate/ComPort");
                            else
                                configSwitchMateComPort = genElement.Value;
                        }
                    }
                }
                // Flasher subsection
                if (fixtureEquipment.hasFlasher)
                {
                    genElement = configdoc.Element("StationConfig").Element("TestStations").Element(configToUse).Element("Flasher");
                    if (genElement == null)     // optional item
                        configFlasherComPort = "COM0";
                    else
                    {
                        genElement.Element("ComPort");
                        if (genElement == null)
                            missingItems.Add("StationConfig/TestStations/" + configToUse + "/Flasher/ComPort");
                        else
                            configFlasherComPort = genElement.Value;
                    }
                }

                // Gain subsection
                if (fixtureEquipment.channelDefs != null)
                {
                    if ((fixtureEquipment.hasEnlightedInterface) & (fixtureEquipment.channelDefs.Exists(x => x.instrumentID == FixtureInstrument.InstrumentID.LABJACK)))
                    {
                        genElement = configdoc.Element("StationConfig").Element("TestStations").Element(configToUse).Element("Gains");
                        if (genElement != null)
                        {
                            genElement = configdoc.Element("StationConfig").Element("TestStations").Element(configToUse).Element("Gains").Element("ControlV");
                        }
                        if (genElement == null)
                            missingItems.Add("StationConfig/TestStations/" + configToUse + "/Gains/ControlV");
                        else
                            configLjChannelContolVGain = Convert.ToDouble(genElement.Value);

                    }
                }

                // LED detector subsection
                if (fixtureEquipment.channelDefs != null)
                {
                    if (fixtureEquipment.channelDefs.Exists(x => x.name == FixtureFunctions.CH_LED_DETECTOR))
                    {
                        genElement = configdoc.Element("StationConfig").Element("TestStations").Element(configToUse).Element("LEDDetector");
                        if (genElement == null)
                            missingItems.Add("StationConfig/TestStations/" + configToUse + "/LEDDetector");
                        else
                        {
                            if (genElement.Element("ComPort") == null)
                                missingItems.Add("StationConfig/TestStations/" + configToUse + "/LEDDetector/ComPort");
                            else
                            {
                                configLedDetector.ComPort = genElement.Element("ComPort").Value;
                                if (genElement.Element("ColorRed") != null)
                                {
                                    configLedDetector.ColorRED = genElement.Element("ColorRed").Value;
                                }
                                if (genElement.Element("ColorGreen") != null)
                                {
                                    configLedDetector.ColorGREEN = genElement.Element("ColorGreen").Value;
                                }
                                if (genElement.Element("ColorBlue") != null)
                                {
                                    configLedDetector.ColorBLUE = genElement.Element("ColorBlue").Value;
                                }
                            }
                        }
                    }
                }

                // Ambient Detector Light Source subsection
                if (fixtureEquipment.channelDefs != null)
                {
                    if (fixtureEquipment.channelDefs.Exists(x => x.name == FixtureFunctions.CH_AMBIENT_DETECTOR_LED))
                    {
                        genElement = configdoc.Element("StationConfig").Element("TestStations").Element(configToUse).Element("AmbientDetectorLightSource");
                        configAmbientDetectorLightSource = new AmbientDetectorLightSource();
                        if (genElement == null)
                            missingItems.Add("StationConfig/TestStations/" + configToUse + "/AmbientDetectorLightSource");
                        else
                        {
                            if (genElement.Element("Voltage") == null)
                                missingItems.Add("StationConfig/TestStations/" + configToUse + "/AmbientDetectorLightSource/Voltage");
                            else
                                configAmbientDetectorLightSource.Voltage = Convert.ToDouble(genElement.Element("Voltage").Value);
                            if (genElement.Element("OnDelayMs") != null)
                                configAmbientDetectorLightSource.OnDelayMs = Int32.Parse(genElement.Element("OnDelayMs").Value, System.Globalization.NumberStyles.AllowThousands);
                            if (genElement.Element("OffDelayMs") != null)
                                configAmbientDetectorLightSource.OffDelayMs = Int32.Parse(genElement.Element("OffDelayMs").Value, System.Globalization.NumberStyles.AllowThousands);
                        }
                    }
                }

                // PIR Detector Light Source subsection
                if (fixtureEquipment.channelDefs != null)
                {
                    if (fixtureEquipment.channelDefs.Exists(x => x.name == FixtureFunctions.CH_PIR_SOURCE_CNTL))
                    {
                        genElement = configdoc.Element("StationConfig").Element("TestStations").Element(configToUse).Element("PIRDetectorLightSource");
                        if (genElement == null)
                            missingItems.Add("StationConfig/TestStations/" + configToUse + "/PIRDetectorLightSource");
                        else
                        {
                            configPIRDetectorLightSource = new PIRDetectorLightSource();
                            if (genElement != null)
                            {
                                if (genElement.Element("OnDelayMs") != null)
                                    configPIRDetectorLightSource.OnDelayMs = Int32.Parse(genElement.Element("OnDelayMs").Value, System.Globalization.NumberStyles.AllowThousands);
                                if (genElement.Element("OffDelayMs") != null)
                                    configPIRDetectorLightSource.OffDelayMs = Int32.Parse(genElement.Element("OffDelayMs").Value, System.Globalization.NumberStyles.AllowThousands);
                            }
                        }
                    }
                }

            }
        }

        /// <summary>
        /// Will get a list of defined stations.
        /// </summary>
        /// <param name="stationTypes">List of test stations</param>
        /// <returns>(int)lclerror.Success or (int)ErrorCodes.ErrorNumbers.MissingFile</returns>
        public int ReadStationTypes(ref List<string> stationTypes)
        {
            XDocument configdoc;

            try     // read the file
            {
                configdoc = XDocument.Load(configConfigFileName);
            }
            catch (Exception e)
            {

                MessageBox.Show("Problem opening the config file " + configConfigFileName + "\n" + e.ToString());
                return (int)ErrorCodes.System.ConfigFileOpenError;
            }

            var xx = configdoc.Element("StationConfig").Element("TestStations").Elements();

            foreach (var item in xx)
            {
                stationTypes.Add(item.Name.ToString());
            }
            return 0;
        }

        /// <summary>
        /// Will get a list of defined stations with their fixture type
        /// </summary>
        /// <param name="stationTypes">List of test stations</param>
        /// <returns>(int)lclerror.Success or (int)ErrorCodes.ErrorNumbers.MissingFile</returns>
        public int ReadStationFixtureTypes(ref List<string> stationTypes, ref List<string> stationNames, ref List<string> fixtureType, ref List<string> fixtureName)
        {
            XDocument configdoc;
            string type = string.Empty;
            StationConfigure stationConfig;

            try     // read the file
            {
                configdoc = XDocument.Load(configConfigFileName);
            }
            catch (Exception e)
            {

                MessageBox.Show("Problem opening the config file " + configConfigFileName + "\n" + e.ToString());
                return (int)ErrorCodes.System.ConfigFileOpenError;
            }

            var xx = configdoc.Element("StationConfig").Element("TestStations");
            if (xx == null)     // if there are no stations defined
            {
                MessageBox.Show("There are no stations setup currently. Click OK to set up a station.");
                string outmsg = string.Empty;
                string selectedFixture = string.Empty;
                string selectedName = string.Empty;
                string selectedSection = string.Empty;
                string selectedStation = string.Empty;
                int i;

                List<string> stationTypesx = new List<string>();
                List<string> fixtureTypes = new List<string>();
                List<string> fixtureNames = new List<string>();
                List<string> stations = new List<string>();
                List<string> stationTitles = new List<string>();
                DialogResult yn = new System.Windows.Forms.DialogResult();

                // read the template file
                stationConfig = new StationConfigure();
                stationConfig.ReadStationTemplateTypes(ref stationTypesx, ref fixtureTypes, ref fixtureNames, ref stations, ref stationTitles);

                // ask what fixture type to use
                PickFixture sForm = new PickFixture(stationTitles);
                yn = sForm.ShowDialog();
                selectedFixture = sForm.SelectedFixture;
                sForm.Dispose();

                if (yn == System.Windows.Forms.DialogResult.OK)
                {
                    // get the section name for the fixture type
                    i = 0;
                    foreach (var item in stationTitles)
                    {
                        if (item == selectedFixture)
                        {
                            selectedSection = stationTypesx[i];
                            selectedStation = stations[i];
                        }
                        i++;
                    }
                    // ask section name
                    GetNewFixtureName gForm = new GetNewFixtureName(fixtureNames);
                    gForm.ShowDialog();
                    selectedName = gForm.NewFixtureName;
                    gForm.Dispose();

                    // let operator edit the Station
                    ConfigFixturesEditStation eForm = new ConfigFixturesEditStation(selectedStation);
                    eForm.stationName = selectedStation;
                    yn = eForm.ShowDialog();
                    if (yn == DialogResult.OK)
                        selectedStation = eForm.stationName;

                    // save in stationconfig.xml
                    stationConfig.CopyFixtureDataFromTemplate(selectedName, selectedSection, selectedStation);
                    try     // read the new file
                    {
                        configdoc = XDocument.Load(configConfigFileName);
                    }
                    catch (Exception e)
                    {

                        MessageBox.Show("Problem opening the config file " + configConfigFileName + "\n" + e.ToString());
                        return (int)ErrorCodes.System.ConfigFileOpenError;
                    }
                }
                else
                {

                    MessageBox.Show("Problem opening the config file " + configConfigFileName);
                    return (int)ErrorCodes.System.ConfigFileOpenError;
                }
            }

            var xxx = configdoc.Element("StationConfig").Element("TestStations").Elements();

            foreach (var item in xxx)
            {
                try
                {
                    stationTypes.Add(item.Name.ToString());
                    type = configdoc.Element("StationConfig").Element("TestStations").Element(item.Name.ToString()).Element("StationTitle").Value.ToString();
                    stationNames.Add(type);
                    type = configdoc.Element("StationConfig").Element("TestStations").Element(item.Name.ToString()).Element("FixtureType").Value.ToString();
                    fixtureType.Add(type);
                    type = configdoc.Element("StationConfig").Element("TestStations").Element(item.Name.ToString()).Element("Fixture").Value.ToString();
                    fixtureName.Add(type);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Missing FixtureType or Fixture in the config file." + ex.Message);
                    return (int)ErrorCodes.System.MissingParameter;
                }
            }
            return 0;
        }


        /// <summary>
        /// Will get a list of defined stations with their fixture type from the template file
        /// </summary>
        /// <param name="stationTypes">List of test stations</param>
        /// <returns>(int)lclerror.Success or (int)ErrorCodes.ErrorNumbers.MissingFile</returns>
        public int ReadStationTemplateTypes(ref List<string> stationTypes, ref List<string> fixtureTypes, ref List<string> fixtureNames,
                                                ref List<string> stations, ref List<string> stationTitles)
        {
            XDocument configdoc;
            string type = string.Empty;

            try     // read the file
            {
                string path;
                path = System.IO.Path.GetDirectoryName(
                   System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                path = Path.Combine(path, configConfigFileTemplateName);
                configdoc = XDocument.Load(path);
            }
            catch (Exception e)
            {

                MessageBox.Show("Problem opening the config file " + configConfigFileTemplateName + "\n" + e.ToString());
                return (int)ErrorCodes.System.ConfigFileOpenError;
            }

            var xx = configdoc.Element("StationConfig").Element("TestStations").Elements();

            foreach (var item in xx)
            {
                try
                {
                    stationTypes.Add(item.Name.ToString());
                    type = configdoc.Element("StationConfig").Element("TestStations").Element(item.Name.ToString()).Element("FixtureType").Value.ToString();
                    fixtureTypes.Add(type);
                    type = configdoc.Element("StationConfig").Element("TestStations").Element(item.Name.ToString()).Element("Fixture").Value.ToString();
                    fixtureNames.Add(type);
                    type = configdoc.Element("StationConfig").Element("TestStations").Element(item.Name.ToString()).Element("Station").Value.ToString();
                    stations.Add(type);
                    type = configdoc.Element("StationConfig").Element("TestStations").Element(item.Name.ToString()).Element("StationTitle").Value.ToString();
                    stationTitles.Add(type);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Missing FixtureType or Fixture in the config file." + ex.Message);
                    return (int)ErrorCodes.System.MissingParameter;
                }
            }
            return 0;
        }


        /// <summary>
        /// Will read the current config file and update any changes.
        /// </summary>
        /// <returns></returns>
        public int UpdateStationData()
        {
            XDocument configdoc;
            bool changed = false;
            string thingsChanged = string.Empty;

            try     // read the file
            {
                configdoc = XDocument.Load(configConfigFileName);
            }
            catch (Exception e)
            {

                MessageBox.Show("Problem opening the config file " + configConfigFileName + "\n" + e.ToString());
                return 1;
            }

            // -- <Station> section
            XElement genElement = configdoc.Element("StationConfig");
            UpdateItem("Name", configStationName, (int)IsItemRequired.YES, ref changed, ref thingsChanged, genElement, "Station");
            UpdateItem("FactoryCode", configFactoryCode, (int)IsItemRequired.YES, ref changed, ref thingsChanged, genElement, "Station");

            // <TestOptions> section
            genElement = configdoc.Element("StationConfig");
            UpdateItem("NextStepAfterFail", configNextStepAfterFail, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "TestOptions");
            UpdateItem("NextStepAfterPass", configNextStepAfterPass, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "TestOptions");
            UpdateItem("DisplaySendChar", configDisplaySendChar, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "TestOptions");
            UpdateItem("DisplayRcvChar", configDisplayRcvChar, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "TestOptions");
            UpdateItem("Debug", configDebug, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "TestOptions");
            UpdateItem("DebugOptions", configDebugOptions, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "TestOptions");
            UpdateItem("EnablePFS", configEnablePFS, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "TestOptions");
            UpdateItem("PFSAskIfFail", configPFSAskIfFail, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "TestOptions");
            UpdateItem("EnableSFS", configEnableSFS, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "TestOptions");
            UpdateItem("EnableConsoleDump", configEnableConsoleDump, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "TestOptions");

            // Directory section
            genElement = configdoc.Element("StationConfig");
            UpdateItem("Report", configReportDirectory, (int)IsItemRequired.YES, ref changed, ref thingsChanged, genElement, "Directory");
            UpdateItem("Labels", ConfigLabelFileDirectory, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "Directory");

            // SQL section
            genElement = configdoc.Element("StationConfig");
            UpdateItem("Server", configSQLServer, (int)IsItemRequired.YES, ref changed, ref thingsChanged, genElement, "SQL");
            UpdateItem("TestLimitsDatabase", configSQLTestLimitsDataName, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "SQL");
            UpdateItem("ResultsDatabase", configSQLResultsDatabaseName, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "SQL");
            UpdateItem("ProductDatabase", configSQLProductDatabaseName, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "SQL");
            UpdateItem("MACDatabase", configSQLMacDatabaseName, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "SQL");
            UpdateItem("SerialNumberDatabase", configSQLSerialNumberDatabaseName, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "SQL");
            UpdateItem("User", configSQLUser, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "SQL");
            UpdateItem("Password", configSQLPassword, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "SQL");
            UpdateItem("RecordSQLOption", configSQLRecordOption, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "SQL");
            UpdateItem("RecordTextFileResults", configRecordTextFile, (int)IsItemRequired.NO, ref changed, ref thingsChanged, genElement, "SQL");

            // --- ShopFloor Section ---
            genElement = configdoc.Element("StationConfig");
            if (genElement.Element("ShopFloor") != null)
            {
                UpdateItem("Directory", configShopFloorDirectory, (int)IsItemRequired.YES, ref changed, ref thingsChanged, genElement, "ShopFloor");
                UpdateItem("FileName", configShopFloorFileName, (int)IsItemRequired.YES, ref changed, ref thingsChanged, genElement, "ShopFloor");
            }

            // --- PFS Section ---
            genElement = configdoc.Element("StationConfig");
            if (genElement.Element("PFS") != null)
            {
                UpdateItem("DatabaseName", configPFSDatabaseName, (int)IsItemRequired.YES, ref changed, ref thingsChanged, genElement, "PFS");
                UpdateItem("ServerIPAddress", configPFSSeverIPAddress, (int)IsItemRequired.YES, ref changed, ref thingsChanged, genElement, "PFS");
                UpdateItem("WorkCenter", configPFSWorkCenter, (int)IsItemRequired.YES, ref changed, ref thingsChanged, genElement, "PFS");
            }
            if (changed)
            {
                thingsChanged = "Updated on " + DateTime.Now.ToString() + "\n" + thingsChanged;
                configdoc.Add(new XComment(thingsChanged));
                configdoc.Save(configConfigFileName);
            }

            return 0;
        }

        /// <summary>
        /// Will read the current config file and update any changes.
        /// </summary>
        /// <returns></returns>
        public int UpdateFixtureData(string configToUse)
        {
            XDocument configdoc;
            bool changed = false;                   // will be set true if any item changes
            string thingsChanged = string.Empty;
            string outmsg = string.Empty;
            string redConfig = string.Empty;
            string greenConfig = string.Empty;
            string blueConfig = string.Empty;
            int index;
            FixtureFunctions lclFixtureFunctions = new FixtureFunctions();
            //StationConfigure lclStationConfig = new StationConfigure();
            FixtureInstrument.fixtureTypes thisFixtureList = new FixtureInstrument.fixtureTypes();

            index = lclFixtureFunctions.fixtureTypeList.FindIndex(x => x.fixtureType == configFixtureType);
            if (index == -1)       // if did not find it
            {
                outmsg = "Could not find Fixture code: " + configFixtureType;
                MessageBox.Show(outmsg);
                return 1;
            }
            thisFixtureList = lclFixtureFunctions.fixtureTypeList[index];


            try     // read the file
            {
                configdoc = XDocument.Load(configConfigFileName);
            }
            catch (Exception e)
            {

                MessageBox.Show("Problem opening the config file " + configConfigFileName + "\n" + e.ToString());
                return 1;
            }

            // <TestStations><configStationToUse> section
            XElement genElement = configdoc.Element("StationConfig").Element("TestStations");
            UpdateItem("Station", configStationToUse, ref changed, ref thingsChanged, genElement, configToUse);
            UpdateItem("StationOperation", configStationOperation, ref changed, ref thingsChanged, genElement, configToUse);
            UpdateItem("StationTitle", configStationTitle, ref changed, ref thingsChanged, genElement, configToUse);
            UpdateItem("FixtureType", configFixtureType, ref changed, ref thingsChanged, genElement, configToUse);

            if (thisFixtureList.channelDefs != null)
            {
                if (thisFixtureList.channelDefs.Exists(x => x.name == FixtureFunctions.CH_TEMPERTURE_PROBE))
                    UpdateItem("TempSensorOutput", configTempSensorOutput, ref changed, ref thingsChanged, genElement, configToUse);
                else
                    RemoveItem("TempSensorOutput", ref changed, ref thingsChanged, genElement, configToUse);
            }

            genElement = configdoc.Element("StationConfig").Element("TestStations").Element(configToUse);
            // DUT subsection
            if (thisFixtureList.hasConsolePort)
            {
                UpdateItem("ComPort", configDUTComPort, ref changed, ref thingsChanged, genElement, "DUT");
            }
            else
                RemoveSection(ref changed, ref thingsChanged, genElement, "DUT");

                // Test Radio subsection
            if (thisFixtureList.hasTestRadio)
            {
                UpdateItem("ComPort", configTestRadioComPort, ref changed, ref thingsChanged, genElement, "TestRadio");
                UpdateItem("Channel", configTestRadioChannel, ref changed, ref thingsChanged, genElement, "TestRadio");
            }
            else
            {
                RemoveSection(ref changed, ref thingsChanged, genElement, "TestRadio");
            }


            // Switch-Mate subsection
            if (thisFixtureList.channelDefs != null)
            {
                if (thisFixtureList.channelDefs.Exists(x => x.instrumentID == FixtureInstrument.InstrumentID.SwitchMate))
                    UpdateItem("ComPort", configSwitchMateComPort, ref changed, ref thingsChanged, genElement, "Switch-Mate");
                else
                    RemoveSection(ref changed, ref thingsChanged, genElement, "Switch-Mate");
            }

            // Flasher subsection
            if (thisFixtureList.hasFlasher)
                UpdateItem("ComPort", configFlasherComPort, ref changed, ref thingsChanged, genElement, "Flasher");
            else
                RemoveSection(ref changed, ref thingsChanged, genElement, "Flasher");

            // Gains subsection - only enlighed interfac
            if (thisFixtureList.hasEnlightedInterface)
                UpdateItem("ControlV", configLjChannelContolVGain.ToString("N3"), ref changed, ref thingsChanged, genElement, "Gains");
            else
                RemoveSection(ref changed, ref thingsChanged, genElement, "Gains");

                // Label printers subsection
            if (thisFixtureList.hasPCBAPrinter)
                UpdateItem("PCBA", configPCBALabelPrinter, ref changed, ref thingsChanged, genElement, "LabelPrinters");
            else
                RemoveItem("PCBA", ref changed, ref thingsChanged, genElement, "LabelPrinters");
            if (thisFixtureList.hasHLAPrinter)
                UpdateItem("Product", configProductLabelPrinter, ref changed, ref thingsChanged, genElement, "LabelPrinters");
            else
                RemoveItem("Product", ref changed, ref thingsChanged, genElement, "LabelPrinters");
            if (!thisFixtureList.hasPCBAPrinter & !thisFixtureList.hasHLAPrinter)
                RemoveSection(ref changed, ref thingsChanged, genElement, "LabelPrinters");

            // LED Dectector subsection
            if (thisFixtureList.channelDefs != null)
            {
                if (thisFixtureList.channelDefs.Exists(x => x.name == FixtureFunctions.CH_LED_DETECTOR))
                {
                    UpdateItem("ComPort", configLedDetector.ComPort, ref changed, ref thingsChanged, genElement, "LEDDetector");
                    UpdateItem("ColorRed", configLedDetector.ColorRED, ref changed, ref thingsChanged, genElement, "LEDDetector");
                    UpdateItem("ColorGreen", configLedDetector.ColorGREEN, ref changed, ref thingsChanged, genElement, "LEDDetector");
                    UpdateItem("ColorBlue", configLedDetector.ColorBLUE, ref changed, ref thingsChanged, genElement, "LEDDetector");
                }
                else
                    RemoveSection(ref changed, ref thingsChanged, genElement, "LEDDetector");
            }

            // Ambient Detector subsection
            if (thisFixtureList.channelDefs != null)
            {
                if (thisFixtureList.channelDefs.Exists(x => x.name == FixtureFunctions.CH_AMBIENT_DETECTOR_LED))
                {
                    // Ambient Detector subsection
                    UpdateItem("Voltage", configAmbientDetectorLightSource.Voltage.ToString("N1"), ref changed, ref thingsChanged, genElement, "AmbientDetectorLightSource");
                    UpdateItem("OnDelayMs", configAmbientDetectorLightSource.OnDelayMs.ToString("N0"), ref changed, ref thingsChanged, genElement, "AmbientDetectorLightSource");
                    UpdateItem("OffDelayMs", configAmbientDetectorLightSource.OffDelayMs.ToString("N0"), ref changed, ref thingsChanged, genElement, "AmbientDetectorLightSource");
                }
                else
                    RemoveSection(ref changed, ref thingsChanged, genElement, "AmbientDetectorLightSource");
            }

            // PIR Detector subsection
            if (thisFixtureList.channelDefs != null)
            {
                if (thisFixtureList.channelDefs.Exists(x => x.name == FixtureFunctions.CH_PIR_SOURCE_CNTL))
                {
                    UpdateItem("OnDelayMs", configPIRDetectorLightSource.OnDelayMs.ToString("N0"), ref changed, ref thingsChanged, genElement, "PIRDetectorLightSource");
                    UpdateItem("OffDelayMs", configPIRDetectorLightSource.OffDelayMs.ToString("N0"), ref changed, ref thingsChanged, genElement, "PIRDetectorLightSource");
                }
                else
                    RemoveSection(ref changed, ref thingsChanged, genElement, "PIRDetectorLightSource");
            }

            if (changed)
            {
                thingsChanged = "Updated on " + DateTime.Now.ToString() + "\n" + thingsChanged;
                configdoc.Add(new XComment(thingsChanged));
                configdoc.Save(configConfigFileName);
            }

            return 0;
        }

        public int DeleteSection(string sectionName)
        {
            XDocument configdoc;

            try     // read the file
            {
                configdoc = XDocument.Load(configConfigFileName);
            }
            catch (Exception e)
            {

                MessageBox.Show("Problem opening the config file " + configConfigFileName + "\n" + e.ToString());
                return 1;
            }

            configdoc.Element("StationConfig").Element("TestStations").Element(sectionName).Remove();
            configdoc.Add(new XComment("Deleted section " + sectionName));
            configdoc.Save(configConfigFileName);
            return 0;
        }

        /// <summary>
        /// Will read the current config file and make a new section base on current.
        /// </summary>
        /// <remarks>
        /// The name passed in configToMake will be used for the section title with the
        /// spaces replaced with '_'. configToMake will be used in the Fixture field.
        /// If station is not blank, the Station field will be updated to it. If blank,
        /// it will use the read Station value.
        /// </remarks>
        /// <returns></returns>
        public int CopyFixtureDataFromConfigFile(string currentConfig, string configToMake, string station)
        {
            XDocument configdoc;
            string sectionName = string.Empty;

            try     // read the file
            {
                configdoc = XDocument.Load(configConfigFileName);
            }
            catch (Exception e)
            {
                MessageBox.Show("Problem opening the config file " + configConfigFileName + "\n" + e.ToString());
                return 1;
            }

            sectionName = configToMake.Replace(' ', '_');

            // get the data for the current station
            try
            {
                XElement fromElement = new XElement(configdoc.Element("StationConfig").Element("TestStations").Element(currentConfig));
                fromElement.Name = sectionName;
                fromElement.Element("Fixture").Value = configToMake;
                if (station != string.Empty)
                {
                    fromElement.Element("Station").Value = station;
                }

                // create the new section
                configdoc.Element("StationConfig").Element("TestStations").Add(fromElement);

                configdoc.Add(new XComment("Add fixture " + configToMake));
                configdoc.Save(configConfigFileName);

                return 0;
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid name. Only letters and number allowed");
                return 1;
            }
        }



        /// <summary>
        /// Will make a new section based from the template in the config file.
        /// </summary>
        /// <remarks>
        /// The name passed in configToMake will be used for the section title with the
        /// spaces replaced with '_'. configToMake will be used in the Fixture field.
        /// </remarks>
        /// <returns>0 - success</returns>
        public int CopyFixtureDataFromTemplate(string configToMake, string templateSection, string station)
        {
            XDocument templateConfigdoc;
            XDocument configdoc;
            string sectionName = string.Empty;
            List<string> missingItems = new List<string>();

            try     // read the template file
            {
                string path;
                path = System.IO.Path.GetDirectoryName(
                   System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                path = Path.Combine(path, configConfigFileTemplateName);
                templateConfigdoc = XDocument.Load(path);
                ReadStationConfigStationSection(templateSection, templateConfigdoc, missingItems);
            }
            catch (Exception e)
            {
                MessageBox.Show("Problem opening the config file " + configConfigFileTemplateName + "\n" + e.ToString());
                return 1;
            }
            try     // read the config file
            {
                configdoc = XDocument.Load(configConfigFileName);
                ReadStationConfigStationSection(templateSection, configdoc, missingItems);
            }
            catch (Exception e)
            {
                MessageBox.Show("Problem opening the config file " + configConfigFileTemplateName + "\n" + e.ToString());
                return 1;
            }

            sectionName = configToMake.Replace(' ', '_');

            // get the data form template
            XElement fromElement = new XElement(templateConfigdoc.Element("StationConfig").Element("TestStations").Element(templateSection));
            fromElement.Name = sectionName;
            fromElement.Element("Fixture").Value = configToMake;
            if (station != fromElement.Element("Station").Value)
            {
                fromElement.Element("Station").Value = station;
            }

            // create the new section
            if (configdoc.Element("StationConfig").Element("TestStations") == null)         // if this is a new config file that does not have any test stations
                configdoc.Element("StationConfig").Add(new XElement("TestStations"));
            configdoc.Element("StationConfig").Element("TestStations").Add(fromElement);

            configdoc.Add(new XComment("Add fixture " + configToMake));
            configdoc.Save(configConfigFileName);

            return (missingItems.Count == 0) ? 1:0;
        }


        /// <summary>
        /// Will check to see if the checkValue is different than the config file item updateElement
        /// (based off of genElement).
        /// </summary>
        /// <param name="updateElement"></param>
        /// <param name="checkValue"></param>
        /// <param name="optionalItem"></param>
        /// <param name="changed"></param>
        /// <param name="thingsChanged"></param>
        /// <param name="genElement"></param>
        private void UpdateItem(string updateElement, string checkValue, int optionalItem, ref bool changed, ref string thingsChanged, XElement genElement, string baseLevel)
        {
            if (genElement.Element(baseLevel) == null)
            {
                MessageBox.Show("Level " + baseLevel + " missing section that contains " + updateElement + ". This parameter will not be updated.");
                return;
            }
            if (optionalItem == (int)IsItemRequired.NO)
            {
                if (genElement.Element(baseLevel).Element(updateElement) == null)         // this is optional so it may not exist
                {
                    if (checkValue != string.Empty)      // if it has something now
                    {
                        changed = true;
                        genElement.Element(baseLevel).Add(new XElement(updateElement, checkValue));
                        thingsChanged = thingsChanged + string.Format("Added {0} {1}\n", genElement.Element(baseLevel).Element(updateElement).Name, checkValue);
                    }
                }
                else
                {
                    if (genElement.Element(baseLevel).Element(updateElement).Value != checkValue)
                    {
                        changed = true;
                        thingsChanged = thingsChanged + string.Format("Changed {0} {1} to {2}\n",
                            genElement.Element(baseLevel).Element(updateElement).Name, genElement.Element(baseLevel).Element(updateElement).Value, checkValue);
                        genElement.Element(baseLevel).Element(updateElement).Value = checkValue;                                   // default to NO
                    }
                }
            }
            else if (genElement.Element(baseLevel).Element(updateElement).Value != checkValue)
            {
                changed = true;
                thingsChanged = thingsChanged + string.Format("Changed {0} {1} to {2}\n", genElement.Element(baseLevel).Element(updateElement).Name,
                    genElement.Element(baseLevel).Element(updateElement).Value, checkValue);
                genElement.Element(baseLevel).Element(updateElement).Value = checkValue;                                   // default to NO
            }
        }

        /// <summary>
        /// Will check to see if the checkValue is different than the config file item updateElement
        /// (based off of genElement).
        /// </summary>
        /// <param name="updateElement"></param>
        /// <param name="checkValue"></param>
        /// <param name="optionalItem"></param>
        /// <param name="changed"></param>
        /// <param name="thingsChanged"></param>
        /// <param name="genElement"></param>
        private void UpdateItem(string updateElement, string checkValue, ref bool changed, ref string thingsChanged, XElement genElement, string baseLevel)
        {
            if (genElement.Element(baseLevel) == null)
            {
                genElement.Add(new XElement(baseLevel));
            }

            if (genElement.Element(baseLevel).Element(updateElement) == null)         // it may not exist
            {
                if (checkValue != string.Empty)      // if it has something now
                {
                    changed = true;
                    genElement.Element(baseLevel).Add(new XElement(updateElement, checkValue));
                    thingsChanged = thingsChanged + string.Format("Added {0} {1}\n", genElement.Element(baseLevel).Element(updateElement).Name, checkValue);
                }
            }
            else
            {
                if (genElement.Element(baseLevel).Element(updateElement).Value != checkValue)
                {
                    changed = true;
                    thingsChanged = thingsChanged + string.Format("Changed {0} {1} to {2}\n",
                        genElement.Element(baseLevel).Element(updateElement).Name, genElement.Element(baseLevel).Element(updateElement).Value, checkValue);
                    genElement.Element(baseLevel).Element(updateElement).Value = checkValue;                                   // default to NO
                }
            }
        }

        /// <summary>
        /// Will remove the item if it exists in the file
        /// </summary>
        /// <param name="updateElement"></param>
        /// <param name="changed"></param>
        /// <param name="thingsChanged"></param>
        /// <param name="genElement"></param>
        /// <param name="baseLevel"></param>
        private void RemoveItem(string updateElement, ref bool changed, ref string thingsChanged, XElement genElement, string baseLevel)
        {
            if (genElement.Element(baseLevel) != null)  // if exists
            {
                if (genElement.Element(baseLevel).Element(updateElement) != null)   // if it exists
                {
                    changed = true;
                    thingsChanged = thingsChanged + string.Format("Removed {0}\n",
                        genElement.Element(baseLevel).Element(updateElement).Name);
                    genElement.Element(baseLevel).Element(updateElement).Remove();
                }
            }
        }

        private void RemoveSection(ref bool changed, ref string thingsChanged, XElement genElement, string baseLevel)
        {
            if (genElement.Element(baseLevel) != null)  // if it exists
            {
                changed = true;
                thingsChanged = thingsChanged + string.Format("Removed {0}\n",
                    genElement.Element(baseLevel).Name);
                genElement.Element(baseLevel).Remove();
            }
        }



    }

}
