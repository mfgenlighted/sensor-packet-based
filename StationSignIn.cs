﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Globalization;
using System.Configuration;

namespace SensorManufSY2
{
    public partial class StationSignIn : Form
    {
        public string UserName { get { return userName.Text; } }
        public string Language { get { return (rbEnglish.Checked ? "en" : "zh-Hans"); } }
        public StationSignIn()
        {
            string lang = string.Empty;

            InitializeComponent();
            lang = Properties.Settings.Default.Language;
            if (lang == "zh-Hans")
                rbChinese.Checked = true;
            else
                rbEnglish.Checked = true;
        }

    }
}
