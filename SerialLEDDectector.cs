﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Text.RegularExpressions;
using System.Diagnostics;

using TestStationErrorCodes;

namespace SensorManufSY2
{
    class SerialLEDDectector : IDisposable
    {
        bool disposed = true;
        int wavelength = 0;
        int intensity = 0;

        //----------------------------------------
        // serial port values
        //----------------------------------------
        private SerialPort serialPort = null;            // detector serial port
        private string comPort = string.Empty;
        private string serialBuffer = string.Empty;      // buffer of incoming serial data
        private string readBuffer = string.Empty;        // buffer read on one event


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if ((serialPort != null) & (serialPort.IsOpen))
                    {
                        serialPort.Close();
                        serialPort.Dispose();
                        disposed = true;
                    }
                }
            }
        }

        //------------------------------------------------------------
        // Open(string comPort)
        /// <summary>
        /// Open the LED detector serial port. Exceptions are throw if errors.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="comPort"></param>
        public void Open(string comPort)
        {
            if (serialPort != null)                        // if already opne
            {
                serialPort.Close();                         // close it
                serialPort.Dispose();                       // and get rid of it
                this.comPort = string.Empty;
            }


            if (CheckForSerialPort(comPort))
            {
                // port valid on this system, so try to open
                try
                {
                    serialPort = new SerialPort(comPort, 19200);
                    serialPort.DtrEnable = false;
                    serialPort.Handshake = Handshake.None;
                    serialPort.Parity = Parity.None;
                    serialPort.DataReceived += new SerialDataReceivedEventHandler(serialDataReceived);
                    serialPort.ReadTimeout = 100;
                    serialPort.Open();           // open the serial port
                    this.comPort = comPort;
                    disposed = false;
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Error opening {0}", comPort), ex);
                }
            }
            else
                throw new Exception("LED Detector serial port " + comPort + " is not a valid com port on this computer");
        }

        //--------------------------------------
        // Close()
        /// <summary>
        /// Close the DUT serial port
        /// </summary>
        public void Close()
        {
            if (serialPort.IsOpen)
                serialPort.Close();
            //serialPort.Dispose();
            Debug.WriteLine("LED port closed");
        }

        //-------------------------------------
        //  ReadLEDValues
        public int ReadLEDValues(ref int wavelength, ref int intestity)
        {
            wavelength = 0;
            intestity = 0;

            if (serialPort == null)     // if it has not be defined
                return (int)ErrorCodes.System.LEDDetectorNotInitialize;

            if (!serialPort.IsOpen)
                return (int)ErrorCodes.System.LEDDetectorNotOpen;

            wavelength = this.wavelength;
            intestity = this.intensity;
            return 0;
        }

        //---------------------
        //  utilities
        //-------------------
        private bool CheckForSerialPort(string comPort)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            // see if this port is in the list of ports on the computer
            foreach (string i in ports)
            {
                if (i.Equals(comPort))         // if found a match
                    return true;
            }
            return false;
        }

        static readonly object serialRcvLock = new object();      // setup so can lock code below
        //-------------------------------------
        // Method: DutSerialDataReceived
        //  StatusUpdated handler for the dut serial port
        //
        void serialDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string msg = string.Empty;
            char[] delimiterChars = { ' ', '=', '\r', '\n'};
            Match m;
            SerialPort sp = (SerialPort)sender;
            string smsg = string.Empty;
            lock (serialRcvLock)
            {
                if (sp.IsOpen)
                {
                    try
                    {
                        wavelength = 0;
                        intensity = 0;
                        msg = sp.ReadLine();
                        m = Regex.Match(msg, @"w=(\d+)");
                        if (m.Success)
                            int.TryParse(m.Value.Substring(2), out wavelength);
                        m = Regex.Match(msg, @"i=(\d+)");
                        if (m.Success)
                            int.TryParse(m.Value.Substring(2), out intensity);
                    }
                    catch (Exception)
                    {
                        return;
                    }
                }
            }
        }

    }
}
