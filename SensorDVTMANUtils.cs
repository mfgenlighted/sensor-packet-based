﻿using System;
using System.Windows.Forms;
using System.Linq;

using LimitsDatabase;
using TestStationErrorCodes;

namespace SensorManufSY2
{


    public partial class SensorDVTMANTests
    {
        //-----------------------------------------------
        //  ReadSensors
        /// <summary> Will read the sensors and store the values locally.</summary>
        /// <param name="data">returned data. Values will be set to -99
        ///  if there was a error reading or converting.</param>
        /// <param name="reporttext">out string - If there was a error, describes the error.</param>
        /// <returns>test status</returns>
        public int ReadSensors(ref SUSensorData data,  MainWindow.uutdata uutData, out string reporttext)
        {
            fixture.SensorConsole.GetSensorData(ref data.PIR, ref data.AmbVisable, ref data.Temperature);
            reporttext = fixture.SensorConsole.LastErrorMessage;
            return fixture.SensorConsole.LastErrorCode;
        }

        //--------------------------------------
        // ReadCPUTemperature
        //
        /// <summary>
        /// Will read the CPU temperature
        /// </summary>
        //
        // Return:
        //  bool - true = dut command send and prompt found
        //          false = problem with dut or conversion. see reporttext. temperature returned as -99.
        //

        public int ReadCPUTemperature(out int temp, MainWindow.uutdata uutData, out string reporttext)
        {
            string msg = string.Empty;
            int status = 0;
            SUSensorData data = new SUSensorData();

            status = ReadSensors(ref data, uutData, out reporttext);
            //temp = data.Temperature;
            temp = 0;
            return status;
        }


        //------------------------------------------//
        //------------------------------------------//
        //                                          //
        //      Manufacturing data routines         //
        //------------------------------------------//
        //------------------------------------------//


        //---------------------------------
        // setPcbaManData
        ///<summary>
        /// This will set the pcba manufacturing data. 
        /// </summary>
        /// <remarks>
        /// Data set is the serial number and part number.
        /// newdata is set when the PCB serial number is gotten(Function:GetSN)
        /// </remarks>
        ///<returns>
        ///     ErrorCodes.Program.Success        0
        ///     ErrorCodes.Program.DUTComError    2
        ///     ErrorCodes.Program.DupicateSN     5
        ///  2   ErrorCodes.Program.DUTComError
        ///  3  ErrorCodes.Program.DutUnexpectedCmdReturn
        ///  6  ErrorCodes.Program.PCBADataMismatch
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        ///</returns>
        private int setPcbaManData(LimitsData.stepsItem parameters, MainWindow.uutdata uutData, bool newdata, out string reporttext)
        {
            string readSN = string.Empty;
            string readPN = string.Empty;

            int stepStatus = 0;
            string outmsg = string.Empty;
            bool manfDataEmpty = true;
            reporttext = string.Empty;

            //// see if manufacturing data is blank
            //if (!fixture.SensorConsole.GetPCBAData(ref readPN, ref readSN))   // if failed to get any data
            //{
            //    stepStatus = fixture.SensorConsole.LastErrorCode;
            //    if (stepStatus < (int)ErrorCodes.System.SystemErrorCodes)
            //        stepStatus = (int)ErrorCodes.Program.DUTComError + (parameters.TestNumber * 100);
            //    reporttext = fixture.SensorConsole.LastErrorMessage;
            //    goto Cleanup;
            //}

            // now see if the data is blank
            manfDataEmpty = (readSN == "");

            //if (manfDataEmpty)      // if nothing programmed in the DUT
            //{
            //    if (newdata) // if this newly generated data, and the board is blank, program
            //    {
                    if (!fixture.SensorConsole.SetPCBAData(uutData.pcbaPN, uutData.pcbaSN))
                    {
                        reporttext = fixture.SensorConsole.LastErrorMessage;
                        stepStatus = fixture.SensorConsole.LastErrorCode;
                        goto Cleanup;
                    }
                    else            // everything seems good
                    {
                        // verify that it programed correctly
                        if (!fixture.SensorConsole.GetPCBAData(ref readPN, ref readSN))     // if what is programmed does not match what should be there
                        {
                            reporttext = fixture.SensorConsole.LastErrorMessage;
                            stepStatus = fixture.SensorConsole.LastErrorCode;
                        }
                        else
                        {
                            if ((readPN.ToUpper().Trim(' ') == uutData.pcbaPN.ToUpper()) & (readSN.ToUpper().Trim(' ') == uutData.pcbaSN.ToUpper()))
                                reporttext = string.Format("Programmed SN: {0} \n\tPN: {1}", uutData.pcbaSN, uutData.pcbaPN);
                            else
                            {
                                reporttext = string.Format("Readback did not match:\n\tPN programed  {0}  read {1}\n\tSN programed {2} read {3}",
                                                            uutData.pcbaPN, readPN, uutData.pcbaSN, readSN);
                                stepStatus = (int)ErrorCodes.Program.PCBADataMismatch;
                            }
                        }
                    }
            //    }
            //    else        // this was data already in the database, but nothing in the DUT
            //    {
            //        if (MessageBox.Show("Is this a retested unit?", "SN Dup Check", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //        {
            //            if (!fixture.SensorConsole.SetPCBAData(uutData.pcbaPN, uutData.pcbaSN))
            //            {
            //                reporttext = fixture.SensorConsole.LastErrorMessage;
            //                stepStatus = fixture.SensorConsole.LastErrorCode + (parameters.TestNumber * 100);
            //                goto Cleanup;
            //            }
            //            else            // everything seems good
            //            {
            //                // verify that it programed correctly
            //                if (!fixture.SensorConsole.GetPCBAData(ref readPN, ref readSN))     // if what is programmed does not match what should be there
            //                {
            //                    reporttext = fixture.SensorConsole.LastErrorMessage;
            //                    stepStatus = fixture.SensorConsole.LastErrorCode + (parameters.TestNumber * 100);
            //                }
            //                else
            //                {
            //                    if ((readPN.ToUpper().Trim(' ') == uutData.pcbaPN.ToUpper()) & (readSN.ToUpper().Trim(' ') == uutData.pcbaSN.ToUpper()))
            //                        reporttext = string.Format("Programmed SN: {0} \n\tPN: {1}", uutData.pcbaSN, uutData.pcbaPN);
            //                    else
            //                    {
            //                        reporttext = string.Format("Readback did not match:\n\tPN programed  {0}  read {1}\n\tSN programed {2} read {3}",
            //                                                    uutData.pcbaPN, readPN, uutData.pcbaSN, readSN);
            //                        stepStatus = (int)ErrorCodes.Program.PCBADataMismatch;
            //                    }
            //                }
            //            }
            //        }
            //        else        // this may be a unit with a dup sn
            //        {
            //            reporttext = "This unit has a dupicate SN with another unit.";
            //            stepStatus = (int)ErrorCodes.Program.DupicateSN + (parameters.TestNumber * 100);
            //        }

            //    }
            //}
            //else            // there is data programmed
            //{
            //    reporttext = "\tFound existing pcba data.\n";
            //    if (!fixture.SensorConsole.GetPCBAData(ref readPN, ref readSN))     // if what is programmed does not match what should be there
            //    {
            //        reporttext = fixture.SensorConsole.LastErrorMessage;
            //        stepStatus = fixture.SensorConsole.LastErrorCode + (parameters.TestNumber * 100);
            //    }
            //    else
            //    {
            //        if ((readPN.ToUpper().Trim(' ') != uutData.pcbaPN.ToUpper()) | (readSN.ToUpper().Trim(' ') != uutData.pcbaSN.ToUpper()))
            //        {
            //            reporttext = string.Format("Existing did not match:\n\tPN existing  '{0}'  read '{1}'\n\tSN existing '{2}' read '{3}'",
            //                                        uutData.pcbaPN, readPN.ToUpper().Trim(' '), uutData.pcbaSN, readSN.ToUpper().Trim(' '));
            //            stepStatus = (int)ErrorCodes.Program.PCBADataMismatch + (parameters.TestNumber * 100);
            //        }
            //    }
            //}

            Cleanup:
            return stepStatus;
        }



        /// <summary>
        /// Will set the HLA data.
        /// </summary>
        /// <param name="uutData"></param>
        /// <param name="reporttext"></param>
        /// <returns>
        ///  2  ErrorCodes.Program.DUTComError
        ///  3  ErrorCodes.Program.DutUnexpectedCmdReturn
        ///  6  ErrorCodes.Program.PCBADataMismatch
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </returns>
        private int setHlaData(LimitsData.stepsItem parameters, MainWindow.uutdata uutData, bool newdata, out string reporttext)
        {
            int status = 0;
            string readSN = string.Empty;
            string readPN = string.Empty;
            string readModel = string.Empty;

            reporttext = string.Empty;

            reporttext = string.Empty;
            if (!fixture.SensorConsole.SetHLAData(uutData.topPN, uutData.topSN, uutData.model))
            {
                reporttext = fixture.SensorConsole.LastErrorMessage;
                status = fixture.SensorConsole.LastErrorCode;
            }
            else
            {
                if (!fixture.SensorConsole.GetHLAData(ref readPN, ref readSN, ref readModel))
                {
                    reporttext = fixture.SensorConsole.LastErrorMessage;
                    status = fixture.SensorConsole.LastErrorCode;
                }
                else
                {
                    if ((uutData.topPN.ToUpper() != readPN.ToUpper().Trim(' ')) | (uutData.topSN.ToUpper() != readSN.ToUpper().Trim(' ')) |
                            (uutData.model != readModel.Trim(' ')))
                    {
                        reporttext = string.Format("Readback did not match:\n\tPN programed  -{0}-  read -{1}-\n\tSN programed -{2}- read -{3}-\n\t programed -{4}- read -{5}-",
                                                    uutData.topPN, readPN.Trim(' '), uutData.topSN, readSN.Trim(' '), uutData.model, readModel.Trim(' '));
                        status = (int)ErrorCodes.Program.PCBADataMismatch;
                    }
                }
            }
            return status;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="uutData"></param>
        /// <param name="reporttext"></param>
        /// <returns>
        ///  2  ErrorCodes.Program.DUTComError
        ///  6  ErrorCodes.Program.PCBADataMismatch
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </returns>
        private int setMACData(LimitsData.stepsItem parameters, MainWindow.uutdata uutData, out string reporttext)
        {
            int status = 0;
            string readMac = string.Empty;
            string readMac2 = string.Empty;

            reporttext = string.Empty;
            

            if (!fixture.SensorConsole.SetMACData(uutData.mac, uutData.mac2))
            {
                reporttext = fixture.SensorConsole.LastErrorMessage;
                status = fixture.SensorConsole.LastErrorCode;
            }
            else
            {
                if (!fixture.SensorConsole.GetMACData(ref readMac, ref readMac2))
                {
                    reporttext = fixture.SensorConsole.LastErrorMessage;
                    status = fixture.SensorConsole.LastErrorCode;
                }
                if ((uutData.mac.ToUpper() != readMac.ToUpper().Trim(' ')) | (uutData.mac2.ToUpper() != readMac2.ToUpper().Trim(' ')))
                {
                    reporttext = string.Format("Readback did not match:\n\tMac programed  -{0}-  read -{1}-\n\tMac2 programed -{2}- read -{3}-",
                                                uutData.mac, readMac.Trim(' '), uutData.mac2, readMac2.Trim(' '));
                    status = (int)ErrorCodes.Program.PCBADataMismatch;

                }
            }
            return status;
        }

        /// <summary>
        /// Sets the hardware configuration byte
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="hwset"></param>
        /// <param name="reporttext"></param>
        /// <returns>
        /// -6  ErrorCodes.System.InvalidParameter
        ///  2  ErrorCodes.Program.DUTComError
        ///  6  ErrorCodes.Program.PCBADataMismatch
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </returns>
        private int setHwConfigData(LimitsData.stepsItem parameters, ref string hwset, out string reporttext)
        {
            int status = 0;
            string configlist = string.Empty;
            byte hwconf;
            
            byte[] readHw;

            reporttext = string.Empty;
            if ((configlist = parameters.GetParameterValue("HWCONFIG")) == string.Empty)
            {
                reporttext = "Error: Missing or empty HwConfig constant";
                status = (int)ErrorCodes.System.MissingParameter;
                return status;
            }

            hwset = configlist;
            // convert the list of numbers to a array
            if (!byte.TryParse(configlist, out hwconf))
            {
                reporttext = "Invalid hardware config in limits. Must be a single number.";
                status = (int)ErrorCodes.System.ParameterFormatError;
                return status;
            }
            if (!fixture.SensorConsole.SetHwConfig(hwconf))
            {
                reporttext = fixture.SensorConsole.LastErrorMessage;
                status = fixture.SensorConsole.LastErrorCode;
            }
            else
            {
                readHw = new byte[255];
                if (!fixture.SensorConsole.GetHwConfigData(ref readHw))
                {
                    reporttext = fixture.SensorConsole.LastErrorMessage;
                    status = fixture.SensorConsole.LastErrorCode;

                }
                if (readHw[0] != hwconf)
                {
                    reporttext = "Readback(" + readHw[0].ToString() + ") of hardware config(" + hwconf.ToString() +") did not match";
                    status = (int)ErrorCodes.Program.PCBADataMismatch;
                }
            }
            return status;
        }

        //--------------------------------------//
        //--------------------------------------//
        //                                      //
        //      Value checking routines         //
        //--------------------------------------//
        //--------------------------------------//

        //-------------------------------------------------
        /// <summary>
        /// This function will check a value as defined in a List type parameters. Will throw Exception for invalid
        /// stuff.
        /// </summary>
        /// <param name="testvalue"></param>
        /// <param name="item"></param>
        /// <returns>bool - true = meets the conditions</returns>
        private static bool CheckResultValue(double testvalue, LimitsDatabase.LimitsData.limitsItem item, ref string resultmessage)
        {
            bool status = true;

            resultmessage = string.Empty;
            switch (item.type.ToUpper())
            {
                case "RANGE":
                    switch (item.operation)
                    {
                        case "GTLT":
                            if ((testvalue <= Convert.ToDouble(item.value1)) || (testvalue >= Convert.ToDouble(item.value2)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: {1} < x < {2}  Found: {3:N2}\n", item.name, item.value1, item.value2, testvalue);
                            break;
                        case "GELE":
                            if ((testvalue < Convert.ToDouble(item.value1)) || (testvalue > Convert.ToDouble(item.value2)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: {1} <= x <= {2}  Found: {3:N2}\n", item.name, item.value1, item.value2, testvalue);
                            break;
                        default:
                            throw new Exception("Invalid operation: " + item.name.ToString() + ":" + item.operation.ToString());
                    }
                    break;

                case "VALUE":
                    switch (item.operation)
                    {
                        case "GE":      // greater than or equal
                            if (!(testvalue >= Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x >= {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        case "EQ":      // equal
                            if (!(testvalue == Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x = {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        case "LE":      // less than or equal
                            if (!(testvalue <= Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x <= {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        case "LT":      // less than
                            if (!(testvalue < Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x < {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        default:
                            throw new Exception("Invalid operation: " + item.name.ToString() + ":" + item.operation.ToString());
                    }
                    break;

                default:
                    throw new Exception("Invalid type: " + item.name.ToString() + ":" + item.operation.ToString());
            }

            return status;
        }

        //------
        // utility for CU test
        uint CrcCalc(byte[] msg, int len)
        {
            uint k, i;
            uint crc = 0;

            const UInt16 poly = 0x07;
            for (i = 0; i < len; i++)
            {
                crc = crc ^ msg[i];
                for (k = 0; k < 8; k++)
                {
                    if ((crc & 0x80) != 0)
                    {
                        crc <<= 1;
                        crc ^= poly;
                    }
                    else
                    {
                        crc <<= 1;
                    }
                }
            }
            return (crc);
        }


                //--------------------------------------//
                //--------------------------------------//
                //                                      //
                // limit file utils                     //
                //--------------------------------------//
                //--------------------------------------//
        ///// <summary>
        ///// Will get the data for a given constant in the CList.
        ///// </summary>
        ///// <param name="testData">structure that has the CList</param>
        ///// <param name="constantName">name of the constant to get data for</param>
        ///// <param name="constData">returned data. string.empty if none found</param>
        ///// <returns>true = name is found</returns>
        //private bool GetConstantValue(TestToRun testData, string constantName, ref string constValue)
        //{
        //    bool tagfound = false;

        //    constValue = string.Empty;
        //    foreach (var item in testData.CList)   // go through the parameters in the test
        //    {
        //        if (item.Name.ToUpper().Equals(constantName.ToUpper()))
        //        {
        //            constValue = item.Value;
        //            tagfound = true;
        //        }
        //    }
        //    return tagfound;
        //}

        //private bool GetParameterStruct(TestToRun testData, string parameter, ref LimitFileFunctions.ListEntry paramStruct)
        //{
        //    bool tagfound = false;

        //    foreach (var item in testData.PList)
        //    {
        //        if (item.Parameter == parameter)
        //        {
        //            tagfound = true;
        //            paramStruct = item;
        //        }
        //    }
        //    return tagfound;
        //}

    }

}