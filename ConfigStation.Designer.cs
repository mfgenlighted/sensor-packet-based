﻿namespace SensorManufSY2
{
    partial class ConfigStation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textPFSWorkCenter = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textPFSServer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textPFSDatabase = new System.Windows.Forms.TextBox();
            this.textReportDirectory = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textSFFileName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textSFDir = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbPFSAskIfFail = new System.Windows.Forms.CheckBox();
            this.textBoxStepAfterFail = new System.Windows.Forms.TextBox();
            this.textBoxStepAfterPass = new System.Windows.Forms.TextBox();
            this.checkBoxEnableConsoleDump = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.checkBoxDebug = new System.Windows.Forms.CheckBox();
            this.checkBoxDisplayDUTSend = new System.Windows.Forms.CheckBox();
            this.checkBoxDisplayDUTRcv = new System.Windows.Forms.CheckBox();
            this.checkBoxEnablePFS = new System.Windows.Forms.CheckBox();
            this.checkBoxEnableSFS = new System.Windows.Forms.CheckBox();
            this.groupBoxSQL = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBoxSQLRecordOption = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textSQLPassword = new System.Windows.Forms.TextBox();
            this.textSQLUser = new System.Windows.Forms.TextBox();
            this.textSQLServer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textStationName = new System.Windows.Forms.TextBox();
            this.buttonSaveSystemConfig = new System.Windows.Forms.Button();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tbFactoryCode = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tbLabelFileDirectory = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.tbDebugOptions = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.tbLimitsDB = new System.Windows.Forms.TextBox();
            this.textSQLSNDB = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textSQLMacDB = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textSQLProductDB = new System.Windows.Forms.TextBox();
            this.textSQLResultDB = new System.Windows.Forms.TextBox();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBoxSQL.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(408, 509);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 7;
            this.buttonCancel.Text = "Done";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.groupBox3.Controls.Add(this.textPFSWorkCenter);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.textPFSServer);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.textPFSDatabase);
            this.groupBox3.Location = new System.Drawing.Point(280, 131);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(260, 119);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "PFS";
            // 
            // textPFSWorkCenter
            // 
            this.textPFSWorkCenter.Location = new System.Drawing.Point(98, 80);
            this.textPFSWorkCenter.Name = "textPFSWorkCenter";
            this.textPFSWorkCenter.Size = new System.Drawing.Size(143, 20);
            this.textPFSWorkCenter.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(21, 80);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Work Center";
            // 
            // textPFSServer
            // 
            this.textPFSServer.Location = new System.Drawing.Point(98, 53);
            this.textPFSServer.Name = "textPFSServer";
            this.textPFSServer.Size = new System.Drawing.Size(143, 20);
            this.textPFSServer.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Server";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Database";
            // 
            // textPFSDatabase
            // 
            this.textPFSDatabase.Location = new System.Drawing.Point(98, 21);
            this.textPFSDatabase.Name = "textPFSDatabase";
            this.textPFSDatabase.Size = new System.Drawing.Size(143, 20);
            this.textPFSDatabase.TabIndex = 0;
            // 
            // textReportDirectory
            // 
            this.textReportDirectory.Location = new System.Drawing.Point(11, 31);
            this.textReportDirectory.Name = "textReportDirectory";
            this.textReportDirectory.Size = new System.Drawing.Size(171, 20);
            this.textReportDirectory.TabIndex = 0;
            this.textReportDirectory.Click += new System.EventHandler(this.textReportDirectory_Clicked);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.groupBox2.Controls.Add(this.textSFFileName);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.textSFDir);
            this.groupBox2.Location = new System.Drawing.Point(280, 23);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(260, 93);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ShopFloor System";
            // 
            // textSFFileName
            // 
            this.textSFFileName.Location = new System.Drawing.Point(98, 53);
            this.textSFFileName.Name = "textSFFileName";
            this.textSFFileName.Size = new System.Drawing.Size(143, 20);
            this.textSFFileName.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 53);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "File Name";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Directory";
            // 
            // textSFDir
            // 
            this.textSFDir.Location = new System.Drawing.Point(98, 21);
            this.textSFDir.Name = "textSFDir";
            this.textSFDir.Size = new System.Drawing.Size(143, 20);
            this.textSFDir.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.groupBox1.Controls.Add(this.tbDebugOptions);
            this.groupBox1.Controls.Add(this.cbPFSAskIfFail);
            this.groupBox1.Controls.Add(this.textBoxStepAfterFail);
            this.groupBox1.Controls.Add(this.textBoxStepAfterPass);
            this.groupBox1.Controls.Add(this.checkBoxEnableConsoleDump);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.checkBoxDebug);
            this.groupBox1.Controls.Add(this.checkBoxDisplayDUTSend);
            this.groupBox1.Controls.Add(this.checkBoxDisplayDUTRcv);
            this.groupBox1.Controls.Add(this.checkBoxEnablePFS);
            this.groupBox1.Controls.Add(this.checkBoxEnableSFS);
            this.groupBox1.Location = new System.Drawing.Point(27, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(235, 214);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Test Options";
            // 
            // cbPFSAskIfFail
            // 
            this.cbPFSAskIfFail.AutoSize = true;
            this.cbPFSAskIfFail.Location = new System.Drawing.Point(81, 161);
            this.cbPFSAskIfFail.Name = "cbPFSAskIfFail";
            this.cbPFSAskIfFail.Size = new System.Drawing.Size(119, 17);
            this.cbPFSAskIfFail.TabIndex = 34;
            this.cbPFSAskIfFail.Text = "Ask to ignore on fail";
            this.cbPFSAskIfFail.UseVisualStyleBackColor = true;
            // 
            // textBoxStepAfterFail
            // 
            this.textBoxStepAfterFail.Location = new System.Drawing.Point(8, 110);
            this.textBoxStepAfterFail.Name = "textBoxStepAfterFail";
            this.textBoxStepAfterFail.Size = new System.Drawing.Size(44, 20);
            this.textBoxStepAfterFail.TabIndex = 4;
            // 
            // textBoxStepAfterPass
            // 
            this.textBoxStepAfterPass.Location = new System.Drawing.Point(8, 86);
            this.textBoxStepAfterPass.Name = "textBoxStepAfterPass";
            this.textBoxStepAfterPass.Size = new System.Drawing.Size(44, 20);
            this.textBoxStepAfterPass.TabIndex = 3;
            // 
            // checkBoxEnableConsoleDump
            // 
            this.checkBoxEnableConsoleDump.AutoSize = true;
            this.checkBoxEnableConsoleDump.Location = new System.Drawing.Point(7, 186);
            this.checkBoxEnableConsoleDump.Name = "checkBoxEnableConsoleDump";
            this.checkBoxEnableConsoleDump.Size = new System.Drawing.Size(131, 17);
            this.checkBoxEnableConsoleDump.TabIndex = 7;
            this.checkBoxEnableConsoleDump.Text = "Enable Console Dump";
            this.checkBoxEnableConsoleDump.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(58, 110);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(110, 13);
            this.label15.TabIndex = 33;
            this.label15.Text = "Default Step After Fail";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(58, 86);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(117, 13);
            this.label14.TabIndex = 31;
            this.label14.Text = "Default Step After Pass";
            // 
            // checkBoxDebug
            // 
            this.checkBoxDebug.AutoSize = true;
            this.checkBoxDebug.Location = new System.Drawing.Point(7, 60);
            this.checkBoxDebug.Name = "checkBoxDebug";
            this.checkBoxDebug.Size = new System.Drawing.Size(88, 17);
            this.checkBoxDebug.TabIndex = 2;
            this.checkBoxDebug.Text = "Debug Mode";
            this.checkBoxDebug.UseVisualStyleBackColor = true;
            // 
            // checkBoxDisplayDUTSend
            // 
            this.checkBoxDisplayDUTSend.AutoSize = true;
            this.checkBoxDisplayDUTSend.Location = new System.Drawing.Point(7, 41);
            this.checkBoxDisplayDUTSend.Name = "checkBoxDisplayDUTSend";
            this.checkBoxDisplayDUTSend.Size = new System.Drawing.Size(114, 17);
            this.checkBoxDisplayDUTSend.TabIndex = 1;
            this.checkBoxDisplayDUTSend.Text = "Display DUT Send";
            this.checkBoxDisplayDUTSend.UseVisualStyleBackColor = true;
            // 
            // checkBoxDisplayDUTRcv
            // 
            this.checkBoxDisplayDUTRcv.AutoSize = true;
            this.checkBoxDisplayDUTRcv.Location = new System.Drawing.Point(7, 21);
            this.checkBoxDisplayDUTRcv.Name = "checkBoxDisplayDUTRcv";
            this.checkBoxDisplayDUTRcv.Size = new System.Drawing.Size(109, 17);
            this.checkBoxDisplayDUTRcv.TabIndex = 0;
            this.checkBoxDisplayDUTRcv.Text = "Display DUT Rcv";
            this.checkBoxDisplayDUTRcv.UseVisualStyleBackColor = true;
            // 
            // checkBoxEnablePFS
            // 
            this.checkBoxEnablePFS.AutoSize = true;
            this.checkBoxEnablePFS.Location = new System.Drawing.Point(6, 162);
            this.checkBoxEnablePFS.Name = "checkBoxEnablePFS";
            this.checkBoxEnablePFS.Size = new System.Drawing.Size(68, 17);
            this.checkBoxEnablePFS.TabIndex = 6;
            this.checkBoxEnablePFS.Text = "Use PFS";
            this.checkBoxEnablePFS.UseVisualStyleBackColor = true;
            // 
            // checkBoxEnableSFS
            // 
            this.checkBoxEnableSFS.AutoSize = true;
            this.checkBoxEnableSFS.Location = new System.Drawing.Point(6, 143);
            this.checkBoxEnableSFS.Name = "checkBoxEnableSFS";
            this.checkBoxEnableSFS.Size = new System.Drawing.Size(133, 17);
            this.checkBoxEnableSFS.TabIndex = 5;
            this.checkBoxEnableSFS.Text = "Use ShopFloor System";
            this.checkBoxEnableSFS.UseVisualStyleBackColor = true;
            // 
            // groupBoxSQL
            // 
            this.groupBoxSQL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.groupBoxSQL.Controls.Add(this.label17);
            this.groupBoxSQL.Controls.Add(this.label10);
            this.groupBoxSQL.Controls.Add(this.tbLimitsDB);
            this.groupBoxSQL.Controls.Add(this.textSQLSNDB);
            this.groupBoxSQL.Controls.Add(this.comboBoxSQLRecordOption);
            this.groupBoxSQL.Controls.Add(this.label21);
            this.groupBoxSQL.Controls.Add(this.label9);
            this.groupBoxSQL.Controls.Add(this.label16);
            this.groupBoxSQL.Controls.Add(this.label8);
            this.groupBoxSQL.Controls.Add(this.textSQLMacDB);
            this.groupBoxSQL.Controls.Add(this.label3);
            this.groupBoxSQL.Controls.Add(this.label6);
            this.groupBoxSQL.Controls.Add(this.textSQLPassword);
            this.groupBoxSQL.Controls.Add(this.label4);
            this.groupBoxSQL.Controls.Add(this.textSQLUser);
            this.groupBoxSQL.Controls.Add(this.textSQLProductDB);
            this.groupBoxSQL.Controls.Add(this.textSQLServer);
            this.groupBoxSQL.Controls.Add(this.textSQLResultDB);
            this.groupBoxSQL.Location = new System.Drawing.Point(33, 256);
            this.groupBoxSQL.Name = "groupBoxSQL";
            this.groupBoxSQL.Size = new System.Drawing.Size(220, 257);
            this.groupBoxSQL.TabIndex = 1;
            this.groupBoxSQL.TabStop = false;
            this.groupBoxSQL.Text = "SQL";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 227);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "DB Record option";
            // 
            // comboBoxSQLRecordOption
            // 
            this.comboBoxSQLRecordOption.FormattingEnabled = true;
            this.comboBoxSQLRecordOption.Items.AddRange(new object[] {
            "OnTheFly",
            "NONE",
            "END"});
            this.comboBoxSQLRecordOption.Location = new System.Drawing.Point(111, 223);
            this.comboBoxSQLRecordOption.Name = "comboBoxSQLRecordOption";
            this.comboBoxSQLRecordOption.Size = new System.Drawing.Size(88, 21);
            this.comboBoxSQLRecordOption.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 201);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Password";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 175);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "User";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Server";
            // 
            // textSQLPassword
            // 
            this.textSQLPassword.Location = new System.Drawing.Point(111, 197);
            this.textSQLPassword.Name = "textSQLPassword";
            this.textSQLPassword.Size = new System.Drawing.Size(100, 20);
            this.textSQLPassword.TabIndex = 6;
            // 
            // textSQLUser
            // 
            this.textSQLUser.Location = new System.Drawing.Point(111, 171);
            this.textSQLUser.Name = "textSQLUser";
            this.textSQLUser.Size = new System.Drawing.Size(100, 20);
            this.textSQLUser.TabIndex = 5;
            // 
            // textSQLServer
            // 
            this.textSQLServer.Location = new System.Drawing.Point(111, 14);
            this.textSQLServer.Name = "textSQLServer";
            this.textSQLServer.Size = new System.Drawing.Size(100, 20);
            this.textSQLServer.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "Report Directory";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Computer Name";
            // 
            // textStationName
            // 
            this.textStationName.Location = new System.Drawing.Point(11, 38);
            this.textStationName.Name = "textStationName";
            this.textStationName.ReadOnly = true;
            this.textStationName.Size = new System.Drawing.Size(141, 20);
            this.textStationName.TabIndex = 0;
            // 
            // buttonSaveSystemConfig
            // 
            this.buttonSaveSystemConfig.BackColor = System.Drawing.Color.Lime;
            this.buttonSaveSystemConfig.Location = new System.Drawing.Point(289, 508);
            this.buttonSaveSystemConfig.Name = "buttonSaveSystemConfig";
            this.buttonSaveSystemConfig.Size = new System.Drawing.Size(103, 23);
            this.buttonSaveSystemConfig.TabIndex = 6;
            this.buttonSaveSystemConfig.Text = "Save";
            this.buttonSaveSystemConfig.UseVisualStyleBackColor = false;
            this.buttonSaveSystemConfig.Click += new System.EventHandler(this.buttonSaveSystemConfig_Click);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.tbFactoryCode);
            this.groupBox4.Controls.Add(this.textStationName);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Location = new System.Drawing.Point(280, 256);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(184, 102);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Station Options";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(19, 72);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 13);
            this.label18.TabIndex = 30;
            this.label18.Text = "Factory Code";
            // 
            // tbFactoryCode
            // 
            this.tbFactoryCode.Location = new System.Drawing.Point(103, 69);
            this.tbFactoryCode.MaxLength = 1;
            this.tbFactoryCode.Name = "tbFactoryCode";
            this.tbFactoryCode.Size = new System.Drawing.Size(27, 20);
            this.tbFactoryCode.TabIndex = 1;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.tbLabelFileDirectory);
            this.groupBox5.Controls.Add(this.textReportDirectory);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Location = new System.Drawing.Point(280, 364);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(260, 136);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Directorys";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(13, 94);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(95, 13);
            this.label20.TabIndex = 38;
            this.label20.Text = "Label File directory";
            // 
            // tbLabelFileDirectory
            // 
            this.tbLabelFileDirectory.Location = new System.Drawing.Point(11, 110);
            this.tbLabelFileDirectory.Name = "tbLabelFileDirectory";
            this.tbLabelFileDirectory.Size = new System.Drawing.Size(165, 20);
            this.tbLabelFileDirectory.TabIndex = 37;
            this.tbLabelFileDirectory.Click += new System.EventHandler(this.tbLabelFileDirectory_Clicked);
            // 
            // tbDebugOptions
            // 
            this.tbDebugOptions.Location = new System.Drawing.Point(105, 58);
            this.tbDebugOptions.Name = "tbDebugOptions";
            this.tbDebugOptions.Size = new System.Drawing.Size(100, 20);
            this.tbDebugOptions.TabIndex = 35;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(16, 44);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "Limits DB";
            // 
            // tbLimitsDB
            // 
            this.tbLimitsDB.Location = new System.Drawing.Point(111, 40);
            this.tbLimitsDB.Name = "tbLimitsDB";
            this.tbLimitsDB.Size = new System.Drawing.Size(100, 20);
            this.tbLimitsDB.TabIndex = 33;
            // 
            // textSQLSNDB
            // 
            this.textSQLSNDB.Location = new System.Drawing.Point(111, 144);
            this.textSQLSNDB.Name = "textSQLSNDB";
            this.textSQLSNDB.Size = new System.Drawing.Size(100, 20);
            this.textSQLSNDB.TabIndex = 32;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(16, 151);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(40, 13);
            this.label21.TabIndex = 31;
            this.label21.Text = "SN DB";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(16, 122);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 13);
            this.label16.TabIndex = 30;
            this.label16.Text = "MAC DB";
            // 
            // textSQLMacDB
            // 
            this.textSQLMacDB.Location = new System.Drawing.Point(111, 118);
            this.textSQLMacDB.Name = "textSQLMacDB";
            this.textSQLMacDB.Size = new System.Drawing.Size(100, 20);
            this.textSQLMacDB.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "Product DB";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "Results DB";
            // 
            // textSQLProductDB
            // 
            this.textSQLProductDB.Location = new System.Drawing.Point(111, 92);
            this.textSQLProductDB.Name = "textSQLProductDB";
            this.textSQLProductDB.Size = new System.Drawing.Size(100, 20);
            this.textSQLProductDB.TabIndex = 26;
            // 
            // textSQLResultDB
            // 
            this.textSQLResultDB.Location = new System.Drawing.Point(111, 66);
            this.textSQLResultDB.Name = "textSQLResultDB";
            this.textSQLResultDB.Size = new System.Drawing.Size(100, 20);
            this.textSQLResultDB.TabIndex = 25;
            this.toolTip1.SetToolTip(this.textSQLResultDB, "debug_ will be added to begining of database if in debug mode");
            // 
            // ConfigStation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 544);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxSQL);
            this.Controls.Add(this.buttonSaveSystemConfig);
            this.Name = "ConfigStation";
            this.Text = "Station Configuration";
            this.Load += new System.EventHandler(this.StationConfigWindow_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxSQL.ResumeLayout(false);
            this.groupBoxSQL.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textPFSWorkCenter;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textPFSServer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textPFSDatabase;
        private System.Windows.Forms.TextBox textReportDirectory;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textSFFileName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textSFDir;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBoxEnablePFS;
        private System.Windows.Forms.CheckBox checkBoxEnableSFS;
        private System.Windows.Forms.GroupBox groupBoxSQL;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBoxSQLRecordOption;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textSQLPassword;
        private System.Windows.Forms.TextBox textSQLUser;
        private System.Windows.Forms.TextBox textSQLServer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textStationName;
        private System.Windows.Forms.Button buttonSaveSystemConfig;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox checkBoxDebug;
        private System.Windows.Forms.CheckBox checkBoxDisplayDUTSend;
        private System.Windows.Forms.CheckBox checkBoxDisplayDUTRcv;
        private System.Windows.Forms.CheckBox checkBoxEnableConsoleDump;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.TextBox textBoxStepAfterFail;
        private System.Windows.Forms.TextBox textBoxStepAfterPass;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbFactoryCode;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbLabelFileDirectory;
        private System.Windows.Forms.CheckBox cbPFSAskIfFail;
        private System.Windows.Forms.TextBox tbDebugOptions;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbLimitsDB;
        private System.Windows.Forms.TextBox textSQLSNDB;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textSQLMacDB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textSQLProductDB;
        private System.Windows.Forms.TextBox textSQLResultDB;
    }
}