﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO.Ports;
using System.Drawing.Printing;
using System.Management;
using System.Diagnostics;
using System.Threading;

using TestStationErrorCodes;
using FixtureInstrumentBase;

namespace SensorManufSY2
{
    public partial class Config_SU_SY2_Fixture : Form
    {
        StationConfigure lclStationConfig;
        string fixtureInUse;
        FixtureFunctions lclFixtureFunctions;

        public Config_SU_SY2_Fixture(string fixture)
        {
            InitializeComponent();
            fixtureInUse = fixture;
        }

        private void SetupForm(object sender, EventArgs e)
        {
            string outmsg = string.Empty;

            lclStationConfig = new StationConfigure();      // make emtpy def to open FixtureFunctions

            lclFixtureFunctions = new FixtureFunctions();
            outmsg = LoadFormData(outmsg);
        }

        private string LoadFormData(string outmsg)
        {
            int index;
            FixtureInstrument.fixtureTypes thisFixtureList = new FixtureInstrument.fixtureTypes();

            if (lclStationConfig.ReadStationConfigFile(this.fixtureInUse, ref outmsg) != 0)
            {
                MessageBox.Show(outmsg);
                return outmsg;
            }

            index = lclFixtureFunctions.fixtureTypeList.FindIndex(x => x.fixtureType == lclStationConfig.ConfigFixtureType);
            if (index > lclFixtureFunctions.fixtureTypeList.Count)       // if did not find it
            {
                outmsg = "Fixture code " + lclStationConfig.ConfigFixtureType;
                MessageBox.Show(outmsg);
                return outmsg;
            }
            thisFixtureList = lclFixtureFunctions.fixtureTypeList[index];

            // these are not changable by the user
            lbFixtureType.Text = lclStationConfig.ConfigFixtureType;    // hardware fixture type
            lbStationDB.Text = lclStationConfig.ConfigStationToUse;     // database station to use
            lbStationSection.Text = lclStationConfig.ConfigFixtureName; // Fixture name

            // used by all stations
            textBoxTSStationTitle.Text = lclStationConfig.StationTitle;
            textBoxTSStationOperation.Text = lclStationConfig.ConfigStationOperation;

            // dut console port
            if (thisFixtureList.hasConsolePort)
            {
                comboBoxTSDUTComPort.Text = lclStationConfig.ConfigDUTComPort;
                comboBoxTSDUTComPort.Visible = true;
            }
            else
            {
                comboBoxTSDUTComPort.Visible = false;
            }

            // test radio
            if (thisFixtureList.hasTestRadio)
            {
                numericUpDownTSTestChannel.Value = Convert.ToInt32(lclStationConfig.ConfigTestRadioChannel);
                comboBoxTSTestRadioComPort.Text = lclStationConfig.ConfigTestRadioComPort;
                numericUpDownTSTestChannel.Visible = true;
                comboBoxTSTestRadioComPort.Visible = true;
                bAutoFindTestRadioComPort.Visible = true;
            }
            else
            {
                numericUpDownTSTestChannel.Visible = false;
                comboBoxTSTestRadioComPort.Visible = false;
                bAutoFindTestRadioComPort.Visible = false;
            }

            // Flasher
            if (thisFixtureList.hasFlasher)
            {
                cbFlasherComPort.Text = lclStationConfig.ConfigFlasherComPort;
                cbFlasherComPort.Visible = true;
            }
            else
            {
                cbFlasherComPort.Visible = false;
                bAutoFindFlasher.Visible = false;
            }


            // Switch-Mate
            cbSwitchMateCom.Visible = false;
            bAutoFindSMComPort.Visible = false;
            if (thisFixtureList.channelDefs != null)
            {
                if (thisFixtureList.channelDefs.Exists(x => x.instrumentID == FixtureInstrument.InstrumentID.SwitchMate))
                {
                    cbSwitchMateCom.Text = lclStationConfig.ConfigSwitchMateComPort;
                    cbSwitchMateCom.Visible = true;
                    bAutoFindSMComPort.Visible = true;
                }
            }

            // printers
            if (thisFixtureList.hasPCBAPrinter)
            {
                comboBoxTSPCBAPrinter.Text = lclStationConfig.ConfigPCBALabelPrinter;
                comboBoxTSPCBAPrinter.Visible = true;
            }
            else
            {
                comboBoxTSPCBAPrinter.Visible = false;
            }

            if (thisFixtureList.hasHLAPrinter)
            {
                comboBoxTSProductPrinter.Text = lclStationConfig.ConfigProductLabelPrinter;
                comboBoxTSProductPrinter.Visible = true;
            }
            else
            {
                comboBoxTSProductPrinter.Visible = false;
            }

            // LED detector
            comboBoxTSLEDDetectorComPort.Visible = false;
            bAutofindLEDComPort.Visible = false;
            textBoxTSRedRange.Visible = false;
            textBoxTSGreenRange.Visible = false;
            textBoxTSBlueRange.Visible = false;
            if (thisFixtureList.channelDefs != null)
            {
                if (thisFixtureList.channelDefs.Exists(x => x.instrumentID == FixtureInstrument.InstrumentID.Optomistic))
                {
                    comboBoxTSLEDDetectorComPort.Text = lclStationConfig.ConfigLedDetector.ComPort;
                    comboBoxTSLEDDetectorComPort.Visible = true;
                    bAutofindLEDComPort.Visible = true;
                    textBoxTSRedRange.Text = lclStationConfig.ConfigLedDetector.ColorRED;
                    textBoxTSGreenRange.Text = lclStationConfig.ConfigLedDetector.ColorGREEN;
                    textBoxTSBlueRange.Text = lclStationConfig.ConfigLedDetector.ColorBLUE;
                    textBoxTSRedRange.Visible = true;
                    textBoxTSGreenRange.Visible = true;
                    textBoxTSBlueRange.Visible = true;
                }
            }

            // used by Sensor type stations
            comboBoxTSTempProbe.Visible = false;
            if (thisFixtureList.channelDefs != null)
            {
                if (thisFixtureList.channelDefs.Exists(x => x.name == FixtureFunctions.CH_TEMPERTURE_PROBE))
                {
                    comboBoxTSTempProbe.Text = lclStationConfig.ConfigTempSensorOutput;
                    comboBoxTSTempProbe.Visible = true;
                }
            }

            // if enlighted interface, it has control voltage
            nudControlVoltageGain.Visible = false;
            if (thisFixtureList.channelDefs != null)
            {
                if (thisFixtureList.channelDefs.Exists(x => x.name == FixtureFunctions.CH_DIM1_OUTPUT))
                {
                    nudControlVoltageGain.Value = Convert.ToDecimal(lclStationConfig.ConfigLjChannelControlVGain);
                    nudControlVoltageGain.Visible = true;
                }
            }

            // Sensors with ambient sensor
            numericUpDownTSAmbDLSLEDV.Visible = false;
            numericUpDownTSAmbDLSOnDelay.Visible = false;
            numericUpDownTSAmbDLSOffDelay.Visible = false;
            if (thisFixtureList.channelDefs != null)
            {
                if (thisFixtureList.channelDefs.Exists(x => x.name == FixtureFunctions.CH_AMBIENT_DETECTOR_LED))
                {
                    numericUpDownTSAmbDLSLEDV.Text = lclStationConfig.ConfigAmbientDetectorLightSource.Voltage.ToString("N3");
                    numericUpDownTSAmbDLSLEDV.Visible = true;
                    numericUpDownTSAmbDLSOnDelay.Value = lclStationConfig.ConfigAmbientDetectorLightSource.OnDelayMs;
                    numericUpDownTSAmbDLSOffDelay.Value = lclStationConfig.ConfigAmbientDetectorLightSource.OffDelayMs;
                    numericUpDownTSAmbDLSOnDelay.Visible = true;
                    numericUpDownTSAmbDLSOffDelay.Visible = true;
                }
            }

            // sensors with PIR sensor
            numericUpDownTSPIRDLSOnDelay.Visible = false;
            numericUpDownTSPIRDLSOffDelay.Visible = false;
            if (thisFixtureList.channelDefs != null)
            {
                if (thisFixtureList.channelDefs.Exists(x => x.name == FixtureFunctions.CH_PIR_SOURCE_CNTL))
                {
                    numericUpDownTSPIRDLSOnDelay.Value = lclStationConfig.ConfigPIRDetectorLightSource.OnDelayMs;
                    numericUpDownTSPIRDLSOffDelay.Value = lclStationConfig.ConfigPIRDetectorLightSource.OffDelayMs;
                    numericUpDownTSPIRDLSOnDelay.Visible = true;
                    numericUpDownTSPIRDLSOffDelay.Visible = true;
                }
            }

            return outmsg;
        }

        private void FixtureToConfig_SelectedIndexChanged(object sender, EventArgs e)
        {
            string outmsg = string.Empty;

            LoadFormData(outmsg);
        }

        // save button
        private void button1_Click(object sender, EventArgs e)
        {

            lclStationConfig.StationTitle = textBoxTSStationTitle.Text;
            lclStationConfig.ConfigStationOperation = textBoxTSStationOperation.Text;
            lclStationConfig.ConfigTempSensorOutput = comboBoxTSTempProbe.Text;

            lclStationConfig.ConfigDUTComPort = comboBoxTSDUTComPort.Text;

            lclStationConfig.ConfigTestRadioComPort = comboBoxTSTestRadioComPort.Text;
            lclStationConfig.ConfigTestRadioChannel = numericUpDownTSTestChannel.Value.ToString();

            lclStationConfig.ConfigSwitchMateComPort = cbSwitchMateCom.Text;
            lclStationConfig.ConfigFlasherComPort = cbFlasherComPort.Text;

            lclStationConfig.ConfigLjChannelControlVGain = Convert.ToDouble(nudControlVoltageGain.Value);

            lclStationConfig.ConfigPCBALabelPrinter = comboBoxTSPCBAPrinter.Text;
            lclStationConfig.ConfigProductLabelPrinter = comboBoxTSProductPrinter.Text;

            lclStationConfig.ConfigLedDetector.ComPort = comboBoxTSLEDDetectorComPort.Text;
            lclStationConfig.ConfigLedDetector.ColorRED = textBoxTSRedRange.Text;
            lclStationConfig.ConfigLedDetector.ColorGREEN = textBoxTSGreenRange.Text;
            lclStationConfig.ConfigLedDetector.ColorBLUE = textBoxTSBlueRange.Text;

            lclStationConfig.ConfigAmbientDetectorLightSource.Voltage = Convert.ToDouble(numericUpDownTSAmbDLSLEDV.Value);
            lclStationConfig.ConfigAmbientDetectorLightSource.OnDelayMs = Convert.ToInt32(numericUpDownTSAmbDLSOnDelay.Value);
            lclStationConfig.ConfigAmbientDetectorLightSource.OffDelayMs = Convert.ToInt32(numericUpDownTSAmbDLSOffDelay.Value);

            lclStationConfig.ConfigPIRDetectorLightSource.OnDelayMs = Convert.ToInt32(numericUpDownTSPIRDLSOnDelay.Value);
            lclStationConfig.ConfigPIRDetectorLightSource.OffDelayMs = Convert.ToInt32(numericUpDownTSPIRDLSOffDelay.Value);

            lclStationConfig.UpdateFixtureData(fixtureInUse);
        }


        private void DutComPort_DropDown(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            ComboBox mybutton = sender as ComboBox;

            mybutton.Items.Clear();
            //foreach (var item in ports)
            //{
            //    mybutton.Items.Add(item.ToString());
            //}

            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_PnPEntity");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    if (queryObj["Caption"].ToString().Contains("(COM"))
                    {
                        mybutton.Items.Add( queryObj["Caption"]);
                    }

                }
            }
            catch (ManagementException exx)
            {
                MessageBox.Show(exx.Message);
            }
        }

        private void buttonFindBarcodeFile_Click(object sender, EventArgs e)
        {

            PrintDocument prtdoc = new PrintDocument();
            ComboBox myprinter = sender as ComboBox;

            //prt.PrinterSettings.PrinterName returns the name of the Default Printer
            string strDefaultPrinter = prtdoc.PrinterSettings.PrinterName;

            //this will loop through all the Installed printers and add the Printer Names to a ComboBox.
            foreach (String strPrinter in PrinterSettings.InstalledPrinters)
            {
                myprinter.Items.Add(strPrinter);

                //This will set the ComboBox Index where the Default Printer Name matches with the current Printer Name returned by for loop
                if (strPrinter.CompareTo(strDefaultPrinter) == 0)
                {
                    myprinter.SelectedIndex = myprinter.Items.IndexOf(strPrinter);
                }
            }

        }

        private void comboBoxTSDUTComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            int start = 0;
            StationConfigure myStationConfig;
             ComboBox mybutton = sender as ComboBox;


            using (FixtureFunctions fixture = new FixtureFunctions())
            {
                myStationConfig = lclStationConfig;                         // make a scratch copy for the init
                myStationConfig.FixtureEquipment.hasConsolePort = false;    // do not init console
                myStationConfig.FixtureEquipment.hasTestRadio = false;      // do not init the test radio
                myStationConfig.FixtureEquipment.hasBLE = false;            // do not init the tag radio
                if (cbSwitchMateCom.Text == "COM0")      // if the switch-mate has not been config
                {
                    MessageBox.Show("Switch-Mate must be configured first");
                    comboBoxTSDUTComPort.BackColor = Color.White;
                    mybutton.Text = "Auto Find";
                    return;
                }
                myStationConfig.ConfigSwitchMateComPort = cbSwitchMateCom.Text;     // load current switch mate port
                fixture.StationConfigure = myStationConfig;
                if (!fixture.InitFixture())
                {
                    MessageBox.Show("Error initializing the fixture. " + fixture.LastFunctionErrorMessage);
                }
                else
                {
                    fixture.SetOutput("DUT_POWER_CNTL", (double)FixtureFunctions.PowerControl.ON);
                    //fixture.ControlDUTPower(FixtureFunctions.PowerControl.ON);         // turn on DUT
                    start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
                    temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
                    mybutton.Items.Clear();
                    mybutton.Items.Add(temp);
                    mybutton.SelectedIndex = 0;
                    fixture.SetOutput("DUT_POWER_CNTL", (double)FixtureFunctions.PowerControl.OFF);
                    //fixture.ControlDUTPower(FixtureFunctions.PowerControl.OFF);         // turn off DUT
                }
            }
        }

        private void comboBoxTSSwitchMateComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            int start = 0;
            SerialPort smport = new SerialPort();

            ComboBox mybutton = sender as ComboBox;
            start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
            temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
            mybutton.Items.Clear();
            mybutton.Items.Add(temp);
            mybutton.SelectedIndex = 0;

            smport.BaudRate = 19200;
            smport.PortName = temp;
            smport.NewLine = "\r";
            smport.Open();
            smport.WriteLine("SM_ID?");
            smport.ReadTimeout = 1000;
            try
            {
                if (!(temp = smport.ReadLine()).Contains("SWITCH-MATE"))
                {
                    MessageBox.Show("Not the Switch-Mate port. Found: " + temp + "\n");
                    mybutton.BackColor = Color.Red;
                }
                else
                { 
                    MessageBox.Show("Correct port");
                    mybutton.BackColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Not the Switch-Mate port. Error: " + ex.Message);
                mybutton.BackColor = Color.Red;
            }
            smport.Close();
        }

        private void cbRefRadioComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            int start = 0;
            SerialPort smport = new SerialPort();

            ComboBox mybutton = sender as ComboBox;
            start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
            temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
            mybutton.Items.Clear();
            mybutton.Items.Add(temp);
            mybutton.SelectedIndex = 0;

            smport.BaudRate = 115200;
            smport.PortName = temp;
            smport.ReadTimeout = 1000;
            smport.Open();
            try
            {
                smport.WriteLine("d v");
                smport.ReadLine();          // get the echo
            }
            catch (Exception ex)
            {
                MessageBox.Show("Not the Ref Radio port. Error: " + ex.Message);
                mybutton.BackColor = Color.Red;
                return;
            }
            try
            {
                if (!(temp = smport.ReadLine()).Contains("Running"))
                {
                    MessageBox.Show("Not the Ref Radio port. Found: " + temp + "\n");
                    mybutton.BackColor = Color.Red;
                }
                else
                {
                    MessageBox.Show("Correct port");
                    mybutton.BackColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Not the Ref Radio port. Error: " + ex.Message);
                mybutton.BackColor = Color.Red;
            }
            smport.Close();
        }

        private void cbFlasherComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            int start = 0;

            ComboBox mybutton = sender as ComboBox;
            start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
            temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
            mybutton.Items.Clear();
            mybutton.Items.Add(temp);
            mybutton.SelectedIndex = 0;

            Flasher flasher = new Flasher(temp);
            flasher.Open();
            temp = flasher.GetStatus();
            if (!flasher.LastErrorStatus)        // if got some valid responses
            {
                MessageBox.Show("Correct port");
                mybutton.BackColor = Color.Green;
            }
            else
            {
                MessageBox.Show("Not the Flasher port. Error");
                mybutton.BackColor = Color.Red;
            }
            flasher.Close();
        }


        private void cbLEDSensorComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            int start = 0;
            SerialPort ledport = new SerialPort();

            ComboBox mybutton = sender as ComboBox;
            start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
            temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
            mybutton.Items.Clear();
            mybutton.Items.Add(temp);
            mybutton.SelectedIndex = 0;

            ledport.BaudRate = 19200;
            ledport.PortName = temp;
            ledport.ReadTimeout = 1000;
            ledport.Open();
            try
            {
                ledport.ReadLine();     // get a couple of lines
                ledport.ReadLine();     // get a couple of lines
            }
            catch (Exception)
            {
                MessageBox.Show("Not the LED Detector port.");
                ledport.Close();
                mybutton.BackColor = Color.Red;
                return;
            }

            try
            {
                if (!(temp = ledport.ReadLine()).Contains("w="))
                {
                    MessageBox.Show("Not the LED Detector port. Found: " + temp + "\n");
                    mybutton.BackColor = Color.Red;
                }
                else
                { 
                    MessageBox.Show("Correct port");
                    mybutton.BackColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Not the LED Detector port. Error: " + ex.Message);
                mybutton.BackColor = Color.Red;
            }
            ledport.Close();
        }




        private void comboBoxTSLEDDetectorType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox mybutton = sender as ComboBox;
            if (mybutton.Text == "USB")
            {
                comboBoxTSLEDDetectorComPort.Visible = true;
                textBoxTSRedRange.Text = "6200,6600";
                textBoxTSGreenRange.Text = "5400,5900";
                textBoxTSBlueRange.Text = "4200,4900";
            }
            else
            {
                comboBoxTSLEDDetectorComPort.Visible = false;
                textBoxTSRedRange.Text = "11000,13000";
                textBoxTSGreenRange.Text = "7000,9000";
                textBoxTSBlueRange.Text = "4200,4900";
            }
        }

        private void comboBoxTSAmbDLSType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox mybutton = sender as ComboBox;
            if (mybutton.Text == "RedLED")
                numericUpDownTSAmbDLSLEDV.Visible = true;
            else
                numericUpDownTSAmbDLSLEDV.Visible = false;

        }

        /// <summary>
        /// process the CU type change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxTSCUType_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox mybutton = sender as ComboBox;
            if (mybutton.Text == "Enlighted")
            {
                nudControlVoltageGain.Visible = true;
            }
            else
            {
                nudControlVoltageGain.Visible = false;
            }
        }




        private void bAutofindRefRadio_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            string temp = string.Empty;
            bool willOpen = true;
            bool tookCmd = false;
            bool foundPort = false;
            string partno = string.Empty;
            string serialno = string.Empty;
            string model = string.Empty;

            PacketBasedConsole radio = new PacketBasedConsole();
            radio.CommandRecieveTimeoutMS = 1000;
            Button mybutton = sender as Button;
            mybutton.Text = "Looking";
            mybutton.BackColor = Color.Yellow;
            Application.DoEvents();
            foreach (var item in ports)
            {
                try // see if it will open
                {
                    willOpen = true;
                    radio.PortName = item;
                    radio.Open();       // try to open the port
                }
                catch (Exception)
                {
                    temp = radio.LastErrorMessage;
                    willOpen = false;
                }
                if (willOpen)  // if it will open, see if it will take the command
                {
                    try
                    {
                        radio.ClearBuffers();
                        tookCmd = radio.GetHLAData(ref partno, ref serialno, ref model);
                    }
                    catch (Exception)
                    {
                        tookCmd = false;
                    }
                    radio.Close();
                }
                if (willOpen & tookCmd) // if it opened and it took the command, check result
                {
                    try
                    {
                        if (model.Contains("CK-1U-01"))
                        {
                            foundPort = true;
                        }
                    }
                    catch (Exception)
                    {
                        foundPort = false;
                    }
                }
                if (foundPort)  // if the port was found
                {
                    comboBoxTSTestRadioComPort.Items.Clear();
                    comboBoxTSTestRadioComPort.Items.Add(item);
                    comboBoxTSTestRadioComPort.SelectedIndex = 0;
                    comboBoxTSTestRadioComPort.BackColor = Color.Green;

                    break;
                }
            }
            mybutton.Text = "Auto Find";
            mybutton.UseVisualStyleBackColor = true;
            if (!foundPort)
            {
                comboBoxTSTestRadioComPort.BackColor = Color.Red;
                comboBoxTSTestRadioComPort.Text = "COM0";
            }

        }

        private void bAutofindFlasher_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            string temp = string.Empty;
            bool willOpen = true;
            bool foundPort = false;
            string partno = string.Empty;
            string serialno = string.Empty;
            string model = string.Empty;
            Flasher flasher = new Flasher();

            Button mybutton = sender as Button;
            mybutton.Text = "Looking";
            mybutton.BackColor = Color.Yellow;
            Application.DoEvents();
            foreach (var item in ports)
            {
                try // see if it will open
                {
                    willOpen = true;
                    flasher.Open(item);       // try to open the port
                }
                catch (Exception)
                {
                    temp = flasher.LastErrorMsg;
                    willOpen = false;
                }
                if (willOpen)  // if it will open, see if it will take the command
                {
                    flasher.ClearBuffers();
                    flasher.GetStatus();
                    foundPort = flasher.LastErrorStatus;
                    flasher.Close();
                }
                if (foundPort)  // if the port was found
                {
                    cbFlasherComPort.Items.Clear();
                    cbFlasherComPort.Items.Add(item);
                    cbFlasherComPort.SelectedIndex = 0;
                    cbFlasherComPort.BackColor = Color.Green;
                    mybutton.UseVisualStyleBackColor = true;
                    break;
                }
            }
            mybutton.Text = "Auto Find";
            mybutton.UseVisualStyleBackColor = true;
            if (!foundPort)
            {
                cbFlasherComPort.BackColor = Color.Red;
                cbFlasherComPort.Text = "COM0";
            }

        }


        /// <summary>
        /// Will try to auto find the CU port.
        /// </summary>
        /// <remarks>
        /// Assumes that there is a working DUT running DVTMAN in the fixture and console
        /// has been configured.
        /// Will turn on the unit
        ///     for each com port other than the console port
        ///         open the port as a CU port
        ///         do a p cu-test
        ///         if it passes, assume it is the correct port
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void autofindPM_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            //SensorPacketConsole dconsole = new SensorPacketConsole();
            //CUConsole cconsole = new CUConsole();
            //SensorDVTMANTests lclmanTests = new SensorDVTMANTests();
            //MainWindow.uutdata uutdata = new MainWindow.uutdata();
            //string msg = string.Empty;
            //LimitFileFunctions.TestToRun testToRun = new LimitFileFunctions.TestToRun();
            //string port = "COM0";
            //int status;

            //comboBoxTSCUComPort.BackColor = Color.Red;
            //Button mybutton = sender as Button;
            //mybutton.Text = "Looking";
            //Application.DoEvents();
            //if (lclStationConfig.ConfigDUTComPort == "COM0")        // if dut com not configured
            //{
            //    MessageBox.Show("DUT Com Port must be configured first.");
            //    return;
            //}
            //lclFixtureFunctions.ControlDUTPower(1);             // power up DUT
            //dconsole.Open(lclStationConfig.ConfigDUTComPort);
            //dconsole.ClearBuffers();
            //foreach (var item in ports)
            //{
            //    if (item != lclStationConfig.ConfigDUTComPort)
            //    {
            //        cconsole.Open(item);
            //        status = lclmanTests.CUPortTest(testToRun, null, uutdata, out msg);
            //        cconsole.Close();
            //        if (status == 0)
            //        {
            //            port = item;
            //            break;
            //        }
            //    }
            //}
            
        }

        private void autofindSM_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            string temp = string.Empty;
            bool foundPort = false;
            SerialPort smport = new SerialPort();

            Button mybutton = sender as Button;
            mybutton.Text = "Looking";
            mybutton.BackColor = Color.Yellow;
            Application.DoEvents();
            foreach (var item in ports)
            {
                smport.BaudRate = 19200;
                smport.PortName = item;
                smport.NewLine = "\r";
                smport.ReadTimeout = 1000;
                smport.Open();
                smport.WriteLine("");               // need to clear any old stuff
                try
                {
                    smport.ReadLine();
                }
                catch (Exception)
                {
                    temp = string.Empty;
                }
                smport.WriteLine("SM_ID?");
                try
                {
                    if ((temp = smport.ReadLine()).Contains("SWITCH-MATE"))
                    {
                        cbSwitchMateCom.Items.Clear();
                        cbSwitchMateCom.Items.Add(item);
                        cbSwitchMateCom.SelectedIndex = 0;
                        smport.Close();
                        foundPort = true;
                        break;
                    }
                }
                catch (Exception)
                {
                    foundPort = false;
                }
                smport.Close();
            }
            mybutton.Text = "Auto Find";
            mybutton.UseVisualStyleBackColor = true;
            if (foundPort)
                cbSwitchMateCom.BackColor = Color.Green;
            else
            {
                cbSwitchMateCom.BackColor = Color.Red;
                cbFlasherComPort.Text = "COM0";
            }
        }

        private void autofindLED_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            string temp = string.Empty;
            bool willOpen = true;
            bool tookCmd = false;

            SerialPort ledport = new SerialPort();

            Button mybutton = sender as Button;
            mybutton.Text = "Looking";
            mybutton.BackColor = Color.Yellow;

            Application.DoEvents();
            foreach (var item in ports)
            {
                temp = string.Empty;
                try // see if it will open
                {
                    willOpen = true;
                    ledport.BaudRate = 19200;
                    ledport.PortName = item;
                    ledport.ReadTimeout = 1000;
                    ledport.Open();
                }
                catch (Exception ex)
                {
                    temp = ex.Message;
                    willOpen = false;
                }
                if (willOpen)  // if it will open, see if it will read values
                {
                    try
                    {
                        tookCmd = true;
                        ledport.ReadLine();     // get a couple of lines
                        ledport.ReadLine();
                        temp = ledport.ReadLine();     // get a couple of lines
                    }
                    catch (Exception ex)
                    {
                        temp = ex.Message;
                        ledport.Close();
                        tookCmd = false;
                    }

                }

                if (willOpen & tookCmd & temp.Contains("w="))  // if the port was found
                {
                    comboBoxTSLEDDetectorComPort.Items.Clear();
                    comboBoxTSLEDDetectorComPort.Items.Add(item);
                    comboBoxTSLEDDetectorComPort.SelectedIndex = 0;
                    comboBoxTSLEDDetectorComPort.BackColor = Color.Green;
                    ledport.Close();
                    break;
                }
                if (ledport.IsOpen)
                    ledport.Close();
            }
            mybutton.Text = "Auto Find";
            mybutton.UseVisualStyleBackColor = true;
            if (comboBoxTSLEDDetectorComPort.BackColor != Color.Green)
            {
                comboBoxTSLEDDetectorComPort.BackColor = Color.Red;
                comboBoxTSLEDDetectorComPort.Text = "COM0";
            }
        }

        /// <summary>
        /// Look for a DUT that is running DVTMAN.
        /// </summary>
        /// <remarks>
        /// It will look for something that response to a get HLA data command. If
        /// it finds one, if it does not have "CK-1U-01" then assumed that it is the DUT.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bAutoFindDUTComPort_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            string temp = string.Empty;
            bool foundPort = false;
            PacketBasedConsole radio = new PacketBasedConsole();
            StationConfigure myStationConfig;
            bool willOpen = true;
            bool tookCmd = true;
            string partno = string.Empty;
            string model = string.Empty;
            string serialno = string.Empty;

            Button mybutton = sender as Button;

            MessageBox.Show("DUT running DVTMAN has to be loaded into the fixture.");
            Application.DoEvents();
            using (FixtureFunctions fixture = new FixtureFunctions())
            {
                myStationConfig = lclStationConfig;                         // make a scratch copy for the init
                myStationConfig.FixtureEquipment.hasConsolePort = false;    // do not init console
                myStationConfig.FixtureEquipment.hasTestRadio = false;      // do not init the test radio
                myStationConfig.FixtureEquipment.hasBLE = false;            // do not init the ble radios
                if (cbSwitchMateCom.Text  == "COM0")      // if the switch-mate has not been config
                {
                    MessageBox.Show("Switch-Mate must be configured first");
                    mybutton.Text = "Auto Find";
                    mybutton.UseVisualStyleBackColor = true;
                    return;
                }
                myStationConfig.ConfigSwitchMateComPort = cbSwitchMateCom.Text;     // load current switch mate port
                fixture.StationConfigure = myStationConfig;
                if (!fixture.InitFixture())
                {
                    MessageBox.Show("Error initializing fixture. " + fixture.LastFunctionErrorMessage);
                }
                else
                {
                    fixture.ControlDUTPower(FixtureFunctions.PowerControl.ON, 100);
                    mybutton.Text = "Looking";
                    mybutton.BackColor = Color.Yellow;
                    Application.DoEvents();

                    foreach (var item in ports)
                    {
                        try // see if it will open
                        {
                            willOpen = true;
                            radio.PortName = item;
                            radio.Open();       // try to open the port
                        }
                        catch (Exception)
                        {
                            temp = radio.LastErrorMessage;
                            willOpen = false;
                        }
                        if (willOpen)  // if it will open, see if it will take the command
                        {
                            try
                            {
                                radio.ClearBuffers();
                                tookCmd = radio.GetHLAData(ref partno, ref serialno, ref model);
                            }
                            catch (Exception)
                            {
                                tookCmd = false;
                            }
                            radio.Close();
                        }
                        if (willOpen & tookCmd) // if it opened and it took the command, check result
                        {
                            try
                            {
                                if (!model.Contains("CK-1U-01"))
                                {
                                    foundPort = true;
                                }
                            }
                            catch (Exception)
                            {
                                foundPort = false;
                            }
                        }
                        if (foundPort)  // if the port was found
                        {
                            comboBoxTSDUTComPort.Items.Clear();
                            comboBoxTSDUTComPort.Items.Add(item);
                            comboBoxTSDUTComPort.SelectedIndex = 0;
                            comboBoxTSDUTComPort.BackColor = Color.Green;
                            break;
                        }
                    }
                    mybutton.Text = "Auto Find";
                    mybutton.UseVisualStyleBackColor = true;
                    if (comboBoxTSDUTComPort.BackColor != Color.Green)
                    {
                        comboBoxTSDUTComPort.BackColor = Color.Red;
                        comboBoxTSDUTComPort.Text = "COM0";
                    }
                    fixture.ControlDUTPower(FixtureFunctions.PowerControl.OFF, 100);
                }
            }

        }

    }
}
