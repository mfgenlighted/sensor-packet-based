﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixtureInstrumentBase;
using System.IO.Ports;
using System.Text.RegularExpressions;

namespace SensorManufSY2
{
    class OptomisticInstrument : FixtureInstrument, IFixtureInstrument, IDisposable
    {
        string comPort = string.Empty;
        SerialPort serialPort = null;
        private string serialBuffer = string.Empty;      // buffer of incoming serial data
        private string readBuffer = string.Empty;        // buffer read on one event

        bool disposed = true;

        //--------------------------------
        // properties
        //----------------------------------
        public int wavelength = 0;
        public int intensity = 0;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if ((serialPort != null) & (serialPort.IsOpen))
                    {
                        serialPort.Close();
                        serialPort.Dispose();
                        disposed = true;
                    }
                }
            }
        }

        public OptomisticInstrument(string port)
        {
            comPort = port;
        }

        public bool AddChannel(string name, int channel, ChannelType type, int range)
        {
            throw new NotImplementedException();
        }

        public bool Connect()
        {
            if (serialPort != null)                        // if already opne
            {
                serialPort.Close();                         // close it
                serialPort.Dispose();                       // and get rid of it
                this.comPort = string.Empty;
            }


            if (CheckForSerialPort(comPort))
            {
                // port valid on this system, so try to open
                try
                {
                    serialPort = new SerialPort(comPort, 19200);
                    serialPort.DtrEnable = false;
                    serialPort.Handshake = Handshake.None;
                    serialPort.Parity = Parity.None;
                    serialPort.DataReceived += new SerialDataReceivedEventHandler(serialDataReceived);
                    serialPort.ReadTimeout = 100;
                    serialPort.Open();           // open the serial port
                    disposed = false;
                }
                catch (Exception ex)
                {
                    LastFunctionErrorMessage = "Error opening " + comPort + " : " + ex.Message;
                    LastFunctionStatus = InsturmentError_CouldNotConnect;
                    return false;
                }
            }
            else
            {
                LastFunctionErrorMessage = "LED Detector serial port " + comPort + " is not a valid com port on this computer";
                LastFunctionStatus = InsturmentError_CouldNotConnect;
                return false;
            }
            return true;
        }

        public void Disconnect()
        {
            if (serialPort != null)                        // if already opne
            {
                serialPort.Close();                         // close it
                serialPort.Dispose();                       // and get rid of it
                this.comPort = string.Empty;
            }
        }

        public double ReadChannel(string channelName)
        {
            wavelength = 0;
            intensity = 0;

            if (serialPort == null)     // if it has not be defined
            {
                LastFunctionErrorMessage = "Serial port is not open";
                LastFunctionStatus = InstrumentError_NotConnected;
            }

            if (!serialPort.IsOpen)
            {
                LastFunctionErrorMessage = "Serial port is not open";
                LastFunctionStatus = InstrumentError_NotConnected;
            }
            return 0;
        }

        public void ReadListOfChannels(ref Channel[] channelNames)
        {
            throw new NotImplementedException();
        }

        public void SetChannel(string channelName, double value)
        {
            throw new NotImplementedException();
        }

        //---------------------
        //  utilities
        //-------------------
        private bool CheckForSerialPort(string comPort)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            // see if this port is in the list of ports on the computer
            foreach (string i in ports)
            {
                if (i.Equals(comPort))         // if found a match
                    return true;
            }
            return false;
        }

        static readonly object serialRcvLock = new object();      // setup so can lock code below
        //-------------------------------------
        // Method: DutSerialDataReceived
        //  StatusUpdated handler for the dut serial port
        //
        void serialDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string msg = string.Empty;
            char[] delimiterChars = { ' ', '=', '\r', '\n' };
            Match m;
            SerialPort sp = (SerialPort)sender;
            string smsg = string.Empty;
            lock (serialRcvLock)
            {
                if (sp.IsOpen)
                {
                    try
                    {
                        wavelength = 0;
                        intensity = 0;
                        msg = sp.ReadLine();
                        m = Regex.Match(msg, @"w=(\d+)");
                        if (m.Success)
                            int.TryParse(m.Value.Substring(2), out wavelength);
                        m = Regex.Match(msg, @"i=(\d+)");
                        if (m.Success)
                            int.TryParse(m.Value.Substring(2), out intensity);
                    }
                    catch (Exception)
                    {
                        return;
                    }
                }
            }
        }

    }
}
