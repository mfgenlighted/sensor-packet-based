﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Enlighted Manufacture Test")]
[assembly: AssemblyDescription("Manufacture test for Enlighted SU-5 sensors")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Enlighted")]
[assembly: AssemblyProduct("SensorTestV5")]
[assembly: AssemblyCopyright("Copyright © Enlighted Inc 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ef6e8803-0df4-4b33-9aea-271f643ea596")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("5.7.3.*")]
[assembly: AssemblyFileVersion("5.7.3.0")]

// Resource stuff
[assembly: NeutralResourcesLanguageAttribute("en")]