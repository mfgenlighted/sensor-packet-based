﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorManufSY2
{
    public partial class ConfigFixtures : Form
    {
        StationConfigure stationConfig;
        string fixtureInUse;

        

        public ConfigFixtures(string fixture)
        {
            InitializeComponent();
            fixtureInUse = fixture;

        }

        private void SetupForm(object sender, EventArgs e)
        {
            //string outmsg = string.Empty;
            //List<string> stationTypes = new List<string>();

            //stationConfig = new StationConfigure();
            //stationConfig.ReadStationTypes(ref stationTypes);

            //int index = 0;
            //int fixtureIndex = 0;
            //foreach (var item in stationTypes)
            //{
            //    this.tbFixtureToConfig.Items.Add(item);
            //    if (item == fixtureInUse)
            //        fixtureIndex = index;
            //    index++;
            //}
            //this.tbFixtureToConfig.SelectedIndex = fixtureIndex;
        }


        private void bEdit_Click(object sender, EventArgs e)
        {
            string outmsg = string.Empty;
            stationConfig = new StationConfigure();

            if (stationConfig.ReadStationConfigFile(fixtureInUse, ref outmsg) != 0)
            {
                MessageBox.Show(outmsg);
            }

            Config_SU_SY2_Fixture fconf = new Config_SU_SY2_Fixture(fixtureInUse);
            fconf.ShowDialog();
            fconf.Dispose();

        }

        /// <summary>
        /// will make a copy of the current fixture.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bCopy_Click(object sender, EventArgs e)
        {
            string outmsg = string.Empty;
            string fixtureType = string.Empty;
            string newFixtureName = string.Empty;
            List<string> stationTypes = new List<string>();
            List<string> stationNames = new List<string>();
            List<string> fixtureTypes = new List<string>();
            List<string> fixtureNames = new List<string>();
            List<string> stations = new List<string>();
            DialogResult yn = new System.Windows.Forms.DialogResult();

            stationConfig = new StationConfigure();
            //if (stationConfig.ReadStationConfigFile(fixtureInUse, ref outmsg) != 0)
            //{
            //    MessageBox.Show(outmsg);
            //}
            stationConfig.ReadStationFixtureTypes(ref stationTypes, ref stationNames, ref fixtureTypes, ref fixtureNames);
            
            GetNewFixtureName getFix = new GetNewFixtureName(fixtureNames);
            yn = getFix.ShowDialog();
            newFixtureName = getFix.NewFixtureName;
            getFix.Dispose();
            if (yn == DialogResult.OK)
            {
                // copy to new section
                stationConfig.CopyFixtureDataFromConfigFile(fixtureInUse, newFixtureName, "");
            }
        }

        private void bDelete_Click(object sender, EventArgs e)
        {
            string outmsg = string.Empty;
            string fixtureName = string.Empty;
            string sectionName = string.Empty;
            List<string> stationTypes = new List<string>();
            List<string> stationNames = new List<string>();
            List<string> fixtureTypes = new List<string>();
            List<string> fixtureNames = new List<string>();
            List<string> stations = new List<string>();
            DialogResult yn = new System.Windows.Forms.DialogResult();

            stationConfig = new StationConfigure();
            if (stationConfig.ReadStationConfigFile(fixtureInUse, ref outmsg) != 0)
            {
                MessageBox.Show(outmsg);
            }
            stationConfig.ReadStationFixtureTypes(ref stationTypes, ref stationNames, ref fixtureTypes, ref fixtureNames);

            SelectStation sForm = new SelectStation(fixtureNames);
            yn = sForm.ShowDialog();
            if (yn == System.Windows.Forms.DialogResult.Cancel)
                return;

            // find the fixture name index
            int x = 0;
            foreach (var item in fixtureNames)
            {
                if (item.ToString() == sForm.Station)
                    break;
                x++;
            }
            sectionName = stationTypes[x].ToString();
            fixtureName = sForm.Station;
            sForm.Dispose();

            yn = MessageBox.Show("Are you sure you want to delete the fixure " + sectionName + "?", "Delete Fixture Profile", MessageBoxButtons.YesNo);
            if (yn == System.Windows.Forms.DialogResult.Yes)
            {
                stationConfig.DeleteSection(sectionName);
            }
        }

        /// <summary>
        /// Create a new fixture based on the StationConfigTemplate.xml.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bNew_Click(object sender, EventArgs e)
        {
            string outmsg = string.Empty;
            string selectedFixture = string.Empty;
            string selectedName = string.Empty;
            string selectedSection = string.Empty;
            string selectedStation = string.Empty;
            int i;

            List<string> stationTypes = new List<string>();
            List<string> fixtureTypes = new List<string>();
            List<string> fixtureNames = new List<string>();
            List<string> stations = new List<string>();
            List<string> stationTitles = new List<string>();
            DialogResult yn = new System.Windows.Forms.DialogResult();

            // read the template file
            stationConfig = new StationConfigure();
            stationConfig.ReadStationTemplateTypes(ref stationTypes, ref fixtureTypes, ref fixtureNames, ref stations, ref stationTitles);

            // ask what fixture type to use
            PickFixture sForm = new PickFixture(stationTitles);
            yn = sForm.ShowDialog();
            selectedFixture = sForm.SelectedFixture;
            sForm.Dispose();

            if (yn == System.Windows.Forms.DialogResult.OK)
            {
                // get the section name for the fixture type
                i = 0;
                foreach (var item in stationTitles)
                {
                    if (item == selectedFixture)
                    {
                        selectedSection = stationTypes[i];
                        selectedStation = stations[i];
                    }
                    i++;
                }
                // ask section name
                GetNewFixtureName gForm = new GetNewFixtureName(fixtureNames);
                gForm.ShowDialog();
                selectedName = gForm.NewFixtureName;
                gForm.Dispose();

                // let operator edit the Station
                ConfigFixturesEditStation eForm = new ConfigFixturesEditStation(selectedStation);
                eForm.stationName = selectedStation;
                yn = eForm.ShowDialog();
                if (yn == DialogResult.OK)
                    selectedStation = eForm.stationName;

                // save in stationconfig.xml
                stationConfig.CopyFixtureDataFromTemplate(selectedName, selectedSection, selectedStation);
            }


        }
    }
}
