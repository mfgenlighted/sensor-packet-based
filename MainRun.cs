﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Reflection;
using System.Drawing;
using System.IO;
using System.Diagnostics;
using System.Resources;
using System.Text.RegularExpressions;

using Seagull.BarTender.Print;

using FixtureDatabase;
using LimitsDatabase;
using fakepfs2;
using PFS_System;
using ShopFloorSystem;
using TestStationErrorCodes;

namespace SensorManufSY2
{
    public partial class MainWindow : Form
    {
        // Test Limits
        public struct uutdata
        {
            public string pcbaSN;
            public string pcbaPN;
            public string mac;
            public string mac2;
            public bool printMac; 
            public string topSN;
            public string topPN;
            public string model;
            public bool s1Set;
            public bool s2Set;
            public string dvtmanPrompt;
            public string appPrompt;
            public int testChannel;
            public string pfsAssemblyNumber;
        }

        string lastPCBABarcodeFileName = string.Empty;       // barcode file name of last label printed
        string lastProductBarcodeFileName = string.Empty;   // barcode file name of last label printed
        string lastProductLabelPrinter = string.Empty;      // printer used for last label printed
        string lastPCBALabelPrinter = string.Empty;         // printer used for last label printed

        public void MainRun()
        {
            bool newPcbaManfData = true;
            bool newHlaManfData = true;
            int debugLoopCount = 0;                     // if in debug mode and DebugOptions has loop=<nonzero>, count of loops done
            int maxDebugLoopCount = 0;                  // value of DebugOptions loop

            uutdata uutData = new uutdata();            // structure to save the data to configure the DUT with
            LimitsData.product_codesItem testsToRun = new LimitsData.product_codesItem();       // All the test steps and globals
            LimitsData.stepsItem testparams = new LimitsData.stepsItem();                       // single test step
            LimitsData testLimits = new LimitsData(stationConfig.SQLServer, stationConfig.SQLTestLimitsDataName,
                                    stationConfig.SQLOperatorName, stationConfig.SQLOperatorPassword, stationConfig.SQLTestLimitsDataName);
            FixtureInstrumentBase.FixtureInstrument.Channel singleChannelData;

            string outmsg = string.Empty;
            string scrstring = string.Empty;

            int stepStatus = 0;   // status for a step
            int firstFailCode = -1;                    // storage to track the first and last error
            string firstFailMessage = string.Empty;
            string firstFailTestName = string.Empty;
            string firstFailedStepNumber = string.Empty;
            int lastFailCode = -1;
            string lastFailMessage = string.Empty;
            string lastFailTestName = string.Empty;
            string lastFailedStepNumber = string.Empty;
            int lastFailedTestNumber = 0;
            string lastSN = string.Empty;
            bool testStatus = false;                    // status of over test. set to false in cause some of the setup fails
            int unitsRun = 0;
            int unitsPassed = 0;
            int unitsFailed = 0;
            DateTime stepStartTime;
            TimeSpan stepTime;
            DateTime testStartTime;
            TimeSpan testTime;

            string dumpFileBaseName = string.Empty;

            //addTextToStatusWindow("Starting Station Initialize\n");
            addTextToStatusWindow(Properties.Resources.StartStationInitialize + "\n");

            abortButtonPushed = false;

            if ((productCodeToTest == string.Empty) | (productCodeToTest.Contains("unkno")))
            {
                MessageBox.Show(Properties.Resources.NoProductConfigured);
                addTextToStatusWindow(Properties.Resources.StationInitializeError + " No product configured." + "\n", false);
                return;
            }

            // figure out if configured product is in the product list
            productCodeToTest = productCodeToTest.Substring(0, 5);      // get product code from the SN
            if (!ProductInfoList.Exists(x => x.product_code == productCodeToTest))
            {
                outmsg = "Product code (" + productCodeToTest + ") does not exist in database.";
                MessageBox.Show(outmsg);
                addTextToStatusWindow(outmsg, false);
                return;
            }

            // data for product configured
            ProductInfo = ProductInfoList.Find(x => x.product_code == productCodeToTest);

            // see if a program version is defined

            if (StationInfo.allowed_testprogram_version != string.Empty)
            {
                if (StationInfo.allowed_testprogram_version.ToUpper()[0] == 'R')
                {
                    if (!Regex.IsMatch(Assembly.GetExecutingAssembly().GetName().Version.ToString(), StationInfo.allowed_testprogram_version.Substring(1)))
                    {
                        if (stationConfig.ConfigDebug )
                        {
                            if (MessageBox.Show("Failed program version check. Do you want to ignore it?", "Version check", MessageBoxButtons.YesNo) == DialogResult.No)
                            {
                                // Running the wrong version of the program. Must be
                                MessageBox.Show(Properties.Resources.WrongPrgVersion + StationInfo.allowed_testprogram_version);
                                addTextToStatusWindow(Properties.Resources.WrongPrgVersion + StationInfo.allowed_testprogram_version + "\n");
                                return;
                            }
                        }
                        else
                        {
                            // Running the wrong version of the program. Must be
                            MessageBox.Show(Properties.Resources.WrongPrgVersion + StationInfo.allowed_testprogram_version);
                            addTextToStatusWindow(Properties.Resources.WrongPrgVersion + StationInfo.allowed_testprogram_version + "\n");
                            return;
                        }
                    }
                }
                else
                {
                    string[] expected = StationInfo.allowed_testprogram_version.Split('.');
                    string[] read = Assembly.GetExecutingAssembly().GetName().Version.ToString().Split('.');
                    if (!expected[0].Equals(read[0]) | !expected[1].Equals(read[1]))
                    {
                        if (stationConfig.ConfigDebug)
                        {
                            if (MessageBox.Show("Failed program version check. Do you want to ignore it?", "Version check", MessageBoxButtons.YesNo) == DialogResult.No)
                            {
                                // Running the wrong version of the program. Must be
                                MessageBox.Show(Properties.Resources.WrongPrgVersion + expected[0] + "." + expected[1] + "." + expected[2]);
                                addTextToStatusWindow(Properties.Resources.WrongPrgVersion + expected[0] + "." + expected[1] + "." + expected[2] + "\n");
                                return;
                            }
                        }
                        else
                        {
                            // Running the wrong version of the program. Must be
                            MessageBox.Show(Properties.Resources.WrongPrgVersion + expected[0] + "." + expected[1] + "." + expected[2]);
                            addTextToStatusWindow(Properties.Resources.WrongPrgVersion + expected[0] + "." + expected[1] + "." + expected[2] + "\n");
                            return;
                        }
                    }
                    if (expected[2].ToUpper() != "X")           // if the last part is defined
                    {
                        if (!expected[2].Equals(read[2]))
                        {
                            if (stationConfig.ConfigDebug)
                            {
                                if (MessageBox.Show("Failed program version check. Do you want to ignore it?", "Version check", MessageBoxButtons.YesNo) == DialogResult.No)
                                {
                                    // Running the wrong version of the program. Must be
                                    MessageBox.Show(Properties.Resources.WrongPrgVersion + expected[0] + "." + expected[1] + "." + expected[2]);
                                    addTextToStatusWindow(Properties.Resources.WrongPrgVersion + expected[0] + "." + expected[1] + "." + expected[2] + "\n");
                                    return;
                                }
                            }
                            else
                            {
                                // Running the wrong version of the program. Must be
                                MessageBox.Show(Properties.Resources.WrongPrgVersion + expected[0] + "." + expected[1] + "." + expected[2]);
                                addTextToStatusWindow(Properties.Resources.WrongPrgVersion + expected[0] + "." + expected[1] + "." + expected[2] + "\n");
                                return;
                            }
                        }
                    }
                }
            }

            //--------------------------------------------------------------------------//
            // read the test limit file                                                 //
            // the fixture limit directory will override the station limit directory.   //
            //--------------------------------------------------------------------------//
            // NOTE: the limitfile name is the limit version to use. productCode is the the productcode + version
            if (!testLimits.GetData(ProductInfo.product_code, ProductInfo.limit_version, StationInfo.function, ref testsToRun, ref outmsg))
            {
                addTextToStatusWindow(Properties.Resources.StationInitializeError + "\n" + outmsg + "\n",false);
                return;
            }

            // see if any tests were found
            if (testsToRun.steps.Count == 0)
            {
                MessageBox.Show("No tests were found for " + ProductInfo.product_code + " version " + ProductInfo.limit_version + ".");
                //addTextToStatusWindow("Starting Station Initialize\n");
                addTextToStatusWindow(Properties.Resources.StationInitializeError + "\n");
                return;
            }

            uutData.dvtmanPrompt = testsToRun.GetGlobalValue("DVTMANPrompt");
            if (uutData.dvtmanPrompt == string.Empty)
                uutData.dvtmanPrompt = "DVTMAN> ";
            uutData.appPrompt = testsToRun.GetGlobalValue("APPPrompt");
            if (uutData.appPrompt == string.Empty)
                uutData.appPrompt = "SU> ";

            if ((stationConfig.ConfigTestRadioChannel == string.Empty) || (stationConfig.ConfigTestRadioChannel == ""))
                uutData.testChannel = 15;
            else
                uutData.testChannel = Convert.ToInt32(stationConfig.ConfigTestRadioChannel);
            //addStationFunctionScreen(stationConfig.ConfigStationOperation);
            addStationFunctionScreen(StationInfo.function);
            addLimitFileNameScreen(ProductInfo.product_code);
            addLimitFileVersionScreen(ProductInfo.limit_version);
            addUnitsRunCounter(0, 0, 0);

            if (stationConfig.ConfigDebugOptions.ToUpper().Contains("LOOP="))   // if in auto loop
            {
                if (!int.TryParse(stationConfig.ConfigDebugOptions.Substring(stationConfig.ConfigDebugOptions.ToUpper().IndexOf("LOOP=") + 5), out maxDebugLoopCount))
                {
                    MessageBox.Show("Error in the debug option Loop. Value not a interger.");
                    return;
                }
            }

            using (ResultData testDB = new ResultData())                                // using for Result database
            using (FixtureFunctions fixture = new FixtureFunctions())                   // labjack, switch-mate, ref radio, dut serial
            {
                // Init the fixture
                fixture.StationConfigure = stationConfig;
                fixture.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);
                if (!fixture.InitFixture())
                {
                    MessageBox.Show("Error initializing fixture: " + fixture.LastFunctionErrorMessage);
                    //addTextToStatusWindow("Starting Station Initialize\n");
                    addTextToStatusWindow(Properties.Resources.StationInitializeError + fixture.LastFunctionErrorMessage + "\n", false);
                    return;
                }
                if (stationConfig.FixtureEquipment.hasConsolePort)
                {
                    fixture.SensorConsole.DisplayRcvChar = stationConfig.ConfigDisplayRcvChar;
                    fixture.SensorConsole.DisplaySendChar = stationConfig.ConfigDisplaySendChar;
                    fixture.SensorConsole.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);
                }
                fixture.ControlDUTPower(0, 0);                 // make sure dut power is off. if there is power control defined

                //--------------------------//
                // wait for fixture to open, if defined //
                //--------------------------//
                if (stationConfig.FixtureEquipment.channelDefs != null)
                {
                    if (stationConfig.FixtureEquipment.channelDefs.Exists(x => x.name == FixtureFunctions.CH_FIXTURE_LID_SWITCH))
                    {
                        singleChannelData = new FixtureInstrumentBase.FixtureInstrument.Channel();
                        fixture.IsCoverClosed();
                        if (fixture.LastFunctionStatus != 0)        // if there was a error
                        {
                            addTextToStatusWindow("Error reading 'FIXTURE_CLOSE_DI'. " + fixture.LastFunctionErrorMessage);
                            MessageBox.Show("Error reading 'FIXTURE_CLOSE_DI'. " + fixture.LastFunctionErrorMessage);
                            return;
                        }
                        //addPromptToStatusWindow("Remove any unit in the test fixture.\n");
                        addPromptToStatusWindow(Properties.Resources.RemoveProduct + "\n");
                        if (stationConfig.ConfigDebug)
                        {
                            if (MessageBox.Show("Bypass lid message?", "Debug mode", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            {
                                MessageBox.Show(Properties.Resources.RemoveProduct);        // "Remove any unit in the test fixture."
                            }
                            else
                            {
                                // wait for fixture to open or the abort button is pushed
                                do
                                {
                                    Thread.Sleep(100);
                                    if (abortButtonPushed)
                                        break;
                                } while (fixture.IsCoverClosed());
                            }
                        }
                        else
                        {
                            // wait for fixture to open or the abort button is pushed
                            do
                            {
                                Thread.Sleep(100);
                                if (abortButtonPushed)
                                    break;
                            } while (fixture.IsCoverClosed());
                        }
                    }
                }

                SensorDVTMANTests sensorDVTMANTests = new SensorDVTMANTests();
                sensorDVTMANTests.fixture = fixture;
                sensorDVTMANTests.stationConfig = stationConfig;
                sensorDVTMANTests.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);

                BluetoothFunctions bleTests = new BluetoothFunctions();
                bleTests.fixture = fixture;
                bleTests.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);
                    

                SensorAPPTests apptests = new SensorAPPTests();
                apptests.stationConfig = stationConfig;

                //----------------------------------------------------------//
                // --- main loop. start at top with waiting for a new unit  //
                //----------------------------------------------------------//
                while (!abortButtonPushed)
                {
                    // reset the first and last errors
                    firstFailCode = 0;
                    firstFailMessage = string.Empty;
                    firstFailedStepNumber = string.Empty;
                    firstFailTestName = string.Empty;
                    lastFailCode = 0;
                    lastFailMessage = string.Empty;
                    lastFailedStepNumber = string.Empty;
                    lastFailTestName = string.Empty;
                    stepStatus = 0;


                    int limitIndex = 0;
                    string result1 = string.Empty;
                    string result2 = string.Empty;
                    string resultmessage = string.Empty;

                    testStatus = true;

                    testStartTime = DateTime.Now;

                    for (limitIndex = 0; limitIndex < testsToRun.steps.Count; limitIndex++)
                    {
                        resultmessage = string.Empty;
                        stepStartTime = DateTime.Now;                                       // save step start time
                        LimitsData.stepsItem  testToRun = new LimitsData.stepsItem();       // holder for a single step
                        testToRun = testsToRun.steps[limitIndex];                           // get the single step
                        addTestHeaderToScreen(limitIndex + 1, testToRun.name);

                        //if (!fixture.SensorConsole.IsOpen())
                        //{
                        //    addTextToStatusWindow("serial reopened. step " + (limitIndex + 1) + "\n");
                        //    fixture.SensorConsole.Open();
                        //    Thread.Sleep(100);
                        //    fixture.SensorConsole.ClearBuffers();
                            
                        //}

                        if (testToRun.step_operation.ToUpper() == "SKIP")
                        {
                            resultmessage = "Test configured to SKIP";
                            addTextToResultsWindow(resultmessage, true);
                            addTextToStatusWindow(resultmessage);
                        }
                        // can only break the sequence in debug mode
                        else if (stationConfig.ConfigDebug & abortButtonPushed)
                        {
                            break;
                        }
                        else
                        {

                            switch (testToRun.function_call.ToUpper())
                            {
                                //  This must be the first one called
                                case "GETSN":
                                    string scanType = string.Empty;

                                    if (stationConfig.ConfigDebug)
                                    {
                                        if (maxDebugLoopCount == 0)
                                        {
                                            // "Warning: This system running in DEBUG mode."
                                            var yn = MessageBox.Show(Properties.Resources.RunningInDebugMode, "DEBUG Mode", MessageBoxButtons.OKCancel);
                                            if (yn == DialogResult.Cancel)
                                            {
                                                stepStatus = (int)ErrorCodes.System.OperatorCancel;
                                                resultmessage = "Operator cancel. In debug mode.";
                                                break;
                                            }
                                        }
                                    }

                                    scanType = testToRun.GetParameterValue("SNScanType");
                                    if (debugLoopCount == 0)            // first loop of debug loop has to get serial data. it is only
                                                                        //  incremented if maxDebugLoopCount != 0, which is set if DebugOptions
                                                                        //  has loop=x
                                    {
                                        if (scanType.ToUpper() == "HLASN")
                                            stepStatus = GetHLASN(fixture, ProductInfo, ref uutData, ref resultmessage);
                                        if (scanType.ToUpper() == "READFROMDUT")
                                        {
                                            stepStatus = ReadPCBASN(fixture, ProductInfo, ref uutData, ref resultmessage);
                                            //stepStatus = GetPCBASN(fixture, ProductInfo, ref uutData, ref resultmessage);
                                        }
                                        else
                                        {
                                            uutData.pcbaSN = string.Empty;      // will make the function prompt operator for SN
                                            if ((stationConfig.ConfigDebug) & (lastSN != string.Empty))
                                                uutData.pcbaSN = "#" + lastSN;
                                            stepStatus = GetPCBASN(fixture, ProductInfo, ref uutData, ref resultmessage);
                                            lastSN = uutData.pcbaSN;
                                        }
                                    }
                                    else
                                        stepStatus = 0;
                                    if (stepStatus == 0)
                                    {
                                        // setup console dump if configed
                                        if (stationConfig.ConfigEnableConsoleDump)
                                        {
                                            dumpFileBaseName = uutData.pcbaSN + "_" + DateTime.UtcNow.ToString("o").Replace(':', '_') + ".txt";
                                            if (debugLoopCount != 0)
                                            {
                                                dumpFileBaseName = "_L" + debugLoopCount + "_" + dumpFileBaseName;
                                            }
                                            fixture.SensorConsole.SetDumpFileName(stationConfig.ConfigReportDirectory, @"/dut" + dumpFileBaseName);
                                            statusWindowFileDump.SetDumpFileName(stationConfig.ConfigReportDirectory, @"/screen" + dumpFileBaseName);
                                            if (stationConfig.FixtureEquipment.hasTestRadio)
                                            {
                                                fixture.RefRadio.SetDumpFileName(stationConfig.ConfigReportDirectory, @"/ref" + dumpFileBaseName);
                                            }
                                        }

                                        // set up the result recording
                                        if (stationConfig.SQLResultsDatabaseName == string.Empty)       // not defined in station config
                                        {
                                            if (stationConfig.ConfigDebug)
                                                testDB.DBInfoDatabase = "debug_production_results";
                                            else
                                                testDB.DBInfoDatabase = "production_results";
                                        }
                                        else
                                        {
                                            if (stationConfig.ConfigDebug)
                                                testDB.DBInfoDatabase = "debug_" + stationConfig.SQLResultsDatabaseName;
                                            else
                                                testDB.DBInfoDatabase = stationConfig.SQLResultsDatabaseName;
                                        }

                                        addResultDBNameScreen(testDB.DBInfoDatabase);

                                        testDB.DBInfoServer = stationConfig.SQLServer;
                                        testDB.DBInfoUser = stationConfig.SQLOperatorName;
                                        testDB.DBInfoPassword = stationConfig.SQLOperatorPassword;
                                        testDB.DBInfoRecordOption = (ResultData.DBRecordType)stationConfig.SQLRecordOption;
                                        testDB.StationName = stationConfig.ConfigStationName + ":" + stationConfig.ConfigFixtureName;
                                        testDB.TestOperator = operatorName;
                                        testDB.TestOperation = StationInfo.function;
                                        testDB.TextFileInfoDirectory = stationConfig.ConfigReportDirectory;
                                        testDB.RecordTextFile = stationConfig.ConfigRecordTextFile;
                                        try
                                        {
                                            string workorder = string.Empty;
                                            if (stationConfig.ConfigEnablePFS)
                                                workorder = ProductInfo.pfs_assembly_number;
                                            if (stationConfig.ConfigEnableSFS)
                                                workorder = sfSystem.WorkOrderNumber;
                                            testDB.CreateTestRecord(uutData.pcbaSN, stationConfig.ConfigStationName, ProductInfo.limit_version, ProductInfo.product_code,
                                                                        Assembly.GetExecutingAssembly().GetName().Version.ToString(), workorder, ProductInfo.product_code);
                                        }
                                        catch (Exception ex)
                                        {
                                            resultmessage = "Error creating test record: " + ex.Message;
                                            stepStatus = (int)ErrorCodes.System.ResultDBRecordCreateError;
                                            break;
                                        }
                                        testDB.CreateStepRecord("GetSN", testToRun.step_number);
                                        testDB.AddStepRef("ProductConfig", stationConfig.ConfigStationToUse, "", "", "");     // record the product config used
                                        testDB.AddStepRef("ProductCode", productCodeToTest, "", "", "");       // record the product config code used

                                        if (uutData.mac2 != string.Empty)                   // if there are 2 macs
                                            addHeaderResultsWindow("Result for:\n " + uutData.pcbaSN + "/" + uutData.topSN + "\n" + uutData.mac+ "/" + uutData.mac2 + "\n");
                                        else
                                            addHeaderResultsWindow("Result for:\n " + uutData.pcbaSN + "/" + uutData.topSN + "\n" + uutData.mac + "\n");
                                        if (stationConfig.ConfigDebug & (maxDebugLoopCount != 0))       // if in debug loop mode
                                        addTextToStatusWindow("Setting up station....\n");
                                        addTextToStatusWindow("   Result Record created for " + uutData.pcbaSN + "\n");
                                        Thread.Sleep(1000);     // give time for power to stablize
                                    }
                                            addHeaderResultsWindow(" --- loop " + (debugLoopCount + 1) + " of " + maxDebugLoopCount + "\n");
                                    if (stationConfig.FixtureEquipment.channelDefs != null)
                                    {
                                        if (stationConfig.FixtureEquipment.channelDefs.Exists(x => x.name == FixtureFunctions.CH_FIXTURE_LID_SWITCH))
                                        {
                                            //addPromptToStatusWindow("Insert the DUT in the test fixture and close the cover to start the test.\n");
                                            addPromptToStatusWindow(Properties.Resources.InsertProduct + "\n");
                                            while (!fixture.IsCoverClosed()) { };
                                            fixture.SensorConsole.ClearBuffers();
                                        }
                                    }
                                    testStartTime = DateTime.Now;
                                    break;

                                case "DLLCALL":
                                        try
                                        {
                                            Assembly asm = Assembly.Load(testToRun.GetParameterValue("FileName"));
                                            Type t = asm.GetType(testToRun.GetParameterValue("FileName") + "." + testToRun.GetParameterValue("Class"));
                                            dynamic dllObject = Activator.CreateInstance(t);
                                        dllObject.RunTest();

                                            //MethodInfo m = t.GetMethod(testToRun.function_call);

                                            //m.Invoke(dllObject, new object[] { testToRun, testDB, uutData, resultmessage });
                                        }
                                        catch (Exception ex)
                                        {

                                            MessageBox.Show(ex.Message);
                                        }
                                    break;

                                case "TURNONDUT":
                                    stepStatus = sensorDVTMANTests.TurnOnDut(testToRun, testDB, uutData, ref resultmessage);
                                    break;

                                //----------- CheckFirmware -------------x//
                                case "CHECKFIRMWARE":
                                    stepStatus = sensorDVTMANTests.SUCheckFirmware(testToRun, testDB, ref resultmessage);
                                    break;

                                //------------ SUBatteryTest ------------x//
                                case "VOLTAGETEST":
                                    stepStatus = sensorDVTMANTests.VoltageTest(testToRun, testDB, ref resultmessage);
                                    break;

                                //------------- CheckControlVoltage -----------//
                                case "CHECKCONTROLOUTPUT":
                                    stepStatus = sensorDVTMANTests.CheckControlOutput(testToRun, testDB, uutData, ref resultmessage);
                                    break;

                                //---------- RadioTest ----------//
                                case "RADIOTEST":
                                    stepStatus = sensorDVTMANTests.TestNetworks(testToRun, testDB, uutData, ref resultmessage);
                                    break;


                                //-------------- CalibrateCPUTemperature --------------
                                case "CALIBRATECPUTEMPERATURE":
                                    stepStatus = sensorDVTMANTests.CalibrateCPUTemperature(testToRun, testDB, uutData, ref resultmessage);
                                    break;


                                //------------------Check PIR, AMB, and Temp sensors
                                case "CHECKSENSORS":
                                    stepStatus = sensorDVTMANTests.AllSensorTest(testToRun, testDB, uutData, ref resultmessage);
                                    break;


                                //------------------- clear NV ---------------
                                case "SETNVCLEAR":
                                    stepStatus = sensorDVTMANTests.SetNVClear(testToRun, testDB, out resultmessage);
                                    break;

                                //------------------- preform reboot ---------------
                                case "PERFORMREBOOT":
                                    stepStatus = sensorDVTMANTests.PerformReboot(testToRun, testDB, ref resultmessage);
                                    break;

                                //------------------- set pcba manufacturing data ---------------
                                case "SETPCBAMANDATA":
                                    stepStatus = sensorDVTMANTests.SetPcbaManData(testToRun, testDB, uutData, newPcbaManfData, out resultmessage);
                                    break;

                                //------------------ set HLA manufacturing data ------------------
                                case "SETHLAMANDATA":
                                    stepStatus = sensorDVTMANTests.SetHlaManData(testToRun, testDB, uutData, newHlaManfData, out resultmessage);
                                    break;

                                //------------------ set all manufacturing data ------------------x
                                case "SETALLMANDATA":
                                    stepStatus = sensorDVTMANTests.SetAllManData(testToRun, testDB, uutData, newPcbaManfData, newHlaManfData, out resultmessage);
                                    break;

                                //------------------Flash dut code ------------------------------
                                case "FLASHDUTCODE":
                                    stepStatus = sensorDVTMANTests.FlashDUTCode(testToRun, testDB, ref resultmessage);
                                    break;

                                //----------------- Verify Label --------------------------------
                                case "VERIFYLABEL":
                                    stepStatus = apptests.VerifyLabel(testToRun, testDB, uutData, out resultmessage);
                                    break;

                                //------------------ POST test ----------------
                                case "CHECKPOST":
                                    stepStatus = sensorDVTMANTests.CheckPOST(testToRun, testDB, out resultmessage);
                                    break;


                                //------------------ Change Over ----------------
                                case "CHANGEOVER":
                                    if (stationConfig.ConfigDebug)        // if in debug mode, give option to not do change over
                                    {
                                        var yn = MessageBox.Show("Running in debug mode. Do you want to do the changeover?", "Debug Changeover", MessageBoxButtons.YesNo);
                                        if (yn == System.Windows.Forms.DialogResult.Yes)
                                        {
                                            stepStatus = sensorDVTMANTests.ChangeOver(testToRun, testDB, uutData, out resultmessage);
                                        }
                                        else    // in debug mode, but choose to not do changeover. 
                                        {
                                            stepStatus = 0;         // make it look like it did the changeover
                                            testDB.CreateStepRecord(testToRun.name, testToRun.step_number);
                                            testDB.AddStepResult("PASS", "In debug mode and did not do the changeover", stepStatus);
                                        }
                                    }
                                    else        // not in debug mode
                                    {
                                        stepStatus = sensorDVTMANTests.ChangeOver(testToRun, testDB, uutData, out resultmessage);
                                    }
                                    break;

                                //-------------------Led check ----------------
                                case "CHECKLEDS":
                                    stepStatus = sensorDVTMANTests.LEDTest(testToRun, testDB, uutData, out resultmessage);
                                    break;

                                //-------------------Led check - Manual----------------
                                case "CHECKLEDSMANUAL":
                                    stepStatus = sensorDVTMANTests.LEDTestManual(testToRun, testDB, out resultmessage);
                                    break;

                                //-------------------Delay x seconds -----------
                                case "DELAY":
                                    Thread.Sleep((int)(Convert.ToDouble(testToRun.parameters[0].value)));
                                    stepStatus = 0;
                                    break;

                                //------------------ Print a label on the PCBA printer ---------------
                                case "PRINTPCBALABEL":
                                    if (stationConfig.ConfigDebug)
                                    {
                                        var yn = MessageBox.Show("In debug mode. Do you want to print a label?", "Label Print", MessageBoxButtons.YesNo);
                                        if (yn == System.Windows.Forms.DialogResult.No)
                                            break;
                                    }
                                    stepStatus = sensorDVTMANTests.PrintPcbaLabel(testToRun, testDB, uutData, stationConfig.ConfigPCBALabelPrinter, out resultmessage, ref lastPCBABarcodeFileName);
                                    lastPCBALabelPrinter = stationConfig.ConfigPCBALabelPrinter;
                                    break;

                                //------------------ Print a label on the Product printer ---------------
                                case "PRINTPRODUCTLABEL":
                                    if (stationConfig.ConfigDebug)
                                    {
                                        var yn = MessageBox.Show("In debug mode. Do you want to print a label?", "Label Print", MessageBoxButtons.YesNo);
                                        if (yn == System.Windows.Forms.DialogResult.No)
                                            break;
                                    }
                                    stepStatus = sensorDVTMANTests.PrintProductLabel(testToRun, testDB, uutData, stationConfig.ConfigProductLabelPrinter, out resultmessage, ref lastProductBarcodeFileName);
                                    lastProductLabelPrinter = stationConfig.ConfigProductLabelPrinter;
                                    break;

                                //--------------- Bluetooth tests -------------------

                                case "BLERADIOTEST": 
                                    stepStatus = sensorDVTMANTests.TestBLE(testToRun, testDB, uutData, ref resultmessage);
                                    break;

                                default:
                                    resultmessage = "There is a invalid test defined in file:" + ProductInfo.limit_version + ", Test: " + testToRun.name + " Function: " + testToRun.function_call;
                                    testStatus = false;
                                    stepStatus = (int)ErrorCodes.System.InvalidTestDefined;
                                    break;
                            }  // end of switch

                        }
                        stepTime = DateTime.Now - stepStartTime;
                        if (stepStatus != 0)               // if the step failed
                        {
                            if (stepStatus == (int)ErrorCodes.System.OperatorCancel)     // and canceled
                            {
                                lastFailedTestNumber = testToRun.step_number;
                                limitIndex = testsToRun.steps.Count + 1;      // force to goto end
                                addTextToStatusWindow("Operator canceled.\n");
                                lastFailCode = stepStatus;
                                lastFailMessage = resultmessage;
                                lastFailTestName = testToRun.function_call;
                                lastFailedStepNumber = (limitIndex + 1).ToString();
                            }
                            else {
                                addTestFooterToScreen(stepStatus, testToRun.step_number, resultmessage);
                                testStatus = false;         // then fail the test
                                if (firstFailCode == 0)
                                {
                                    firstFailCode = stepStatus;
                                    firstFailMessage = resultmessage;
                                    firstFailTestName = testToRun.function_call;
                                }
                                lastFailCode = stepStatus;
                                lastFailMessage = resultmessage;
                                lastFailTestName = testToRun.function_call;
                                lastFailedTestNumber = testToRun.step_number;
                                lastFailedStepNumber = (limitIndex + 1).ToString();
                                // if it is a system error, do not do any more tests
                                if (lastFailCode < 0)
                                    limitIndex = testsToRun.steps.Count + 1;
                                // get the FailTestOption option
                                if (!testToRun.do_on_fail.ToUpper().Equals("NEXT"))
                                {
                                    if (testToRun.do_on_fail.ToUpper().Equals("END"))
                                        limitIndex = testsToRun.steps.Count + 1;      // force to goto end
                                    if (testToRun.do_on_fail.ToUpper().Equals("SYSTEMCONFIG"))
                                        if (stationConfig.ConfigNextStepAfterFail.ToUpper().Equals("END"))
                                            limitIndex = testsToRun.steps.Count + 1;
                                }
                            }
                        }
                        else        // passed the step
                        {
                            addTestFooterToScreen(stepStatus, testToRun.step_number, resultmessage);
                            if (testToRun.do_on_pass.ToUpper().Equals("END"))
                                limitIndex = testsToRun.steps.Count + 1;      // force to goto end
                            if (testToRun.do_on_pass.ToUpper().Equals("SYSTEMCONFIG"))
                                if (stationConfig.ConfigNextStepAfterPass.ToUpper().Equals("END"))
                                    limitIndex = testsToRun.steps.Count + 1;
                        }
                        if (stationConfig.ConfigDebug)
                            addTextToStatusWindow("time of last test = " + stepTime.ToString("ss") + "\n");
                    }   // end of foreach loop

                    testTime = DateTime.Now - testStartTime;
                    if (stationConfig.ConfigDebug)
                        addTextToStatusWindow("time of test = " + testTime.ToString(@"mm\:ss") + "\n");

                    fixture.ControlDUTPower(0, 0);             // turn off DUT, if there is power control definded
                    fixture.SensorConsole.EndDump();        // turn off any dumping

                    // only log to pfs and shopfloor if it is NOT a system error or Operator cancel(from get SN)
                    if ((lastFailCode >= 0) & !stationConfig.ConfigDebug)
                    {
                        // write data to shop floor if setup
                        if (stationConfig.ConfigEnableSFS)
                        {
                            //ShopFloorInterface sfSystem = new ShopFloorInterface(stationConfig.ConfigShopFloorDirectory,
                            //                stationConfig.ConfigShopFloorFileName, stationConfig.ConfigStationName, stationConfig.StationOperator);
                            sfSystem.TopSN = uutData.topSN;
                            sfSystem.PcbaSN = uutData.pcbaSN;
                            sfSystem.MAC = uutData.mac;
                            sfSystem.TestStatus = testStatus ? "PASS" : "FAIL";
                            sfSystem.ErrorCode = lastFailCode.ToString();
                            sfSystem.StartTime = DateTime.Now;
                            sfSystem.RecordShopData();
                        }
                        if (stationConfig.ConfigEnablePFS & (!uutData.pcbaSN.Equals("")))      // if PFS is enabled and there is a sn
                        {
                            int sfStatus = 0;
                            string sfresultMsg = string.Empty;

                            if (testStatus)     // if the unit passed all tests
                            {
                                pfsInterface.PSFSendResults(stationConfig.PFSWorkCenter, testStatus, ref outmsg);
                                if (!outmsg.Contains("OK"))
                                {
                                    sfStatus = (int)ErrorCodes.System.SFSErrorSendingResult;
                                    sfresultMsg = "PFS Send Result error: " + outmsg;
                                    MessageBox.Show(sfresultMsg + "\n");
                                    addTextToStatusWindow(sfresultMsg, false);
                                }
                            }
                            else
                            {
                                if (stationConfig.ConfigPFSAskIfFail)      // if station configured to allow NOT logging in PFS if fail
                                {
                                    //var opAnswer = MessageBox.Show("Unit FAILED. Do you want to send the Fail result to PFS?", "Unit Failed", MessageBoxButtons.YesNo);
                                    var opAnswer = MessageBox.Show(Properties.Resources.FailedUnitSendFailToPFSQ, Properties.Resources.UnitFailed, MessageBoxButtons.YesNo);
                                    if (opAnswer == DialogResult.Yes)
                                    {
                                        string fmsg = lastFailMessage;
                                        if (lastFailMessage.Length > 512)
                                            fmsg = lastFailMessage.Substring(0, 512);
                                        fmsg = fmsg.Replace('\n', '-');
                                        fmsg = fmsg.Replace('\r', '-');
                                        fmsg = fmsg.Replace('\t', '-');
                                        fmsg = fmsg.Replace('>', 'G');
                                        //pfsInterface.PSFSendFailResults(wsfixture.PFSWorkCenter, lastFailTestName, fmsg, ref outmsg);
                                        pfsInterface.PSFSendFailResults(stationConfig.PFSWorkCenter, lastFailCode.ToString(), lastFailTestName + ":" + fmsg, ref outmsg);
                                        if (!outmsg.Contains("OK"))
                                        {
                                            //MessageBox.Show("PFS Send Fail Result error: " + outmsg);
                                            //addTextToStatusWindow("PFS Send Fail Result error: " + outmsg + "\nComment: " + fmsg + "\n", false);
                                            sfresultMsg = Properties.Resources.PFSSendFail + outmsg;
                                            sfStatus = (int)ErrorCodes.System.SFSErrorSendingResult;
                                            MessageBox.Show(Properties.Resources.PFSSendFail + outmsg);
                                            addTextToStatusWindow(Properties.Resources.PFSSendFail + outmsg + "\nComment: " + fmsg + "\n", false);
                                        }

                                    }
                                    else
                                    {
                                        //addTextToResultsWindow("Fail status not sent to PFS.\n", false);
                                        //addTextToStatusWindow("Fail status not sent to PFS.", false);
                                        sfresultMsg = Properties.Resources.FailNotSentToPFS;
                                        sfStatus = (int)ErrorCodes.System.SFSFailNotSentToSFS;
                                        addTextToResultsWindow(Properties.Resources.FailNotSentToPFS + "\n", false);
                                        addTextToStatusWindow(Properties.Resources.FailNotSentToPFS + "\n", false);
                                    }
                                }
                                else        // configured to always send fail results to PFS
                                {
                                    string fmsg = lastFailMessage;
                                    if (lastFailMessage.Length > 512)
                                        fmsg = lastFailMessage.Substring(0, 512);
                                    fmsg = fmsg.Replace('\n', '-');
                                    fmsg = fmsg.Replace('\r', '-');
                                    fmsg = fmsg.Replace('\t', '-');
                                    fmsg = fmsg.Replace('>', 'G');
                                    //pfsInterface.PSFSendFailResults(wsfixture.PFSWorkCenter, lastFailTestName, fmsg, ref outmsg);
                                    pfsInterface.PSFSendFailResults(stationConfig.PFSWorkCenter, lastFailCode.ToString(), lastFailTestName + ":" + fmsg, ref outmsg);
                                    if (!outmsg.Contains("OK"))
                                    {
                                        sfStatus = (int)ErrorCodes.System.SFSErrorSendingResult;
                                        sfresultMsg = "PFS Send Fail Result error: " + outmsg;
                                        MessageBox.Show("PFS Send Fail Result error: " + outmsg);
                                        addTextToStatusWindow("PFS Send Fail Result error: " + outmsg + "\nComment: " + fmsg + "\n", false);
                                    }
                                }
                            }
                            if (testDB != null)     // if the database has been opened. May not be open if PFS rejected the SN at GetSN
                            {
                                testDB.CreateStepRecord("RecordPFS", 27);
                                testDB.AddStepResult((sfStatus == 0) ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED,
                                        sfresultMsg, sfStatus);
                            }
                        }
                    }

                    try
                    {
                        if (lastFailedStepNumber == string.Empty)
                            lastFailedStepNumber = "0";
                        testDB.CloseTestRecord(lastFailCode, lastFailMessage, Convert.ToInt32(lastFailedStepNumber), lastFailTestName, lastFailedTestNumber);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error writing out database. " + ex.Message);
                        lastFailCode = (int)ErrorCodes.System.ResultDatabaseWriteFail;
                        lastFailMessage = "Error writing out database. " + ex.Message;
                    }
                    // just in case console dump is enabled
                    //sensor.EndDump();
                    //cuConsole.EndDump();
                    statusWindowFileDump.EndDump();


                    if (stepStatus == (int)ErrorCodes.System.OperatorCancel)
                        break;

                    addTextToStatusWindow("Done\n");
                    Banner pfForm = new Banner(fixture);
                    if (abortButtonPushed)
                    {
                        //addTextToStatusWindow("--  Test was ABORTED\n", testStatus);
                        //addTextToResultsWindow("--  Test was ABORTED\n", testStatus);
                        //pfForm.PFText = ("ABORTED");
                        addTextToStatusWindow("--  " + Properties.Resources.TestAborted + "\n", testStatus);
                        addTextToResultsWindow("--  " + Properties.Resources.TestAborted + "\n", testStatus);
                        pfForm.PFText = (Properties.Resources.Aborted);
                        pfForm.BackColor = Color.Orange;
                    }
                    else if (lastFailCode < 0)       // system failure
                    {
                        //addTextToStatusWindow("--  Test was a System Error\n", testStatus);
                        //addTextToResultsWindow("--  Test was a System Error\n", testStatus);
                        //pfForm.PFText = ("System Error");
                        addTextToStatusWindow("--  " + Properties.Resources.TestWasSytemError + "\n", testStatus);
                        addTextToResultsWindow("--  " + Properties.Resources.TestWasSytemError + "\n", testStatus);
                        pfForm.PFText = (Properties.Resources.SystemError);
                        pfForm.BackColor = Color.Yellow;
                    }
                    else
                    {
                        unitsRun++;
                        if (testStatus)
                            unitsPassed++;
                        else
                            unitsFailed++;
                        addUnitsRunCounter(unitsRun, unitsPassed, unitsFailed);
                        //addTextToStatusWindow("--  Test has " + (testStatus ? "PASS" : "FAIL") + "\n", testStatus);
                        //addTextToResultsWindow("--  Test has " + (testStatus ? "PASS" : "FAIL") + "\n", testStatus);
                        //pfForm.PFText = (testStatus ? "PASS" : "FAIL");
                        addTextToStatusWindow("--  " + Properties.Resources.TestHas + (testStatus ? Properties.Resources.Pass : Properties.Resources.Fail) + "\n", testStatus);
                        addTextToResultsWindow("--  " + Properties.Resources.TestHas + (testStatus ? Properties.Resources.Pass : Properties.Resources.Fail) + "\n", testStatus);
                        pfForm.PFText = (testStatus ? Properties.Resources.Pass : Properties.Resources.Fail);
                        pfForm.BackColor = testStatus ? Color.Lime : Color.Red;
                    }
                    if (uutData.pcbaSN != null)
                        pfForm.SN = uutData.pcbaSN;
                    else
                        pfForm.SN = "unknown";
                    if (uutData.mac != null)
                        pfForm.MAC = uutData.mac;
                    else
                        pfForm.MAC = "unknown";
                    pfForm.FirstFailCode = lastFailCode.ToString();
                    pfForm.StepNumber = lastFailedStepNumber + " : " + lastFailTestName;
                    pfForm.ErrorMessage = lastFailMessage;
                    if (maxDebugLoopCount == 0)
                        pfForm.ShowDialog();
                    pfForm.Dispose();
                    if (maxDebugLoopCount != 0)
                    {
                        debugLoopCount++;
                        if (debugLoopCount == maxDebugLoopCount)
                            break;
                        addTextToStatusWindow("Done " + debugLoopCount + " of " + maxDebugLoopCount);
                    }
                    // can only abort the test run in debug mode
                    if (stationConfig.ConfigDebug & abortButtonPushed)
                    {
                        abortButtonPushed = false;
                        break;
                    }

                    if (fixture.FindChannelData(FixtureFunctions.CH_FIXTURE_LID_SWITCH) != null)
                    {
                        //addPromptToStatusWindow("Remove unit from fixture\n");
                        addPromptToStatusWindow(Properties.Resources.RemoveProduct + "\n");
                        if (stationConfig.ConfigDebug)
                        {
                            if (maxDebugLoopCount == 0)
                            {
                                if (MessageBox.Show("Bypass lid message?", "Debug mode", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    MessageBox.Show(Properties.Resources.RemoveProduct);        // "Remove any unit in the test fixture."
                                }
                                else
                                {
                                    // wait for fixture to open or the abort button is pushed
                                    while (fixture.IsCoverClosed() && !abortButtonPushed) { };
                                }
                            }
                        }
                        else
                        {
                            // wait for fixture to open or the abort button is pushed
                            while (fixture.IsCoverClosed() && !abortButtonPushed) { };
                        }
                    }

                } // end of main while loop


            }   // end of using
        }

    }
}
