﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorManufSY2
{
    public partial class GetNewFixtureName : Form
    {
        public string NewFixtureName = string.Empty;

        List<string> currentFixtures = new List<string>();

        public GetNewFixtureName(List<string> CurrentFixtures)
        {
            InitializeComponent();
            currentFixtures = CurrentFixtures;
        }

        /// <summary>
        /// Checks to make sure not dup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bSave_Click(object sender, EventArgs e)
        {
            if (currentFixtures.Contains(this.tbFixtureName.Text))
            {
                MessageBox.Show("This name is already used.");
                this.tbFixtureName.Text = "";
            }
            else
            {
                NewFixtureName = this.tbFixtureName.Text;
                DialogResult = System.Windows.Forms.DialogResult.OK;

            }
        }
    }
}
