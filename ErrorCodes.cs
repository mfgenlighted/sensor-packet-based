﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestStationErrorCodes
{
    class ErrorCodes
    {
        public static class SystemGroups
        {
            public const int LimitsErrors = -1000;
            public const int ResultDatabaseErrors = -2000;
            public const int MACDatabaseErrors = -3000;
                                //Open,                   //  1 - database open error
                                //NUMBER_PER_Read,        //  2 - error reading NUMBER_PER field
                                //MAC_DataRead,           //  3 - error reading MAC data
                                //SN_Format,              //  4 - incorrect SN format
                                //MAC_Gen,                //  5 - error generating MACs
                                //LockingTable,           //  6 - error locking mac table
                                //MAC_NotValidForFamily,  //  7 - mac being used is not in the family pool
                                //MAC_AssignedToDiffSN,   //  8 - mac is assigned to a different sn
                                //MAC_Recording,          //  9 - error recording mac(s) in data base
                                //MAC_Mismatch,           //  10 - MAC does not match SN in database
                                //Not_Open,               //  12 - database not open when assumed it should be
                                //Last_MAC_Read,          //  13 - error reading the last mac used
                                //Read_StartStop,         //  14 - error reading the start/stop mac data
                                //MAC_OutOfRange,         //  15 - new mac is out of the start/stop range.

            public const int SNDatabaseErrors = -4000;
                                //open,                   // 1  - database open error
                                //locking,                // 2  - error locking the table
                                //recordingData,          // 3  - error trying to record data
                                //not_open,               // 4 -  expected database to be open
                                //last_sn_read,           // 5  - error reading the last serial number
                                //dup_check,              // 6  - database error looking for duplicate sn
                                //dup_sn_found,           // 7  - duplicate sn was found
                                //xxxxx,          // 8  - xxxxx
                                //mac_find_db_error,      // 9  - error looking for mac in productcode table

            public const int ProductDatabaseErrors = -5000;
            public const int FixtureErrorsBase = -6000;
            public const int ShopFloorErrors = -7000;
        }

        public static class System
        {
            public const int ParameterFormatError = -1;                 // parameter entered by operator is incorrect format
            public const int OperatorCancel = -2;                       // operator canceled 
            public const int InvalidTestDefined = -3;                   // test defined in the limit file is not a defined test
            public const int ProductDBMissingProdCode = -4;             // product code defined is not in the product database
            public const int TestNotImplemented = -5;                   // placeholder code that has not been wrote yet
            public const int MissingParameter = -6;                     // missing a manitory parameter
            public const int MissingLabelFile = -7;                     // label file missing
            public const int InvalidLabelFieldName = -8;                // the label field name does not exist in the label file
            public const int LabelPrintError = -9;                      // there was a error printing the label
            public const int ConfigFileOpenError = -10;                 // error opening the station configuration file

            public const int InvalidLimitOperation = -1000;             // a limit operation or type is invalid

            public const int ResultDBRecordCreateError = -2000;         // error trying to create a test record head
            public const int ResultDatabaseWriteFail = -2001;                 // error trying to write data to the result database


            public const int ComponetInitFail = -6000;                  // fixture componet failed to initialize
            public const int MoreThanOneInstrDefined = -6001;           // can not have LabJack and A34970A enabled at the same time
            public const int VisaSendError = -6002;                     // got a visa error trying to send visa instrument command
            public const int InstrumentNotConnected = -6003;               // instrument is not connected
            public const int VisaReadError = -6004;                     // got a visa error reading a instrument
            public const int InstrumentReturnedBadFormat = -6005;       // the data returned from the instrument was not in the correct format
            public const int InvalidOutputType = -6006;                 // the channel type define did not match the ouput type of the instrument
            public const int VisaOpenError = -6007;                     // error opening the visa port
            public const int NotCorrectInstrument = -6008;              // was no the instrument expected
            public const int NotCorrectCard = -6009;                    // not expected card in the instrument
            public const int MissingInstrumentOption = -6010;           // a option in the instrument is not present
            public const int InstrumentErrorMsg = -6011;                // found a error message in a instrument
            public const int StationConfigNotRead = -6012;              // station configuration must be read before trying to intialize fixture
            public const int InvalidChannelData = -6013;                // the channel data define has invalid data
            public const int InvalidInstrumentDefined = -6014;          // the instrument in the definition is not on this station
            public const int InstrumentReadError = -6015;              // non visa instrument read error
            public const int LabjackConnectError = -6016;               // error trying to connect to the labjack
            public const int LabjackSetupError = -6017;                 // error trying to setup the labjack
            public const int RelayReadbackWrong = -6018;                // the readback of the relay after it was set is wrong
            public const int RelayControlError = -6019;                 // error trying to control the relay
            public const int SensorConsoleOpenError = -6020;            // error opening the sensor console port
            public const int SensorConsoleNotOpen = -6021;              // trying to use the sensor console port but it is not open
            public const int SensorConsoleWriteError = -6022;           // error writing to the sensor console
            public const int LEDDetectorNotInitialize = -6023;          // LED detector port has not been initialized
            public const int LEDDetectorNotOpen = -6024;                // LED detector port is not open
            public const int SnifferPortNotInitialized = -6025;         // sniffer port is not initialized
            public const int SnifferPortNotOpen = -6026;                // sniffer port is not open
            public const int SnifferWriteError = -6027;                 // sniffer port write error

            public const int FlasherBase = -6100;                       // base code for flasher errors
                                                                        // Error_PortNotOpen = 1;
                                                                        // Error_ErrResponse = 2;
                                                                        // Error_OpenError = 3;
                                                                        // Error_PortNotExist = 4;
                                                                        // Error_PortNotInitialized = 5;
                                                                        // Error_FileSelectionFailed_Timeout = 6;
                                                                        // Error_FileSelectionFailed_NACKReceived = 7;
                                                                        // Error_FileSelectionFailed_OKNotReceived = 8;
                                                                        // Error_ProgrammingFailed_Timeout = 9;
                                                                        // Error_ProgrammingFailed_NACKReceived = 10;
                                                                        // Error_StatusRead_Timeout = 11;
                                                                        // Error_StatusRead_NACKReceived = 12;
                                                                        // Error_StatusRead_Error = 13;

            public const int DAQBase = -6150;                           // base of DAQ errors. See FixturInstrBase for defines

            public const int SFSErrorSendingResult = -7000;             // did not get a OK back when sending results
            public const int SFSFailNotSentToSFS = -7001;               // operator choose to not send fail to SFS
        }

        //public enum System
        //{
        //    MissingParameter = -1,               // 1 - one or more test parameters are missing
        //    ComponetInitFail = -2,               // 2 - one or more of the station componets failed to initialize
        //    MacDBFailure = -3,                   // 3 - MAC database failed
        //    SNDBFailure = -4,                    // 4 - SN database failed
        //    ResultDBFailure = -5,                // 5 - Result database failed
        //    InvalidParameter = -6,               // 6 - Specified parameter is not a valid parameter
        //    OperatorCancel = -7,                 // 7 - Operator canceled the opertaion
        //    FileOpenError = -8,                  // 8 - error opening a file
        //    LabJackNotSetup = -9,                // 9    - Labjack has not been setup
        //    LabJackError = -10,                   // 10   - Labjack error executing a function
        //    LabelPrintError = -11,                // 11   - label printing error
        //    LEDDetectorNotInitialize = -12,       // 12   - serial LED detector has not be initialized
        //    InstrumentNotSetUp = -13,             // 13 - instrument not set up 
        //    ACMeterReadError = -14,               // 14 - error reading the power meter
        //    FixtureError = -15,                   // 15 - fixture had a error
        //    CommandTimeout = -16,                 // 16 - command timeout
        //    DatabaseSetupError = -17,             // 17 - error in the database setup
        //    NoStationsDefined = -18,              // 18 - there is no TestStation section in the config file
        //    TestNotImplemented = -19,             // 19 - the test has not been wrote yet
        //    ParameterFormatError = -20,           // 20 - the format of a parameter is bad
        //    InvalidMeasurement = -21,             // 21 - invalide measurement for a instrument
        //    InvalidInstrument = -22,                // 22 - instrument not defined for station
        //    InstrumentFailure = -23,                // 23 - there was a instrument failure
        //}

        //static public string[] SystemStrings =
        //{
        //    "Missing one or more test parameters",          // 1
        //    "System componet failed",                       // 2
        //    "MAC Database failure",                         // 3
        //    "SN Database failure",                          // 4
        //    "Result Database failure",                      // 5
        //    "Invalid Parametere",                           // 6
        //    "Operator Cancel",                              // 7
        //    "Error opening a file",                         // 8
        //    "Labjack has not been set up",                  // 9
        //    "Labjack error executing a function",           // 10
        //    "Error trying to print a label",                // 11
        //    "Serial LED detector has not be initialized",   // 12
        //    "Fixture instrument not setup",                 // 13
        //    "AC Meter read error",                          // 14
        //    "Fixture error",                                // 15
        //    "Command timeout",                              // 16
        //    "Database has a setup error",                   // 17
        //    "No TestStation defined in the StationConfig file", // 18
        //    "Test not wrote yet",                           // 19
        //    "Parameter format is bad",                      // 20
        //    "Invalid measurement for instrument",           // 21
        //    "Instrurment not defined for station",          // 22
        //    "Instrurment failure",                          // 23
        //};

        public static class Program
        {
            public const int ValueOutsideOfLimit        = 100;
            public const int DupicateSN = 101;
            public const int PCBADataMismatch = 102;
            public const int NotInDVTMAN = 103;
            public const int AdvertNotFound = 104;
            public const int VersionCheckFailed = 105;
            public const int ProgramFlashFailed = 106;
            public const int DUTHardwareError = 107;
            public const int ChangedOverError = 108;
            public const int OperatorAbort = 109;

            public const int CUComError                 = 200;
            public const int FailedToPerformCUFunction = 201;

            public const int DutUnexpectedCmdReturn     = 300;
            public const int CommandTimeout             = 301;
            public const int CmdResponseStatusFailed    = 302;
            public const int CmdResponsePayloadLenWrong = 303;
            public const int CmdPacketCheckSumError     = 304;
            public const int CmdResponseHeaderLenWrong  = 305;
            public const int CmdPacketFormatError       = 306;
            public const int CmdMsgTypeNotDVTMAN        = 307;

            public const int RefusedByPFS = 400;

        }

        //public enum Program
        //{
        //    Success,                        // 0
        //    ValueOutsideOfLimit,            // 1  x
        //    DUTComError,                    // 2  
        //    DutUnexpectedCmdReturn,         // 3  x
        //    CUComError,                     // 4  x
        //    DupicateSN,                     // 5  x
        //    PCBADataMismatch,               // 6  x
        //    NotConfForChangeover,           // 7
        //    SeqFail,                        // 8
        //    NotInDVTMAN,                    // 9  x
        //    FailedToPerformCUFunction,      // 10  x
        //    OperatorAbort,                  // 11
        //    FixtureHardwareError,           // 12
        //    ChangedOver,                    // 13  x
        //    RefusedByPFS,                   // 14  x
        //    AdvertNotFound,                 // 15  x
        //    CommandTimeout,                 // 16  x
        //    DUTHardwareError,               // 17  x
        //    CmdResponseStatusFailed,        // 18  x
        //    CmdResponsePayloadLenWrong,     // 19  x
        //    CmdPacketCheckSumError,         // 20  x
        //    CmdMsgTypeNotDVTMAN,            // 21  x
        //    CmdResponseHeaderLenWrong,      // 22  x
        //    CmdPacketFormatError,           // 23  x
        //    VersionCheckFailed,             // 24  x
        //    ProgramFlashFailed,             // 25  x
        //}

        //static public string[] ProgramStrings =
        //{
        //    "Success",                              // 0
        //    "Value outside of limits",              // 1
        //    "DUT com error",                        // 2
        //    "DUT unexpected command return",        // 3
        //    "CU com error",                         // 4
        //    "Dupicate Serial Number found",         // 5
        //    "PCBA Data Mismatch",                   // 6
        //    "Not configured for Change-over",       // 7
        //    "Sequence failure",                     // 8
        //    "DUT not in DVTMAN",                    // 9
        //    "Failed to perform a CU function call", // 10
        //    "Operator abort",                       // 11
        //    "Fixture hardware error",               // 12
        //    "DUT has changed over",                 // 13
        //    "Refused by PFS",                       // 14
        //    "Advert not found",                     // 15
        //    "Command timeout",                      // 16
        //    "DUT Hardware error found",             // 17
        //    "Got a fail status response",           // 18
        //    "Command payload lenght incorrect",     // 19
        //    "Command checksum error",               // 20
        //    "Command Message type not DVTMAN",      // 21
        //    "Command response header to short",     // 22
        //    "Command packet format is wrong",       // 23
        //    "Version check failed",                 // 24
        //    "Failed to flash DUT",                  // 25
        //};

        ///// <summary>
        ///// Return the Error String for a error code.
        ///// </summary>
        ///// <param name="ErrorCode"></param>
        ///// <param name="ErrorString"></param>
        //static public string GetErrorString(int ErrorCode)
        //{
        //    string ErrorString = string.Empty;

        //    if (ErrorCode < 0)      // if a system error code
        //    {
        //        ErrorString = SystemStrings[Math.Abs(ErrorCode) - 1];
        //    }
        //    else
        //    {
        //        ErrorString = ProgramStrings[ErrorCode];

        //    }
        //    return ErrorString;
        //}
    }
}
