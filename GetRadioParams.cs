﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorManufSY2
{
    public partial class GetRadioParams : Form
    {
        public uint Channel;
        public uint PanID;
        public uint TxPower;
        public string EncryptKey;
        public uint Ttl;
        public uint Rate;

        public GetRadioParams()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            Channel = (uint)nudChannel.Value;
            PanID = (uint)nudPanID.Value;
            TxPower = (uint)nudTxPower.Value;
            EncryptKey = tbEncryptKey.Text;
            Ttl = (uint)nudTtl.Value;
            Rate = (uint)nudRate.Value;
            DialogResult = DialogResult.OK;
            return;
        }
    }
}
