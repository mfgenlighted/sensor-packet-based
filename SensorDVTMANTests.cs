﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;

using Seagull.BarTender.Print;

using LimitsDatabase;
using FixtureDatabase;
using System.Reflection;
using TestStationErrorCodes;

namespace SensorManufSY2
{
    public class SUSensorData
    {
        public UInt16 PIR;
        public UInt32 AmbVisable;
        public UInt16 Temperature;

        public SUSensorData ()
        {
            PIR = 0;
            AmbVisable = 0;
            Temperature = 0;
        }
    }


    public partial class SensorDVTMANTests
    {
        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs>
            StatusUpdated;

        protected virtual void
            OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }

        private void UpdateTestHeader(int step, int test, string name)
        {
            int newStep = step;
            int newTest = test;
            string newName = name;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = name;
            nuea.stepNumber = step;
            nuea.testNumber = test;
            OnStatusUpdated(nuea);
        }

        private void UpdateTestFooter(int testResultCode, string errormsg)
        {
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = errormsg;
            nuea.resultCode = testResultCode;
            OnStatusUpdated(nuea);
        }

        public FixtureFunctions fixture;
        public StationConfigure stationConfig;


        public SensorDVTMANTests()
        {
            fixture = null;
            stationConfig = null;
        }


        //---------------------------------------------------
        /// <summary>
        /// VoltageTest
        ///  Will measure and verify voltages.The voltage name is in the limits and must be
        ///  a voltage that is defined in the instrument list
        /// </summary>
        /// <param name="parameters">test to run structure</param>
        /// <param name="testDB">test database object</param>
        /// <param name="resultmsg">result message</param>
        /// <returns>
        ///   11  ErrorCodes.Program.OperatorAbort
        ///   1  ErrorCodes.Program.ValueOutsideOfLimit
        ///   -1  ErrorCodes.System.MissingParameter 
        ///   -6  ErrorCodes.System.InvalidParameter
        ///   -10  ErrorCodes.System.LabJackError
        ///   -13  ErrorCodes.System.InstrumentNotSetUp
        ///   -14  ErrorCodes.System.ACMeterReadErro
        ///   -21 ErrorCodes.System.InvalidMeasurement
        /// 
        /// </returns>
        public int VoltageTest(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, ref string resultmsg)
        {
            int status = 0;           // overall test status
            int[] stepStatus;                 // voltage loop status
            bool voltageFailed = false;                             // one or more voltages failed
            string[] stepMsg;              // voltage loop messages

            List<double> voltages = new List<double>();
            FixtureFunctions.PassData[] voltageNames;
            LimitsData.limitsItem[] limits;

            int currentRetrys = 0;
            int maxRetrys = 0;
            string smaxRetrys = string.Empty;
            bool reseatOnRetry = false;
            int testNumber = 0;

            resultmsg = string.Empty;
            testDB.CreateStepRecord(parameters.name, testNumber);      // create the test record header.

            if (parameters.limits.Count == 0)        // if the limit list is missing
            {
                testDB.AddStepResult("FAIL", "System Error: Missing the Limit List", (int)ErrorCodes.System.MissingParameter);
                return (int)ErrorCodes.System.MissingParameter;
            }
            voltageNames = new FixtureFunctions.PassData[parameters.limits.Count];
            limits = new LimitsData.limitsItem[parameters.limits.Count];
            stepMsg = new string[parameters.limits.Count];
            stepStatus = new int[parameters.limits.Count];

            // create a list of voltage names
            for (int i = 0; i < parameters.limits.Count; i++)
            {
                voltageNames[i].channelName = parameters.limits[i].name;
                limits[i] = parameters.limits[i];
                stepMsg[i] = string.Empty;
                stepStatus[i] = 0;
            }

            currentRetrys = 0;
            maxRetrys = 0;              // default to no retrys
            reseatOnRetry = false;     // default to not reseating on retrys
            if ((smaxRetrys = parameters.GetParameterValue("RETRYS")) != string.Empty)
            {
                maxRetrys = Convert.ToInt32(smaxRetrys);
                if ((smaxRetrys = parameters.GetParameterValue("RESEAT")) != string.Empty)
                    reseatOnRetry = smaxRetrys.ToUpper().Equals("YES");
            }

            status = 0; 
            currentRetrys = 0;

            // start retry loop. any measurment will cause the retry to happen.
            // it will start at the begining of the list again.
            do
            {
                voltageFailed = false;                                  // set as voltages are good
                fixture.ReadMeasurements(ref voltageNames);
                if (fixture.LastFunctionStatus != 0)         // if there was a fail
                {                                           // exit the test
                    resultmsg = fixture.LastFunctionErrorMessage; ;
                    testDB.AddStepResult("FAIL", resultmsg, fixture.LastFunctionStatus);
                    return fixture.LastFunctionStatus;
                }

                try         // catch CheckResultValue function
                {
                    for (int i = 0; i < limits.Length; i++)  
                    {
                        stepStatus[i] = 0;
                        stepMsg[i] = string.Empty;
                        if (!CheckResultValue(voltageNames[i].returnedData, limits[i], ref resultmsg)) // if fail
                        {
                            stepStatus[i] = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                            stepMsg[i] = resultmsg;
                            voltageFailed = true;
                        }
                    }
                }
                catch (Exception ex)    // to catch exceptions from the CheckResultValue method
                {
                    resultmsg = ex.Message;
                    status = (int)ErrorCodes.System.InvalidLimitOperation;
                    testDB.AddStepResult("FAIL", resultmsg, status);
                    return status;                                          // get out of here
                }
                for (int i = 0; i < limits.Length; i++)      // record failures
                {
                    if (stepStatus[i] != 0)
                        testDB.AddStepRef(limits[i].name, string.Format("Reseat= {0}, Try #= {1}, {3}= {2}", (reseatOnRetry ? "YES" : "NO"), currentRetrys, voltageNames[i].returnedData,limits[i].name), "", "", resultmsg);
                }
                if (voltageFailed)      // if one or more voltages failed
                {
                    if (currentRetrys < maxRetrys)  // if there are retrys to try
                    {
                        UpdateStatusWindow("Retrying test\n");
                        if (reseatOnRetry)
                        {
                            ReloadBanner rlb = new ReloadBanner(fixture);
                            if (rlb.ShowDialog() == DialogResult.Abort)
                            {
                                currentRetrys = 999;                    // force to exit
                                status = (int)ErrorCodes.Program.OperatorAbort;
                            }
                            rlb.Dispose();
                        }
                    }
                }
            } while ((status != 0) & (currentRetrys++ < maxRetrys));

            // record all the status
            for (int i = 0; i < limits.Length; i++)
            {
                testDB.AddStepResult(limits[i].name, ((stepStatus[i] == 0) ? "PASS" : "FAIL"), limits[i].operation,
                                        Convert.ToDouble(limits[i].value1), Convert.ToDouble(limits[i].value2), voltageNames[i].returnedData, "V", "N2", stepMsg[i], stepStatus[i]);
                resultmsg = string.Format("\t Expected: {0} - {1}   Measured: {2:N2}", limits[i].value1, limits[i].value2, voltages[i]);
            }

            UpdateStatusWindow(resultmsg + "\n");

            return status;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="testDB"></param>
        /// <param name="reporttext"></param>
        /// <returns>
        ///  3  ErrorCodes.Program.DutUnexpectedCmdReturn
        ///  16 ErrorCodes.Program.CommandTimeout
        ///  -6  ErrorCodes.System.InvalidParameter
        /// </returns>
        public int SetNVClear(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, out string reporttext)
        {
            string outmsg = string.Empty;
            int stepStatus = 0;
            int currentRetrys = 0;
            int maxRetrys = 0;
            string smaxRetrys = string.Empty;
            bool reseatOnRetry = false;
            int timeOutSecs = 2;
            string stimeOutSecs = string.Empty;
            int testNumber = 0;

            testNumber = parameters.step_number;
            testDB.CreateStepRecord(parameters.name, testNumber);

            currentRetrys = 0;
            maxRetrys = 0;              // default to no retrys
            reseatOnRetry = false;     // default to not reseating on retrys
            if ((stimeOutSecs = parameters.GetParameterValue("RETRYS")) != string.Empty)
            {
                 if (!int.TryParse(stimeOutSecs, out timeOutSecs))
                {
                    reporttext = "Retrys parameter must be a int value. Found:" + stimeOutSecs;
                    testDB.AddStepResult(ResultData.TEST_FAILED, reporttext, (int)ErrorCodes.System.ParameterFormatError);
                    return (int)ErrorCodes.System.ParameterFormatError;
                }
            }

            do
            {
                stepStatus = 0;
                reporttext = string.Empty;
                fixture.SensorConsole.CommandRecieveTimeoutMS = 4000;   
                if (!fixture.SensorConsole.ClearManData())
                {
                    if (currentRetrys < maxRetrys)  // if there are retrys to try
                    {
                        UpdateStatusWindow("Retrying test\n");
                        if (reseatOnRetry)
                        {
                            ReloadBanner rlb = new ReloadBanner(fixture);
                            rlb.ShowDialog();
                            rlb.Dispose();
                        }
                        testDB.AddStepRef("SetNVClearRetry", string.Format("Reseat= {0}, Try #= {1}", (reseatOnRetry ? "YES" : "NO"), currentRetrys), "", "", reporttext);
                    }
                    else        // no more retries for fail
                    {
                        stepStatus = fixture.SensorConsole.LastErrorCode;
                        reporttext = fixture.SensorConsole.LastErrorMessage;
                    }
                }
            } while ((stepStatus != 0) & (currentRetrys++ < maxRetrys));

            testDB.AddStepResult((stepStatus == 0) ? "PASS" : "FAIL", reporttext, stepStatus);

            return stepStatus;
        }


        //---------------------------------------------------
        // SUCheckFirmware
        /// <summary>
        /// Will verify the ImageID is correct.
        /// </summary>
        /// <param name="parameters">test to run structure</param>
        /// <param name="testDB">test result recording object</param>
        /// <param name="resultmessage">error message</param>
        /// <returns>
        /// ErrorCodes.Program.Success
        ///   1  ErrorCodes.Program.ValueOutsideOfLimit
        ///  -1  ErrorCodes.System.MissingParameter
        ///  3  ErrorCodes.Program.DutUnexpectedCmdReturn
        ///  2  ErrorCodes.Program.DUTComError
        ///  18 ErrorCodes.Program.CmdResponseStatusFailed
        /// </returns>
        public int SUCheckFirmware(LimitsData.stepsItem parameters, ResultData testDB, ref string resultmessage)
        {
            string outmsg = string.Empty;
            string imageIDRead = string.Empty;
            LimitsData.limitsItem limit = new LimitsData.limitsItem();

            resultmessage = string.Empty;
            // if the limit file defines a test number, use it
            testDB.CreateStepRecord(parameters.name, parameters.step_number);

            if (!parameters.DoesLimitExist("IMAGEID"))
            {
                resultmessage = string.Format("There is a error in the Limit File. ImageID is missing");
                testDB.AddStepResult("FAIL", resultmessage, (int)ErrorCodes.System.MissingParameter);         // record PASS/FAIL type
                return (int)ErrorCodes.System.MissingParameter;
            }
            else
                limit = parameters.GetLimitValues("IMAGEID");

            if (!fixture.SensorConsole.GetImageID(ref imageIDRead))
            {
                resultmessage = fixture.SensorConsole.LastErrorMessage;
                testDB.AddStepResult("FAIL", resultmessage, fixture.SensorConsole.LastErrorCode);             // record PASS/FAIL type
                return fixture.SensorConsole.LastErrorCode;
            }

            if (imageIDRead != limit.value1)
            {
                resultmessage = "ImageID did not match. ImageID: " + limit.value1 + " readID: " + imageIDRead;
                testDB.AddStepResult("FlashImage", FixtureDatabase.ResultData.TEST_FAILED, "EQ", limit.value1, imageIDRead, resultmessage, (int)ErrorCodes.Program.ValueOutsideOfLimit);             // record PASS/FAIL type
                return (int)ErrorCodes.Program.ValueOutsideOfLimit;
            }
            UpdateStatusWindow("Found imageID " + limit.value1 + "\n", StatusType.passed);
            testDB.AddStepResult("FlashImage", FixtureDatabase.ResultData.TEST_PASSED, "EQ", limit.value1, imageIDRead, "", 0);             // record PASS/FAIL type
            return 0;
        }

        //--------------------------------
        // SetPcbaManData
        ///<summary>This will set the pcba manufacturing data.
        /// </summary>
        ///<param name="mac">string - mac to save in the dut. Format xxxxxxxxxxxx.</param>
        ///<param name="pn">string - part number to save in the dut.</param>
        ///<param name="sn">string - serial number to save in the dut.</param>
        ///<param name="newdata">bool - true if this was generated MAC, not found in the database.</param>
        ///<param name="reporttext">out string - description of the error if there is a failure.</param>
        ///<returns>
        ///     ErrorCodes.Program.Success        0
        ///  2   ErrorCodes.Program.DUTComError
        ///  3  ErrorCodes.Program.DutUnexpectedCmdReturn
        ///  6  ErrorCodes.Program.PCBADataMismatch
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        ///</returns>
        ///
        public int SetPcbaManData(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB,  MainWindow.uutdata uutData, bool newdata, out string reporttext)
        {
            int status;

            testDB.CreateStepRecord(parameters.name, parameters.step_number);
            status = setPcbaManData(parameters, uutData, newdata, out reporttext);
            testDB.AddStepResult((status == 0) ? "PASS" : "FAIL", reporttext, status);
            UpdateStatusWindow(reporttext + "\n");
            return status;
        }

        //--------------------------------
        // SetHlaManData
        ///<summary>This will set the HLA manufacturing data. Verifies the data.</summary>
        ///<param name="reporttext">status of the test</param>
        ///<param name="testDB">test result database instance</param>
        ///<param name="LimitsData.stepsItem">test parameters</param>
        ///<param name="uutData">data for the uut</param>
        ///<returns>
        ///  2  ErrorCodes.Program.DUTComError
        ///  3  ErrorCodes.Program.DutUnexpectedCmdReturn
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        ///</returns>
        public int SetHlaManData(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, bool newdata, out string reporttext)
        {
            int status;

            testDB.CreateStepRecord(parameters.name, parameters.step_number);
            status = setHlaData(parameters, uutData, newdata, out reporttext);
            testDB.AddStepResult((status == 0) ? "PASS" : "FAIL", reporttext, status);
            if (status == 0)
            {
                testDB.AddStepRef("HLA_SN", uutData.topSN, "", "", "");
            }
            UpdateStatusWindow(reporttext + "\n");
            return status;
        }

        /// <summary>
        /// Will write out the hardware config bytes.
        /// </summary>
        /// <param name="LimitsData.stepsItem"></param>
        /// <param name="testDB"></param>
        /// <param name="reporttext"></param>
        /// <returns>
        /// -6  ErrorCodes.System.InvalidParameter
        ///  2  ErrorCodes.Program.DUTComError
        ///  6  ErrorCodes.Program.PCBADataMismatch
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </returns>
        public int SetHwConfigData(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, out string reporttext)
        {
            int status;
            string hwset = string.Empty;
            testDB.CreateStepRecord(parameters.name, parameters.step_number);
            status = setHwConfigData(parameters, ref hwset, out reporttext);
            testDB.AddStepResult((status == 0) ? "PASS" : "FAIL", reporttext, status);
            if (status == 0)
            {
                testDB.AddStepRef("HWConfig", hwset, "", "", "");
            }
            UpdateStatusWindow(reporttext + "\n");

            return status;

        }

        //---------------------------------
        // setAllManData
        ///<summary>This will set the all the manufacturing data.
        /// </summary>
        /// <returns>
        ///  2   ErrorCodes.Program.DUTComError
        ///  3  ErrorCodes.Program.DutUnexpectedCmdReturn
        ///  6  ErrorCodes.Program.PCBADataMismatch
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </returns>
        public int SetAllManData(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, bool newPcbaData, bool newHlaData, out string reporttext)
        {
            int status;
            string tempout = string.Empty;

            testDB.CreateStepRecord(parameters.name, parameters.step_number);
            fixture.SensorConsole.CommandRecieveTimeoutMS = 3000;
            fixture.SensorConsole.ClearBuffers();
            status = setPcbaManData(parameters, uutData, newPcbaData, out reporttext);   // program the pcba data
            if (status == 0)                              // if programed pcba ok
                status = setHlaData(parameters, uutData, newHlaData, out reporttext);               // program hla data
            if (status == 0)                              // if programed hla ok
            {
                testDB.AddStepRef("HLA_SN", uutData.topSN, "", "", "");
                status = setMACData(parameters, uutData, out reporttext);
            }
            if (status == 0)                              // if programed mac ok
            {
                testDB.AddStepRef("PCBA_MAC1", uutData.mac, "", "", "");
                testDB.AddStepRef("PCBA_MAC2", uutData.mac2, "", "", "");
                status = setHwConfigData(parameters, ref tempout, out reporttext);
            }
            if (status == 0)                              // if programed hw config ok
            {
                testDB.AddStepRef("HWConfig", tempout, "", "", "");
                if (uutData.mac2 == string.Empty)
                    UpdateStatusWindow(string.Format("Unit set to:\n\tPCBA SN: {0}  PN: {1}  MAC: {2}\n\tHLA  SN: {3}  PN: {4}  Model: {5}\n",
                                                uutData.pcbaSN, uutData.pcbaPN, uutData.mac, uutData.topSN, uutData.topPN, uutData.model));
                else
                    UpdateStatusWindow(string.Format("Unit set to:\n\tPCBA SN: {0}  PN: {1}  MAC: {2}:{6}\n\tHLA  SN: {3}  PN: {4}  Model: {5}\n",
                                                uutData.pcbaSN, uutData.pcbaPN, uutData.mac, uutData.topSN, uutData.topPN, uutData.model, uutData.mac2));
                UpdateStatusWindow("HW set: " + tempout + "\n");
            }
            testDB.AddStepResult((status == 0) ? "PASS" : "FAIL", reporttext, status);

            return status;

        }

        /// <summary>
        /// Flash the code into the DUT. Optionaly can pass a file name.
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="testDB"></param>
        /// <param name="reporttext"></param>
        /// <returns>
        /// -15  ErrorCodes.System.FixtureError
        /// 17  ErrorCodes.Program.DUTHardwareError
        /// </returns>
        public int FlashDUTCode(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, ref string reporttext)
        {
            int status = 0;

            testDB.CreateStepRecord(parameters.name, parameters.step_number);
            if (fixture.ControlDUTPower(FixtureFunctions.PowerControl.ON, 500) != 0)             // turn on the DUT power
            {
                status = fixture.LastFunctionStatus;
                reporttext = fixture.LastFunctionErrorMessage;
            }
            if (status == 0)
            {
                Flasher flasher = new Flasher(stationConfig.ConfigFlasherComPort);
                if (!flasher.Open())
                {
                    status = (int)ErrorCodes.System.FlasherBase - flasher.LastErrorCode;
                    reporttext = flasher.LastErrorMsg;
                }
                else
                {
                    if (!flasher.ProgramDevice("", 10, 1))
                    {
                        // try a second time
                        fixture.ControlDUTPower(FixtureFunctions.PowerControl.OFF, 1000);
                        fixture.ControlDUTPower(FixtureFunctions.PowerControl.ON, 1000);
                        if (!flasher.ProgramDevice("", 10, 1))
                        {
                            status = (int)ErrorCodes.System.FlasherBase - flasher.LastErrorCode;
                            reporttext = flasher.LastErrorMsg;
                        }
                    }
                }
                flasher.Close();
                fixture.ControlDUTPower(FixtureFunctions.PowerControl.OFF, 0);
            }
            testDB.AddStepResult((status == 0) ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED, reporttext, status);
            UpdateStatusWindow(reporttext + "\n");
            return status;
        }


        //-------------------------------------------------
        // CheckContolOutput
        ///<summary>Will verify that the control voltage output is working. Will record the results.
        ///</summary>
        ///<remarks>
        /// Voltage limits are optional. If not set, defaults are as follows:
        ///     0 volt - 0 to 0.5
        ///     5 volt - 4.5 to 5.5
        ///     10 volt - 9.5 to 10.5
        ///     
        ///  will do the following test:
        ///     set both to 0 volts - verify the 0VoltLimit for both outputs
        ///     set dim1 to 5 volts, dim2 to 0 volts - verify the 5VoltLimit for dim1, 0VoltLimit for dim2
        ///     set dim1 to 10 volts, dim2 to 0 volts - verify the 10VoltLimit for dim1, 0VoltLimit for dim2
        ///     set dim1 to 0 volts, dim2 to 5v - verify the 0VoltLimit for dim1, 5VoltLimit for dim2
        ///     set dim1 to 0 volts, dim2 to 10v - verify the 0VoltLimit for dim1, 10VoltLimit for dim2
        ///     set dim1 and dim2 to 0 volts
        ///     
        ///  it will not test any other conditions once a fail is found.
        ///</remarks>
        ///<param name="parameters">test stucture</param>
        ///<param name="testDB">object of result recording</param>
        ///<param name="resultmessage">error message</param>
        ///<returns>
        ///     ErrorCodes.Program.Success                  0
        ///     ErrorCodes.System.InvalidParameter          -6
        ///  1  ErrorCodes.Program.ValueOutsideOfLimit
        ///  2  ErrorCodes.Program.DUTComError
        ///  3  ErrorCodes.Program.DutUnexpectedCmdReturn
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        ///</returns>
        public int CheckControlOutput(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, ref string resultmessage)
        {
            LimitsData.limitsItem limit0V;
            LimitsData.limitsItem limit5V;
            LimitsData.limitsItem limit10V;

            int testStatus = 0;
            string cmd = string.Empty;
            string cmdVolts = string.Empty;
            string outmsg = string.Empty;
            string reportText = string.Empty;
            double readdim1 = 0;
            double readdim2 = 0;
            int settleTime = 1000;
            string singleFailmsg = string.Empty;

            int testNumber = parameters.step_number;
            resultmessage = string.Empty;
            testDB.CreateStepRecord(parameters.name, testNumber);      // create the test record header.

            // get the timeout values
            if ((cmd = parameters.GetParameterValue("SETSETTLETIMEMS")) != string.Empty)
            {
                if (!Int32.TryParse(cmd, out settleTime))    // if bad number format
                {
                    testStatus = (int)ErrorCodes.System.ParameterFormatError;
                    resultmessage = "Invalid SetSettleTimeMS value. Can only be interger. " + cmd;
                    testDB.AddStepResult("FAIL", resultmessage, testStatus);
                    return testStatus;
                }
            }

            // make default limit values
            limit0V = new LimitsData.limitsItem() { name = "0VoltLimit", type = "Range", operation = "GELE", value1 = "0.0", value2 = "0.8"};
            limit5V = new LimitsData.limitsItem() { name = "5VoltLimit", type = "Range", operation = "GELE", value1 = "4.5", value2 = "5.5"};
            limit10V = new LimitsData.limitsItem() { name = "10VoltLimit", type = "Range", operation = "GELE", value1 = "9.5", value2 = "10.5" }; ;

            // get the voltage limits if they exist
            testStatus = 0;

            if (parameters.GetLimitValues("0VoltLimit").name != null)
                limit0V = parameters.GetLimitValues("0VoltLimit");
            if (parameters.GetLimitValues("5VoltLimit").name != null)
                limit5V = parameters.GetLimitValues("5VoltLimit");
            if (parameters.GetLimitValues("10VoltLimit").name != null)
                limit10V = parameters.GetLimitValues("10VoltLimit");

            // now have the voltage limits
            // start the test
            testStatus = 0;

            UpdateStatusWindow(" set to 0,0\n");
            // set both to 0 volts
            if (!fixture.SensorConsole.SetDimVoltage(0, 0))
            {
                testStatus = fixture.SensorConsole.LastErrorCode;
                resultmessage = fixture.SensorConsole.LastErrorMessage;
            }
            // verify in range
            if (testStatus == 0)
            {
                Thread.Sleep(settleTime);
                try
                {
                    readdim1 = fixture.ReadOneMeasurement("DIM1_DC") * stationConfig.ConfigLjChannelControlVGain;
                    readdim2 = fixture.ReadOneMeasurement("DIM2_DC") * stationConfig.ConfigLjChannelControlVGain;
                    testDB.AddStepRef("Set0V0V", readdim1.ToString("N1") + "," + readdim2.ToString("N1"), "V", "N1", "");
                    if (!CheckResultValue(readdim1, limit0V, ref singleFailmsg) | !CheckResultValue(readdim2, limit0V, ref singleFailmsg))
                    {
                        testStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        resultmessage = "Setting both to 0v. One or more wrong. dim1=" + readdim1.ToString("N1") + " dim2=" + readdim2.ToString("N1");
                    }
                }
                catch (Exception ex)
                {
                    testStatus = (int)ErrorCodes.System.InvalidLimitOperation;
                    resultmessage = ex.Message;
                }
            }

            // set dim1=5 dim2=0 volts
            UpdateStatusWindow(" set to 5,0\n");
            if (!fixture.SensorConsole.SetDimVoltage(50, 0))
            {
                testStatus = fixture.SensorConsole.LastErrorCode;
                resultmessage = fixture.SensorConsole.LastErrorMessage;
            }
            // verify in range
            if (testStatus == 0)
            {
                Thread.Sleep(settleTime);
                try
                {
                    readdim1 = fixture.ReadOneMeasurement("DIM1_DC") * stationConfig.ConfigLjChannelControlVGain;
                    readdim2 = fixture.ReadOneMeasurement("DIM2_DC") * stationConfig.ConfigLjChannelControlVGain;
                    testDB.AddStepRef("Set5V0V", readdim1.ToString("N1") + "," + readdim2.ToString("N1"), "V", "N1", "");
                    if (!CheckResultValue(readdim1, limit5V, ref singleFailmsg) | !CheckResultValue(readdim2, limit0V, ref singleFailmsg))
                    {
                        testStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        resultmessage = "Setting dim1=5 dim2=0. One or more wrong. dim1=" + readdim1.ToString("N1") + " dim2=" + readdim2.ToString("N1");
                    }
                }
                catch (Exception ex)
                {
                    testStatus = (int)ErrorCodes.System.InvalidLimitOperation;
                    resultmessage = ex.Message;
                }
            }

            // set dim1=10 dim2=0 volts
            UpdateStatusWindow(" set to 10,0\n");
            if (!fixture.SensorConsole.SetDimVoltage(100, 0))
            {
                testStatus = fixture.SensorConsole.LastErrorCode;
                resultmessage = fixture.SensorConsole.LastErrorMessage;
            }
            // verify in range
            if (testStatus == 0)
            {
                Thread.Sleep(settleTime);
                try
                {
                    readdim1 = fixture.ReadOneMeasurement("DIM1_DC") * stationConfig.ConfigLjChannelControlVGain;
                    readdim2 = fixture.ReadOneMeasurement("DIM2_DC") * stationConfig.ConfigLjChannelControlVGain;
                    testDB.AddStepRef("Set10V0V", readdim1.ToString("N1") + "," + readdim2.ToString("N1"), "V", "N1", "");
                    if (!CheckResultValue(readdim1, limit10V, ref singleFailmsg) | !CheckResultValue(readdim2, limit0V, ref singleFailmsg))
                    {
                        testStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        resultmessage = "Setting dim1=10 dim2=0. One or more wrong. dim1=" + readdim1.ToString("N1") + " dim2=" + readdim2.ToString("N1");
                    }
                }
                catch (Exception ex)
                {
                    testStatus = (int)ErrorCodes.System.InvalidLimitOperation;
                    resultmessage = ex.Message;
                }
            }

            // set dim1=0 dim2=5 volts
            UpdateStatusWindow(" set to 0,5\n");
            if (!fixture.SensorConsole.SetDimVoltage(0, 50))
            {
                testStatus = fixture.SensorConsole.LastErrorCode;
                resultmessage = fixture.SensorConsole.LastErrorMessage;
            }
            // verify in range
            if (testStatus == 0)
            {
                Thread.Sleep(settleTime);
                try
                {
                    readdim1 = fixture.ReadOneMeasurement("DIM1_DC") * stationConfig.ConfigLjChannelControlVGain;
                    readdim2 = fixture.ReadOneMeasurement("DIM2_DC") * stationConfig.ConfigLjChannelControlVGain;
                    testDB.AddStepRef("Set0V5V", readdim1.ToString("N1") + "," + readdim2.ToString("N1"), "V", "N1", "");
                    if (!CheckResultValue(readdim1, limit0V, ref singleFailmsg) | !CheckResultValue(readdim2, limit5V, ref singleFailmsg))
                    {
                        testStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        resultmessage = "Setting dim1=0 dim2=5. One or more wrong. dim1=" + readdim1.ToString("N1") + " dim2=" + readdim2.ToString("N1");
                    }
                }
                catch (Exception ex)
                {
                    testStatus = (int)ErrorCodes.System.InvalidLimitOperation;
                    resultmessage = ex.Message;
                }
            }

            // set dim1=0 dim2=10 volts
            UpdateStatusWindow(" set to 01,0\n");
            if (!fixture.SensorConsole.SetDimVoltage(0, 100))
            {
                testStatus = fixture.SensorConsole.LastErrorCode;
                resultmessage = fixture.SensorConsole.LastErrorMessage;
            }
            // verify in range
            if (testStatus == 0)
            {
                Thread.Sleep(settleTime);
                try
                {
                    readdim1 = fixture.ReadOneMeasurement("DIM1_DC") * stationConfig.ConfigLjChannelControlVGain;
                    readdim2 = fixture.ReadOneMeasurement("DIM2_DC") * stationConfig.ConfigLjChannelControlVGain;
                    testDB.AddStepRef("Set0V10V", readdim1.ToString("N1") + "," + readdim2.ToString("N1"), "V", "N1", "");
                    if (!CheckResultValue(readdim1, limit0V, ref singleFailmsg) | !CheckResultValue(readdim2, limit10V, ref singleFailmsg))
                    {
                        testStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        resultmessage = "Setting dim1=0 dim2=10. One or more wrong. dim1=" + readdim1.ToString("N1") + " dim2=" + readdim2.ToString("N1");
                    }
                }
                catch (Exception ex)
                {
                    testStatus = (int)ErrorCodes.System.InvalidLimitOperation;
                    resultmessage = ex.Message;
                }
            }

            // set both to 0
            if (!fixture.SensorConsole.SetDimVoltage(0, 0))
            {
                testStatus = fixture.SensorConsole.LastErrorCode;
                resultmessage = fixture.SensorConsole.LastErrorMessage;
            }

            testDB.AddStepResult((testStatus == 0) ? ResultData.TEST_PASSED : ResultData.TEST_FAILED, resultmessage, testStatus);

            return testStatus;
        }

        /// <summary>
        /// Test all the sensors on the board.
        /// <remarks>
        ///  Turn off the light
        ///  Gather the sensor data with the light off
        ///  Gather the sensor data with the light on
        ///  Turn off the light
        ///  Test the various sensors according to the list of limits
        ///  
        ///     Limit param="PIROff"
        ///     Limit param="PIROn"
        ///     Limit param="PIROnDiff"
        ///     Limit param="AMBVisableOff"
        ///     Limit param="AMBVisableOn"
        ///     Limit param="AMBIROff"
        ///     Limit param="AMBIROn"
        ///     
        /// If the AMBSource is LED, then the ambient sensor test will be done with the
        /// Lamp off. If it is LAMP then both ambient and PIR will be done at the same time.
        /// </remarks>
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="testDB"></param>
        /// <param name="resultmessage"></param>
        /// <returns>
        ///  -6  ErrorCodes.System.InvalidParameter
        /// </returns>
        public int AllSensorTest(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, ref string resultmessage)
        {
            SUSensorData sdataOff = new SUSensorData();
            SUSensorData sdataOn1 = new SUSensorData();
            SUSensorData sdataOn2 = new SUSensorData();

            int testStatus = 0;
            int stepStatus = 0;
            double testvalue = 0;
            string stepresult = string.Empty;
            int maxRetrys = 1;
            string smaxRetrys = string.Empty;
            int reTrysDone = 0;

            resultmessage = string.Empty;
            if ((smaxRetrys = parameters.GetParameterValue("RETRYS")) != string.Empty)
            {
                if (!int.TryParse(smaxRetrys, out maxRetrys))
                {
                    testStatus = (int)ErrorCodes.System.ParameterFormatError;
                    resultmessage = "Retrys must be a interger value";
                    UpdateStatusWindow(resultmessage + "\n");
                    return testStatus;
                }
            }


            int testNumber = parameters.step_number;
            testDB.CreateStepRecord(parameters.name, testNumber);    // make the header for the test result

            do
            {
                if (reTrysDone != 0)   // if failure and still have retrys
                {
                    testDB.AddStepRef("Retry", reTrysDone.ToString(), "", "", "");
                }
                testStatus = 0;
                // first get all the sensor data with lamp and led off
                if (fixture.ControlPIRSource(FixtureFunctions.PowerControl.OFF) != 0)                                        // just make sure it is off
                {
                    resultmessage = fixture.LastFunctionErrorMessage;
                    testStatus = fixture.LastFunctionStatus;
                    testDB.AddStepResult(ResultData.TEST_FAILED, resultmessage, testStatus);
                    return fixture.LastFunctionStatus;
                }
                if (!fixture.SensorConsole.SetLeds(false, false, false))          // make sure LEDs are off
                {
                    resultmessage = fixture.SensorConsole.LastErrorMessage;
                    testStatus = fixture.SensorConsole.LastErrorCode;
                    testDB.AddStepResult(ResultData.TEST_FAILED, resultmessage, testStatus);
                    return testStatus;
                }
                if (fixture.SetAmbientSensorLED(0) != 0)                                        // just make sure it is off
                {
                    resultmessage = fixture.LastFunctionErrorMessage;
                    testStatus = fixture.LastFunctionStatus;
                    testDB.AddStepResult(ResultData.TEST_FAILED, resultmessage, testStatus);
                    return fixture.LastFunctionStatus;
                }

                // get the dark readings
                if (!fixture.SensorConsole.GetSensorData(ref sdataOff.PIR, ref sdataOff.AmbVisable, ref sdataOff.Temperature))
                {
                    resultmessage = fixture.SensorConsole.LastErrorMessage;
                    testStatus = fixture.SensorConsole.LastErrorCode;
                    testDB.AddStepResult(ResultData.TEST_FAILED, resultmessage, testStatus);
                    return testStatus;
                }
                
               
                if (parameters.DoesLimitExist("PIRIRON") | parameters.DoesLimitExist("PIRIRONDIFF"))
                {
                    // PIR is source is always the lamp, so turn on the lamp and read
                    fixture.ControlPIRSource(FixtureFunctions.PowerControl.ON);
                }
                if (parameters.DoesLimitExist("AMBVISABLEON"))
                {
                    fixture.SetAmbientSensorLED(stationConfig.ConfigAmbientDetectorLightSource.Voltage);
                }

                Thread.Sleep(Math.Max(stationConfig.ConfigPIRDetectorLightSource.OnDelayMs, stationConfig.ConfigAmbientDetectorLightSource.OnDelayMs));
                fixture.SensorConsole.GetSensorData(ref sdataOn2.PIR, ref sdataOn2.AmbVisable, ref sdataOn2.Temperature);
                fixture.ControlPIRSource(0);
                fixture.SetAmbientSensorLED(0);

                // now is the time to figure out what results are need
                foreach (var item in parameters.limits)
                {
                    if (item.name.ToUpper().Contains("PIRIROFF"))
                        testvalue = sdataOff.PIR;
                    if (item.name.ToUpper().Contains("PIRIRON"))
                        testvalue = sdataOn2.PIR;
                    if (item.name.ToUpper().Contains("TEMP"))
                        testvalue = sdataOn2.Temperature;
                    if (item.name.ToUpper().Contains("PIRIRONDIFF"))
                        testvalue = Math.Abs((int)sdataOn2.PIR - (int)sdataOff.PIR);
                    if (item.name.ToUpper().Contains("AMBVISABLEOFF"))
                        testvalue = sdataOff.AmbVisable;
                    if (item.name.ToUpper().Contains("AMBVISABLEON"))
                        testvalue = sdataOn2.AmbVisable;

                    if (!CheckResultValue(testvalue, item, ref stepresult))
                        stepStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                    else
                        stepStatus = 0;
                    resultmessage = resultmessage + "\t  " + stepresult;
                    if (stepStatus != 0)      // if step fail, set the test as fail
                        testStatus = stepStatus;
                    if (item.type.ToUpper().Equals("RANGE"))
                        testDB.AddStepResult(item.name, (stepStatus == 0) ? "PASS" : "FAIL", item.operation,
                                        Convert.ToDouble(item.value1), Convert.ToDouble(item.value2), testvalue, "", "", stepresult, stepStatus);
                    else
                        testDB.AddStepResult(item.name, (stepStatus == 0) ? "PASS" : "FAIL", item.operation,
                                        Convert.ToDouble(item.value1), testvalue, "", "", stepresult, stepStatus);
                }

            } while ((testStatus != 0) & (reTrysDone++ < maxRetrys));

            UpdateStatusWindow(resultmessage + "\n");
            
            return testStatus;
        }

        //-------------------------------------------------
        // CalibrateCPUTemperature
        /// <summary>
        /// Will calibrate the CPU Temperature.
        /// </summary>
        /// <param name="parameters">test parameters</param>
        /// <param name="testDB">result database</param>
        /// <param name="reporttext">messages if the test fail</param>
        /// <returns>
        ///     0           ErrorCodes.Program.Success
        ///     2           ErrorCodes.Program.DUTComError
        ///     100025      ErrorCodes.System.ResultDBFailure
        ///     100028      ErrorCodes.System.LabJackReadError
        /// </returns>
        public int CalibrateCPUTemperature(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, ref string reporttext)
        {
            double probeTemp = 0;
            uint rawtemp = 0;
            uint prgtemp = 0;

            int stepStatus = 0;

            reporttext = string.Empty;
            int testNumber = parameters.step_number;
            testDB.CreateStepRecord(parameters.name, testNumber);      // create the test record header.

            probeTemp = fixture.ReadTemperatureProbe();
            if (fixture.LastFunctionStatus != 0)    // system error
            {
                testDB.AddStepResult("FAIL", reporttext, stepStatus);
                return stepStatus;
            }

            if (!fixture.SensorConsole.PreformTempCal((uint)Math.Truncate(probeTemp), ref rawtemp))
            {
                reporttext = fixture.SensorConsole.LastErrorMessage;
                stepStatus = fixture.SensorConsole.LastErrorCode;
            }

            testDB.AddStepResult((stepStatus == 0) ? "PASS" : "FAIL", reporttext, stepStatus);
            testDB.AddStepRef("RawTempReading", rawtemp.ToString(), "", "", "");
            prgtemp = (uint)Math.Truncate(probeTemp);
            testDB.AddStepRef("CalTemp", prgtemp.ToString() , "C", "", "");
            UpdateStatusWindow("Caled to probe temp " + (uint)Math.Truncate(probeTemp) + " C");

            return stepStatus;
        }

        //----------------------------------------
        // LEDTestManual
        /// <summary>
        /// Will do a manual LED test. It will require the operator to open the
        /// box and verify that the LEDs are working.
        /// </summary>
        /// <param name="name">string - name of the test to record in the result database.</param>
        /// <param name="testDB">FixtureDatabase.ResultDatabase - result database</param>
        /// <param name="reportText">string - if failure, details of failure</param>
        /// <returns>bool - true = passed</returns>
        public int LEDTestManual(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, out string reportText)
        {
            int status = 0;

            reportText = string.Empty;

            int testNumber = parameters.step_number;
            testDB.CreateStepRecord(parameters.name, testNumber);

            if (!fixture.SensorConsole.SetLeds(true, false, false))      // turn on red
            {
                reportText = fixture.SensorConsole.LastErrorMessage;
                status = fixture.SensorConsole.LastErrorCode;
                testDB.AddStepResult("FAIL", reportText, status);
                return status;
            }
            var resultR = MessageBox.Show("Open the test box and look at the RED LED. Is it on?", "RED LED Test", MessageBoxButtons.YesNo);
            if (!fixture.SensorConsole.SetLeds(false, true, false))
            {
                reportText = fixture.SensorConsole.LastErrorMessage;    // there was a error
                status = fixture.SensorConsole.LastErrorCode;
                testDB.AddStepResult("FAIL", reportText, status);
            }
            var resultG = MessageBox.Show("Is the GREEN LED on?", "Greem LED Test", MessageBoxButtons.YesNo);
            if ((resultR == DialogResult.No) | (resultG == DialogResult.No))
                status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
            if (resultR == DialogResult.No)
                reportText = "Red LED failed. ";
            else
                reportText = "Red LED passed. ";
            if (resultG == DialogResult.No)
                reportText = reportText + "Green LED failed.";
            else
                reportText = reportText + "Green LED passed.";
            UpdateStatusWindow(reportText + "\n");
            testDB.AddStepResult((status == 0) ? "PASS" : "FAIL", reportText, status);
            return status;
        }

        //----------------------------------------
        // LEDTest
        /// <summary>
        /// Will do a LED test.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <param name="name">string - name of the test to record in the result database.</param>
        /// <param name="testDB">FixtureDatabase.ResultDatabase - result database</param>
        /// <param name="reportText">string - if failure, details of failure</param>
        /// <returns>bool - true = passed</returns>

        public int LEDTest(LimitsData.stepsItem testParams, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData,
                            out string reportText)
        {
            int testStatus = 0;
            int stepStatus = 0;
            int freqStatus = 0;
            int intensityStatus = 0;
            int i;
            int lowFvalue = -1;
            int highFvalue = -1;
            int lowIvalue = -1;
            int highIvalue = -1;
            string statusString = string.Empty;
            int iledFreq = 0;
            int iledIntensity = 0;
            string testName = testParams.name;
            string ledcmd = string.Empty;
            string outmsg = string.Empty;
            DateTime startTime;
            int timeOutMs = 500;
            int turnOnDelayMS = 100;                // delay between issuing the LED command before doing the read test 

            string[] ledColors = {"RED", "GREEN", "BLUE"};
            bool[,] cmdValues =
            {
                {true, false, false},
                {false, true, false},
                {false, false, true}
            };
            bool[,] ledTests = 
            {   {false, false},         // Red - color, intensity
                {false, false},         // Green
                {false, false},         // Blue
            };
            int[,] colorLimits =       // will hold the color limits
            {
                {-1, -1 },
                {-1, -1 },
                {-1, -1 },
            };
            int[,] intesityLimits =       // will hold the intesity limits
            {
                {-1, -1 },
                {-1, -1 },
                {-1, -1 },
            };

            bool found = false;

            int testNumber = testParams.step_number;
            testDB.CreateStepRecord(testParams.name, testNumber);

            if (!fixture.SensorConsole.SetLeds(false, false, false))         // make sure all the LEDs are off
            {
                testStatus = fixture.SensorConsole.LastErrorCode;            // there was a error sending the command
                reportText = fixture.SensorConsole.LastErrorMessage;
                testDB.AddStepResult(ResultData.TEST_FAILED, reportText, testStatus);
                return testStatus;
            }

            // figure out what test need to be done
            i = 0;  // parameter number
            foreach (var item in testParams.limits)
            {
                found = false;
                if (item.name.ToUpper().Contains("COLOR") | item.name.Contains("INT"))
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (item.name.ToUpper().Contains(ledColors[j]))    // if found a led type/color
                        {
                            if (item.name.ToUpper().Contains("COLOR"))
                            {
                                ledTests[j, 0] = true;
                                colorLimits[j, 0] = Convert.ToInt32(item.value1);
                                colorLimits[j, 1] = Convert.ToInt32(item.value2);
                                found = true;
                            }
                            if (item.name.ToUpper().Contains("INT"))
                            {
                                ledTests[j, 1] = true;
                                intesityLimits[j, 0] = Convert.ToInt32(item.value1);
                                intesityLimits[j, 1] = Convert.ToInt32(item.value2);
                                found = true;
                            }
                        }
                    }
                    if (!found)
                    {
                        reportText = "Parameter " + item.name + " is invalid.";
                        testStatus = (int)ErrorCodes.System.MissingParameter;
                        testDB.AddStepResult(ResultData.TEST_FAILED, reportText, testStatus);
                        return testStatus;
                    }
                    i++;
                }
                if (item.name.ToUpper().Equals("TURNONDELAYMS"))
                {
                    if (!int.TryParse(item.value1, out turnOnDelayMS))
                    {
                        reportText = "Parameter TurnONDelayMS is invalid. It must be a interger.";
                        testStatus = (int)ErrorCodes.System.InvalidLimitOperation;
                        testDB.AddStepResult(ResultData.TEST_FAILED, reportText, testStatus);
                        return testStatus;
                    }
                }
            }

            // cycle through the 3 colors and get data
            reportText = string.Empty;
            for (i = 0; i < 3; i++)
            {
                string lclreportText = string.Empty;
                stepStatus = 0;

                if (ledTests[i, 0] | ledTests[i, 1])      // if this color is to test color or intensity
                {
                    UpdateStatusWindow("Tesing LED " + ledColors[i]);

                    if (!fixture.SensorConsole.SetLeds(cmdValues[i,0], cmdValues[i,1], cmdValues[i,2])) // issue command to turn on led
                    {
                        reportText = fixture.SensorConsole.LastErrorMessage;                // if command failed
                        testDB.AddStepResult(ledColors[i], "FAIL", reportText, testStatus);
                        return testStatus;
                    }
                    Thread.Sleep(200);              // let the command take effect
                    if (ledTests[i, 0])      // if there is a freq test
                    {
                        lowFvalue = colorLimits[i, 0];
                        highFvalue = colorLimits[i, 1];
                    }
                    else
                    {
                        lowFvalue = -1;
                        highFvalue = -1;
                    }
                    if (ledTests[i, 1])      // if there is a intensity test
                    {
                        lowIvalue = intesityLimits[i, 0];
                        highIvalue = intesityLimits[i, 1];
                    }
                    else
                    {
                        lowIvalue = -1;
                        highIvalue = -1;
                    }
                    startTime = DateTime.Now;
                    do
                    {
                        stepStatus = fixture.CheckLedValues(lowFvalue, highFvalue, lowIvalue, highIvalue,
                                                        ref freqStatus, ref intensityStatus, ref iledFreq, ref iledIntensity, ref lclreportText);
                    } while ((stepStatus != 0) & (DateTime.Now < startTime.AddMilliseconds(timeOutMs)));

                    if (reportText == string.Empty)
                        reportText = lclreportText;
                    else
                        reportText = reportText + "\n" + lclreportText;
                    if (stepStatus < 0)         // if there was a system error
                    {
                        testDB.AddStepResult("FAIL", reportText, testStatus);
                        return testStatus;
                    }
                    if (stepStatus != 0)
                    {
                        testStatus = stepStatus;
                    }
                    if (ledTests[i, 0])         // color test
                    {
                        UpdateStatusWindow(" - Freq: " + ((0 == freqStatus) ? "PASS" : "FAIL"),
                                            (0 == freqStatus) ? StatusType.passed : StatusType.failed);
                        testDB.AddStepResult(ledColors[i] + "Color", (0 == freqStatus) ? "PASS" : "FAIL",
                            "GELE",
                            lowFvalue, highFvalue,
                            iledFreq, "", "", reportText, freqStatus);
                    }
                    if (ledTests[i, 1])
                    {
                        UpdateStatusWindow(" - Intensity: " + ((0 == intensityStatus) ? "PASS" : "FAIL"),
                                            (0 == intensityStatus) ? StatusType.passed : StatusType.failed);
                        testDB.AddStepResult(ledColors[i] + "Intensity", (0 == intensityStatus) ? "PASS" : "FAIL",
                            "GELE",
                            lowIvalue, highIvalue,
                            iledIntensity, "", "", reportText, intensityStatus);
                    }
                    // make sure all leds are off at end of the test
                    fixture.SensorConsole.SetLeds(false, false, false);
                    UpdateStatusWindow("\n");
                }
            }

            return testStatus;
        }




        //-----------------------------------------
        // CheckPOST
        /// <summary>
        /// Will verify that the POST message returns a pass.
        /// </summary>
        /// <param name="reporttext">string - returns failure message if there is a error.</param>
        /// <returns>status code.</returns>
        public int CheckPOST(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, out string reporttext)
        {
            bool status;
            int postStatus = 0;
            int errorCode = 0;
            UInt32 savedCRC = 0;
            UInt32 calCRC = 0;

            testDB.CreateStepRecord(parameters.name, parameters.step_number);
            reporttext = string.Empty;
            status = fixture.SensorConsole.PerformPost(ref postStatus, ref savedCRC, ref calCRC);
            if (!status)      // if failed to send/recive
            {
                reporttext = fixture.SensorConsole.LastErrorMessage;
                errorCode = fixture.SensorConsole.LastErrorCode;
                testDB.AddStepResult( "FAIL", reporttext, errorCode);
            }
            else        // good send/recive
            {
                if (postStatus != 0)  // if the status is fail
                {
                    reporttext = "Error: POST returned fail code " + postStatus.ToString() + " saved CRC: " + savedCRC.ToString("X") + " calc CRC: " + calCRC.ToString("X");
                    errorCode = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                    testDB.AddStepResult("FAIL", reporttext, errorCode);
                }
                else                // the status is good
                    testDB.AddStepResult("PASS", "", errorCode);
            }

            return errorCode;
        }

        //-------------------------------------------------
        // ChangeOver
        /// <summary> 
        /// Will do the change over function.
        /// If LEDPattern is defined, will wait for led pattern.
        /// </remarks>
        /// <param name="reporttext">string - output of the dut if there was a error.</param>
        /// <returns>bool - true = all messages found</returns>
        public int ChangeOver(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, out string reporttext)
        {
            bool status;
            int postStatus = 0;
            int errorCode = 0;
            int timeOutMs = 2000;                               // default timeout for response 3s
            int offDelay = 5000;                                 // delay after change over
            string redColor = string.Empty;
            string greenColor = string.Empty;
            string blueColor = string.Empty;
            string ledPattern = string.Empty;
            int ledIntesity = 0;
            int ledFreq = 0;
            string manualMessage = string.Empty;

            if (parameters.parameters != null)
            {
                foreach (var item in parameters.parameters)
                {
                    if (item.name.ToUpper() == "SUTIMEOUTSEC")
                    {
                        if (!Int32.TryParse(item.value, out timeOutMs))    // if bad number format
                        {
                            reporttext = "Invalid SUTimeoutSed value. Can only be interger. " + item.value;
                            return (int)ErrorCodes.System.ParameterFormatError;
                        }
                        timeOutMs = timeOutMs * 1000;
                    }
                    if (item.name.ToUpper() == "DELAYOFFSEC")
                    {
                        if (!Int32.TryParse(item.value, out offDelay))    // if bad number format
                        {
                            reporttext = "Invalid DelayOffSec value. Can only be interger. " + item.value;
                            return (int)ErrorCodes.System.ParameterFormatError;
                        }
                        offDelay = offDelay * 1000;
                    }
                    if (item.name.ToUpper() == "MANUALMESSAGE")
                    {
                        manualMessage = item.value;
                    }
                    if (item.name.ToUpper() == "REDCOLOR")
                    {
                        redColor = item.value;
                    }
                    if (item.name.ToUpper() == "GREENCOLOR")
                    {
                        greenColor = item.value;
                    }
                    if (item.name.ToUpper() == "BLUECOLOR")
                    {
                        blueColor = item.value;
                    }
                    if (item.name.ToUpper() == "LEDPATTERN")
                    {
                        ledPattern = item.value;
                    }
                }
            }

            testDB.CreateStepRecord(parameters.name, parameters.step_number);
            reporttext = string.Empty;
            status = fixture.SensorConsole.PreformChangeOver(timeOutMs, ref postStatus);
            if (!status)      // if failed to send/recive
            {
                reporttext = fixture.SensorConsole.LastErrorMessage;
                errorCode = fixture.SensorConsole.LastErrorCode;
                testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, reporttext, errorCode);
                return errorCode;
            }
            else        // good send/recive
            {
                if (postStatus != 0)  // if the status is fail
                {
                    reporttext = fixture.SensorConsole.LastErrorMessage;
                    errorCode = fixture.SensorConsole.LastErrorCode;
                    testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, reporttext, errorCode);
                    return errorCode;
                }
            }

            // Check for manual version first
            if (ledPattern.ToUpper() == "MANUAL")
            {
                UpdateStatusWindow("Waiting for bootup...");
                Thread.Sleep(offDelay);
                UpdateStatusWindow("Checking LED Pattern.");
                var yn = MessageBox.Show(manualMessage, "LED Check", MessageBoxButtons.YesNo);
                if (yn == DialogResult.No)
                {
                    reporttext = "Did not find LED pattern";
                    errorCode = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                }
                ledPattern = string.Empty;      // set so auto mode does not work
            }

            // see if checking for led pattern
            if (ledPattern != string.Empty)
            {
                string testColor = string.Empty;
                string testFreqs = string.Empty;
                DateTime startTime;
                for (int i = 0; i < ledPattern.Length; i++)
                {
                    bool colorFound = false;
                    if (ledPattern.ToUpper()[i] == 'R')
                    {
                        testColor = "Red";
                        testFreqs = redColor;
                    }
                    if (ledPattern.ToUpper()[i] == 'G')
                    {
                        testColor = "Green";
                        testFreqs = greenColor;
                    }
                    if (ledPattern.ToUpper()[i] == 'B')
                    {
                        testColor = "Blue";
                        testFreqs = blueColor;
                    }
                    startTime = DateTime.Now;
                    UpdateStatusWindow("  Looking for " + testColor + " led...");
                    do
                    {
                        if (fixture.CheckLedColor(testColor, testFreqs, ref ledFreq, ref ledIntesity, ref reporttext) == 0)
                            colorFound = true;
                    } while (!colorFound & (DateTime.Now < startTime.AddMilliseconds(timeOutMs)));
                    if (!colorFound)
                    {
                        reporttext = "LED color " + testColor + " not found";
                        errorCode = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        UpdateStatusWindow("Not found\n;", StatusType.failed);
                        break;
                    }
                    else
                    {
                        UpdateStatusWindow("Found\n", StatusType.passed);
                        Thread.Sleep(offDelay);
                    }
                }
            }
            if (errorCode == 0)
            {
                testDB.AddStepResult(FixtureDatabase.ResultData.TEST_PASSED, "", 0);
            }
            else
                testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, "Did not find LED pattern", (int)ErrorCodes.Program.ValueOutsideOfLimit);

            return errorCode;

        }

        /// <summary>
        /// print a label.
        /// </summary>
        /// <param name="LimitsData.stepsItem"></param>
        /// <param name="testDB"></param>
        /// <param name="uutData"></param>
        /// <param name="printerName"></param>
        /// <param name="resultmessage"></param>
        /// <param name="lastMacBarcodeFileName"></param>
        /// <returns></returns>
        public int PrintPcbaLabel(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, string printerName,
                                    out string resultmessage, ref string lastMacBarcodeFileName)
        {
            string outmsg = string.Empty;
            int stepStatus = 0;
            string fullLabelName = lastMacBarcodeFileName;
            string printedData = string.Empty;
            int numLabelsToPrint = 1;

            resultmessage = string.Empty;
            testDB.CreateStepRecord(parameters.name, parameters.step_number);
            lastMacBarcodeFileName = parameters.GetParameterValue("BarcodeFile");
            if ( parameters.DoesParameterExist( "NumberOfLabelsToPrint"))
            {
                if (!int.TryParse(parameters.GetParameterValue("NumberOfLabelsToPrint"), out numLabelsToPrint))
                {
                    stepStatus = (int)ErrorCodes.System.ParameterFormatError;
                    resultmessage = "System Error: The NumberOfLabelsToPrint value is not a interger. Found:" + parameters.GetParameterValue("NumberOfLabelsToPrint");
                    testDB.AddStepResult("FAIL", resultmessage, stepStatus);
                }
            }

            //--------------------------------------------------------------------------//
            // read the test label file                                                 //
            // the fixture label directory will override the station limit directory.   //
            //--------------------------------------------------------------------------//
            if (stationConfig.ConfigLabelFileDirectory == string.Empty)     // if no directory is defined
            {
                String strAppDir = Path.GetDirectoryName(Application.ExecutablePath);
                fullLabelName = Path.Combine(strAppDir, lastMacBarcodeFileName);
            }
            else                                                            // directory is defined
            {
                fullLabelName = Path.Combine(stationConfig.ConfigLabelFileDirectory, lastMacBarcodeFileName);
            }


            if (!File.Exists(fullLabelName))
            {
                stepStatus = (int)ErrorCodes.System.MissingLabelFile;
                resultmessage = "System Error: The barcode file " + fullLabelName + " is missing.";
                testDB.AddStepResult("FAIL", resultmessage, stepStatus);
            }
            if (stepStatus == 0)
            {
                try
                {
                    var btengine = new Engine(true);
                    var btformat = btengine.Documents.Open(fullLabelName);
                    btformat.PrintSetup.PrinterName = printerName;
                    btformat.PrintSetup.IdenticalCopiesOfLabel = numLabelsToPrint;
                    foreach (var item in btformat.SubStrings.ToList())
                    {
                        if (item.Name == "MAC")
                        {
                            btformat.SubStrings["MAC"].Value = uutData.mac;
                            printedData = uutData.mac;
                        }
                        else if (item.Name == "SN")
                        {
                            btformat.SubStrings["SN"].Value = uutData.pcbaSN;
                            if (printedData != string.Empty)
                                printedData = printedData + ":";
                            printedData = printedData + uutData.pcbaSN;
                        }
                        else if (item.Name == "PCBA_SN")
                        {
                            btformat.SubStrings["PCBA_SN"].Value = uutData.pcbaSN;
                            if (printedData != string.Empty)
                                printedData = printedData + ":";
                            printedData = printedData + uutData.pcbaSN;
                        }
                        else if (item.Name == "Product_SN")
                        {
                            btformat.SubStrings["Product_SN"].Value = uutData.topSN;
                            if (printedData != string.Empty)
                                printedData = printedData + ":";
                            printedData = printedData + uutData.topSN;
                        }
                        else
                        {
                            stepStatus = (int)ErrorCodes.System.InvalidLabelFieldName;
                            resultmessage = "System Error: There is a invalid field(" + item.Name + ") in the barcode file.";
                            testDB.AddStepResult("FAIL", resultmessage, stepStatus);
                            return stepStatus;
                        }

                    }
                    btformat.Save();
                    Result result = btformat.Print();
                    btformat.Close(SaveOptions.DoNotSaveChanges);
                    btengine.Dispose();
                    resultmessage = "Label printed: " + printedData;
                    stepStatus = 0;
                    testDB.AddStepResult("PASS", resultmessage, stepStatus);
                }
                catch (Exception ex)
                {
                    stepStatus = (int)ErrorCodes.System.LabelPrintError;
                    resultmessage = "System Error: " + ex.Message;
                    testDB.AddStepResult("FAIL", resultmessage, stepStatus);
                }
            }
            return stepStatus;
        }

        public int PrintProductLabel(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, string printerName,
                                    out string resultmessage, ref string lastMacBarcodeFileName)
        {
            string outmsg = string.Empty;
            int stepStatus = 0;
            string fullLabelName = lastMacBarcodeFileName;
            string printedData = string.Empty;
            int numLabelsToPrint = 1;

            resultmessage = string.Empty;
            testDB.CreateStepRecord(parameters.name, parameters.step_number);
            lastMacBarcodeFileName = parameters.GetParameterValue("BarcodeFile");
            if (parameters.DoesParameterExist("NumberOfLabelsToPrint"))
            {
                if (!int.TryParse(parameters.GetParameterValue("NumberOfLabelsToPrint"), out numLabelsToPrint))
                {
                    stepStatus = (int)ErrorCodes.System.ParameterFormatError;
                    resultmessage = "System Error: The NumberOfLabelsToPrint value is not a interger. Found:" + parameters.GetParameterValue("NumberOfLabelsToPrint");
                    testDB.AddStepResult("FAIL", resultmessage, stepStatus);
                }
            }

            //--------------------------------------------------------------------------//
            // read the test label file                                                 //
            // the fixture label directory will override the station limit directory.   //
            //--------------------------------------------------------------------------//
            if (stationConfig.ConfigLabelFileDirectory == string.Empty)     // if no directory is defined
            {
                String strAppDir = Path.GetDirectoryName(Application.ExecutablePath);
                fullLabelName = Path.Combine(strAppDir, lastMacBarcodeFileName);
            }
            else                                                            // directory is defined
            {
                fullLabelName = Path.Combine(stationConfig.ConfigLabelFileDirectory, lastMacBarcodeFileName);
            }

            if (!File.Exists(fullLabelName))
            {
                stepStatus = (int)ErrorCodes.System.MissingLabelFile;
                resultmessage = "System Error: The barcode file " + fullLabelName + " is missing.";
                testDB.AddStepResult("FAIL", resultmessage, stepStatus);
            }
            if (stepStatus == 0)
            {
                try
                {
                    var btengine = new Engine(true);
                    var btformat = btengine.Documents.Open(fullLabelName);
                    btformat.PrintSetup.PrinterName = printerName;
                    btformat.PrintSetup.IdenticalCopiesOfLabel = numLabelsToPrint;
                    foreach (var item in btformat.SubStrings.ToList())
                    {
                        if (item.Name == "MAC")
                        {
                            btformat.SubStrings["MAC"].Value = uutData.mac;
                            printedData = uutData.mac;
                        }
                        else if (item.Name == "SN")
                        {
                            btformat.SubStrings["SN"].Value = uutData.topSN;
                            if (printedData != string.Empty)
                                printedData = printedData + ":";
                            printedData = printedData + uutData.topSN;
                        }
                        else if (item.Name == "PCBA_SN")
                        {
                            btformat.SubStrings["PCBA_SN"].Value = uutData.pcbaSN;
                            if (printedData != string.Empty)
                                printedData = printedData + ":";
                            printedData = printedData + uutData.pcbaSN;
                        }
                        else if (item.Name == "Product_SN")
                        {
                            btformat.SubStrings["Product_SN"].Value = uutData.topSN;
                            if (printedData != string.Empty)
                                printedData = printedData + ":";
                            printedData = printedData + uutData.topSN;
                        }
                        else
                        {
                            stepStatus = (int)ErrorCodes.System.LabelPrintError;
                            resultmessage = "System Error: There is a invalid field(" + item.Name + ") in the barcode file.";
                            testDB.AddStepResult("FAIL", resultmessage, stepStatus);
                            return stepStatus;
                        }

                    }
                    btformat.Save();
                    Result result = btformat.Print();
                    btformat.Close(SaveOptions.DoNotSaveChanges);
                    btengine.Dispose();
                    resultmessage = "Label printed: " + printedData;
                    stepStatus = 0;
                    testDB.AddStepResult("PASS", resultmessage, stepStatus);
                }
                catch (Exception ex)
                {
                    stepStatus = (int)ErrorCodes.System.LabelPrintError;
                    resultmessage = "System Error: " + ex.Message;
                    testDB.AddStepResult("FAIL", resultmessage, stepStatus);
                }
            }
            return stepStatus;
        }


        /// <summary>
        /// Turn the DUT power on and wait for the DVTMAN prompt. Verify firmware version if
        /// configured in the limits.
        /// </summary>
        /// <remarks>
        /// Apply power to the unit and wait for the DVTMAN version response. The timeout will be defined
        /// in a TestConstant param = WaitTimeSecs. If the TestConstant does not exits, 1 seconds will be used.
        /// If there is a Limit with the param = DVTMANVersion, then the
        /// boot up message will be checked for Limit param = DVTMANVersion
        /// 
        /// error codes
        /// 0 - ErrorCodes.Program.Success
        /// 1 - ErrorCodes.Program.ValueOutsideOfLimit, voltage out of limit
        /// 2 - ErrorCodes.Program.DUTComError, com error sending/reciveing DUT command
        ///         16  ErrorCodes.Program.CommandTimeout, waiting for response from DUT
        ///         19  ErrorCodes.Program.CmdResponsePayloadLenWrong, waiting for response from DUT
        ///         23  ErrorCodes.Program.CmdPacketFormatError, waiting for response from DUT
        ///         22  ErrorCodes.Program.CmdResponseHeaderLenWrong, waiting for response from DUT
        ///         21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN, waiting for response from DUT
        ///         20  ErrorCodes.Program.CmdPacketCheckSumError, waiting for response from DUT
        /// 3  -  ErrorCodes.Program.DutUnexpectedCmdReturn, sending command to DUT
        /// 24 - DVTMAN version check failed
        /// 25 - Flash failed. additional error codes
        ///            Error_PortNotOpen = -1;
        ///            Error_ErrResponse = -2;
        ///            Error_OpenError = -3;
        ///            Error_PortNotExist = -4;
        ///            Error_PortNotInitialized = -5;
        ///            Error_FileSelectionFailed_Timeout = -6;
        ///            Error_FileSelectionFailed_NACKReceived = -7;
        ///            Error_FileSelectionFailed_OKNotReceived = -8;
        ///            Error_ProgrammingFailed_Timeout = -9;
        ///            Error_ProgrammingFailed_NACKReceived = -10;
        ///            Error_StatusRead_Timeout = -11;
        ///            Error_StatusRead_NACKReceived = -12;
        ///            Error_StatusRead_Error = -13;
        ///  -10  ErrorCodes.System.LabJackError, labjack error or not present
        ///   -13  ErrorCodes.System.InstrumentNotSetUp, instrument in the defined list is not in station
        ///   -14  ErrorCodes.System.ACMeterReadError, error reading data
        ///  -21  ErrorCodes.System.InvalidMeasurement, voltage name not found in defined inputs
        ///  -22    ErrorCodes.System.InvalidInstrument, not defined for fixture
        ///   -23   ErrorCodes.System.InstrumentFailure
        /// </remarks>
        /// <param name="LimitsData.stepsItem"></param>
        /// <param name="testDB"></param>
        /// <param name="uutData"></param>
        /// <param name="resultmsg"></param>
        /// <returns>error code</returns>
        public int TurnOnDut(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, ref string resultmsg)
        {

            int versionStatus = 0;
            string versionResultmsg = string.Empty;
            string voltageResultmsg = string.Empty;
            int testStatus = 0;
            int programStatus = 0;
            string programResultmsg = string.Empty;
            int startupDelayMs = 1000;
            int version = 0;
            List<LimitsData.limitsItem> voltLimitData;
            bool checkVoltage = false;          // set to true if voltage limit found
            bool programCode = false;           // set to true if ProgramCode parameter is found
            bool checkVersion = false;          // set to true if DVTMANVersion limit is found
            bool versionCheckGood = true;       // version check limit passed
            bool voltageCheckGood = true;       // all voltages passed
            bool programCheckGood = true;       // programing passed
            bool programCodeRan = false;        // set to true if tried to program
            bool checkVersionRan = false;       // set to true if tried to check the version
            bool flasherOpenStatus = true;      // set to true if com port is OK and returns READY
            int readversion = 0;
            bool reseat = true;
            int retrys = 3;
            string programCodeFileName = string.Empty;
            bool powerIsOn = false;
            LimitsData.limitsItem limitsItem;

            byte[] payload = new byte[256];

            resultmsg = string.Empty;
            
            // get parameters
            if (parameters.GetParameterValue("WaitTimeSecs") != string.Empty)
                startupDelayMs = Convert.ToInt32(parameters.GetParameterValue("WaitTimeSecs")) * 1000;
            if (parameters.GetParameterValue( "Reseat") != string.Empty)
                reseat = true;
            if (parameters.GetParameterValue( "Retrys") != string.Empty)
                retrys = Convert.ToInt32(parameters.GetParameterValue("Retrys"));
            if (parameters.GetParameterValue( "ProgramCode") != string.Empty)
                programCode = parameters.GetParameterValue("ProgramCode").ToUpper().Equals("YES");
            programCodeFileName = parameters.GetParameterValue("ProgramCodeFileName");

            //get DVTMAN limits
            if (parameters.DoesLimitExist("DVTMANVersion"))
            {
                if ((limitsItem = parameters.GetLimitValues("DVTMANVersion")).name != null)
                {
                    version = Convert.ToByte(limitsItem.value1);
                    checkVersion = true;
                }
            }
            else
                checkVersion = false;

            //get voltage limits
           voltLimitData = parameters.limits.FindAll(x => x.name.ToUpper().Contains("VOLTAGE:"));
            if (voltLimitData.Count != 0)
                checkVoltage = true;

            testDB.CreateStepRecord(parameters.name, parameters.step_number);

            versionCheckGood = false;       // version check limit failed so it will try at least once
            voltageCheckGood = false;       // all voltages failed so it will try at least once
            programCheckGood = false;       // programing failed so it will try at least once
            for (int i = 0; i < (retrys + 1); i++)         // try at least once
            {
                // clear all the statuses
                programCodeRan = false;        // set to true if tried to program
                checkVersionRan = false;       // set to true if tried to check the version
                resultmsg = string.Empty;
                versionStatus = 0;
                versionResultmsg = string.Empty;
                voltageResultmsg = string.Empty;
                testStatus = 0;
                programStatus = 0;
                programResultmsg = string.Empty;

                if (i != 0)     // must be a retry
                {
                    testDB.AddStepRef("TurnOnDutRetry", i.ToString(), "", "", "");
                }
                fixture.ControlDUTPower(FixtureFunctions.PowerControl.ON, startupDelayMs);             // turn on the DUT power
                if (fixture.LastFunctionStatus != 0)        // if there was a error
                {
                    testDB.AddStepResult("FAIL", fixture.LastFunctionErrorMessage, fixture.LastFunctionStatus);
                    return fixture.LastFunctionStatus;
                }
                powerIsOn = true;

                // if 'check voltage' option is selected, check for correct voltage
                if (checkVoltage)
                {
                    FixtureFunctions.PassData[] channelNames = new FixtureFunctions.PassData[voltLimitData.Count];
                    LimitsData.limitsItem[] channelLimits = new LimitsData.limitsItem[voltLimitData.Count];
                    for (int j = 0; j < voltLimitData.Count; j++)
                    {
                        channelNames[j] = new FixtureFunctions.PassData();
                        channelNames[j].channelName = voltLimitData[j].name.Substring(voltLimitData[j].name.IndexOf(':') + 1);
                        channelLimits[j] = voltLimitData[j];
                    }
                    fixture.ReadMeasurements(ref channelNames);
                    if (fixture.LastFunctionStatus != 0) // if there is a error, it will be a system type error
                    {
                        testDB.AddStepResult("FAIL", fixture.LastFunctionErrorMessage, fixture.LastFunctionStatus);
                        UpdateStatusWindow("Voltage check failure. " + fixture.LastFunctionErrorMessage);
                        return fixture.LastFunctionStatus;
                    }
                    voltageCheckGood = true;
                    voltageResultmsg = string.Empty;
                    for (int j = 0; j < voltLimitData.Count; j++)
                    {
                        string oneResultmsg = string.Empty;
                        UpdateStatusWindow("Voltage " + channelNames[j].channelName + ": " + channelNames[j].returnedData.ToString("N1"));
                        if (!CheckResultValue(channelNames[j].returnedData, channelLimits[j], ref oneResultmsg))
                        {
                            voltageResultmsg += channelNames[j] + " out of spec. " + oneResultmsg + "\n";
                            voltageCheckGood = false;
                            UpdateStatusWindow(" - FAIL\n", StatusType.failed);
                            testDB.AddStepResult(channelNames[j].channelName, FixtureDatabase.ResultData.TEST_FAILED, channelLimits[j].operation, Convert.ToDouble(channelLimits[j].value1), Convert.ToDouble(channelLimits[j].value2),
                                                    channelNames[j].returnedData, "V", "N2", oneResultmsg, (int)ErrorCodes.Program.ValueOutsideOfLimit);
                        }
                        else
                        { 
                            UpdateStatusWindow(" - PASS\n", StatusType.passed);
                            testDB.AddStepResult(channelNames[j].channelName, FixtureDatabase.ResultData.TEST_PASSED, channelLimits[j].operation, Convert.ToDouble(channelLimits[j].value1), Convert.ToDouble(channelLimits[j].value2),
                                                    channelNames[j].returnedData, "V", "N2", oneResultmsg, 0);
                        }
                    }
                }
                else
                {
                    voltageCheckGood = true;        // beings it is not being checked, set to pass so the other parts will run
                }

                // if inline programming is enabled, only do it if the voltage is correct.
                //  if there was no 'check voltage' option, it will default to pass
                if (programCode & voltageCheckGood)        // if set to do inline programming
                {
                    programCodeRan = true;
                    programCheckGood = true;
                    programResultmsg = string.Empty; ;
                    programStatus = 0;

                    UpdateStatusWindow("DUT programing start - ");
                    testDB.AddStepRef("ProgramType", ((stationConfig.ConfigFlasherComPort != string.Empty) & (stationConfig.ConfigFlasherComPort.ToUpper() != "COM0")) ? "AUTO" : "MANUAL", "", "", "");
                    if ((stationConfig.ConfigFlasherComPort != string.Empty) & (stationConfig.ConfigFlasherComPort.ToUpper() != "COM0"))    // if the flasher is connected for auto program
                    {
                        // -------------- do automatic ----------------
                        Flasher flasher = new Flasher(stationConfig.ConfigFlasherComPort);
                        flasher.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(StatusUpdated);

                        // try to open the flasher. Can fail if: 1-port does not open, 2-port opens but does not get READY for STATUS check
                        flasherOpenStatus = true;
                        if (flasher.Open())     // if port opened OK
                        {
                            if (!(programResultmsg = flasher.GetStatus()).Contains("READY"))    // see if Flasher is READY
                            {
                                // Flasher did not return READY status. Verify there is a LED on on the Flasher. Unplug and replug the USB if no LEDs are on.
                                MessageBox.Show(Properties.Resources.FlasherStatusReadyError);
                                // Is there a LED on on the Flasher?
                                var yn = MessageBox.Show(Properties.Resources.IsFlasherLEDON, "Check Flasher LED", MessageBoxButtons.YesNo);
                                if (yn == DialogResult.No)
                                {
                                    programCheckGood = false;
                                    programResultmsg = Properties.Resources.NoFlasherLEDOn;
                                    programStatus = ErrorCodes.System.FlasherBase + flasher.LastErrorCode;
                                    UpdateStatusWindow(Properties.Resources.NoFlasherLEDOn + "\n", StatusType.failed);
                                    flasherOpenStatus = false;
                                }
                            }
                        }
                        else            // port did not open
                        {
                            programCheckGood = false;
                            programResultmsg = flasher.LastErrorMsg;
                            programStatus = ErrorCodes.System.FlasherBase + flasher.LastErrorCode;
                            UpdateStatusWindow(flasher.LastErrorMsg + "\n", StatusType.failed);
                            flasherOpenStatus = false;
                        }

                        if (flasherOpenStatus)      // if everthing OK with flasher open
                        {
                            if (!flasher.ProgramDevice(programCodeFileName, 10, 1))
                            {
                                // failed to program. try one more time
                                UpdateStatusWindow("Retry\n", StatusType.failed);
                                // lets try one more time
                                fixture.ControlDUTPower(FixtureFunctions.PowerControl.OFF, 1000);     // turn off juct in case stuck connecting
                                fixture.ControlDUTPower(FixtureFunctions.PowerControl.ON, 1000);
                                flasher.ClearBuffers();
                                for (int sc = 0; sc < 10; sc++)     // make sure all errors are cleared
                                {
                                    flasher.GetStatus();
                                    Thread.Sleep(100);
                                }
                                if (!(programResultmsg = flasher.GetStatus()).Contains("READY"))                     // clear out error messages
                                {
                                    programCheckGood = false;
                                    programResultmsg = "Error get STATUS response. " + programResultmsg;
                                    programStatus = ErrorCodes.Program.ProgramFlashFailed;
                                    UpdateStatusWindow("Failed getting READY on retry\n" + programResultmsg + "\n", StatusType.failed);
                                }
                                else
                                {
                                    if (!flasher.ProgramDevice(programCodeFileName, 10, 1))
                                    {
                                        programCheckGood = false;
                                        programResultmsg = flasher.LastErrorMsg;
                                        programStatus = flasher.LastErrorCode;
                                        UpdateStatusWindow("Failed\n", StatusType.failed);
                                    }
                                    else
                                    {
                                        UpdateStatusWindow("Passed\n", StatusType.passed);
                                        fixture.ControlDUTPower(FixtureFunctions.PowerControl.OFF, startupDelayMs);             // turn off the DUT power, need to get DVTMAN version
                                        fixture.SensorConsole.ClearBuffers();                                   // clear out junk
                                        fixture.ControlDUTPower(FixtureFunctions.PowerControl.ON, 0);             // turn on the DUT power
                                        Thread.Sleep(startupDelayMs);                                           // time to let dut power up
                                    }
                                }

                            }
                            else
                            { 
                                UpdateStatusWindow("Passed\n", StatusType.passed);
                                fixture.ControlDUTPower(FixtureFunctions.PowerControl.OFF, startupDelayMs);             // turn off the DUT power, need to get DVTMAN version
                                fixture.SensorConsole.ClearBuffers();                                   // clear out junk
                                fixture.ControlDUTPower(FixtureFunctions.PowerControl.ON, 0);             // turn on the DUT power
                                Thread.Sleep(startupDelayMs);                                           // time to let dut power up
                            }
                            flasher.Close();
                        }
                    }
                    else                                                        // must do it manualy
                    {
                        // ------------- do manualy -----------------
                        FirmwareProgramWait prg = new FirmwareProgramWait(fixture);
                        var yn = prg.ShowDialog();
                        if (yn == DialogResult.No)          // if red light is on, it failed to program
                        {
                            programCheckGood = false;
                            programResultmsg = "Programmer had a red led";
                            programStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                            UpdateStatusWindow("FAILED\n", StatusType.failed);
                        }
                        else
                        {
                            programCheckGood = true;
                            UpdateStatusWindow("PASSED\n", StatusType.passed);
                            fixture.ControlDUTPower(FixtureFunctions.PowerControl.OFF, startupDelayMs);             // turn off the DUT power
                            fixture.SensorConsole.ClearBuffers();                                   // clear out junk
                            fixture.ControlDUTPower(FixtureFunctions.PowerControl.ON, 0);             // turn on the DUT power
                            Thread.Sleep(startupDelayMs);                                           // time to let dut power up
                        }
                        prg.Dispose();
                    }
                }
                else
                {
                    programCheckGood = true;        // beings not being checked, set to pass so the other tests will run
                }

                // if the 'check version' is enabled, only do it if voltage check is Ok and programming is OK.
                //  voltage check and programming will default to OK if they were not enabled.
                if (voltageCheckGood & programCheckGood & checkVersion)
                {
                    UpdateStatusWindow("DVTMAN Version check - ");
                    checkVersionRan = true;
                    fixture.ControlDUTPower(FixtureFunctions.PowerControl.OFF, 0);             // turn off the DUT power
                    fixture.SensorConsole.ClearBuffers();                                   // clear out junk
                    Thread.Sleep(startupDelayMs);
                    fixture.ControlDUTPower(FixtureFunctions.PowerControl.ON, 0);             // turn on the DUT power
                    for (int j = 0; j < 5; j++)             // try mulitiple times just in case trash when turned on
                    {
                        versionStatus = 0;
                        versionResultmsg = string.Empty;
                        versionCheckGood = true;
                        UpdateStatusWindow(".");
                        if (!fixture.SensorConsole.WaitForMsgVersion(ref readversion, 1000))       // if bad
                        {
                            versionResultmsg = "WaitForMsgVersion error code: " + fixture.SensorConsole.LastErrorCode + ", " + fixture.SensorConsole.LastErrorMessage;             // save results
                            versionStatus = fixture.SensorConsole.LastErrorCode;
                            versionCheckGood = false;
                        }
                        else        // found msg version response
                        {
                            if (version != readversion)     // if bad
                            {
                                versionResultmsg = "Error: DVTMAN Version wrong. Should be " + version.ToString() + ", read " + readversion.ToString();
                                versionStatus = (int)ErrorCodes.Program.VersionCheckFailed;
                                versionCheckGood = false;
                                i = retrys;             // force to end retry loop. do not try again
                                break;
                            }
                            else
                                break;  // was good, so get out of retry loop
                        }
                        Thread.Sleep(500);

                    }
                    UpdateStatusWindow(versionCheckGood ? " PASSED\n" : " FAILED. " + versionResultmsg + "\n", versionCheckGood ? StatusType.passed:StatusType.failed);
                }
                else
                {
                    versionCheckGood = true;        // beings not being run, set to pass so the other tests will run
                }
                    
                // if there was a failure in check voltage, check version, or programming
                if (!versionCheckGood | !programCheckGood | !voltageCheckGood)    // if it did not find the response or voltage out of spec
                {
                    fixture.ControlDUTPower(FixtureFunctions.PowerControl.OFF, 100);
                    powerIsOn = false;
                    if (reseat)                                     // and reset option on
                    {
                        if (i != retrys) // if reties left
                        {
                            MessageBox.Show(Properties.Resources.RetryPowerOn);     // 'Remove and reinstall unit in the fixture'
                        }
                    }
                }
                else        // found a response and voltage good, or defaulted to good
                {
                    break;          // so get out of here
                }
            }       // end retry loop

            testStatus = 0;

            if (checkVoltage)
            {
                if (!voltageCheckGood)
                    testStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                resultmsg = voltageResultmsg;
            }
            if (checkVersion & checkVersionRan)
            {
                testDB.AddStepResult("DVTMANVersion", versionCheckGood ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED,
                                                "EQ", version.ToString(), readversion.ToString(), versionResultmsg, versionStatus);
                if (!versionCheckGood)
                    testStatus = (int)ErrorCodes.Program.VersionCheckFailed;
                resultmsg += ((resultmsg == string.Empty) ? "" : "\n") + versionResultmsg;
            }
            if (programCode & programCodeRan)
            {
                testDB.AddStepResult("ProgramCode", programCheckGood ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED, 
                    programResultmsg, programStatus);
                if (!programCheckGood)
                    testStatus = (int)ErrorCodes.Program.ProgramFlashFailed;
                resultmsg += ((resultmsg == string.Empty) ? "" : "\n") + programResultmsg;
            }
            // if 'check voltage', 'check version', or 'program' were all not selected, just record that the program turned on the unit
            if (!checkVersion & !checkVoltage & !programCode)
            {
                testDB.AddStepResult(powerIsOn ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED,
                    powerIsOn ? "Power is on" : "Power is off",
                    powerIsOn ? 0 : (int)ErrorCodes.Program.ValueOutsideOfLimit);
                if (!powerIsOn)
                    testStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit;
            }
            return testStatus;
        }

    }


}
