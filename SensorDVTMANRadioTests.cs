﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LimitsDatabase;
using FixtureDatabase;
using TestStationErrorCodes;

namespace SensorManufSY2
{
    public partial class SensorDVTMANTests
    {

        /// <summary>
        /// This will test the BLE radio.
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="testDB"></param>
        /// <param name="uutData"></param>
        /// <param name="resultmessage"></param>
        /// <returns></returns>
        public int TestBLE(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, ref string resultmessage)
        {
            int status = 0;
            int dtorStatus = 0;   // dut to ref status
            int rtodStatus = 0;   // ref to dut status
            string dtorResultMsg = string.Empty;                // dut ot ref result message
            string rtodResultMsg = string.Empty;                // ref to dut result message
            string stepResult = string.Empty;
            int testNumber = parameters.step_number;
            int timeoutSecs = 1;                                // default beacon timeout
            uint numberOfBeaconsFound = 0;                      // returned by test
            int outputLevel = 15;                               // default to full power
            int minBeaconsToFind = 5;                           // default minimum number of beacons that must be found
            bool testDUTtoRef = true;
            bool testReftoDUT = true;

            BluetoothFunctions ble = new BluetoothFunctions();
            ble.fixture = fixture;

            resultmessage = string.Empty;

            if (fixture.RefBLEMAC.Length != 12)
            {
                status = (int)ErrorCodes.System.ParameterFormatError;
                resultmessage = "The test BLE radio mac is not the correct lenght. Found '" + fixture.RefBLEMAC + "'";
                return status;
            }

            if (parameters.DoesParameterExist("TIMEOUT"))
                timeoutSecs = Convert.ToInt32(parameters.GetParameterValue("TIMEOUT"));
            if (parameters.DoesParameterExist("OUTPUTLEVEL"))
                outputLevel = Convert.ToInt32(parameters.GetParameterValue("OUTPUTLEVEL"));
            if (parameters.DoesParameterExist("MINBEACONSTOFIND"))
                minBeaconsToFind = Convert.ToInt32(parameters.GetParameterValue("MINBEACONSTOFIND"));
            if (parameters.DoesParameterExist("TESTDUTTOREF"))
                testDUTtoRef = parameters.GetParameterValue("TESTDUTTOREF").ToUpper().Equals("YES");
            if (parameters.DoesParameterExist("TESTREFTODUT"))
                testReftoDUT = parameters.GetParameterValue("TESTREFTODUT").ToUpper().Equals("YES");

            if (testDB != null)
                testDB.CreateStepRecord(parameters.name, testNumber);      // create the test record header.

            if (testDUTtoRef)
            {
                // test DUT to ref direction
                if (!ble.BluetoothTest(BluetoothFunctions.BLEDirection.DUTtoRef, uutData.mac2, (uint)timeoutSecs, ref numberOfBeaconsFound))
                {
                    dtorResultMsg = ble.FailMessage;                                    // got a com error of some sort
                    resultmessage = dtorResultMsg;
                    dtorStatus = ble.FailErrorCode;
                    testDB.AddStepResult("DUTtoRefBLE", "FAIL", dtorResultMsg, dtorStatus);
                    if (dtorStatus < 0)           // if this is a system error
                        return dtorStatus;                                              // kick out now
                }
                else        // no com errors so record the result
                {
                    if (numberOfBeaconsFound < minBeaconsToFind)          // if not enough beacons were found
                    {
                        dtorResultMsg = "DUT to ref:Not enough beacons were found";
                        dtorStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                    }
                    testDB.AddStepResult("DUTtoRefBLE", (numberOfBeaconsFound >= minBeaconsToFind) ? "PASS" : "FAIL", "GE", (double)minBeaconsToFind, (double)numberOfBeaconsFound, "", "", dtorResultMsg, dtorStatus);
                    testDB.AddStepRef("BeaconsFound", numberOfBeaconsFound.ToString(), "", "N1", "");
                    UpdateStatusWindow("Dut to Ref beacons found " + numberOfBeaconsFound.ToString() + "\n");
                }
            }
            if (testReftoDUT)
            {
                // test ref to DUT direction
                if (!ble.BluetoothTest(BluetoothFunctions.BLEDirection.ReftoDUT, uutData.mac2, (uint)timeoutSecs, ref numberOfBeaconsFound))
                {
                    rtodResultMsg = ble.FailMessage;
                    resultmessage = (resultmessage.Equals("") ? "" : (resultmessage + ":")) + rtodResultMsg;
                    rtodStatus = ble.FailErrorCode;
                    testDB.AddStepResult("ReftoDUTBLE", "FAIL", rtodResultMsg, rtodStatus);
                    if (rtodStatus < 0)           // if this is a system error
                        return rtodStatus;                                              // kick out now
                }
                else        // no com errors so record the result
                {
                    if (numberOfBeaconsFound < minBeaconsToFind)      // if not enough beacons found
                    {
                        rtodResultMsg = "Ref to DUT:Not enough beacons were found";
                        resultmessage = (resultmessage.Equals("") ? "" : (resultmessage + ":")) + rtodResultMsg;
                        rtodStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                    }
                    testDB.AddStepResult("ReftoDUTBLE", (numberOfBeaconsFound >= minBeaconsToFind) ? "PASS" : "FAIL", "GE", (double)minBeaconsToFind, (double)numberOfBeaconsFound, "", "", rtodResultMsg, rtodStatus);
                    testDB.AddStepRef("BeaconsFound", numberOfBeaconsFound.ToString(), "", "N1", "");
                    UpdateStatusWindow("Ref to Dut beacons found " + numberOfBeaconsFound.ToString() + "\n");
                }
            }

            if ((dtorStatus != 0) | (rtodStatus != 0))  // if one or more failed
                status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
            return status;
        }

        //---------------------------------------
        // TestNetworks
        ///<summary>
        /// Tests the enlighted radio. Will record the test results
        ///</summary>
        /// <remarks>
        /// It will set the channel to configTestRadioTestChannel.
        /// Will use the test radio at:
        ///      configTestRadioMac
        /// if parameters.Function is blank, resultmessage will have the LQI values for pass or fail
        /// </remarks>
        ///<param name="parameters">TestToRun - list of the test parameters. Assumed to have two parameters: RadioLQIm and RadioLQIt</param>
        ///<param name="testDB">FixtureDatabase.ResultDatabase - database to log the data</param>
        ///<param name="resultmessage">failmsg - if there is a failure, this string will have the failure message.</param>
        ///<returns>
        ///    ErrorCodes.Program.Success         0 
        ///    ErrorCodes.Program.DUTComError     2
        ///    ErrorCodes.System.InvalidParameter     100026
        ///    ErrorCodes.Program.ValueOutsideOfLimit     1
        ///</returns>
        public int TestNetworks(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, ref string resultmessage)
        {
            string rcvbuf = string.Empty;
            string stepFailmsg = string.Empty;
            int testStatus = 0;
            bool status = true;
            int retryCount = 0;
            int retests = 0;                    // defaults to no retests if option is not present
            int timeoutms = 10000;               // default the test timeout to 10secs

            int radioLQIm = 0;              // set up as no response was found
            int radioLQIt = 0;
            bool bLQImResult = true;
            string sLQImResult = string.Empty;
            bool bLQItResult = true;
            string sLQItResult = string.Empty;

            int testNumber = parameters.step_number;


            if (testDB != null)
                testDB.CreateStepRecord(parameters.name, testNumber);      // create the test record header.
            resultmessage = string.Empty;

            if (fixture.RefRadioMAC.Length != 12)
            {
                testStatus = ErrorCodes.System.ParameterFormatError;
                resultmessage = "The test radio mac is not the correct lenght. Found '" + fixture.RefRadioMAC + "'";
                return testStatus;
            }

            foreach (var item in parameters.parameters)
            {
                if (item.name.ToUpper().Contains("RETRYS"))
                    retests = Convert.ToInt32(item.value);
                if (item.name.ToUpper().Contains("TIMEOUT"))
                    timeoutms = Convert.ToInt32(item.value);
            }

            //foreach (var pitem in parameters.limits)
            //{
            //    if (pitem.Parameter.Equals("RADIOLQIM", StringComparison.InvariantCultureIgnoreCase))
            //        radioLQImLimit = Convert.ToDouble(pitem.Value1);
            //    if (pitem.Parameter.Equals("RADIOLQIT", StringComparison.InvariantCultureIgnoreCase))
            //        radioLQItLimit = Convert.ToDouble(pitem.Value1);
            //}
            //if ((radioLQImLimit == 0) | (radioLQItLimit == 0))
            //{
            //    resultmessage = "System Error: Did not find both RadioLQIm and RadioLQIt in limits.";
            //    if (testDB != null)
            //        testDB.AddStepResult("FAIL", resultmessage, (int)ErrorCodes.System.MissingParameter);
            //    return (int)ErrorCodes.System.MissingParameter;
            //}

            // make sure radio at a know state
            UpdateStatusWindow("Setting to channel " + uutData.testChannel.ToString() + "\n");
            if (!fixture.RefRadio.SetRadioData(Convert.ToByte(stationConfig.ConfigTestRadioChannel), fixture.RadioDefaultPanID, fixture.RadioDefaultTxPower,
                                            Encoding.ASCII.GetBytes(fixture.RadioDefaultEncryptKey), fixture.RadioDefaultTTL, fixture.RadioDeraultRate))
            {
                testStatus = fixture.RefRadio.LastErrorCode;
                resultmessage = fixture.RefRadio.LastErrorMessage;
                return testStatus;
            }
            if (!fixture.SensorConsole.SetRadioData(Convert.ToByte(stationConfig.ConfigTestRadioChannel), fixture.RadioDefaultPanID, fixture.RadioDefaultTxPower,
                                            Encoding.ASCII.GetBytes(fixture.RadioDefaultEncryptKey), fixture.RadioDefaultTTL, fixture.RadioDeraultRate))
            {
                testStatus = fixture.SensorConsole.LastErrorCode;
                resultmessage = fixture.SensorConsole.LastErrorMessage;
                return testStatus;
            }

            do
            {
                UpdateStatusWindow("Radio test start...");
                testStatus = 0;
                resultmessage = string.Empty;
                status = fixture.SensorConsole.PerformWtest(fixture.RefRadioMAC, timeoutms, ref radioLQIm, ref radioLQIt);
                if (!status)            // if bad send/recieve
                {
                    testStatus = fixture.SensorConsole.LastErrorCode;
                    if (testDB != null)
                        testDB.AddStepResult(((retryCount <= retests) ? "RETEST" : "FAIL"), fixture.SensorConsole.LastErrorMessage, testStatus);
                    resultmessage = fixture.SensorConsole.LastErrorMessage + "\n";
                }
                else
                {

                    foreach (var pitem in parameters.limits)
                    {
                        if (pitem.name.Equals("RADIOLQIM", StringComparison.InvariantCultureIgnoreCase))
                            bLQImResult = CheckResultValue(radioLQIm, pitem, ref sLQImResult);
                        if (pitem.name.Equals("RADIOLQIT", StringComparison.InvariantCultureIgnoreCase))
                            bLQItResult = CheckResultValue(radioLQIt, pitem, ref sLQItResult);
                    }
                    if (!bLQImResult | !bLQItResult)        // if there is a error
                    {
                        testStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        resultmessage = sLQImResult + ((sLQImResult.Length != 0) ? " : " : "") + sLQItResult;
                    }
                }
                UpdateStatusWindow("  LQIm: " + radioLQIm.ToString() + " LQIt: " + radioLQIt.ToString() + "\n", (testStatus == 0) ? StatusType.passed : StatusType.failed);
            } while ((testStatus != 0) && (retryCount++ <= retests));

            if ((parameters.function_call == string.Empty) | (parameters.function_call == null))    // special case. being called by something other than the test loop
            {
                resultmessage = "Pass: LQIm: " + radioLQIm.ToString() + " LQIt: " + radioLQIt.ToString();
            }
            else        // normal sequence call, log
            {
                testDB.AddStepResult((testStatus == 0) ? ResultData.TEST_PASSED : ResultData.TEST_FAILED, resultmessage, testStatus);
                testDB.AddStepRef("RadioLQIm", radioLQIm.ToString(), "", "", "");
                testDB.AddStepRef("RadioLQIt", radioLQIt.ToString(), "", "", "");
            }
            return testStatus;
        }

    }
}
