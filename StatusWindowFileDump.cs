﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SensorManufSY2
{
    class StatusWindowFileDump
    {
        private string dumpFileName = string.Empty;
        //private string directory;


        /// <summary>
        /// Will start the dump. Creates the filename.
        /// </summary>
        /// <param name="baseTitle"></param>
        public void SetDumpFileName(string directory, string baseTitle)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            dumpFileName = directory + @"\" + baseTitle;
        }

        public void DumpData(string dataToDump)
        {
            if (dumpFileName != string.Empty)
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(dumpFileName, true))
                {
                    file.Write(dataToDump);
                }
            }
        }

        public void EndDump()
        {
            dumpFileName = string.Empty; ;
        }

    }
}
