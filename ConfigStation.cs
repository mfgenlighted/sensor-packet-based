﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Configuration;

namespace SensorManufSY2
{
    public partial class ConfigStation : Form
    {
//        FixtureFunctions fixconfig;
        StationConfigure stationConfig;
        string fixtureInUse;

        public ConfigStation(string fixture)
        {
            InitializeComponent();
            fixtureInUse = fixture;
        }


        private void StationConfigWindow_Load(object sender, EventArgs e)
        {


            string outmsg = string.Empty;

            stationConfig = new StationConfigure();
            stationConfig.ReadStationConfigFile(fixtureInUse, ref outmsg);

            // station options
            this.textStationName.Text = stationConfig.ConfigStationName;
            this.tbFactoryCode.Text = stationConfig.ConfigFactoryCode;

            // test options
            this.checkBoxDisplayDUTSend.Checked = stationConfig.ConfigDisplaySendChar;
            this.checkBoxDisplayDUTRcv.Checked = stationConfig.ConfigDisplayRcvChar;
            this.checkBoxDebug.Checked = stationConfig.ConfigDebug;
            this.tbDebugOptions.Text = stationConfig.ConfigDebugOptions;
            this.textBoxStepAfterPass.Text = stationConfig.ConfigNextStepAfterPass;
            this.textBoxStepAfterFail.Text = stationConfig.ConfigNextStepAfterFail;
            this.checkBoxEnablePFS.Checked = stationConfig.ConfigEnablePFS;
            this.cbPFSAskIfFail.Checked = stationConfig.ConfigPFSAskIfFail;
            this.checkBoxEnableSFS.Checked = stationConfig.ConfigEnableSFS;
            this.checkBoxEnableConsoleDump.Checked = stationConfig.ConfigEnableConsoleDump;

            // directories
            this.textReportDirectory.Text = stationConfig.ConfigReportDirectory;
            this.tbLabelFileDirectory.Text = stationConfig.ConfigLabelFileDirectory;

            // SQL
            this.textSQLServer.Text = stationConfig.SQLServer;
            this.textSQLResultDB.Text = stationConfig.SQLResultsDatabaseName;
            this.textSQLMacDB.Text = stationConfig.SQLMacDatabaseName;
            this.textSQLProductDB.Text = stationConfig.SQLProductDatabaseName;
            this.textSQLSNDB.Text = stationConfig.SQLSerialNumberDatabaseName;
            this.tbLimitsDB.Text = stationConfig.SQLTestLimitsDataName;
            this.textSQLUser.Text = stationConfig.SQLOperatorName;
            this.textSQLPassword.Text = stationConfig.SQLOperatorPassword;
            if (stationConfig.ConfigDebug)
            {
                this.textSQLResultDB.Enabled = true;
                this.textSQLMacDB.Enabled = true;
                this.textSQLProductDB.Enabled = true;
                this.textSQLSNDB.Enabled = true;
                this.textSQLUser.Enabled = true;
                this.textSQLPassword.Enabled = true;
                this.tbLimitsDB.Enabled = true;
            }
            else
            {
                this.textSQLResultDB.Enabled = false;
                this.textSQLMacDB.Enabled = false;
                this.textSQLProductDB.Enabled = false;
                this.textSQLSNDB.Enabled = false;
                this.textSQLUser.Enabled = false;
                this.textSQLPassword.Enabled = false;
                this.tbLimitsDB.Enabled = false;
            }
            this.comboBoxSQLRecordOption.SelectedIndex = (int)stationConfig.SQLRecordOption;

            // shopfloor
            this.textSFDir.Text = stationConfig.ConfigShopFloorDirectory;
            this.textSFFileName.Text = stationConfig.ConfigShopFloorFileName;

            // pfs
            this.textPFSDatabase.Text = stationConfig.PFSDatabaseName;
            this.textPFSServer.Text = stationConfig.PFSServerIPAddress;
            this.textPFSWorkCenter.Text = stationConfig.PFSWorkCenter;
        }

        private void buttonSaveSystemConfig_Click(object sender, EventArgs e)
        {

            stationConfig.ConfigFactoryCode = this.tbFactoryCode.Text;

            stationConfig.ConfigDisplaySendChar = this.checkBoxDisplayDUTSend.Checked;
            stationConfig.ConfigDisplayRcvChar = this.checkBoxDisplayDUTRcv.Checked;
            stationConfig.ConfigDebug = this.checkBoxDebug.Checked;
            stationConfig.ConfigDebugOptions = this.tbDebugOptions.Text;
            stationConfig.ConfigNextStepAfterPass = this.textBoxStepAfterPass.Text;
            stationConfig.ConfigNextStepAfterFail = this.textBoxStepAfterFail.Text;
            stationConfig.ConfigEnablePFS = this.checkBoxEnablePFS.Checked;
            stationConfig.ConfigPFSAskIfFail = this.cbPFSAskIfFail.Checked;
            stationConfig.ConfigEnableSFS = this.checkBoxEnableSFS.Checked;
            stationConfig.ConfigEnableConsoleDump = this.checkBoxEnableConsoleDump.Checked;

            stationConfig.ConfigReportDirectory = this.textReportDirectory.Text;
            stationConfig.ConfigLabelFileDirectory = this.tbLabelFileDirectory.Text;

            stationConfig.SQLServer = this.textSQLServer.Text;
            stationConfig.SQLRecordOption = (StationConfigure.DBRecordType)this.comboBoxSQLRecordOption.SelectedIndex;
            stationConfig.SQLResultsDatabaseName = this.textSQLResultDB.Text;
            stationConfig.SQLMacDatabaseName = this.textSQLMacDB.Text;
            stationConfig.SQLProductDatabaseName = this.textSQLProductDB.Text;
            stationConfig.SQLTestLimitsDataName = this.tbLimitsDB.Text;
            stationConfig.SQLSerialNumberDatabaseName = this.textSQLSNDB.Text;
            stationConfig.SQLOperatorName = this.textSQLUser.Text;
            stationConfig.SQLOperatorPassword = this.textSQLPassword.Text;
            stationConfig.ConfigShopFloorDirectory = this.textSFDir.Text;
            stationConfig.ConfigShopFloorFileName = this.textSFFileName.Text;

            stationConfig.PFSDatabaseName = this.textPFSDatabase.Text;
            stationConfig.PFSServerIPAddress = this.textPFSServer.Text;
            stationConfig.PFSWorkCenter = this.textPFSWorkCenter.Text;

            stationConfig.UpdateStationData();
        }

        private void textReportDirectory_Clicked(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Show the FolderBrowserDialog.
            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                textReportDirectory.Text = folderDlg.SelectedPath;
                //Environment.SpecialFolder root = folderDlg.RootFolder;
            }
        }


        private void tbLabelFileDirectory_Clicked(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Show the FolderBrowserDialog.
            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                tbLabelFileDirectory.Text = folderDlg.SelectedPath;
                //Environment.SpecialFolder root = folderDlg.RootFolder;
            }

        }

    }
}
