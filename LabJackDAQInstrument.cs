﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using FixtureInstrumentBase;
using LabJack.LabJackUD;

namespace SensorManufSY2
{
    class LabJackDAQInstrument : FixtureInstrument, IFixtureInstrument
    {
        private U3 u3 = null;                               // instance for the LabJack

        public LabJackDAQInstrument()
        {
            currentInstrument = InstrumentID.LABJACK;
        }

        //---------------------------------------
        //--------- interface functions
        //---------------------------------------

        /// <summary>
        /// Add channels to the channel list.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="channel"></param>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        public bool AddChannel(string name, int channel, ChannelType type, int range)
        {
            if (u3 != null)
            {
                LastFunctionStatus = InstrumentError_ChannelAddInstOpen;
                LastFunctionErrorMessage = "Labjack is open. Disconnect before adding channels";
                return false;
            }
            if (ChannelList == null)
                ChannelList = new List<Channel>();
            Channel temp = new Channel();
            temp.name = name;
            temp.channel = channel;
            temp.type = type;
            temp.lastReading = 0;
            temp.instrumentID = currentInstrument;
            temp.range = range;
            ChannelList.Add(temp);
            return true;
        }

        /// <summary>
        /// Connect to the instrument. Channel List needs to be setup before calling.
        /// </summary>
        /// <returns></returns>
        public bool Connect()
        {
            LJUD.IO ioType = 0;
            LJUD.CHANNEL channel = 0;
            double dblValue = 0;
            int dummyInt = 0;
            double dummyDouble = 0;
            bool finished = false;

            if (ChannelList == null)
            {
                LastFunctionErrorMessage = "Channel List not setup. Call AddChannel first.";
                LastFunctionStatus = InstrumentError_NoChannelList;
                return false;
            }

            try
            {
                u3 = new U3(LJUD.CONNECTION.USB, "0", true); // Connection through USB
            }
            catch (Exception)
            {
                LastFunctionErrorMessage = "Error connecting to the LabJack";
                LastFunctionStatus = InsturmentError_CouldNotConnect;
                return false;
            }
            // now have to figure out the channel config.

            //Start by using the pin_configuration_reset IOType so that all
            //pin assignments are in the factory default condition.
            LJUD.ePut(u3.ljhandle, LJUD.IO.PIN_CONFIGURATION_RESET, 0, 0, 0);

            // now loop thru and set the correct pin defs
            foreach (var item in ChannelList)
            {
                if ((item.type == ChannelType.ACInput) | (item.type == ChannelType.DCInput) | (item.type == ChannelType.Temperature))
                {
                    LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_ANALOG_ENABLE_BIT, item.channel, 1, 0, 0);
                }
                if ((item.type == ChannelType.DInput) | (item.type == ChannelType.DOutput) | (item.type == ChannelType.Contact))
                {
                    LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_ANALOG_ENABLE_BIT, item.channel, 0, 0, 0);
                }
                if ((item.type == ChannelType.DOutput) | (item.type == ChannelType.Contact))
                {
                    LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_DIGITAL_BIT, item.channel, 0, 0, 0);
                }
                    if (item.type == ChannelType.Frequency)
                {
                    // it has a LED detector connected to the LabJack
                    // set timer0 to F105, 48MHz
                    LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_CONFIG, LJUD.CHANNEL.TIMER_COUNTER_PIN_OFFSET, 5, 0, 0);
                    LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_CONFIG, LJUD.CHANNEL.TIMER_CLOCK_BASE, (double)LJUD.TIMERCLOCKS.MHZ48_DIV, 0, 0);
                    LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_CONFIG, LJUD.CHANNEL.TIMER_CLOCK_DIVISOR, 1, 0, 0);
                    LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_CONFIG, LJUD.CHANNEL.NUMBER_TIMERS_ENABLED, 1, 0, 0);
                    //              LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_TIMER_MODE, 0, (double)LJUD.TIMERMODE.FALLINGEDGES32, 0, 0);
                    LJUD.AddRequest(u3.ljhandle, LJUD.IO.PUT_TIMER_MODE, 0, (double)LJUD.TIMERMODE.DUTYCYCLE, 0, 0);
                }
            }
            LJUD.GoOne(u3.ljhandle);
            // clear out the results from all the adds
            try { LJUD.GetFirstResult(u3.ljhandle, ref ioType, ref channel, ref dblValue, ref dummyInt, ref dummyDouble); }
            catch (Exception e)
            {
                LastFunctionErrorMessage = "Error setting up LabJack. " + e.Message;
                LastFunctionStatus = InstrumentError_ChannelAdd;
                return false;
            }

            while (!finished)
            {
                try { LJUD.GetNextResult(u3.ljhandle, ref ioType, ref channel, ref dblValue, ref dummyInt, ref dummyDouble); }
                catch (LabJackUDException e)
                {
                    // If we get an error, report it.  If the error is NO_MORE_DATA_AVAILABLE we are done
                    if (e.LJUDError == UE9.LJUDERROR.NO_MORE_DATA_AVAILABLE)
                        finished = true;
                    else
                    {
                        LastFunctionErrorMessage = "Error setting up LabJack. " + e.Message;
                        LastFunctionStatus = InstrumentError_ChannelAdd;
                        return false;
                    }
                }
            }

            return true;
        }

        public void Disconnect()
        {
            u3 = null;
            ChannelList = null;
        }

        public double ReadChannel(string channelName)
        {
            Channel[] channelData = new Channel[1];

            channelData[0] = new Channel();

            channelData[0].name = channelName;
            ReadListOfChannels(ref channelData); // do not have to do errorcheck. LastErrorxxx set by ReadListOfChannels
            return channelData[0].lastReading;

        }

        /// <summary>
        /// Measure the inputs defined in channelNames. Only the name is used. The value is passed back in the
        /// same list.
        /// </summary>
        /// <param name="channelNames"></param>
        public void ReadListOfChannels(ref Channel[] channelsData)
        {
            bool bresult = false;
            Channel channelData;
            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            if (u3 == null)          // if the labjack is not set up
            {
                LastFunctionStatus = InsturmentError_CouldNotConnect;
                LastFunctionErrorMessage = "Labjack has not been setup.";
                return;
            }
            for (int i = 0; i < channelsData.Length; i++)
            {
                if ((channelData = FindChannelData(channelsData[i].name)) == null)       // if the name was not found
                {
                    LastFunctionErrorMessage = "Channel " + channelsData[i].name + " is not defined.";
                    LastFunctionStatus = InstrumentError_RequestedChannelNotFound;
                    return;
                }
                try
                {
                    if ((channelData.type == ChannelType.ACInput) | (channelData.type == ChannelType.DCInput) | (channelData.type == ChannelType.Temperature))
                    {
                        channelsData[i].lastReading = ReadAnalogChannel(channelData.name);
                        if (LastFunctionStatus != 0)     // LastErrorxxx set by ReadAnalogChannel
                        {
                            return;
                        }
                    }
                    else if (channelData.type == ChannelType.DInput)
                    {
                        bresult = ReadDigitalChannel(channelData.name);
                        if (LastFunctionStatus != 0)
                        {
                            return;
                        }
                        channelsData[i].lastReading = bresult ? 1 : 0;
                    }
                    else if (channelData.type == ChannelType.Frequency)
                    {
                        channelsData[i].lastReading = ReadFrequency(channelData.name);
                        if (LastFunctionStatus != 0)
                        {
                            return;
                        }
                    }
                    else
                    {
                        LastFunctionStatus = InstrumentError_WrongChannelType;
                        LastFunctionErrorMessage = "Channel " + channelsData[i].name + " is not a input channel.";
                        return;
                    }
                }
                catch (LabJackUDException ex)
                {
                    LastFunctionErrorMessage = ex.Message + ex.LJUDError.ToString();
                    LastFunctionStatus = InstrumentError_ReadError;
                    return;
                }
            }
            return;
        }

        public void SetChannel(string channelName, double value)
        {
            string[] channelNames = new string[1];
            Channel channelData;
            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            channelNames[0] = channelName;

            if ((channelData = FindChannelData(channelNames[0])) == null)       // if the name was not found
            {
                LastFunctionErrorMessage = "Channel " + channelNames[0] + " is not defined.";
                LastFunctionStatus = InstrumentError_RequestedChannelNotFound;
                return;
            }

            if ((channelData.type == ChannelType.DOutput) | (channelData.type == ChannelType.Contact))
            {
                SetDigitalChannel(channelName, (int)value);
            }
            else if(channelData.type == ChannelType.DAC)
            {
                SetDAC(channelName, value);
            }
            else {
                LastFunctionErrorMessage = "Channel " + channelNames[0] + " is not a output";
                LastFunctionStatus = InstrumentError_WrongChannelType;
                return;
            }
        }

        //-------------------------------------------------
        //  LabJack support for interface functions
        //------------------------------------------------

        private double ReadAnalogChannel(string channelName)
        {
            double readvalue = 0;
            Channel channelToRead = null;

            if (u3 == null)          // if the labjack is not set up
            {
                LastFunctionStatus = InsturmentError_CouldNotConnect;
                LastFunctionErrorMessage = "Labjack has not been setup.";
                return 0;
            }

            if ((channelToRead = FindChannelData(channelName)) == null)       // if the name was not found
            {
                LastFunctionErrorMessage = "Channel " + channelName + " is not defined.";
                LastFunctionStatus = InstrumentError_RequestedChannelNotFound;
                return 0;
            }
            if ((channelToRead.type != ChannelType.ACInput) & (channelToRead.type != ChannelType.DCInput) &
                        (channelToRead.type != ChannelType.Temperature))
            {
                LastFunctionErrorMessage = "Channel " + channelName + " is not a analog input.";
                LastFunctionStatus = InstrumentError_WrongChannelType;
                return 0;
            }
            try
            {
                LJUD.eGet(u3.ljhandle, LJUD.IO.GET_AIN, channelToRead.channel, ref readvalue, 0);
            }
            catch (LabJackUDException ex)
            {
                LastFunctionErrorMessage = ex.Message + ex.LJUDError.ToString();
                LastFunctionStatus = InstrumentError_ReadError;
                return 0;
            }
            return readvalue;
        }

        /// <summary>
        /// Read a digital input. Check LastFunctionStatus to make sure successfull.
        /// </summary>
        /// <param name="channelName">Text name of channel to read</param>
        /// <returns></returns>
        private bool ReadDigitalChannel(string channelName)
        {
            int readvalue = 0;
            Channel channelToRead;


            if (u3 == null)          // if the labjack is not set up
            {
                LastFunctionStatus = InsturmentError_CouldNotConnect;
                LastFunctionErrorMessage = "Labjack has not been setup.";
                return false;
            }

            if ((channelToRead = FindChannelData(channelName)) == null)       // if the name was not found
            {
                LastFunctionErrorMessage = "Channel " + channelName + " is not defined.";
                LastFunctionStatus = InstrumentError_RequestedChannelNotFound;
                return false;
            }
            if (channelToRead.type != ChannelType.DInput)
            {
                LastFunctionErrorMessage = "Channel " + channelName + " is not a digital input.";
                LastFunctionStatus = InstrumentError_WrongChannelType;
                return false;
            }
            try
            {
                LJUD.eDI(u3.ljhandle, channelToRead.channel, ref readvalue);
            }
            catch (LabJackUDException ex)
            {
                LastFunctionErrorMessage = ex.Message + ex.LJUDError.ToString();
                LastFunctionStatus = InstrumentError_ReadError;
                return false;
            }
            if (readvalue == 0)     // weak pull up shorted when cover is closed
                return true;
            else
                return false;
        }

        private void SetDigitalChannel(string channelName, int value)
        {
            Channel channelToRead;

            if (u3 == null)          // if the labjack is not set up
            {
                LastFunctionStatus = InsturmentError_CouldNotConnect;
                LastFunctionErrorMessage = "Labjack has not been setup.";
                return;
            }

            if ((channelToRead = FindChannelData(channelName)) == null)       // if the name was not found
            {
                LastFunctionErrorMessage = "Channel " + channelName + " is not defined.";
                LastFunctionStatus = InstrumentError_RequestedChannelNotFound;
                return;
            }
            if (channelToRead.type != ChannelType.DOutput)
            {
                LastFunctionErrorMessage = "Channel " + channelName + " is not a digital output.";
                LastFunctionStatus = InstrumentError_WrongChannelType;
                return;
            }
            try
            {
                LJUD.eDO(u3.ljhandle, channelToRead.channel, value);
            }
            catch (LabJackUDException ex)
            {
                LastFunctionErrorMessage = ex.Message + ex.LJUDError.ToString();
                LastFunctionStatus = InstrumentError_ReadError;
                return;
            }
        }

        void SetDAC(string channelName, double value)
        {
            Channel channelToRead;

            if (u3 == null)          // if the labjack is not set up
            {
                LastFunctionStatus = InsturmentError_CouldNotConnect;
                LastFunctionErrorMessage = "Labjack has not been setup.";
                return;
            }

            if ((channelToRead = FindChannelData(channelName)) == null)       // if the name was not found
            {
                LastFunctionErrorMessage = "Channel " + channelName + " is not defined.";
                LastFunctionStatus = InstrumentError_RequestedChannelNotFound;
                return;
            }
            if (channelToRead.type != ChannelType.DAC)
            {
                LastFunctionErrorMessage = "Channel " + channelName + " is not a DAC.";
                LastFunctionStatus = InstrumentError_WrongChannelType;
                return;
            }
            try
            {
                LJUD.eDAC(u3.ljhandle, channelToRead.channel, value, 0, 0, 0);
            }
            catch (LabJackUDException ex)
            {
                LastFunctionErrorMessage = ex.Message + ex.LJUDError.ToString();
                LastFunctionStatus = InstrumentError_ReadError;
            }
        }

        private double ReadFrequency(string channelName)
        {
            double dblValue = 0;
            double[] valuearraylow = new double[10];
            double[] valuearrayhigh = new double[10];
            double[] valuearrayraw = new double[10];
            double[] dummyDoubleArray = { 0 };
            double[] valuearray = new double[10];
            double[] valuearraydc = new double[10];
            double low = 0;
            double high = 0;


            try
            {
                for (int i = 0; i < 10; i++)
                {
                        //Request a read from the timer0.
                        LJUD.eGet(u3.ljhandle, LJUD.IO.GET_TIMER, (LJUD.CHANNEL)0, ref dblValue, dummyDoubleArray);
                        valuearrayraw[i] = dblValue;
                        low = (double)(((ulong)dblValue) / (65536));
                        valuearraylow[i] = low;
                        high = (double)(((ulong)dblValue) % (65536));
                        valuearrayhigh[i] = high;
                        valuearraydc[i] = 100 * (high / (high + low));
                        valuearray[i] = 48000000 / (low + high);          // using a 48Mhz clock
                        Thread.Sleep(10);
                        if ((low == 0) | (high == 0))
                            i--;
                }
            }
            catch (LabJackUDException ex)
            {
                LastFunctionStatus = InstrumentError_ReadError;
                LastFunctionErrorMessage = "Error reading Frequency. " + ex.Message;
                return 0;
            }

            return  valuearray.Average();
        }

    }
}
