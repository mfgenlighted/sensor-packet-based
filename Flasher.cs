﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.IO.Ports;
using System.Diagnostics;

namespace SensorManufSY2
{
    public class Flasher
    {
        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        // 
        // From the initializing routine, add
        //     sensor.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(xxxx);
        //  where xxxx is the routine to update the windows.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs> StatusUpdated;

        protected virtual void OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }

        public string LastErrorMsg = string.Empty;
        public bool LastErrorStatus = true;
        public int LastErrorCode = Error_NoError;
        public List<string> ErrMessages = null;         // holds the #ERR message recived
        private string comPort = string.Empty;          // Flasher serial port name
        private SerialPort serialPort = null;            // Flasher serial port

        public const int Error_NoError = 0;

        // system errors
        public const int Error_PortNotOpen = 1;
        public const int Error_ErrResponse = 2;
        public const int Error_OpenError = 3;
        public const int Error_PortNotExist = 4;
        public const int Error_PortNotInitialized = 5;
        public const int Error_FileSelectionFailed_Timeout = 6;
        public const int Error_FileSelectionFailed_NACKReceived = 7;
        public const int Error_FileSelectionFailed_OKNotReceived = 8;
        public const int Error_ProgrammingFailed_Timeout = 9;
        public const int Error_ProgrammingFailed_NACKReceived = 10;
        public const int Error_StatusRead_Timeout = 11;
        public const int Error_StatusRead_NACKReceived = 12;
        public const int Error_StatusRead_Error = 13;
        public const int Error_FlasherNotReady = 14;
        public const int Error_FileDoesNotExist = 15;



        public Flasher(string comPort)
        {
            this.comPort = comPort;
        }

        public Flasher() { }
         

        public bool Open(string comPort)
        {
            this.comPort = comPort;
            return Open();
        }

        /// <summary>
        /// Will open the Flasher com port.
        /// </summary>
        /// <remarks>
        /// baud: 9600
        /// 8 data
        /// no parity
        /// 1 stop
        /// XON/OFF disabled
        /// 
        /// LastErrorCode
        /// -3  Error_OpenError
        /// -4  Error_PortNotExist
        /// </remarks>
        /// <returns></returns>
        public bool Open()
        {
            bool status = true;

            LastErrorMsg = string.Empty;
            LastErrorCode = Error_NoError;
            LastErrorStatus = status;

            if (serialPort != null)                        // if already opne
            {
                if (serialPort.IsOpen)
                    serialPort.Close();                         // close it
            }


            if (CheckForSerialPort(comPort))
            {
                // port valid on this system, so try to open
                try
                {
                    serialPort = new SerialPort(comPort, 9600);
                    serialPort.ReadTimeout = 500;
                    serialPort.WriteTimeout = 500;
                    serialPort.DtrEnable = false;
                    serialPort.Handshake = Handshake.None;
                    serialPort.Parity = Parity.None;
                    serialPort.NewLine = "\r";
                    serialPort.Open();           // open the serial port
                }
                catch (Exception ex)
                {
                    LastErrorMsg = string.Format("Error opening {0}", comPort + "Error:" + ex);
                    LastErrorCode = Error_OpenError;
                    status = false;
                }
            }
            else
            {
                LastErrorMsg = "Dut serial port " + comPort + " is not a valid com port on this computer";
                LastErrorCode = Error_PortNotExist;
                status = false;
            }

            LastErrorStatus = status;
            return status;
        }

        public void ClearBuffers()
        {
            serialPort.DiscardOutBuffer();
            serialPort.DiscardInBuffer();
        }

        /// <summary>
        /// Will read any messages pending in the flasher
        /// </summary>
        public void ClearFlasher()
        {
            serialPort.ReadExisting();
        }

        /// <summary>
        /// Closes the Flasher com port
        /// </summary>
        public void Close()
        {
            LastErrorMsg = string.Empty;
            LastErrorCode = Error_NoError;
            LastErrorStatus = true;

            if (serialPort != null)
            {
                serialPort.Close();
                serialPort.Dispose();
            }
        }

        /// <summary>
        /// Check to see if file is on the Flasher.
        /// </summary>
        /// <param name="file">file name to check for. Can ber a partial</param>
        /// <returns>true = file found, false=not found or flasher error. sets LastError info</returns>
        public bool CheckForFile(string file)
        {
            string response = string.Empty;
            LastErrorMsg = string.Empty;
            LastErrorCode = 0;
            int oldtimout = serialPort.ReadTimeout;
            bool fileFound = false;

            ClearBuffers();
            if (!SendCommand("#FLIST", ref response))
            {
                LastErrorMsg = "Error reading file list. Got " + response;
                LastErrorCode = Error_FileSelectionFailed_NACKReceived;
                return false;
            }
            try
            {
                serialPort.ReadTimeout = 5000;
                response = serialPort.ReadTo("#OK");
                serialPort.ReadLine();                  // get the CR at end
                serialPort.ReadTimeout = oldtimout;
            }
            catch (Exception)
            {
                LastErrorMsg = "Error reading file list. No #OK found. Got " + response;
                LastErrorCode = Error_FileSelectionFailed_OKNotReceived;
                return false;
            }
            if (response.Contains(file))
                fileFound = true;

            return fileFound;
        }

        /// <summary>
        /// Program the dut. If a file name is passed, it will program with that file name.
        /// </summary>
        /// <param name="file">optional file name</param>
        /// <returns></returns>
        enum waitStates { start, ack, status_initializing, status_connecting, status_erasing, status_programming, status_verifying, ok};
        public bool ProgramDevice(string file, int maxTimeoutSecs, int retries)
        {
            bool status = true;
            string response = string.Empty;
            DateTime startStepTime = DateTime.Now;
            int maxProgrammingWaitTimeSecs = maxTimeoutSecs;
            bool foundOK;
            bool doneLooking;
            waitStates current = waitStates.start;

            LastErrorMsg = string.Empty;
            LastErrorCode = Error_NoError;
            LastErrorStatus = status;

            if (serialPort == null)
            {
                LastErrorMsg = "Port not initialized";
                LastErrorCode = Error_PortNotInitialized; ;
                LastErrorStatus = false;
                return false;
            }

            ClearBuffers();
            if (file == string.Empty)
            {
                if (CheckForFile("FLASHER"))
                {
                    if (!SelectFile("FLASHER", ref LastErrorMsg, ref LastErrorCode))
                    {
                        LastErrorStatus = false;
                        return false;
                    }
                }
                else        // file does not exist. CheckForFile sets LastError info
                    return false;
            }
            else
            {
                if (CheckForFile(file))
                { 
                    if (!SelectFile(file, ref LastErrorMsg, ref LastErrorCode))
                    {
                        LastErrorStatus = false;
                        return false;
                    }
                }
                else        // file does not exist. CheckForFile sets LastError info
                    return false;
            }

            // program the part
            ClearBuffers();
            UpdateStatusWindow("PRG Status:");
            doneLooking = false;
            foundOK = false;
            current = waitStates.start;         // start the programming
            do
            {
                switch (current)
                {
                    case waitStates.start:      // isuue the #auto command to start programming
                        try
                        {
                            serialPort.WriteLine("#AUTO");
                        }
                        catch (TimeoutException)
                        {
                            foundOK = false;
                            doneLooking = true;
                            LastErrorMsg = "Timed out sending #AUTO";
                            LastErrorCode = Error_StatusRead_Timeout;
                            LastErrorStatus = false;
                        }
                        if (!GetCmdResponse(out response, 1))
                        {
                            foundOK = false;
                            doneLooking = true;
                            LastErrorMsg = "Timed out waiting for #AUTO ACK responses";
                            LastErrorCode = Error_StatusRead_Timeout;
                            LastErrorStatus = false;
                        }
                        if (!response.Contains("#ACK"))
                        {
                            foundOK = false;
                            doneLooking = true;
                            LastErrorMsg = "Response to #AUTO was not #ACK. Found '" + response + "'";
                            LastErrorCode = Error_StatusRead_Error;
                            LastErrorStatus = false;
                        }
                        else
                        {
                            // found the #ACK
                            current = waitStates.status_initializing;       // now wait for the #STATUS:INITALIZING message
                            startStepTime = DateTime.Now;
                        }
                        break;

                    // wait for #STATUS:INITIALIZING message 
                    case waitStates.status_initializing:
                        if (!GetCmdResponse(out response, 2))       // should be very quick
                        {
                            foundOK = false;
                            doneLooking = true;
                            LastErrorMsg = "Timed out waiting for #STATUS:INITIALIZING responses";
                            LastErrorCode = Error_StatusRead_Timeout;
                            LastErrorStatus = false;
                        }
                        if (!response.Contains("#STATUS:INITIALIZING"))
                        {
                            foundOK = false;
                            doneLooking = true;
                            LastErrorMsg = "Message #STATUS:INITIALIZING not found. Found '" + response + "'";
                            LastErrorCode = Error_StatusRead_Error;
                            LastErrorStatus = false;
                        }
                        else
                        {
                            // found the #STATUS:INITIALIZING
                            current = waitStates.status_connecting;       // now wait for the #STATUS:CONNECTING message
                            startStepTime = DateTime.Now;
                        }
                        break;

                    // wait for the #STATUS:CONNECTING message
                    case waitStates.status_connecting:
                        if (!GetCmdResponse(out response, 4))           // time to do the initializing step
                        {
                            foundOK = false;
                            doneLooking = true;
                            LastErrorMsg = "Timed out waiting for #STATUS:CONNECTING responses";
                            LastErrorCode = Error_StatusRead_Timeout;
                            LastErrorStatus = false;
                        }
                        if (!response.Contains("#STATUS:CONNECTING"))
                        {
                            foundOK = false;
                            doneLooking = true;
                            LastErrorMsg = "Message #STATUS:CONNECTING not found. Found '" + response + "'";
                            LastErrorCode = Error_StatusRead_Error;
                            LastErrorStatus = false;
                        }
                        else
                        {
                            // found the #STATUS:CONNECTING
                            current = waitStates.status_erasing;       // now wait for the #STATUS:CONNECTING message
                            startStepTime = DateTime.Now;
                        }
                        break;

                    // wait for the #STATUS:ERASING message
                    // it sometimes get stuck here.
                    case waitStates.status_erasing:
                        if (!GetCmdResponse(out response, 5))       // time to do the connect step
                        {
                            foundOK = false;
                            doneLooking = true;
                            LastErrorMsg = "Timed out waiting for #STATUS:CONNECTING responses";
                            LastErrorCode = Error_StatusRead_Timeout;
                            LastErrorStatus = false;
                        }
                        if (!response.Contains("#STATUS:ERASING"))
                        {
                            foundOK = false;
                            doneLooking = true;
                            LastErrorMsg = "Message #STATUS:ERASING not found. Found '" + response + "'";
                            LastErrorCode = Error_StatusRead_Error;
                            LastErrorStatus = false;
                        }
                        else
                        {
                            // found the #STATUS:ERASING
                            current = waitStates.status_programming;       // now wait for the #STATUS:CONNECTING message
                            startStepTime = DateTime.Now;
                        }
                        break;

                    // wait for the #STATUS:PROGRAMMING message
                    case waitStates.status_programming:       // waiting for #STATUS:PROGRAMMING
                        if (!GetCmdResponse(out response, 2))              // time to do the errasing step
                        {
                            foundOK = false;
                            doneLooking = true;
                            LastErrorMsg = "Timed out waiting for #STATUS:CONNECTING responses";
                            LastErrorCode = Error_StatusRead_Timeout;
                            LastErrorStatus = false;
                    }
                        if (!response.Contains("#STATUS:PROGRAMMING"))
                        {
                            foundOK = false;
                            doneLooking = true;
                            LastErrorMsg = "Message #STATUS:PROGRAMMING not found. Found '" + response + "'";
                            LastErrorCode = Error_StatusRead_Error;
                            LastErrorStatus = false;
                        }
                        else
                        {
                            // found the #STATUS:PROGRAMMING
                            current = waitStates.status_verifying;       // now wait for the #STATUS:VERIFYING message
                            startStepTime = DateTime.Now;
                        }
                        break;


                    // wait for the #STATUS:VERIFYING message
                    case waitStates.status_verifying:
                        if (!GetCmdResponse(out response, 10))          // time it takes to do the programming step
                        {
                            foundOK = false;
                            doneLooking = true;
                            LastErrorMsg = "Timed out waiting for #STATUS:VERIFYING responses";
                            LastErrorCode = Error_StatusRead_Timeout;
                            LastErrorStatus = false;
                        }
                        if (!response.Contains("#STATUS:VERIFYING"))
                        {
                            foundOK = false;
                            doneLooking = true;
                            LastErrorMsg = "Message #STATUS:VERIFYING not found. Found '" + response + "'";
                            LastErrorCode = Error_StatusRead_Error;
                            LastErrorStatus = false;
                        }
                        else
                        {
                            // found the #STATUS:VERIFYING
                            current = waitStates.ok;       // now wait for the #OK message
                            startStepTime = DateTime.Now;
                        }
                        break;

                    // wait for the #OK message
                    case waitStates.ok:
                        if (!GetCmdResponse(out response, 2))       // time it takes to do the verify step
                        {
                            foundOK = false;
                            doneLooking = true;
                            LastErrorMsg = "Timed out waiting for #OK responses";
                            LastErrorCode = Error_StatusRead_Timeout;
                            LastErrorStatus = false;
                        }
                        if (!response.Contains("#OK"))
                        {
                            foundOK = false;
                            doneLooking = true;
                            LastErrorMsg = "Message #OK not found. Found '" + response + "'";
                            LastErrorCode = Error_StatusRead_Error;
                            LastErrorStatus = false;
                        }
                        else
                        {
                            // found the #OK
                            foundOK = true;
                            doneLooking = true;
                        }
                        break;

                    default:
                        break;
                }
            } while (!doneLooking);
            return foundOK;
        }

        /// <summary>
        /// get the response to the #STATUS command.
        /// </summary>
        /// <returns></returns>
        public string GetStatus()
        {
            string response = string.Empty;
            bool stillLooking = true;

            LastErrorMsg = string.Empty;
            LastErrorCode = Error_NoError;
            LastErrorStatus = true;

            if (serialPort == null)
            {
                LastErrorMsg = "Port not initialized";
                LastErrorCode = Error_PortNotInitialized; ;
                LastErrorStatus = false;
                return string.Empty;
            }

            serialPort.WriteLine("#STATUS");    // use default
            do
            {
                try
                {
                    response += serialPort.ReadLine();
                }
                catch (Exception ex)
                {
                    LastErrorMsg = "Error getting status. " + ex.Message;
                    LastErrorCode = Error_StatusRead_Timeout;
                    LastErrorStatus = false;
                    return response;
                }
                // got a response. Should be ACK or NACK or ERR
                if (response.Contains("#NACK"))
                {
                    LastErrorMsg = "Error getting status. Got " + response;
                    LastErrorCode = Error_StatusRead_NACKReceived;
                    LastErrorStatus = false;
                    return string.Empty;
                }
                else if (response.Contains("#ACK"))
                {
                    // if it was ACK, wait for the  status
                    try
                    {
                        response += serialPort.ReadLine();
                    }
                    catch (Exception ex)
                    {
                        LastErrorMsg = "Error getting status, waiting for STATUS: after ACK. " + ex.Message;
                        LastErrorCode = Error_StatusRead_Timeout;
                        LastErrorStatus = false;
                    }
                    stillLooking = false;

                }
                else        // got something other than ACK or NACK. capture it
                {
                    response = response.Substring(response.IndexOf(':') + 1);
                }

            } while (stillLooking);
            return response;
        }

        public string CancelOperation()
        {
            string response = string.Empty;

            LastErrorMsg = string.Empty;
            LastErrorCode = Error_NoError;
            LastErrorStatus = true;

            if (serialPort == null)
            {
                LastErrorMsg = "Port not initialized";
                LastErrorCode = Error_PortNotInitialized; ;
                LastErrorStatus = false;
                return string.Empty;
            }

            serialPort.WriteLine("#CANCEL");    // use default
            try
            {
                response = serialPort.ReadLine();
            }
            catch (Exception ex)
            {
                LastErrorMsg = "Error getting status. " + ex.Message;
                LastErrorCode = Error_StatusRead_Timeout;
                LastErrorStatus = false;
                return string.Empty;
            }
            // got a response. Should be ACK or NACK
            if (response.Contains("#NACK"))
            {
                LastErrorMsg = "Error getting status. Got " + response;
                LastErrorCode = Error_StatusRead_NACKReceived;
                LastErrorStatus = false;
                return string.Empty;
            }

            try
            {
                response = serialPort.ReadLine();
            }
            catch (Exception ex)
            {
                LastErrorMsg = "Error getting status. " + ex.Message;
                LastErrorCode = Error_StatusRead_Timeout;
                LastErrorStatus = false;
                return string.Empty;
            }
            return response;
        }

        public bool SelectFile(string filename, ref string lastErrorMsg, ref int lastErrorCode)
        {
            string response = string.Empty;
            int oldtimeout = serialPort.ReadTimeout;

            if (filename == string.Empty)
            {
                if (!SendCommand("#SELECT FLASHER", ref response))
                {
                    lastErrorMsg = "Error selecting file FLASHER to program. Got " + response;
                    lastErrorCode = Error_FileSelectionFailed_NACKReceived;
                    return false;
                }
            }
            else
            {
                if (!SendCommand("#SELECT " + filename, ref response))
                {
                    lastErrorMsg = "Error selecting file " + filename + " to program. Got " + response;
                    lastErrorCode = Error_FileSelectionFailed_NACKReceived;
                    return false;
                }
            }

            // if it was ACK, wait for the OK
            try
            {
                response = serialPort.ReadLine();
            }
            catch (Exception ex)
            {
                lastErrorMsg = "Error selecting file to program, waiting for OK after ACK. " + ex.Message;
                lastErrorCode = Error_FileSelectionFailed_Timeout;
                return false;
            }
            if (!response.Contains("#OK"))      // if did not get OK
            {
                lastErrorMsg = "Error selecting file to program. Did not get OK after ACK. Got " + response;
                lastErrorCode = Error_FileSelectionFailed_OKNotReceived;
                return false;
            }
            // so now file is set. now do the programing.

            return true;
        }

        public bool GetExistingFileToUse(ref string pickedfile, ref string lastErrorMsg, ref int lastErrorCode)
        {
            string response = string.Empty;
            List<string> filelist;
            string[] readlist;
            lastErrorMsg = string.Empty;
            lastErrorCode = 0;
            int oldtimout = serialPort.ReadTimeout;

            ClearBuffers();
            if (!SendCommand("#FLIST", ref response))
            {
                lastErrorMsg = "Error reading file list. Got " + response;
                lastErrorCode = Error_FileSelectionFailed_NACKReceived;
                return false;
            }
            try
            {
                serialPort.ReadTimeout = 5000;
                response = serialPort.ReadTo("#OK");
                serialPort.ReadTimeout = oldtimout;
            }
            catch (Exception)
            {
                lastErrorMsg = "Error reading file list. No #OK found. Got " + response;
                lastErrorCode = Error_FileSelectionFailed_OKNotReceived;
                return false;
            }
            readlist = response.Split('\n');
            filelist = new List<string>();
            foreach (var item in readlist)
            {
                if (item.Contains("DAT"))
                    filelist.Add(item.Substring(0, item.IndexOf('.')));
            }
            FlasherFilePicker iform = new FlasherFilePicker(filelist);
            iform.ShowDialog();
            pickedfile = iform.PickedFile;
            iform.Dispose();
            return true;
        }

        //---------------------
        //  utilities
        //-------------------
        public bool CheckForSerialPort(string comPort)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            // see if this port is in the list of ports on the computer
            foreach (string i in ports)
            {
                if (i.Equals(comPort))         // if found a match
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Send command and wait for ACK/NACK
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="timeoutsec"></param>
        /// <param name=""></param>
        /// <returns></returns>
        private bool SendCommand(string cmd, ref string foundstatus)
        {
            DateTime starttime = DateTime.Now;

            serialPort.WriteLine(cmd);                              // send the command
            try
            {
                foundstatus = serialPort.ReadLine();
            }
            catch (Exception)
            {
                return false;
            }
            // got a response. Should be ACK or NACK
            if (!foundstatus.Contains("#ACK"))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Will read lines of responses till it finds #...CR
        /// </summary>
        /// <param name="response"></param>
        /// <returns>true = #..CR found</returns>
        private bool GetCmdResponse(out string response, int timeoutSecs)
        {
            DateTime startTime = DateTime.Now;
            string readLine = string.Empty;
            response = string.Empty;

            do
            {
                try
                {
                    readLine = serialPort.ReadLine();
                    response = response + readLine;
                    if (response.Contains("#"))
                        return true;
                }
                catch (TimeoutException)
                {
                    if (DateTime.Now > startTime.AddSeconds(timeoutSecs))
                    {
                        return false;
                    }
                }
            } while (true);

        }
    }
}
