﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using LimitsDatabase;
using TestStationErrorCodes;

namespace SensorManufSY2
{
    public partial class SensorDVTMANTests
    {
        public int PerformReboot(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, ref string resultmessage)
        {
            string outmsg = string.Empty;
            int readversion = 0;
            int versionStatus = 0;
            string versionResultmsg = string.Empty;
            bool versionCheckGood = true;
            int startupDelayMs = 10000;

            resultmessage = string.Empty;

            testDB.CreateStepRecord(parameters.name, parameters.step_number);

            if (parameters.DoesParameterExist("TimeOutMS"))
            {
                if (!Int32.TryParse(parameters.GetParameterValue("TimeOutMS"), out startupDelayMs))
                {
                    resultmessage = "Parameter TimeOutMS must be a interger. Found " + startupDelayMs;
                    testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, resultmessage, (int)ErrorCodes.System.ParameterFormatError);
                    return (int)ErrorCodes.System.ParameterFormatError;
                }

            }

            if (!fixture.SensorConsole.PerformReboot())
            {
                resultmessage = fixture.SensorConsole.LastErrorMessage;
                testDB.AddStepResult("FAIL", resultmessage, fixture.SensorConsole.LastErrorCode);             // record PASS/FAIL type
                return fixture.SensorConsole.LastErrorCode;
            }
            // wait for display msg version. just look for any
            for (int j = 0; j < 3; j++)             // try mulitiple times just in case trash when turned on
            {
                versionStatus = 0;
                versionResultmsg = string.Empty;
                versionCheckGood = true;
                if (!fixture.SensorConsole.WaitForMsgVersion(ref readversion, startupDelayMs))       // if bad
                {
                    versionResultmsg = "timeout waiting for display msg version";             // save results
                    versionStatus = (int)ErrorCodes.Program.CommandTimeout;
                    versionCheckGood = false;
                }
                else        // found msg version response
                {
                    break;  // don't care what it is, so get out of retry loop
                }
            }
            if (versionCheckGood)       // if found the message
                testDB.AddStepResult("PASS", "", 0);             // record PASS/FAIL type
            else
                testDB.AddStepResult("FAIL", versionResultmsg, versionStatus);             // record PASS/FAIL type

            return 0;

        }

    }
}
