﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Diagnostics;

namespace SensorManufSY2
{
    public class TestRadioConsole : IDisposable
    {
        private bool disposed = false;

        //----------------------------------------
        // serial port values
        //----------------------------------------
        private SerialPort serialPort = null;            // dut serial port
        private string comPort = string.Empty;
        private string serialBuffer = string.Empty;      // buffer of incoming serial data
        private string readBuffer = string.Empty;        // buffer read on one event
        private string readTestRadioMac = string.Empty;  // mac read from the test radio
        private int setTestChannel = 15;

        public string LastErrorMessage = string.Empty;

        private bool displaySendChar = false;
        private bool displayRcvChar = false;

        private bool foundLF = false;
        private string dumpFileName = string.Empty;
        private object fileLock = new object();

        public bool DisplaySendChar { set { displaySendChar = value; } }
        public bool DisplayRcvChar { set { displayRcvChar = value; } }
        public string MAC { get { return readTestRadioMac; } }

        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs>
            StatusUpdated;

        protected virtual void
            OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            Close();
            disposed = true;
        }

        public void Open(string comPort)
        {
            if (serialPort != null)                        // if already opne
            {
                serialPort.Close();                         // close it
                serialPort.Dispose();                       // and get rid of it
                this.comPort = string.Empty;
            }

            if (CheckForSerialPort(comPort))
            {
                // port valid on this system, so try to open
                try
                {
                    Debug.WriteLine("Opening Ref Radio port");
                    serialPort = new SerialPort(comPort, 115200);
                    serialPort.DtrEnable = false;
                    serialPort.Handshake = Handshake.None;
                    serialPort.Parity = Parity.None;
                    serialPort.ReadTimeout = 10000;     // 10 second timeout
                    //serialPort.DataReceived += new SerialDataReceivedEventHandler(serialDataReceived);
                    serialPort.Open();           // open the serial port
                    this.comPort = comPort;
                    disposed = false;
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Error opening {0}: {1}", comPort, ex.Message));
                }
            }
            else
                throw new Exception("Test Radio serial port " + comPort + " is not a valid com port on this computer");
        }

        public void ClearBuffers()
        {
            try
            {
                if ((serialPort != null) & (serialPort.IsOpen))
                {
                    serialPort.DiscardInBuffer();
                    serialPort.DiscardOutBuffer();
                    serialBuffer = string.Empty;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error clearing serial buffers", ex);
            }
        }

        public void Close()
        {
            //if (this.disposed)
            //{
            //    throw new ObjectDisposedException(this.GetType().Name, "Cannot use a disposed object.");
            //}

            try
            {
                Debug.WriteLine("Closing Ref Radio port");
                if (serialPort != null)
                {
                    if (serialPort.IsOpen)
                        serialPort.Close();
                    serialPort.Dispose();
                }
            }
            catch (Exception ex)
            {
                UpdateStatusWindow("Error while closing sensor console port. IsOpen = " + serialPort.IsOpen + "\n" + ex.Message + "\n", StatusType.failed);
            }
        }

        /// <summary>
        /// Sets up the test radio. Port must be open first.
        /// </summary>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        public int Setup(int testChannel, ref string outmsg)
        {
            string rcvline;
            string cmd;

            if (serialPort == null)
            {
                outmsg = "Test Radio Serial port not open.";
                return -1;
            }

            setTestChannel = testChannel;

            outmsg = string.Empty;
            serialPort.WriteLine("d mac");
            rcvline = serialPort.ReadLine();   // read echo
            rcvline = serialPort.ReadLine();   // read mac line
            if (!rcvline.Contains("MAC="))
                throw new Exception("Error reading Test Radio MAC. " + rcvline);
            //configTestRadioMac = rcvline.Substring(10, 2) + rcvline.Substring(13, 2) + rcvline.Substring(16, 2) +
            //            rcvline.Substring(19, 2) + rcvline.Substring(22, 2) + rcvline.Substring(25, 2);
            readTestRadioMac = rcvline.Substring(10, 17);
            serialPort.WriteLine("s d r");
            Thread.Sleep(3000);                     // to let the reset work
            rcvline = serialPort.ReadLine();
            cmd = "s r " + setTestChannel;
            serialPort.WriteLine(cmd);
            rcvline = serialPort.ReadLine();
           
            return 0;
        }

        public void ReadChars()
        {
            throw new System.NotImplementedException();
        }

        public int ReadLine()
        {
            throw new System.NotImplementedException();
        }

        public void WaitForPrompt()
        {
            throw new System.NotImplementedException();
        }

        public void WriteChars()
        {
            throw new System.NotImplementedException();
        }

        //-------------------------------
        // WriteLine
        ///<summary>
        ///  Send a string to DUT
        ///  Will send the string to the dut. CR will be added to the end of the string.
        ///  After sending the string, it will wait for the echo.
        /// </summary> 
        /// <param name="cmdString">string cmdString - string to send. CR will be sent at end.</param>
        /// <param name="timeout">int - number of ms to wait for the echo</param>
        /// <returns>bool - true = command was sent and echo received</returns>
        public bool WriteLine(string cmdString, int timeout)
        {
            string rtnstring = string.Empty;
            bool status = true;
            bool echobad = false;
            int timesSent = 0;
            int x;
            char[] cmdBuf;
            string msg;

            cmdBuf = cmdString.ToCharArray();

            do
            {
                echobad = false;
                try
                {
                    msg = DateTime.Now.ToString("HH:mm:ss.fff") + "->" + cmdString + "\n";
                    if (displaySendChar)         // if display window is enabled
                        UpdateStatusWindow(timesSent.ToString() + ": " + msg, StatusType.serialSend);
                    if (dumpFileName != string.Empty)
                    {
                        lock (fileLock)
                        {
                            using (System.IO.StreamWriter file = new System.IO.StreamWriter(dumpFileName, true))
                            {
                                file.Write("\n" + msg);
                            }
                        }
                    }
                    for (x = 0; x < cmdString.Length; x++)
                    {
                        serialPort.Write(cmdBuf, x, 1);

                        Thread.Sleep(5);
                    }
                    //serialPort.WriteLine(cmdString);
                    serialPort.Write("\n");
                    Thread.Sleep(100);          // give it time to echo
                }
                catch (Exception ex)
                {
                    LastErrorMessage = "WriteLine: Exception sending '" + cmdBuf + "'. Message: " + ex.Message;
                    Trace.TraceInformation(LastErrorMessage);
                    return false;
                }

                status = ReadLine(ref rtnstring, timeout);   // get the echo
                if (!rtnstring.Contains(cmdString))             // if the echo is bad
                {
                    Debug.WriteLine("echo bad. Expect:'" + cmdString + "' Got:'" + rtnstring + "'");
                    Thread.Sleep(500);
                    status = ReadLine(ref rtnstring, timeout);   // get the echo
                    if (!rtnstring.Contains(cmdString))             // if the echo is bad
                    {
                        Debug.WriteLine("Reread echo bad. Got:'" + rtnstring + "'");
                        echobad = true;
                        timesSent++;
                        //DutWaitForPrompt(2000, uutPrompt, out rtnstring);       // eat the prompt
                        //Thread.Sleep(500);
                    }
                }

            } while (echobad && (timesSent <= 1));

            return status;
        }

        //--------------------------------------------
        // Method: ReadLine
        /// <summary>
        ///  Reads the serial input buffer till a newline character is found.
        ///  If the newline character is not found, false is returned.
        /// </summary>
        ///
        ///<param name="rcvCount">int - Number of characters to look for</param>
        ///<param name="returnedLine">ref string - characters found. Will be a empty string
        ///if at least rcvCount characters are not found</param>
        ///<param name="timeoutms">int - Number of ms to wait for the characters</param>
        ///<returns>bool - true = at least rcvCount characters are found and returned in returnedLine
        ///                 false = not enough characters were found in time. Empty string returned.</returns>
        public bool ReadLine(ref string returnedLine, int timeoutms)
        {
            DateTime endTime;
            bool status = true;

            endTime = DateTime.Now.AddMilliseconds(timeoutms);
            while (!serialBuffer.Contains('\n') && (endTime > DateTime.Now))
            {
                Thread.Sleep(100);
            }

            if (serialBuffer.Contains('\n'))     // if line feed found
            {
                int nlIndex = serialBuffer.IndexOf('\n');
                returnedLine = serialBuffer.Substring(0, nlIndex);
                serialBuffer = serialBuffer.Substring(nlIndex + 1, serialBuffer.Length - nlIndex - 1);
            }
            else
            {
                returnedLine = string.Empty;
                status = false;
            }
            return status;
        }

        public void SetDumpFileName(string directory, string baseTitle)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            dumpFileName = directory + @"\dut" + baseTitle;
        }

        public void EndDump()
        {
            dumpFileName = string.Empty;
        }


        //---------------------
        //  utilities
        //-------------------
        public bool CheckForSerialPort(string comPort)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            // see if this port is in the list of ports on the computer
            foreach (string i in ports)
            {
                if (i.Equals(comPort))         // if found a match
                    return true;
            }
            return false;
        }


        //-------------------------------------
        // Method: DutSerialDataReceived
        //  StatusUpdated handler for the dut serial port
        //
        // If foundLF=true, means that the last character found was a line feed. So
        // if a non LF is found, it is a start of a new line, so add the time
        // to the dump file, if dumping.
        void serialDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            string smsg = string.Empty;         // corrected string removing '\0'
            string msg = string.Empty;          // raw string read
            string fmsg = string.Empty;         // string to write to file
            string rewritemsg = string.Empty;

            msg = sp.ReadExisting();
            foreach (var item in msg)
            {
                if (foundLF)
                {
                    fmsg = fmsg + DateTime.Now.ToString("HH:mm:ss.fff") + "<-";
                    foundLF = false;                // wait for next lf
                }
                if (item != '\0')
                {
                    fmsg = fmsg + item;
                    smsg = smsg + item;
                    if (item == '\n')
                        foundLF = true;
                }
            }
            serialBuffer += smsg;
            if (displayRcvChar)         // if display window is enabled
            {
                UpdateStatusWindow(fmsg, StatusType.serialRcv);
            }
            if (dumpFileName != string.Empty)
            {
                lock (fileLock)
                {
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(dumpFileName, true))
                    {
                        file.Write(fmsg);
                    }
                }
            }
        }

    }
}
