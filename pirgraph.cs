﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace SensorManufSY2
{
    public partial class pirgraph : Form
    {
        private PacketBasedConsole senConsole;
        private SensorDVTMANTests senTests;
        private FixtureFunctions fixFunctions;

        private int plowvalue = 0, phighvalue = 0;
        int tickcnt = 0;
        bool oldrcv = true, oldtrx = true;
        int lightStatus;
        double ledStatus;
        private int lastpirread;
        private MainWindow.uutdata uutData = new MainWindow.uutdata();


        public pirgraph(StationConfigure stationConfig, PacketBasedConsole console, SensorDVTMANTests tests, FixtureFunctions fix)
        {
            SUSensorData sdata = new SUSensorData();

            string outmsg;

            InitializeComponent();
            senConsole = console;
            senTests = tests;
            fixFunctions = fix;

           
            uutData.dvtmanPrompt = "DVTMAN> ";
            fixFunctions.ControlDUTPower(FixtureFunctions.PowerControl.ON, 0);

            senTests.ReadSensors(ref sdata, uutData, out outmsg);
            timer1.Interval = 500;
            numericTimeInterval.Value = timer1.Interval;
            fixFunctions.ControlPIRSource(FixtureFunctions.PowerControl.OFF);
            lightStatus = 0;
            ledStatus = 0;
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled)
            {
                timer1.Enabled = false;
                buttonStart.Text = "Start";
                buttonStart1sec.Enabled = true;
            }
            else
            {
                timer1.Enabled = true;
                buttonStart.Text = "Stop";
                lastpirread = 0;
            }

        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            chart1.Series["Data"].Points.Clear();
            plowvalue = 100000;
            phighvalue = 0;
            tickcnt = 0;
            datapoints.Clear();
            senConsole.ClearBuffers();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            SUSensorData sdata = new SUSensorData();
            string outmsg;
            senTests.ReadSensors(ref sdata, uutData, out outmsg);
            if (radioButtonPIR.Checked)
            {
                lastpirread = (int)sdata.PIR;
                chart1.Series["Data"].Points.AddXY(tickcnt * timer1.Interval / 1000.0, lastpirread);
                datapoints.AppendText(string.Format("{0:N1} : {1}\n", tickcnt * timer1.Interval / 1000.0, lastpirread.ToString()));
                if (lastpirread < plowvalue)
                {
                    plowvalue = lastpirread;
                    lowvalue.Text = plowvalue.ToString();
                    lowtickcnt.Text = (tickcnt * timer1.Interval / 1000).ToString();
                }
                if (lastpirread > phighvalue)
                {
                    phighvalue = lastpirread;
                    highvalue.Text = phighvalue.ToString();
                    hightickcnt.Text = (tickcnt * timer1.Interval / 1000).ToString();
                }
            }
            else
            {
                chart1.Series["Data"].Points.AddXY(tickcnt * timer1.Interval / 1000.0, sdata.AmbVisable);
                datapoints.AppendText(string.Format("{1:N1} V:{0}\n", sdata.AmbVisable,
                                           tickcnt * timer1.Interval / 1000.0));
            }
            tickcnt++;
            texttickcnt.Text = tickcnt.ToString();
        }


        private void pirgraph_FormClosed(object sender, FormClosedEventArgs e)
        {
            senConsole.DisplaySendChar = oldtrx;
            senConsole.DisplayRcvChar = oldrcv;
            fixFunctions.ControlDUTPower(0, 0);
        }

        private void buttonLampToggle_Click(object sender, EventArgs e)
        {
            if (lightStatus == 0)
            {
                lightStatus = 1;
                buttonLampToggle.Text = "Turn light OFF";
            }
            else
            {
                lightStatus = 0;
                buttonLampToggle.Text = "Turn light ON";
            }
            fixFunctions.ControlPIRSource((lightStatus == 0) ? FixtureFunctions.PowerControl.OFF : FixtureFunctions.PowerControl.ON);
        }

        private void buttonStart1sec_Click(object sender, EventArgs e)
        {
            if (!timer1.Enabled)
            {
                timer1.Enabled = true;
                Thread.Sleep(1000);
                fixFunctions.ControlPIRSource(FixtureFunctions.PowerControl.ON);
                buttonStart.Text = "Stop";
                buttonStart1sec.Enabled = false;
                buttonLampToggle.Text = "Turn light OFF";
            }
        }

        private void numericTimeInterval_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = (int)numericTimeInterval.Value;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            chart1.ChartAreas[0].AxisY.Minimum = (double)graphYMin.Value;
        }

        private void toggleDUTPower_Click(object sender, EventArgs e)
        {
            if (btoggleDUTPower.Text.ToUpper().Contains("OFF"))      // if on already
            {
                fixFunctions.ControlDUTPower(FixtureFunctions.PowerControl.OFF, 0);        // turn off
                btoggleDUTPower.Text = "Turn DUT power On";
            }
            else
            {
                fixFunctions.ControlDUTPower(FixtureFunctions.PowerControl.ON, 0);        // turn off
                btoggleDUTPower.Text = "Turn DUT power Off";
            }
        }

        private void graphYmax_ValueChanged(object sender, EventArgs e)
        {
            chart1.ChartAreas[0].AxisY.Maximum = (double)graphYmax.Value;

        }


        private void buttonLEDToggle_Click(object sender, EventArgs e)
        {
            if (ledStatus == 0)
            {
                ledStatus = Convert.ToDouble(LEDVoltage.Text);
                buttonLEDToggle.Text = "Turn LED OFF";
            }
            else
            {
                ledStatus = 0;
                buttonLEDToggle.Text = "Turn LED ON";
            }
            fixFunctions.SetAmbientSensorLED(ledStatus);

        }

    }
}
