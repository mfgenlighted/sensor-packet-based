﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorManufSY2
{
    public partial class FirmwareProgramWait : Form
    {
        FixtureFunctions lclFixture;

        public FirmwareProgramWait(FixtureFunctions fixture)
        {
            InitializeComponent();
            lclFixture = fixture;
            timer1.Enabled = true;
        }

        private void checkBoxButton(object sender, EventArgs e)
        {
            if (lclFixture.IsBoxButtonPushed())
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
                timer1.Enabled = false;
                return;
            }
        }
    }
}
