﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Diagnostics;
using System.Text.RegularExpressions;

using FixtureDatabase;
using TestStationErrorCodes;

namespace SensorManufSY2
{

    class BluetoothFunctions
    {
        private enum BLEModes {IDLE, na1, na2, BECON, RAW, BEACON_MFG}

        public FixtureFunctions fixture;
         
        public string FailMessage = string.Empty;
        public int FailErrorCode = 0;

        public enum BLEDirection { DUTtoRef, ReftoDUT}

        public BluetoothFunctions()
        {
        }

        /// <summary>
        /// Will run the BLE test between DUT and ref radios based on BLEDirection.
        /// </summary>
        /// <remarks>
        /// step 1
        ///     set 'from' unit to BEACON
        ///     look for success response
        /// step 2
        ///     set 'to' unit SCAN_RAW, looking for the 'from' MAC for x seconds
        ///     look for response that should have the 'from' MAC and number of beacons found
        /// step 3
        ///     set 'from' unit to IDLE
        /// </remarks>
        /// <param name="direction">directrion to do the test</param>
        /// <param name="DUTBLEMac">mac of the DUT</param>
        /// <param name="timeoutSecs">number of seconds to look for beacons</param>
        /// <param name="numberOfBeaconsFound">number of beacons found</param>
        /// <returns>true = no com errors, false=com errors</returns>
        public bool BluetoothTest(BLEDirection direction, string DUTBLEMac, uint timeoutSecs, ref uint numberOfBeaconsFound)
        {
            string foundMac = string.Empty;
            string snothing = string.Empty; // dummy values
            uint nnothing = 0;              // dummy values
            int nothing = 0;

            // make sure the sensor and ref radio consoles are open.
            // assumed that they have beein initialized
            if (!fixture.SensorConsole.IsOpen())
            {
                fixture.SensorConsole.Open();
                Trace.WriteLine("BluetooghTest had to reopen SensorConsole");
            }
            if (!fixture.RefRadio.IsOpen())
            {
                fixture.RefRadio.Open();
                Trace.WriteLine("BluetoothTest had to reopen RefRadio");
            }

            if (direction == BLEDirection.DUTtoRef )
            {
                // setup for DUT to ref, so set DUT to beacon. foundMac and beaconsFound return is not used.
                if (!fixture.SensorConsole.PerformBLEMode((uint)BLEModes.BEACON_MFG, "", 0, ref snothing, ref nnothing))
                {
                    FailErrorCode = fixture.SensorConsole.LastErrorCode;
                    FailMessage = fixture.SensorConsole.LastErrorMessage;
                    return false;
                }
                // start, set ref to raw looking for the DUT MAC
                if (!fixture.RefRadio.PerformBLEMode((uint)BLEModes.RAW, DUTBLEMac, timeoutSecs, ref foundMac, ref numberOfBeaconsFound))
                {
                    // sometimes the ref radio quits doing this function. Reboot and try again
                    Debug.Write("Start ref radio reboot " + DateTime.Now);
                    fixture.RefRadio.ClearBuffers();
                    fixture.RefRadio.PerformReboot();
                    fixture.RefRadio.WaitForMsgVersion(ref nothing, 10000);
                    Thread.Sleep(1000);
                    Debug.WriteLine("End Ref Radio." + DateTime.Now);
                    if (!fixture.RefRadio.PerformBLEMode((uint)BLEModes.RAW, DUTBLEMac, timeoutSecs, ref foundMac, ref numberOfBeaconsFound))
                    {
                        FailErrorCode = fixture.RefRadio.LastErrorCode;
                        FailMessage = fixture.RefRadio.LastErrorMessage;
                        return false;
                    }
                }
                // turn off beacons
                if (!fixture.SensorConsole.PerformBLEMode((uint)BLEModes.IDLE, "", 0, ref snothing, ref nnothing))
                {
                    FailErrorCode = fixture.SensorConsole.LastErrorCode;
                    FailMessage = fixture.SensorConsole.LastErrorMessage;
                    return false;
                }
            }
            else
            {
                // setup for ref to DUT
                if (!fixture.RefRadio.PerformBLEMode((uint)BLEModes.BEACON_MFG, "", 0, ref snothing, ref nnothing))
                {
                    FailErrorCode = fixture.RefRadio.LastErrorCode;
                    FailMessage = fixture.RefRadio.LastErrorMessage;
                    return false;
                }
                // start
                if (!fixture.SensorConsole.PerformBLEMode((uint)BLEModes.RAW, fixture.RefBLEMAC, timeoutSecs, ref foundMac, ref numberOfBeaconsFound))
                {
                    FailErrorCode = fixture.SensorConsole.LastErrorCode;
                    FailMessage = fixture.SensorConsole.LastErrorMessage;
                    return false;
                }
                // turn off beacons
                if (!fixture.RefRadio.PerformBLEMode((uint)BLEModes.IDLE, "", 3, ref snothing, ref nnothing))
                {
                    FailErrorCode = fixture.SensorConsole.LastErrorCode;
                    FailMessage = fixture.SensorConsole.LastErrorMessage;
                    return false;
                }
            }
            FailErrorCode = 0;
            FailMessage = string.Empty;

            return true;
        }

        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs>
            StatusUpdated;

        protected virtual void
            OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }

    }
}
