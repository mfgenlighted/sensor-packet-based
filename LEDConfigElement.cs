﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace SensorManufSY2
{

    //-------------------------------------------------------------------------
    //  Class to retrive the LED Configuration data from the
    //  .conf file of the application.
    //
    //      LEDConfigSection section = new LEDConfigSection();
    //      section = LEDConfigSection.GetLEDSection();
    //      outmsg = section.LEDSettings["dutRED"].Command;
    //      v1 = Convert.ToInt32(section.LEDSettings["dutRED"].LowRange);
    //      v2 = Convert.ToInt32(section.LEDSettings["dutRED"].HighRange);
    //
    //      sectionName
    //          = "LEDUSBConfiguration" << this is the default section. Uses the USB detector
    //          = "LEDLJConfiguration"  << can be changed to this for the LabJack detector
    //--------------------------------------------------------------------------
    public class LEDConfigElement : ConfigurationElement
    {
        private const string ATTRIBUTE_LED_TYPE = "LEDType";
        private const string ATTRIBUTE_LOW_RANGE = "LowRange";
        private const string ATTRIBUTE_HIGH_RANGE = "HighRange";
        private const string ATTRIBUTE_COMMAND = "Command";

        [ConfigurationProperty(ATTRIBUTE_LED_TYPE, IsRequired = true, IsKey = true)]
        public string LEDType
        {
            get { return (string)this[ATTRIBUTE_LED_TYPE]; }
            set { this[ATTRIBUTE_LED_TYPE] = value; }
        }

        [ConfigurationProperty(ATTRIBUTE_LOW_RANGE, IsRequired = true, IsKey = false)]
        public string LowRange
        {
            get { return (string)this[ATTRIBUTE_LOW_RANGE]; }
            set { this[ATTRIBUTE_LOW_RANGE] = value; }
        }

        [ConfigurationProperty(ATTRIBUTE_HIGH_RANGE, IsRequired = true, IsKey = false)]
        public string HighRange
        {
            get { return (string)this[ATTRIBUTE_HIGH_RANGE]; }
            set { this[ATTRIBUTE_HIGH_RANGE] = value; }
        }

        [ConfigurationProperty(ATTRIBUTE_COMMAND, IsRequired = true, IsKey = false)]
        public string Command
        {
            get { return (string)this[ATTRIBUTE_COMMAND]; }
            set { this[ATTRIBUTE_COMMAND] = value; }
        }
    }

    public class LEDConfigElementCollection : ConfigurationElementCollection
    {
        public LEDConfigElementCollection()
        {
            this.AddElementName = "LEDSettings";
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as LEDConfigElement).LEDType;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new LEDConfigElement();
        }

        public new LEDConfigElement this[string key]
        {
            get { return base.BaseGet(key) as LEDConfigElement; }
        }

        public LEDConfigElement this[int ind]
        {
            get { return base.BaseGet(ind) as LEDConfigElement; }
        }
    }

    public class LEDConfigSection : ConfigurationSection
    {
        public const string usbSectionName = "LEDUSBConfiguration";
        public const string labjackSectionName = "LEDLJConfiguration";

        [ConfigurationProperty("", IsDefaultCollection = true)]
        public LEDConfigElementCollection LEDSettings
        {
            get
            {
                return this[""] as LEDConfigElementCollection;
            }
        }

        public static LEDConfigSection GetLEDSection(string whichSection)
        {
            return (LEDConfigSection)ConfigurationManager.GetSection(whichSection);
        }

        public static LEDConfigSection GetLEDSection()
        {
            return (LEDConfigSection)ConfigurationManager.GetSection(usbSectionName);
        }
    }
}
