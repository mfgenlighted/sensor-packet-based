﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO.Ports;
using System.IO;
using System.Diagnostics;
using System.Collections.Concurrent;

using TestStationErrorCodes;

/// <summary>
/// This is the sensor commuications using the packet structure.
/// </summary>

namespace SensorManufSY2
{
    public class PacketBasedConsole : IDisposable
    {
        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        // 
        // From the initializing routine, add
        //     sensor.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(xxxx);
        //  where xxxx is the routine to update the windows.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs> StatusUpdated;
        BlockingCollection<byte[]> sensorBlockData = new BlockingCollection<byte[]>();

        protected virtual void OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }


        // locks
        static readonly object locker = new object();           // used to lock the response que
        static readonly object flocker = new object();          // used to lock the output file

        // error reporting
        public string LastErrorMessage = string.Empty;
        public int LastErrorCode = 0;
        public char[] LastPayload;

        // com defines
        public string PortName = string.Empty;
        public int BaudRate = 115200;                  // default to 115200

        // command parameters
        public int CommandRecieveTimeoutMS = 3000;      // number of ms to what for a response after a command
        public bool DisplayRcvChar = false;             // true= display recieved characters on screen
        public bool DisplaySendChar = false;              // true = display sent characters on screeen
        public bool IsThisDUTConsole = true;                // used for character display colors. true=dut false=sniffer

        private SerialPort serial = null;
        private int bufferSaveIndex = 0;                            // next location in the packet buffer to put data
        private byte[] packetBuffer = new byte[256];                // incoming packet buffer
        private List<byte[]> packetList = new List<byte[]>();       // this is a list of the packets recived.
        private string dumpFileName = string.Empty;                 // if file name present, dump data to file

        // transport values
        private byte packetTxSeqNumber = 0;                       // incremented after each packet transmit
        private byte packetRxSeqNumber = 0;                       // incremented after each packet recieve

        // queing
        ConcurrentQueue<byte[]> rcvDataQue = new ConcurrentQueue<byte[]>();

        // defined commands
        //
        //The Dvtman message consists of three parts, which are message type, message ID and parameters.
        //    1st byte: Message type/cateory for Dvtman messages, which differentiates Dvtman message type from other COBS message types.
        //    2nd byte: Message ID
        //    3rd byte and so on: Parameter list for the Dvtman command associated with the above message ID.
        // The table blow shows the messages defined for Dvtman.Notations used in the table are described as
        //    :P: param   B: bytes
        // The first byte in respond message is “stats”. The “stats” is  1 byte command status. 0 represents 
        //    success which means the command is received and executed successfully.
        //    If the “stats” is not 0, a non-zero stats shows the error code

        public const byte DVTMANMessageType = 0x57;
        public enum DVTMANCommandStatus
        {
            GOOD,
            BAD,
        }

        public enum DVTMANCommands
        {
            CMD_DISPLAY_MSG_VERSION = 0,    // display the current message version
            RSP_DISPLAY_MSG_VERSION,        // P1: stats  P2: version [1B]

            CMD_SET_NV_MAC,            // mac address for radio, ble and ethernet, P1: radio mac [6B, LSB]  P2: ble mac [6B, LSB]  P3: ble mac [6B, LSB]	
            RSP_SET_NV_MAC,             // P1: stats
            CMD_SET_NV_HW_CONFIG,       // default radio config, P1: pirSensorType[1B]  
            RSP_SET_NV_HW_CONFIG,       // P1: stats
            CMD_SET_NV_PCBA,            // set pcba config in nv flash, P1:  partNo [15B, char] P2:  serailNo [20B, char] 
            RSP_SET_NV_PCBA,
            CMD_SET_NV_HLA,             // set hla config in nv flash , P1: hlaPartNo [15B, char] P2: hlaSerialNo [20B, char] P3: hlaModelname [12B, char]
            RSP_SET_NV_HLA,
            CMD_SET_RADIO,              // set temporary radio configh
                                        //      P1: channelD [1B] P2: panID [2B, LSB hex] P3: txPower [1B]
                                        //      P4: encrypt_key [16B, char] P5: ttl [1B] P6: rate [1B data rate eg 16:1mbps] 
            RSP_SET_RADIO,
            CMD_SET_NV_CLEAR,           // clear non-violatile config, no param
            RSP_SET_NV_CLEAR,

            CMD_DISPLAY_VERSION,        // Image ID 
            RSP_DISPLAY_VERSION,        // P1: stats P2: image version ID[4B LSB]
            CMD_DISPLAY_BLE_MODE,       // current ble mode 
            RSP_DISPLAY_BLE_MODE,       // P1: stats P2: ble mode [1B]
            CMD_DISPLAY_NV_MAC,         // return both radio&ble 
            RSP_DISPLAY_NV_MAC,         // P1: stats P2: radio mac[6B] P3: ble mac[6B]
            CMD_DISPLAY_NV_PCBA,        // pcba config in nv flash
            RSP_DISPLAY_NV_PCBA,        // P1: stats P2:  partNo [15B] P3:  serailNo [20B]
            CMD_DISPLAY_NV_HLA,         // hla config in nv flash 
            RSP_DISPLAY_NV_HLA,         // P1: stats P2: hlaPartNo [15B, char] P3: hlaSerialNo [20B, char] P4: hlaModelname [12B, hex]
            CMD_DISPLAY_NV_HW_CONFIG,   // Get the hardware config 
            RSP_DISPLAY_NV_HW_CONFIG,   // variable data
            CMD_DISPLAY_RADIO,          // current radio param used 
            RSP_DISPLAY_RADIO,          // P1: stats P2: channelD [1B] P3: panID [2B, LSB hex] P4: txPower [1B]
                                        // P5: encrypt_key [16B, char] P6: ttl [1B] P7: dataRate [1B]

            CMD_PERFORM_POST,           // power on self test, do before change over, no param 
            RSP_PERFORM_POST,           // P1: status P2: crc saved[4B LSB] p3: crc calculated[4B LSB]
            CMD_PERFORM_CHANGE,         // change over to application image. Kona/kalapana app firmware might output stats or report profile through CU port 
            RSP_PERFORM_CHANGE,
            CMD_PERFORM_TEMP_CAL,       // temperature calibration. Input current room temperarture, P1: room temperature[1B, celsius] 
            RSP_PERFORM_TEMP_CAL, 		// P1: stats P2:raw temperature reading[2B] 
            CMD_PERFORM_DIM_VOLTAGE,    // specify dim1 voltage and dim2 voltage in range of 0 to 100(0~10v), P1: dim1 voltage[1B] P2: dim2 voltage[1B]
            RSP_PERFORM_DIM_VOLTAGE,    // P1: stats
            CMD_PERFORM_LED,            // red, green, blue 
            RSP_PERFORM_LED,
            CMD_PERFORM_SENSOR_READ,    // sensor read: P1:status, P2:pir[2B, 14bits], P3:ambient_lux[4B], P5:temperature[2B]
            RSP_PERFORM_SENSOR_READ,
            CMD_PERFORM_REBOOT,         // system reboot, no param 
            RSP_PERFORM_REBOOT,
            CMD_PERFORM_BLE_MODE,       // ble mode tests between two devices which can be programmed into idle, scan raw or beacon modes 
            RSP_PERFORM_BLE_MODE,
            CMD_PERFORM_WTEST,          // radio 802.15.4 test, P1: destination mac[6B] 
            RSP_PERFORM_WTEST,          // P1: stats P2: destination mac [6B] P3: destination LQI [1B] P4: source LQI  [1B
        }

        public struct commandStruct
        {
            public string CommandText;
            public bool Display;
            public byte CmdId;
            public object commandFunction;
        }
        public commandStruct[] Commmands =
        {
            new commandStruct{CommandText = "Get Display Message Version", Display = true, CmdId = (byte)DVTMANCommands.CMD_DISPLAY_MSG_VERSION},
            new commandStruct{CommandText = "Get Display Message Version response", Display = false, CmdId = (byte)DVTMANCommands.RSP_DISPLAY_MSG_VERSION },
            new commandStruct{CommandText = "Set MAC 3 addresses", Display = true, CmdId = (byte)DVTMANCommands.CMD_SET_NV_MAC},
            new commandStruct{CommandText = "Set MAC addresses response", Display = false, CmdId = (byte)DVTMANCommands.RSP_SET_NV_MAC},
            new commandStruct {CommandText = "Set hardware configuration", Display = true, CmdId = (byte)DVTMANCommands.CMD_SET_NV_HW_CONFIG },
            new commandStruct {CommandText = "Set hardware configuration response", Display = false, CmdId = (byte)DVTMANCommands.RSP_SET_NV_HW_CONFIG },
            new commandStruct{CommandText = "Set pcba config in nv flash", Display = true, CmdId = (byte)DVTMANCommands.CMD_SET_NV_PCBA},
            new commandStruct{CommandText = "Set pcba config in nv flash response", Display = false, CmdId = (byte)DVTMANCommands.RSP_SET_NV_PCBA},
            new commandStruct{CommandText = "Set hla config in nv flash", Display = true, CmdId = (byte)DVTMANCommands.CMD_SET_NV_HLA},
            new commandStruct{CommandText = "Set hla config in nv flash response", Display = false, CmdId = (byte)DVTMANCommands.RSP_SET_NV_HLA},
            new commandStruct{CommandText = "Set temporary radio config", Display = true, CmdId = (byte)DVTMANCommands.CMD_SET_RADIO},
            new commandStruct{CommandText = "Set temporary radio config response", Display = false, CmdId = (byte)DVTMANCommands.RSP_SET_RADIO},
            new commandStruct{CommandText = "Clear non-violatile config", Display = true, CmdId = (byte)DVTMANCommands.CMD_SET_NV_CLEAR},
            new commandStruct{CommandText = "Clear non-violatile config response", Display = false, CmdId = (byte)DVTMANCommands.RSP_SET_NV_CLEAR},
            new commandStruct{CommandText = "Return firmware, bootloader, dvtman", Display = true, CmdId = (byte)DVTMANCommands.CMD_DISPLAY_VERSION},
            new commandStruct{CommandText = "Return firmware, bootloader, dvtman response", Display = false, CmdId = (byte)DVTMANCommands.RSP_DISPLAY_VERSION},
            new commandStruct{CommandText = "Return current ble mode", Display = true, CmdId = (byte)DVTMANCommands.CMD_DISPLAY_BLE_MODE},
            new commandStruct{CommandText = "Return current ble mode response", Display = false, CmdId = (byte)DVTMANCommands.RSP_DISPLAY_BLE_MODE},
            new commandStruct{CommandText = "Return both radio & ble", Display = true, CmdId = (byte)DVTMANCommands.CMD_DISPLAY_NV_MAC},
            new commandStruct{CommandText = "Return both radio & ble response", Display = false, CmdId = (byte)DVTMANCommands.RSP_DISPLAY_NV_MAC},
            new commandStruct{CommandText = "Return pcba config in nv flash", Display = true, CmdId = (byte)DVTMANCommands.CMD_DISPLAY_NV_PCBA},
            new commandStruct{CommandText = "Return pcba config in nv flash response", Display = false, CmdId = (byte)DVTMANCommands.RSP_DISPLAY_NV_PCBA},
            new commandStruct{CommandText = "Return hla config in nv flash", Display = true, CmdId = (byte)DVTMANCommands.CMD_DISPLAY_NV_HLA},
            new commandStruct{CommandText = "Return hla config in nv flash response", Display = false, CmdId = (byte)DVTMANCommands.RSP_DISPLAY_NV_HLA},
            new commandStruct{CommandText = "Return hardware config", Display = true, CmdId = (byte)DVTMANCommands.CMD_DISPLAY_NV_HW_CONFIG},
            new commandStruct{CommandText = "Return hardware config response", Display = false, CmdId = (byte)DVTMANCommands.RSP_DISPLAY_NV_HW_CONFIG},
            new commandStruct{CommandText = "Return current radio param used", Display = true, CmdId = (byte)DVTMANCommands.CMD_DISPLAY_RADIO},
            new commandStruct{CommandText = "Return current radio param used response", Display = false, CmdId = (byte)DVTMANCommands.RSP_DISPLAY_RADIO},
            new commandStruct{CommandText = "power on self test", Display = true, CmdId = (byte)DVTMANCommands.CMD_PERFORM_POST},
            new commandStruct{CommandText = "power on self tests response", Display = false, CmdId = (byte)DVTMANCommands.RSP_PERFORM_POST},
            new commandStruct{CommandText = "change over to application image", Display = true, CmdId = (byte)DVTMANCommands.CMD_PERFORM_CHANGE},
            new commandStruct{CommandText = "change over to application image response", Display = false, CmdId = (byte)DVTMANCommands.RSP_PERFORM_CHANGE},
            new commandStruct{CommandText = "temperature calibration. Input current room temperarture", Display = true, CmdId = (byte)DVTMANCommands.CMD_PERFORM_TEMP_CAL},
            new commandStruct{CommandText = "temperature calibration response", Display = false, CmdId = (byte)DVTMANCommands.RSP_PERFORM_TEMP_CAL},
            new commandStruct{CommandText = "specify dim1 voltage and dim2 voltage in range of 0 to 100(0~10v), read external measurement", Display = true, CmdId = (byte)DVTMANCommands.CMD_PERFORM_DIM_VOLTAGE},
            new commandStruct{CommandText = "Dim response", Display = false, CmdId = (byte)DVTMANCommands.RSP_PERFORM_DIM_VOLTAGE},
            new commandStruct{CommandText = "Set LEDs", Display = true, CmdId = (byte)DVTMANCommands.CMD_PERFORM_LED},
            new commandStruct{CommandText = "Set LEDs response", Display = false, CmdId = (byte)DVTMANCommands.RSP_PERFORM_LED},
            new commandStruct{CommandText = "Return pir, ambient, temperature", Display = true, CmdId = (byte)DVTMANCommands.CMD_PERFORM_SENSOR_READ},
            new commandStruct{CommandText = "Return pir, ambient, temperatur response", Display = false, CmdId = (byte)DVTMANCommands.RSP_PERFORM_SENSOR_READ},
            new commandStruct{CommandText = "System reboot", Display = true, CmdId = (byte)DVTMANCommands.CMD_PERFORM_REBOOT},
            new commandStruct{CommandText = "System reboot response", Display = false, CmdId = (byte)DVTMANCommands.RSP_PERFORM_REBOOT},
            new commandStruct{CommandText = "ble mode tests between two devices which can be programmed into idle, scan raw or beacon modes", Display = true, CmdId = (byte)DVTMANCommands.CMD_PERFORM_BLE_MODE},
            new commandStruct{CommandText = "ble test response", Display = false, CmdId = (byte)DVTMANCommands.RSP_PERFORM_BLE_MODE},
            new commandStruct{CommandText = "radio 802.15.4 tes", Display = true, CmdId = (byte)DVTMANCommands.CMD_PERFORM_WTEST},
            new commandStruct{CommandText = "radio 802.15.4 tes response", Display = false, CmdId = (byte)DVTMANCommands.RSP_PERFORM_WTEST},
        };


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (serial != null)
                    {
                        if (serial.IsOpen)
                            serial.Close();
                    }
                }
                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion

        //-----------------------------------
        // Housekeeping
        //----------------------------------
        public bool Open()
        {
            if (PortName == string.Empty )
            {
                LastErrorCode = (int)ErrorCodes.System.MissingParameter;
                LastErrorMessage = "Com port is not defined";
                return false;
            }
            try
            {
                if (serial == null)
                    serial = new SerialPort();
                serial.PortName = PortName;
                serial.BaudRate = BaudRate;
                serial.Handshake = Handshake.None;
                serial.Parity = Parity.None;
                serial.ReadTimeout = 100;
                serial.StopBits = StopBits.One;
                serial.WriteTimeout = 100;
                serial.ReadBufferSize = 2096;
                serial.WriteBufferSize = 512;
                serial.ErrorReceived += HandleError;
                bufferSaveIndex = 0;
                packetList.Clear();
                serial.Open();           // open the serial port
                Thread.Sleep(100);
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error opening sensor port; " + ex.Message;
                LastErrorCode = (int)ErrorCodes.System.SensorConsoleOpenError;
                return false;
            }
            return true;
        }


        public bool Open(string comport, int comspeed)
        {
            PortName = comport;
            BaudRate = comspeed;
            packetRxSeqNumber = 0;      // values for the transport layer of dut commications
            packetTxSeqNumber = 0;
            return Open();
        }

        public bool IsOpen()
        {
            return serial.IsOpen;
        }

        public bool Close()
        {
            try
            {
                if (serial != null)
                {
                    Thread.Sleep(500);          // just in case something else is in the process of closing it
                    if (serial.IsOpen)
                    {
                        serial.BaseStream.Flush();
                        serial.BaseStream.Close();
                        serial.Close();
                        serial.Dispose();
                        Thread.Sleep(100);
                    }
                }
            }
            catch (Exception ex)
            {
                UpdateStatusWindow("Error while closing sensor console port. IsOpen = " + serial.IsOpen + "\n" + ex.Message + "\n", StatusType.failed);
            }
            return true;
        }

        public void SetDumpFileName(string directory, string baseTitle)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            dumpFileName = directory + @"/" + baseTitle;
        }

        public void EndDump()
        {
            dumpFileName = string.Empty;
        }

        public void ClearBuffers()
        {
            if (serial.IsOpen)
            {
                packetList.Clear();
                serial.DiscardInBuffer();
                bufferSaveIndex = 0;
                Thread.Sleep(100);
            }
        }

        //-----------------------------------
        //  Commands
        //-----------------------------------

        //----------------------
        //  Set commands


        /// <summary>
        /// Set the radio and ble MACS. The strings should have no colons.
        /// </summary>
        /// <remarks>
        /// P1: 6 bytes, enlighted radio MAC
        /// P2: 6 bytes, ble radio MAC
        /// ex: 6854f5112233 -> byte[0]=68, byte[1]=54, ....
        /// 
        /// LastErrorCode
        ///  2  ErrorCodes.Program.DUTComError
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        /// <param name="radioMAC">MAC for the enlighted radio. no colons</param>
        /// <param name="bleMAC">MAC for the ble radio. no colons</param>
        /// <returns></returns>
        public bool SetMACData(string radioMAC, string bleMAC)
        {
            bool status = true;
            byte[] cmd = new byte[14];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_SET_NV_MAC;
            for (int i = 0; i < 6; i++)
            {
                cmd[2 + i] = (byte) Int32.Parse(radioMAC.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
            }
            for (int i = 0; i < 6; i++)
            {
                cmd[8 + i] = (byte) Int32.Parse(bleMAC.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
            }

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_SET_NV_MAC)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_MAC].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_MAC].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// Set the hardware configuration data.
        /// </summary>
        /// <param name="ambientScaling"></param>
        /// <param name="pirLensType"></param>
        /// <remarks>
        /// LastErrorCode
        ///  2  ErrorCodes.Program.DUTComError
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        /// <returns></returns>
        public bool SetHwConfig(byte hwConfig)
        {
            bool status = true;
            byte[] cmd;
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;

            // pakage the command
            cmd = new byte[3];
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_SET_NV_HW_CONFIG;
            cmd[2] = hwConfig;

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_SET_NV_HW_CONFIG)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_HW_CONFIG].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_HW_CONFIG].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else                // timed out
                {
                    LastErrorCode = (int)ErrorCodes.Program.CommandTimeout;
                    LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_HW_CONFIG].CommandText + ". Timed out";
                    return false;

                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// Set up the PCBA data
        /// </summary>
        /// <remarks>
        /// P1: partNo[15B]
        /// P2: serialNo[20B]
        /// 
        /// LastErrorCode
        ///  2   ErrorCodes.Program.DUTComError
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        ///  
        /// </remarks>
        /// <param name="partNumber">string up to 15 bytes long</param>
        /// <param name="serialNumber">string up to 20 bytes long</param>
        /// <returns></returns>
        public bool SetPCBAData(string partNumber, string serialNumber)
        {
            bool status = true;
            byte[] cmd = new byte[2 + 15 + 20];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;
            byte[] precmd = new byte[2];

            // pakage the command
            precmd[0] = DVTMANMessageType;
            precmd[1] = (byte)DVTMANCommands.CMD_SET_NV_PCBA;
            byte[] pnarray = Encoding.ASCII.GetBytes(partNumber);
            byte[] snarray = Encoding.ASCII.GetBytes(serialNumber);
            Array.Copy(precmd, cmd, 2);
            Array.Copy(pnarray, 0, cmd, 2, pnarray.Length);
            Array.Copy(snarray, 0, cmd, 17, snarray.Length);

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_SET_NV_PCBA)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_PCBA].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_PCBA].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else                // recive error. set by receive_response
                {
                    return false;

                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// Set HLA data.
        /// </summary>
        /// <remarks>
        /// P1: hlaPartNo[15B, char]
        /// P2: hlsSerialNo [20B, char]
        /// P3: hlaModelname [12B, char]
        /// 
        /// LastErrorCode
        ///  2  ErrorCodes.Program.DUTComError
        ///  3  ErrorCodes.Program.DutUnexpectedCmdReturn
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        /// <param name="partno"></param>
        /// <param name="serialno"></param>
        /// <param name="modelname"></param>
        /// <returns></returns>
        public bool SetHLAData(string partNumber, string serialNumber, string modelname)
        {
            bool status = true;
            byte[] cmd = new byte[2 + 15 + 20 + 12];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;
            byte[] precmd = new byte[2];

            // pakage the command
            precmd[0] = DVTMANMessageType;
            precmd[1] = (byte)DVTMANCommands.CMD_SET_NV_HLA;
            byte[] pnarray = Encoding.ASCII.GetBytes(partNumber);
            byte[] snarray = Encoding.ASCII.GetBytes(serialNumber);
            byte[] modelarray = Encoding.ASCII.GetBytes(modelname);
            Array.Copy(precmd, cmd, 2);
            Array.Copy(pnarray, 0, cmd, 2, pnarray.Length);
            Array.Copy(snarray, 0, cmd, 17, snarray.Length);
            Array.Copy(modelarray, 0, cmd, 37, modelarray.Length);

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_SET_NV_HLA)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_HLA].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_HLA].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        public bool SetRadioData(uint channelID, uint panID, uint txpower, byte[] key, uint ttl, uint rate)
        {
            bool status = true;
            byte[] cmd = new byte[2 + 22];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_SET_RADIO;
            cmd[2] = (byte)channelID;
            cmd[3] = (byte)(panID / 256);
            cmd[4] = (byte)(panID - ((panID / 256) * 256));
            cmd[5] = (byte)txpower;
            Array.Copy(key, 0, cmd, 6, 16);
            cmd[22] = (byte)ttl;
            cmd[23] = (byte)rate;
            // send it
            ClearBuffers();
            status = send_cmd(cmd);


            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_SET_RADIO)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_RADIO].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_RADIO].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else            // issue geting the response
                {
                    // last error codes and message set by send_cmd
                    return false;
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        public bool PreformTempCal(uint roomtemp, ref uint rawtemp)
        {
            bool status = true;
            byte[] cmd = new byte[3];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_TEMP_CAL;
            cmd[2] = (byte)roomtemp;
            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_TEMP_CAL)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending Temp Cal. Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending Temp Cal. Returned cmd: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                    else
                    {
                        rawtemp = (uint)payload[0] + ((uint)payload[1] * 256);
                    }
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// Set the two diming outputs
        /// </summary>
        /// <remarks>
        /// P1: dim1 voltage[1B] 0-100 -> 0-10V
        /// P2: dim2 voltage[1B] 0-100 -> 0-10V
        /// 
        /// LastErrorCode
        ///  2  ErrorCodes.Program.DUTComError
        ///  3  ErrorCodes.Program.DutUnexpectedCmdReturn
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        /// <param name="dim1"></param>
        /// <param name="dim2"></param>
        /// <returns></returns>
        public bool SetDimVoltage(uint dim1, uint dim2)
        {
            bool status = true;
            byte[] cmd = new byte[2 + 2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_DIM_VOLTAGE;
            cmd[2] = (byte)dim1;
            cmd[3] = (byte)dim2;

            // send it
            status = send_cmd(cmd);


            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_DIM_VOLTAGE)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_DIM_VOLTAGE].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_DIM_VOLTAGE].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else            // issue geting the response
                {
                    // last error codes and message set by send_cmd
                    return false;
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        public bool SetLeds(bool redon, bool greenon, bool blueon)
        {
            bool status = true;
            byte[] cmd = new byte[2 + 3];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_LED;
            cmd[2] = (byte)(redon ? 1:0);
            cmd[3] = (byte)(greenon ? 1 : 0);
            cmd[4] = (byte)(blueon ? 1 : 0);

            // send it
            status = send_cmd(cmd);


            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_LED)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_LED].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_LED].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else            // issue geting the response
                {
                    // last error codes and message set by send_cmd
                    return false;
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        //--------------------------------------------
        //  query commands
        //--------------------------------------------
        public bool GetMsgVersion(ref int version)
        {
            bool status;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_DISPLAY_MSG_VERSION;

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_MSG_VERSION)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_MSG_VERSION].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_MSG_VERSION].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                    version = payload[0];
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// Wait for the Message Version packet
        /// </summary>
        /// <remarks>
        /// LastErrorCode
        /// 3  -  ErrorCodes.Program.DutUnexpectedCmdReturn
        ///  16  ErrorCodes.Program.CommandTimeout, waiting for response
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        /// <param name="version"></param>
        /// <param name="timeoutms"></param>
        /// <returns></returns>
        public bool WaitForMsgVersion(ref int version, int timeoutms)
        {
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;

            LastErrorCode = 0;
            LastErrorMessage = string.Empty;


            // look for response
            if (receive_response(ref cmdId, ref cmdStatus, out payload, timeoutms))
            {
                if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_MSG_VERSION)
                {
                    LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                    LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_MSG_VERSION].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                    return false;
                }
                if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                {
                    LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                    LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_MSG_VERSION].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                    return false;
                }
                version = payload[0];
            }
            else
            {
                return false;       // receive_response sets error messages
            }

            return true;
        }

        /// <summary>
        /// Get the firmware versions
        /// </summary>
        /// <remarks>
        /// P1: stats
        /// P2: image ID[4B LSB]
        /// LastErrorCode
        ///  3  ErrorCodes.Program.DutUnexpectedCmdReturn
        ///  2  ErrorCodes.Program.DUTComError
        ///  18 ErrorCodes.Program.CmdResponseStatusFailed
        /// </remarks>
        /// <param name="imageID></param>
        /// <returns></returns>
        public bool GetImageID(ref string imageID)
        {
            bool status;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;
            byte[] precmd = new byte[2];

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_DISPLAY_VERSION;

            ClearBuffers();
            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_VERSION)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_VERSION].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.CmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_VERSION].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                    string lclimageID = BitConverter.ToString(payload);
                    imageID = lclimageID.Replace("-", "");                  // strip out '-'s
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        public bool GetBLEMode(ref uint mode)
        {
            return false;
        }

        /// <summary>
        /// read the mac data. will only return the first two.
        /// </summary>
        /// <param name="mac"></param>
        /// <param name="mac2"></param>
        /// <returns></returns>
        public bool GetMACData(ref string mac, ref string mac2)
        {
            string mac3 = string.Empty;
            return GetMACData(ref mac, ref mac2, ref mac3);
        }

        /// <summary>
        /// read the mac data.
        /// </summary>
        /// <remarks>
        /// P1: stats
        /// P2: radio mac[6B]
        /// P3: ble mac[6B]
        /// P4: ethernet mac[6B] - only when unit is gateway
        /// </remarks>
        /// <param name="mac"></param>
        /// <param name="mac2"></param>
        /// <param name="mac3"></param>
        /// <returns></returns>
        public bool GetMACData(ref string mac, ref string mac2, ref string mac3)
        {
            bool status;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];
            string lclmac = string.Empty;
            string lclmac2 = string.Empty;
            string lclmac3 = string.Empty;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_DISPLAY_NV_MAC;

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload, 3000))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_NV_MAC)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_NV_MAC].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_NV_MAC].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                    byte[] macarray = new byte[6];
                    byte[] mac2array = new byte[6];
                    byte[] mac3array = new byte[6];
                    Array.Copy(payload, 0, macarray, 0, 6);
                    Array.Copy(payload, 6, mac2array, 0, 6);
                    lclmac = BitConverter.ToString(macarray);
                    lclmac2 = BitConverter.ToString(mac2array);
                    if (payload.Length > 12)    // if there is a third mac
                    {
                        Array.Copy(payload, 12, mac3array, 0, 6);
                        lclmac3 = BitConverter.ToString(mac3array);
                    }
                    mac = lclmac.Replace("-", "");                  // strip out '-'s
                    mac2 = lclmac2.Replace("-", "");
                    mac3 = lclmac3.Replace("-", "");
                }
                else
                {
                    return false;
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// LastErrorCode
        ///  2  ErrorCodes.Program.DUTComError
        ///  3  ErrorCodes.Program.DutUnexpectedCmdReturn
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        /// <param name="partno"></param>
        /// <param name="serialno"></param>
        /// <returns></returns>
        public bool GetPCBAData(ref string partno, ref string serialno)
        {
            bool status;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_DISPLAY_NV_PCBA;

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload, 3000))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_NV_PCBA)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_NV_PCBA].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_NV_PCBA].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                    byte[] pnarray = new byte[15];
                    byte[] snarray = new byte[20];
                    Array.Copy(payload, 0, pnarray, 0, 15);
                    Array.Copy(payload, 15, snarray, 0, 20);
                    partno = string.Empty;
                    if (pnarray[0] != 0xFF)     // if it has valid data
                        partno = Encoding.ASCII.GetString(pnarray).TrimEnd('\0');
                    serialno = string.Empty;
                    if (snarray[0] != 0xFF)     // if it has valid data
                        serialno = Encoding.ASCII.GetString(snarray).TrimEnd('\0');
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// Get the HLA data.
        /// </summary>
        /// <remarks>
        /// P1: stats
        /// P2: hlaPartNo[15B, char]
        /// P3: hlaSerialNo[20B, char]
        /// P4: hlaModelname[12B, hex]
        /// </remarks>
        /// <param name="partno"></param>
        /// <param name="serialno"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool GetHLAData(ref string partno, ref string serialno, ref string model)
        {
            bool status;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];

            // make sure there is no old data
            partno = string.Empty;
            serialno = string.Empty;
            model = string.Empty;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_DISPLAY_NV_HLA;

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload, 3000))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_NV_HLA)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_NV_HLA].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_NV_HLA].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                    byte[] pnarray = new byte[15];
                    byte[] snarray = new byte[20];
                    byte[] modelarray = new byte[12];
                    Array.Copy(payload, 0, pnarray, 0, 15);
                    Array.Copy(payload, 15, snarray, 0, 20);
                    Array.Copy(payload, 35, modelarray, 0, 12);
                    partno = string.Empty;
                    if (pnarray[0] != 0xFF)     // if it has real data
                        partno = Encoding.ASCII.GetString(pnarray).TrimEnd('\0');
                    serialno = string.Empty;
                    if (snarray[0] != 0xFF)     // if it has real data
                        serialno = Encoding.ASCII.GetString(snarray).TrimEnd('\0');
                    model = string.Empty;
                    if (modelarray[0] != 0xFF)     // if it has real data
                        model = Encoding.ASCII.GetString(modelarray).TrimEnd('\0');
                }
                else
                    return false;       // error reciving response
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// Return the hardware config. 
        /// </summary>
        /// <remarks>
        /// returns:
        ///     P1:stats
        ///     P2:pirSensorType[1B] - this is the byte that is programed with CMD_SET_NV_HW_CONF
        ///     P3 - P17: other stuff
        ///     
        /// LastErrorCode
        ///  2  ErrorCodes.Program.DUTComError
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool GetHwConfigData(ref byte[] data)
        {
            bool status = true;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_DISPLAY_NV_HW_CONFIG;

            // send it
            status = send_cmd(cmd);         // sets LastErrorCode and LastErrorMessage if error

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_NV_HW_CONFIG)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_NV_HW_CONFIG].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_NV_HW_CONFIG].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else            // timed out
                {
                    LastErrorCode = (int)ErrorCodes.Program.CommandTimeout;
                    LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_CLEAR].CommandText + ". Timed out.";
                    return false;

                }
                data = payload;
            }
            return status;
        }

        /// <summary>
        /// Get the radio data.
        /// </summary>
        /// <remarks>
        /// P1: stats
        /// P2: channelID[1B]
        /// P3: panID[2B]
        /// P4: txPower[1B]
        /// P5: encrypt_key[16B]
        /// P6: ttl[1B]
        /// P7: dataRate[1B]
        /// </remarks>
        /// <param name="channel"></param>
        /// <param name="pan"></param>
        /// <param name="txpower"></param>
        /// <param name="key"></param>
        /// <param name="ttl"></param>
        /// <param name="datarate"></param>
        /// <returns></returns>
        public bool GetRadioData(ref uint channel, ref uint pan, ref uint txpower, ref byte[] key, ref uint ttl, ref uint datarate)
        {
            bool status;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_DISPLAY_RADIO;

            // send it
            status = send_cmd(cmd);         // sets LastErrorCode and LastErrorMessage if error

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_RADIO)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_RADIO].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_RADIO].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else            // timed out
                {
                    LastErrorCode = (int)ErrorCodes.Program.CommandTimeout;
                    LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_CLEAR].CommandText + ". Timed out.";
                    return false;

                }
                channel = (uint)payload[0];
                pan = (uint)((payload[1] * 256) + payload[2]);
                txpower = (uint)payload[3];
                Array.Copy(payload, 4, key, 0, 16);
                ttl = (uint)payload[20];
                datarate = (uint)payload[21];
            }
        return status;
        }

        public bool GetSensorData(ref UInt16 pir, ref UInt32 ambient, ref UInt16 temp)
        {
            bool status;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_SENSOR_READ;

            // send it
            status = send_cmd(cmd);         // sets LastErrorCode and LastErrorMessage if error

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload, 2000))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_SENSOR_READ)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_SENSOR_READ].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_SENSOR_READ].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else            // recive error
                {
                    return false;

                }
                pir = (UInt16)(payload[0] + (payload[1] * 256));
                ambient = (UInt32)(payload[2] + (payload[3] * 256) + (payload[4] * 65536) + (payload[5] * 16777216));
                temp = (UInt16)(payload[6] + (payload[7] * 256));
            }
            return status;
        }

        //----------------------------------------------
        //  action commands
        //---------------------------------------------
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// LastErrorCode
        ///  3  ErrorCodes.Program.DutUnexpectedCmdReturn
        ///  16 ErrorCodes.Program.CommandTimeout
        /// </remarks>
        /// <returns></returns>
        public bool ClearManData()
        {
            bool status;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];

            ClearBuffers();             // make sure buffers are cleared. Could be 1st command of test

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_SET_NV_CLEAR;

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_SET_NV_CLEAR)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_CLEAR].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_CLEAR].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                    Thread.Sleep(500);          // seeing if this helps the 'port closed' issue with the reset command issued after this
                }
                else            // timed out
                {
                    LastErrorCode = (int)ErrorCodes.Program.CommandTimeout;
                    LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_CLEAR].CommandText + ". Timed out.";
                    return false;

                }
            }
            return true;
        }

        /// <summary>
        /// Reads the POST return code.
        /// </summary>
        /// <remarks>
        /// P1: stats P2: crc saved[4B LSB] P3: crc calculated[4B LSB]
        /// </remarks>
        /// <param name="statuscode"></param>
        /// <returns>false = error sending or reciveing</returns>
        public bool PerformPost(ref int statuscode, ref UInt32 savedCRC, ref UInt32 calCRC)
        {
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];

            savedCRC = 0;
            calCRC = 0;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_POST;

            // verify response
            if (send_cmd(cmd))
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_POST)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_POST].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    savedCRC = (uint)payload[0] + ((uint)payload[1] * 256) + ((uint)payload[2] * 65536) + ((uint)payload[3] * 16777216);
                    calCRC = (uint)payload[4] + ((uint)payload[5] * 256) + ((uint)payload[6] * 65536) + ((uint)payload[7] * 16777216);
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.CmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_POST].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else            // did not recieve response to command
                {
                    LastErrorCode = (int)ErrorCodes.Program.CommandTimeout;
                    LastErrorMessage = "Error no response to command.  " + Commmands[(int)DVTMANCommands.CMD_PERFORM_POST].CommandText;
                    return false;
                }
            }
            else            // failed to send the command. LastErrorCode and LastErrorMessage set by send_cmd
            {
                return false;

            }

            statuscode = (int)cmdStatus;
            return true;
        }

        public bool PreformChangeOver(int timeoutMs, ref int statuscode)
        {
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_CHANGE;

            // verify response
            if (send_cmd(cmd))
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload, timeoutMs))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_CHANGE)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_CHANGE].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.CmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_CHANGE].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else        // timed out
                {
                    LastErrorCode = (int)ErrorCodes.Program.CommandTimeout;
                    LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_CHANGE] + ". Timed out";
                    return false;

                }
            }
            else            // failed to send the command. LastErrorCode and LastErrorMessage set by send_cmd
            {
                return false;

            }

            statuscode = (int)cmdStatus;
            return true;
        }

        /// <summary>
        /// Send Reboot command to unit.
        /// Send parameters: none
        /// Response parameters: P1: status
        /// </summary>
        /// <returns></returns>
        public bool PerformReboot()
        {
            bool status = true;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_REBOOT;
            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_REBOOT)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending reboot. Returned cmd: " + cmdId.ToString("X2");
                        return false; 
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending reboot. Returned cmd: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else
                {
                    // last error codes and message set by receive_response
                    return false;
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// perform ble mode. Returns false if comm error.
        /// </summary>
        /// <remarks>
        /// P1:mode[1B]   idle=0, scan_raw=4, beacon_mfg=5
        /// </remarks>
        /// <param name="mode"></param>
        /// <param name="targetMac"></param>
        /// <param name="timeout">seconds</param>
        /// <param name="detectedMac"></param>
        /// <param name="rcvbeacons"></param>
        /// <returns>false = comm issues. LastErrorCode and LastErrorMessage has reasons</returns>
        public bool PerformBLEMode(uint mode, string targetMac, uint timeoutSecs, ref string detectedMac, ref uint rcvbeacons)
        {
            bool status = true;
            byte[] cmd = new byte[10];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] macarray = new byte[6];
            string lclmac;

            detectedMac = string.Empty;                     // just in case nothing is found
            rcvbeacons = 0;                                 // and no beacons found

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_BLE_MODE;
            cmd[2] = (byte)mode;
            if (targetMac.Length != 0)
            {
                for (int i = 0; i < 6; i++)
                {
                    cmd[3 + i] = (byte)Int32.Parse(targetMac.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                }
            }
            cmd[9] = (byte)timeoutSecs;

            // send it
            status = send_cmd(cmd);     // if there is a error, LastErrorCode and LastErrorMessage already set

            // verify response
            if (status)
            {
                if ((status = receive_response(ref cmdId, ref cmdStatus, out payload, (int)(timeoutSecs +1) * 1000)))   // give it a extra second to complete
                                                                                                                        // LastErrorxxx set by receive_response
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_BLE_MODE)     // if not the expected response
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending BLE MODE command. Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)    // a return of BAD means that no BEACONs where found.
                    {
                        detectedMac = string.Empty;                     // so retrun empty string to indicate nothing found
                        rcvbeacons = 0;                                 // and no beacons found
                        return true;
                    }
                    if (payload.Length > 1) 
                    {
                        Array.Copy(payload, 0, macarray, 0, 6);
                        lclmac = BitConverter.ToString(macarray);
                        detectedMac = lclmac.Replace("-", "");                  // strip out '-'s
                        rcvbeacons = (uint)((payload[7] * 256) + payload[6]);
                    }
                }
            }

            // return status
            return status;
        }

        /// <summary>
        /// Do the radio test
        /// </summary>
        /// <remarks>
        /// P1: destination mac[3B]
        /// resuts
        /// P1: stats
        /// P2: destination mac[3B]
        /// P3: destination LQI[1B}
        /// P4: source LQI[1B]
        /// </remarks>
        /// <param name="destinationMac">mac with no colons</param>
        /// <param name="destLQI"></param>
        /// <param name="sourceLQI"></param>
        /// <returns></returns>
        public bool PerformWtest(string destinationMac, int timeoutms, ref int destLQI, ref int sourceLQI)
        {
            bool status = true;
            byte[] cmd = new byte[5];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_WTEST;
            for (int i = 3; i < 6; i++)
            {
                cmd[i - 1] = (byte)Int32.Parse(destinationMac.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
            }
            ClearBuffers();
            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload, timeoutms))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_WTEST)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending WTEST command. Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = (int)ErrorCodes.Program.DutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending WTEST command. Returned cmd: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                    if (payload.Length == 5)
                    {
                        destLQI = (int)payload[3];
                        sourceLQI = (int)payload[4];
                    }
                    else        // payload was not the correct length
                    {
                        LastErrorCode = (int)ErrorCodes.Program.CmdResponsePayloadLenWrong;
                        LastErrorMessage = "Error sending WTEST command. Payload len wrong. Should be 5, was " + payload.Length.ToString();
                        return false;

                    }
                }
                else            // timed out
                {
                    LastErrorCode = (int)ErrorCodes.Program.CommandTimeout;
                    LastErrorMessage = "Error sending WTEST command. Response timed out. Timeout set to" + timeoutms.ToString() + "ms";
                    return false;
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status
            return true;
        }

        //---------------------------------------
        // utils
        //---------------------------------------

        /// <summary>
        /// will send the byte array to the serial port. It will first convert it to the
        /// encoded version.
        /// </summary>
        /// <remarks>
        /// LastErrorCode
        ///  2  ErrorCodes.Program.DUTComError
        /// </remarks>
        /// <param name="cmd">byte array to send to serial port</param>
        /// <returns>true = sent</returns>
        private bool send_cmd(byte[] cmd)
        {
            byte[] convertedCmd;
            byte[] endbyte = { 0 };
            byte[] header = new byte[4];
            byte[] xmitarray;
            byte chksum = 0;
            string statusString;

            // now wrap in the transport layer
            //      uint32_t version    :2	        transport protocol version, currently 0
            //      uint32_t rsvd       :5	        reserved, always set to 0
            //      uint32_t secure     :1	        set if secure payload, clear if payload is not secure
            //      uint32_t sequence_num: 8	    incremented after each packet transmit
            //                                      used by receiver to determine lost packets
            //      uint32_t payload_size:	:8;     number of bytes in the transport payload
            //      uint32_t ck_sum :8	            checksum
            header[0] = 0;
            header[1] = packetTxSeqNumber++;
            header[2] = (byte)cmd.Length;
            header[3] = 0;
            xmitarray = new byte[4 + cmd.Length];
            Buffer.BlockCopy(header, 0, xmitarray, 0, 4);
            Buffer.BlockCopy(cmd, 0, xmitarray, 4, cmd.Length);
            foreach (byte item in xmitarray)
            {
                chksum += item;
            }
            xmitarray[3] = (byte)~chksum;       // ones complement

            convertedCmd = COBSCodec.encode(xmitarray);
            Array.Resize<byte>(ref convertedCmd, convertedCmd.Length + 1);
            convertedCmd[convertedCmd.Length - 1] = 0;                      // add trailing 0

            try
            { 
                if (serial.IsOpen)
                {
                    serial.Write(convertedCmd, 0, convertedCmd.Length);
                    statusString = DateTime.Now.ToString("HH:mm:ss.fff") + "->" + BitConverter.ToString(convertedCmd) + "\n";
                }
                else
                {
                    LastErrorMessage = "Error sending data. Port is closed";
                    LastErrorCode = (int)ErrorCodes.System.SensorConsoleNotOpen;
                    return false;
                }
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error sending data. " + ex.Message;
                LastErrorCode = (int)ErrorCodes.System.SensorConsoleWriteError;
                return false;
            }

            if (DisplaySendChar)
            {
                UpdateStatusWindow(statusString, IsThisDUTConsole ? StatusType.serialSend :StatusType.snifferSend);
            }
            if (dumpFileName != string.Empty)
            {
                lock (flocker)
                {
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(dumpFileName, true))
                    {
                        file.Write(statusString);
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Receive a response from a sent command. It will read until a 0 byte, or timeout(CommandRecieveTimeoutMS).
        /// </summary>
        /// <remarks>
        /// The decoded response will always have the following format:
        ///     byte 0 - command ID
        ///     byte 1 - command status
        ///     byte 2 and on - payload. the payload could be 0 bytes
        ///     
        /// LastErrorCode
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        /// <param name="payloadLenght = expected payload length"></param>
        /// <returns></returns>
        private bool receive_response(ref byte cmdId, ref byte cmdStatus, out byte[] payload)
        {
            return receive_response(ref cmdId, ref cmdStatus, out payload, CommandRecieveTimeoutMS);
        }

        /// <summary>
        /// recicve a response from the port. Will decode the valid packet. 
        /// </summary>
        /// <param name="cmdId"></param>
        /// <param name="cmdStatus"></param>
        /// <param name="payload">valid payload. null if invalid</param>
        /// <param name="timeoutMs"></param>
        /// <returns>true = valid packet</returns>
        private bool receive_response(ref byte cmdId, ref byte cmdStatus, out byte[] payload, int timeoutMs)
        {
            DateTime starttime = DateTime.Now;
            byte[] fullPacket;
            bool status = true;
            string fmsg = string.Empty;
            bool foundend = false;
            int bytesRead = 0;
            int endIndex = 0;


            LastErrorCode = 0;
            LastErrorMessage = string.Empty;
            payload = null;

            if (!serial.IsOpen)
            {
                LastErrorCode = (int)ErrorCodes.System.InstrumentNotConnected;
                LastErrorMessage = "Error: Timeout waiting for receive response";
                return false;
            }
            // read till 0 found or timeout
            do
            {
                try
                {
                    bytesRead = serial.BaseStream.Read(packetBuffer, bufferSaveIndex, 255 - bufferSaveIndex);
                    Debug.Print("bytes read:" + bytesRead + "\n");
                }
                catch (Exception)
                {
                    LastErrorCode = (int)ErrorCodes.Program.CommandTimeout;
                    LastErrorMessage = "Error: Timeout waiting for receive response";
                    status = false;
                    Thread.Sleep(50);
                }
                bufferSaveIndex += bytesRead;
                if (bufferSaveIndex != 0)     // something in read buffer
                {
                    for (int i = 0; i < bufferSaveIndex; i++)
                    {
                        if (packetBuffer[i] == 0)           // if found end of packet
                        {
                            endIndex = i;
                            foundend = true;
                            break;
                        }
                    }
                }
                
                if (foundend)                            // if found the end of a packet
                {
                    packetRxSeqNumber++;                                    // inc the rcv packet counter
                    byte[] savebuf = new byte[endIndex + 1];             // make the correct size buffer
                    Array.Copy(packetBuffer, savebuf, endIndex + 1);        // copy the data. it start at begining of buffer
                    packetList.Add(savebuf);                                // save in list
                    // get rid of data in the master buffer. at this point 
                    // endIndex is where the zero is.
                    Array.Copy(packetBuffer, endIndex + 1, packetBuffer, 0, bufferSaveIndex - endIndex - 1);
                    bufferSaveIndex -= endIndex + 1;                     // fix where to put new data
                    fmsg = DateTime.Now.ToString("HH:mm:ss.fff(" + packetList.Count.ToString() + ")") + "<-";
                    if (DisplayRcvChar)         // if display window is enabled
                    {
                        UpdateStatusWindow(fmsg + BitConverter.ToString(savebuf) + "\n", IsThisDUTConsole ? StatusType.serialRcv : StatusType.snifferRcv);
                    }
                }

            } while (!foundend & (starttime.AddMilliseconds(timeoutMs) > DateTime.Now));     // keep trying if no packet and not timed out

            if (packetList.Count != 0)          // have something
            {
                fullPacket = packetList.First();    // get the packet
                packetList.RemoveAt(0);         // pop it off the list
                status = (decodePacket(fullPacket, ref cmdId, ref cmdStatus, out payload));     // if decode fails, LastErrorxxx is set by decodePacket
                if (DisplayRcvChar)
                {
                    if (payload == null)
                        UpdateStatusWindow("Rcv id=" + cmdId.ToString() + " status=" + cmdStatus.ToString() + " Payload= none\n", IsThisDUTConsole ? StatusType.serialRcv : StatusType.snifferRcv);
                    else
                        UpdateStatusWindow("Rcv id=" + cmdId.ToString() + " status=" + cmdStatus.ToString() + " Payload= " + BitConverter.ToString(payload) + "\n", IsThisDUTConsole ? StatusType.serialRcv : StatusType.snifferRcv);
                }
                fmsg = DateTime.Now.ToString("HH:mm:ss.fff") + "<-";
                if (dumpFileName != string.Empty)
                {
                    lock (flocker)
                    {
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(dumpFileName, true))
                        {
                            file.Write(fmsg + BitConverter.ToString(fullPacket) + "\n");
                        }
                    }
                }
            }
            else
            {
                LastErrorCode = (int)ErrorCodes.Program.CommandTimeout;
                LastErrorMessage = "Error: Timeout waiting for receive response";
                status = false;
            }

            return status;
        }


        /// <remarks>
        /// transport layer
        ///     uint32_t version    :2	        transport protocol version, currently 0
        ///      uint32_t rsvd       :5	        reserved, always set to 0
        ///      uint32_t secure     :1	        set if secure payload, clear if payload is not secure
        ///      uint32_t sequence_num: 8	    incremented after each packet transmit
        ///                                      used by receiver to determine lost packets
        ///      uint32_t payload_size:	:8;     number of bytes in the transport payload
        ///      uint32_t ck_sum :8	            checksum
        ///     byte[] packet
        ///     
        /// The packet will always have the following format:
        ///     byte 0 - Message type -> 0x57 for DVTMAN commands
        ///     byte 1 - Message ID
        ///     byte 2 - Message status
        ///     byte 3 and on - payload. the payload could be 0 bytes
        ///     
        /// LastErrorCode
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        private bool decodePacket(byte[] packet, ref byte cmdId, ref byte cmdStatus, out byte[] payload)
        {
            byte[] datapacket;                      // data part of the response
            byte[] header = new byte[4];
            byte[] commandresponse;                 // decoded response
            byte chksum = 0;

            LastErrorCode = 0;
            LastErrorMessage = string.Empty;

            payload = null;
            if (packet.Length == 0)                 // if nothing 
            {
                LastErrorCode = (int)ErrorCodes.Program.CmdResponsePayloadLenWrong;
                LastErrorMessage = "Error: Decoding packet. Found 0 bytes";
                return false;
            }

            byte[] decodepacket = new byte[packet.Length - 1];      // need to remove the trailing 0
            Array.Copy(packet, decodepacket, packet.Length - 1);
            try
            {
                commandresponse = COBSCodec.decode(decodepacket);
            }
            catch (Exception ex)   // problem with the packet format
            {
                LastErrorCode = (int)ErrorCodes.Program.CmdPacketFormatError;
                LastErrorMessage = "Error: Error trying to decode paccket. " + ex.Message;
                return false;
            }
            if (commandresponse.Length < 7)             // if it does not have the minimum number of bytes
            {
                LastErrorCode = (int)ErrorCodes.Program.CmdResponseHeaderLenWrong;
                LastErrorMessage = "Error: Packet has less than 7 bytes. Found " + commandresponse.Length.ToString() + " bytes";
                return false;
            }
            foreach (byte item in commandresponse)      // get the checksum
            {
                chksum += item;
            }
            if (chksum != 0xFF)           // check the checksum
            {
                LastErrorCode = (int)ErrorCodes.Program.CmdPacketCheckSumError;
                LastErrorMessage = "Error: Checksum is wrong";
                return false;
            }
            if (commandresponse[4] != DVTMANMessageType)    // make sure it is the correct message type
            {
                LastErrorCode = (int)ErrorCodes.Program.CmdMsgTypeNotDVTMAN;
                LastErrorMessage = "Error: Incorrect message type. Found:" + commandresponse[4].ToString();
                return false;
            }
            // make sure the specified payload lenght match the actual payload len
            datapacket = new byte[commandresponse.Length - 4];
            Array.Copy(commandresponse, 4, datapacket, 0, commandresponse.Length - 4);
            if (datapacket.Length != commandresponse[2])
            {
                LastErrorCode = (int)ErrorCodes.Program.CmdResponsePayloadLenWrong;
                LastErrorMessage = "Error: Payload lenght wrong. Should be " + commandresponse[2].ToString() + " but found " + datapacket.Length + "bytes";
                return false;
            }
            // OK. Everything looks good now
            cmdId = datapacket[1];              // get command ID
            cmdStatus = datapacket[2];          // get status
            payload = new byte[datapacket.Length - 3];
            Array.Copy(datapacket, 3, payload, 0, datapacket.Length - 3);
            return true;
        }

        //--------------------------
        // Events
        //---------------------------

        private void HandleError(object sender, SerialErrorReceivedEventArgs e)
        {
            Console.Write("Serial error: " + e.EventType.ToString() + "\n");
            UpdateStatusWindow("Serial error: " + e.EventType.ToString() + "\n");
        }
    }
}
