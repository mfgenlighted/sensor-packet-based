﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

using TestStationErrorCodes;

using A34970ADriver;
using FixtureInstrumentBase;

namespace SensorManufSY2
{
    public partial class GeneralFixtureControl : Form
    {
        FixtureFunctions fixture;
        StationConfigure stationConfig;
        MainWindow mainWindow;
        public delegate void SetTextCallback(string text);
        string msg = string.Empty;
        public delegate void updateText(string msg);


        private void addTextToStatusWindow(string msg)
        {
            if (this.rtStatusWindow.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(addTextToStatusWindow);
                this.Invoke(d, new object[] { msg });
            }
            else
            {
                this.rtStatusWindow.AppendText(msg);
                this.rtStatusWindow.ScrollToCaret();
                this.rtStatusWindow.Update();
            }
        }

        public GeneralFixtureControl(MainWindow mainWindowobj)
        {
            InitializeComponent();
            stationConfig = mainWindowobj.stationConfig;
            mainWindow = mainWindowobj;
            string[] bldstr = new string[4];


            fixture = new FixtureFunctions();
            fixture.StationConfigure = mainWindowobj.stationConfig;
            if (!fixture.InitFixture())
            {
                MessageBox.Show("Error setting up the fixture: " + fixture.LastFunctionErrorMessage);
                return;
            }

            //foreach (var item in fixture.)
            //{
            //    bldstr[0] = item.name;
            //    bldstr[2] = item.channel.ToString();
            //    bldstr[1] = item.type.ToString();
            //    bldstr[3] = "0";
            //    dgChannelData.Rows.Add(bldstr);
            //    this.cbMeasurementToRead.Items.Add(item.name);
            //}

            if (!stationConfig.FixtureEquipment.hasEnlightedInterface)
            {
                this.nudDim1Voltage.Enabled = false;
                this.nudDim2Voltage.Enabled = false;
                this.bSetDim1.Enabled = false;
            }

            if (!stationConfig.FixtureEquipment.channelDefs.Exists(x => x.name == FixtureFunctions.CH_LED_DETECTOR))
            {
                this.rbBlue.Enabled = false;
                this.rbGreen.Enabled = false;
                this.rbNone.Enabled = false;
                this.rbRed.Enabled = false;
                this.bLEDDetectorRead.Enabled = false;
            }

        }

        // get all measuements
        private void readACDCValues_Click(object sender, EventArgs e)
        {
            FixtureFunctions.PassData[] channels = new FixtureFunctions.PassData[dgChannelData.Rows.Count];


            int numOfRows = dgChannelData.Rows.Count;
            for (int i = 0; i < numOfRows; i++)
            {
                if (((string)dgChannelData.Rows[i].Cells[1].Value == FixtureInstrument.ChannelType.DCInput.ToString()) |
                    ((string)dgChannelData.Rows[i].Cells[1].Value == FixtureInstrument.ChannelType.ACInput.ToString()))
                    channels[i].channelName = dgChannelData.Rows[i].Cells[0].Value.ToString();
            }


            fixture.ReadMeasurements(ref channels);

            if (fixture.LastFunctionStatus != 0)
            {
                addTextToStatusWindow("Error getting measurement values. \n   " + fixture.LastFunctionErrorMessage + "\n");
                return;
            }

            for (int j = 0; j < dgChannelData.Rows.Count; j++)
            { 
                for (int i = 0; i < numOfRows; i++)
                {
                    if ((string)dgChannelData.Rows[i].Cells[0].Value == channels[j].channelName)
                        dgChannelData.Rows[i].Cells[3].Value = channels[j].returnedData.ToString();
                }
            }
        }

        // monitor one measurement
        private void readDCValue_Click(object sender, EventArgs e)
        {
            double read;
            FixtureInstrument.Channel channel = new FixtureInstrument.Channel();

            int numOfRows = dgChannelData.Rows.Count;
            for (int i = 0; i < numOfRows; i++)
            {
                if ((string)dgChannelData.Rows[i].Cells[0].Value == this.cbMeasurementToRead.SelectedItem.ToString())
                    channel.name = dgChannelData.Rows[i].Cells[0].Value.ToString();
            }
            read = fixture.ReadOneMeasurement(channel.name);
            if (fixture.LastFunctionStatus != 0)
            {
                addTextToStatusWindow("Error: " + fixture.LastFunctionErrorMessage + "\n");
            }
            else
            {
                for (int i = 0; i < numOfRows; i++)
                {
                    if ((string)dgChannelData.Rows[i].Cells[0].Value == this.cbMeasurementToRead.SelectedItem.ToString())
                        dgChannelData.Rows[i].Cells[3].Value = read.ToString();
                }
            }
        }

        private void bWriteValue_Click(object sender, EventArgs e)
        {
            double setValue = 0;
            string channel = string.Empty;

            int numOfRows = dgChannelData.Rows.Count;
            for (int i = 0; i < numOfRows; i++)
            {
                if ((string)dgChannelData.Rows[i].Cells[0].Value == this.cbMeasurementToRead.SelectedItem.ToString())
                {
                    channel = dgChannelData.Rows[i].Cells[0].Value.ToString();
                }
            }


            setValue = (double)this.nudSingleValue.Value;
            fixture.SetOutput(channel, setValue);
            if (fixture.LastFunctionStatus != 0)
            {
                addTextToStatusWindow("Error: " + fixture.LastFunctionErrorMessage + "\n");
            }
            else
            {
                for (int i = 0; i < numOfRows; i++)
                {
                    if ((string)dgChannelData.Rows[i].Cells[0].Value == this.cbMeasurementToRead.SelectedItem.ToString())
                        dgChannelData.Rows[i].Cells[3].Value = setValue.ToString();
                }
            }
        }

        private void bProgramCode_Click(object sender, EventArgs e)
        {
            Flasher flasher = new Flasher(stationConfig.ConfigFlasherComPort);
        }
    }
}
