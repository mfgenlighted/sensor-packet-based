﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FixtureInstrumentBase;

namespace SensorManufSY2
{
    interface IFixtureInstrument
    {
        bool Connect();

        void Disconnect();

        bool AddChannel(string name, int channel, FixtureInstrument.ChannelType type, int range);


        void ReadListOfChannels(ref FixtureInstrument.Channel[] channelNames);

        double ReadChannel(string channelName);

        void SetChannel(string channelName, double value);
        
    }
}
