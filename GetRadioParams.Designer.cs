﻿namespace SensorManufSY2
{
    partial class GetRadioParams
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbEncryptKey = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.bOK = new System.Windows.Forms.Button();
            this.nudChannel = new System.Windows.Forms.NumericUpDown();
            this.nudTxPower = new System.Windows.Forms.NumericUpDown();
            this.nudRate = new System.Windows.Forms.NumericUpDown();
            this.nudTtl = new System.Windows.Forms.NumericUpDown();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.nudPanID = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nudChannel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTxPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTtl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPanID)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "channel(0-15)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "PanID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "txPower(0-15)";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "encrypt_key";
            // 
            // tbEncryptKey
            // 
            this.tbEncryptKey.Location = new System.Drawing.Point(47, 166);
            this.tbEncryptKey.MaxLength = 16;
            this.tbEncryptKey.Name = "tbEncryptKey";
            this.tbEncryptKey.Size = new System.Drawing.Size(100, 20);
            this.tbEncryptKey.TabIndex = 3;
            this.tbEncryptKey.Text = "manufactureTest1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(196, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "ttl";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(186, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "rate";
            // 
            // bOK
            // 
            this.bOK.Location = new System.Drawing.Point(189, 166);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(75, 23);
            this.bOK.TabIndex = 6;
            this.bOK.Text = "OK";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.label3_Click);
            // 
            // nudChannel
            // 
            this.helpProvider1.SetHelpString(this.nudChannel, "Set to 255 for defaut. Range 0-15");
            this.nudChannel.Location = new System.Drawing.Point(52, 38);
            this.nudChannel.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudChannel.Name = "nudChannel";
            this.helpProvider1.SetShowHelp(this.nudChannel, true);
            this.nudChannel.Size = new System.Drawing.Size(69, 20);
            this.nudChannel.TabIndex = 0;
            this.nudChannel.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // nudTxPower
            // 
            this.helpProvider1.SetHelpString(this.nudTxPower, "0-highest, 15-lowest");
            this.nudTxPower.Location = new System.Drawing.Point(47, 121);
            this.nudTxPower.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudTxPower.Name = "nudTxPower";
            this.helpProvider1.SetShowHelp(this.nudTxPower, true);
            this.nudTxPower.Size = new System.Drawing.Size(62, 20);
            this.nudTxPower.TabIndex = 2;
            this.nudTxPower.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // nudRate
            // 
            this.nudRate.Location = new System.Drawing.Point(189, 78);
            this.nudRate.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudRate.Name = "nudRate";
            this.nudRate.Size = new System.Drawing.Size(62, 20);
            this.nudRate.TabIndex = 5;
            this.nudRate.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            // 
            // nudTtl
            // 
            this.nudTtl.Location = new System.Drawing.Point(189, 38);
            this.nudTtl.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudTtl.Name = "nudTtl";
            this.nudTtl.Size = new System.Drawing.Size(62, 20);
            this.nudTtl.TabIndex = 4;
            this.nudTtl.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // nudPanID
            // 
            this.helpProvider1.SetHelpString(this.nudPanID, "Hex value");
            this.nudPanID.Hexadecimal = true;
            this.nudPanID.Location = new System.Drawing.Point(47, 78);
            this.nudPanID.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudPanID.Name = "nudPanID";
            this.helpProvider1.SetShowHelp(this.nudPanID, true);
            this.nudPanID.Size = new System.Drawing.Size(62, 20);
            this.nudPanID.TabIndex = 1;
            this.nudPanID.Value = new decimal(new int[] {
            26708,
            0,
            0,
            0});
            // 
            // GetRadioParams
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 246);
            this.Controls.Add(this.nudPanID);
            this.Controls.Add(this.nudTtl);
            this.Controls.Add(this.nudRate);
            this.Controls.Add(this.nudTxPower);
            this.Controls.Add(this.nudChannel);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbEncryptKey);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "GetRadioParams";
            this.Text = "GetRadioParams";
            ((System.ComponentModel.ISupportInitialize)(this.nudChannel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTxPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTtl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPanID)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbEncryptKey;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.NumericUpDown nudChannel;
        private System.Windows.Forms.NumericUpDown nudTxPower;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.NumericUpDown nudRate;
        private System.Windows.Forms.NumericUpDown nudTtl;
        private System.Windows.Forms.NumericUpDown nudPanID;
    }
}