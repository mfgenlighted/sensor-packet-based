﻿namespace SensorManufSY2
{
    partial class GetNewFixtureName
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bSave = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbFixtureName = new System.Windows.Forms.TextBox();
            this.ttNameBox = new System.Windows.Forms.ToolTip(this.components);
            this.ttSave = new System.Windows.Forms.ToolTip(this.components);
            this.ttCancel = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // bSave
            // 
            this.bSave.BackColor = System.Drawing.Color.Lime;
            this.bSave.Location = new System.Drawing.Point(164, 79);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(75, 23);
            this.bSave.TabIndex = 1;
            this.bSave.Text = "Save";
            this.ttSave.SetToolTip(this.bSave, "Create the new fixture in the Station Config file.");
            this.bSave.UseVisualStyleBackColor = false;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // bCancel
            // 
            this.bCancel.BackColor = System.Drawing.Color.Red;
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(264, 79);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 2;
            this.bCancel.Text = "Cancel";
            this.ttCancel.SetToolTip(this.bCancel, "Do not create a new fixture in the station config file.");
            this.bCancel.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name of new Fixture";
            // 
            // tbFixtureName
            // 
            this.tbFixtureName.Location = new System.Drawing.Point(48, 35);
            this.tbFixtureName.Name = "tbFixtureName";
            this.tbFixtureName.Size = new System.Drawing.Size(232, 20);
            this.tbFixtureName.TabIndex = 0;
            this.ttNameBox.SetToolTip(this.tbFixtureName, "Unique for this physical fixture.");
            // 
            // GetNewFixtureName
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 127);
            this.ControlBox = false;
            this.Controls.Add(this.tbFixtureName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bSave);
            this.Name = "GetNewFixtureName";
            this.Text = "Get New Fixture Name";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbFixtureName;
        private System.Windows.Forms.ToolTip ttSave;
        private System.Windows.Forms.ToolTip ttCancel;
        private System.Windows.Forms.ToolTip ttNameBox;
    }
}