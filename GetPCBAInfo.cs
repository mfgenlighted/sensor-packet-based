﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace SensorManufSY2
{
    public partial class GetPCBAInfo : Form
    {
        public string SN = string.Empty;
        public string pcbaPN = string.Empty;

        private string hlaPN = string.Empty;
        private string model = string.Empty;

        public GetPCBAInfo()
        {
            InitializeComponent();
        }


        private void btOK_Click(object sender, EventArgs e)
        {
            Regex fmt = new Regex(@"^....\d\d\d\d\d\d\d\d\d\d\d$");          // new SN format
            tbSN.Text = tbSN.Text.ToUpper();
            if (!fmt.Match(tbSN.Text).Success)       // if failed
            {
                MessageBox.Show("Invalid Serial Number format");
                tbSN.Focus();
                this.DialogResult = DialogResult.None;
                return;
            }

            SN = tbSN.Text;
            pcbaPN = tbPN.Text;
            DialogResult = System.Windows.Forms.DialogResult.OK;
            return;
        }
    }
}
