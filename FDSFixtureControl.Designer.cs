﻿namespace SensorManufSY2
{
    partial class FDSFixtureControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textLidStatus = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textPushButtonStatus = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TempProbeReading = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.DUT3p3vReading = new System.Windows.Forms.Label();
            this.checkBoxTestLamp = new System.Windows.Forms.CheckBox();
            this.numericUpDownLEDVoltage = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.buttonExit = new System.Windows.Forms.Button();
            this.checkBoxTestIndicator = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textLEDFreq = new System.Windows.Forms.Label();
            this.textLEDIntest = new System.Windows.Forms.Label();
            this.LEDColor = new System.Windows.Forms.TextBox();
            this.rtStatusWindow = new System.Windows.Forms.RichTextBox();
            this.nBTtimeout = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbTestRadioMAC = new System.Windows.Forms.Label();
            this.bTestRaio = new System.Windows.Forms.Button();
            this.nudLQIM = new System.Windows.Forms.NumericUpDown();
            this.nudLQIT = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lbTestChannel = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tControlVoltage = new System.Windows.Forms.Label();
            this.RadioTest = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.nudPowerLevel = new System.Windows.Forms.NumericUpDown();
            this.bReftoDUT = new System.Windows.Forms.Button();
            this.bDUTtoRef = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbOscStatus = new System.Windows.Forms.Label();
            this.lbPIRMin = new System.Windows.Forms.Label();
            this.lbPIRMax = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.bPIROscTest = new System.Windows.Forms.Button();
            this.bPCBAConfig = new System.Windows.Forms.Button();
            this.bHLAConfig = new System.Windows.Forms.Button();
            this.tControlVoltage2 = new System.Windows.Forms.Label();
            this.DebugWindowtoolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.bGetVersions = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.lbTagRadioMac = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bMACConfig = new System.Windows.Forms.Button();
            this.bReadManData = new System.Windows.Forms.Button();
            this.bClearManData = new System.Windows.Forms.Button();
            this.bSetRadioParams = new System.Windows.Forms.Button();
            this.bSetTestRadioParams = new System.Windows.Forms.Button();
            this.bReadDUTRadioParams = new System.Windows.Forms.Button();
            this.bReadTestRadioParams = new System.Windows.Forms.Button();
            this.cbDisplayData = new System.Windows.Forms.CheckBox();
            this.bClearInputBuffers = new System.Windows.Forms.Button();
            this.cbSetRefRadio = new System.Windows.Forms.CheckBox();
            this.bHwConfig = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbNone = new System.Windows.Forms.RadioButton();
            this.rbBlue = new System.Windows.Forms.RadioButton();
            this.rbGreen = new System.Windows.Forms.RadioButton();
            this.rbRed = new System.Windows.Forms.RadioButton();
            this.bSetDim1 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.nudDim2Voltage = new System.Windows.Forms.NumericUpDown();
            this.nudDim1Voltage = new System.Windows.Forms.NumericUpDown();
            this.bReadSensors = new System.Windows.Forms.Button();
            this.tbAmbient = new System.Windows.Forms.TextBox();
            this.tbTemp = new System.Windows.Forms.TextBox();
            this.tbPIR = new System.Windows.Forms.TextBox();
            this.tbTempx = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.bPOST = new System.Windows.Forms.Button();
            this.bReboot = new System.Windows.Forms.Button();
            this.nudRebootWaitMS = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.bCalTemp = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.bProgramCode = new System.Windows.Forms.Button();
            this.bClearWindow = new System.Windows.Forms.Button();
            this.bStartbackground = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLEDVoltage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nBTtimeout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQIM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQIT)).BeginInit();
            this.RadioTest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPowerLevel)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDim2Voltage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDim1Voltage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRebootWaitMS)).BeginInit();
            this.SuspendLayout();
            // 
            // textLidStatus
            // 
            this.textLidStatus.AutoSize = true;
            this.textLidStatus.Location = new System.Drawing.Point(123, 17);
            this.textLidStatus.Name = "textLidStatus";
            this.textLidStatus.Size = new System.Drawing.Size(19, 13);
            this.textLidStatus.TabIndex = 0;
            this.textLidStatus.Text = "----";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Lid Status";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Push Button Status";
            // 
            // textPushButtonStatus
            // 
            this.textPushButtonStatus.AutoSize = true;
            this.textPushButtonStatus.Location = new System.Drawing.Point(123, 32);
            this.textPushButtonStatus.Name = "textPushButtonStatus";
            this.textPushButtonStatus.Size = new System.Drawing.Size(19, 13);
            this.textPushButtonStatus.TabIndex = 3;
            this.textPushButtonStatus.Text = "----";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Temp Probe";
            // 
            // TempProbeReading
            // 
            this.TempProbeReading.AutoSize = true;
            this.TempProbeReading.Location = new System.Drawing.Point(123, 47);
            this.TempProbeReading.Name = "TempProbeReading";
            this.TempProbeReading.Size = new System.Drawing.Size(13, 13);
            this.TempProbeReading.TabIndex = 5;
            this.TempProbeReading.Text = "--";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "3.3V reading";
            // 
            // DUT3p3vReading
            // 
            this.DUT3p3vReading.AutoSize = true;
            this.DUT3p3vReading.Location = new System.Drawing.Point(123, 82);
            this.DUT3p3vReading.Name = "DUT3p3vReading";
            this.DUT3p3vReading.Size = new System.Drawing.Size(16, 13);
            this.DUT3p3vReading.TabIndex = 7;
            this.DUT3p3vReading.Text = "---";
            // 
            // checkBoxTestLamp
            // 
            this.checkBoxTestLamp.AutoSize = true;
            this.checkBoxTestLamp.Location = new System.Drawing.Point(38, 149);
            this.checkBoxTestLamp.Name = "checkBoxTestLamp";
            this.checkBoxTestLamp.Size = new System.Drawing.Size(76, 17);
            this.checkBoxTestLamp.TabIndex = 8;
            this.checkBoxTestLamp.Text = "Test Lamp";
            this.checkBoxTestLamp.UseVisualStyleBackColor = true;
            this.checkBoxTestLamp.CheckedChanged += new System.EventHandler(this.checkBoxTestLamp_CheckedChanged);
            // 
            // numericUpDownLEDVoltage
            // 
            this.numericUpDownLEDVoltage.DecimalPlaces = 1;
            this.numericUpDownLEDVoltage.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownLEDVoltage.Location = new System.Drawing.Point(9, 173);
            this.numericUpDownLEDVoltage.Maximum = new decimal(new int[] {
            48,
            0,
            0,
            65536});
            this.numericUpDownLEDVoltage.Name = "numericUpDownLEDVoltage";
            this.numericUpDownLEDVoltage.Size = new System.Drawing.Size(41, 20);
            this.numericUpDownLEDVoltage.TabIndex = 9;
            this.numericUpDownLEDVoltage.ValueChanged += new System.EventHandler(this.numericUpDownLEDVoltage_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(57, 179);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "LED Voltage";
            // 
            // buttonExit
            // 
            this.buttonExit.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExit.Location = new System.Drawing.Point(28, 603);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(75, 23);
            this.buttonExit.TabIndex = 17;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            // 
            // checkBoxTestIndicator
            // 
            this.checkBoxTestIndicator.AutoSize = true;
            this.checkBoxTestIndicator.Location = new System.Drawing.Point(38, 199);
            this.checkBoxTestIndicator.Name = "checkBoxTestIndicator";
            this.checkBoxTestIndicator.Size = new System.Drawing.Size(82, 17);
            this.checkBoxTestIndicator.TabIndex = 21;
            this.checkBoxTestIndicator.Text = "DUT Power";
            this.checkBoxTestIndicator.UseVisualStyleBackColor = true;
            this.checkBoxTestIndicator.CheckedChanged += new System.EventHandler(this.checkBoxTestIndicator_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 219);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "LED detector";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(33, 238);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Freq";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(33, 251);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Intest";
            // 
            // textLEDFreq
            // 
            this.textLEDFreq.AutoSize = true;
            this.textLEDFreq.Location = new System.Drawing.Point(84, 238);
            this.textLEDFreq.Name = "textLEDFreq";
            this.textLEDFreq.Size = new System.Drawing.Size(16, 13);
            this.textLEDFreq.TabIndex = 25;
            this.textLEDFreq.Text = "---";
            // 
            // textLEDIntest
            // 
            this.textLEDIntest.AutoSize = true;
            this.textLEDIntest.Location = new System.Drawing.Point(84, 251);
            this.textLEDIntest.Name = "textLEDIntest";
            this.textLEDIntest.Size = new System.Drawing.Size(16, 13);
            this.textLEDIntest.TabIndex = 26;
            this.textLEDIntest.Text = "---";
            // 
            // LEDColor
            // 
            this.LEDColor.Location = new System.Drawing.Point(154, 238);
            this.LEDColor.Name = "LEDColor";
            this.LEDColor.ReadOnly = true;
            this.LEDColor.Size = new System.Drawing.Size(22, 20);
            this.LEDColor.TabIndex = 27;
            this.LEDColor.TabStop = false;
            // 
            // rtStatusWindow
            // 
            this.rtStatusWindow.Location = new System.Drawing.Point(290, 29);
            this.rtStatusWindow.Name = "rtStatusWindow";
            this.rtStatusWindow.Size = new System.Drawing.Size(743, 218);
            this.rtStatusWindow.TabIndex = 36;
            this.rtStatusWindow.Text = "";
            // 
            // nBTtimeout
            // 
            this.nBTtimeout.Location = new System.Drawing.Point(63, 84);
            this.nBTtimeout.Name = "nBTtimeout";
            this.nBTtimeout.Size = new System.Drawing.Size(52, 20);
            this.nBTtimeout.TabIndex = 37;
            this.nBTtimeout.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(33, 64);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 13);
            this.label10.TabIndex = 38;
            this.label10.Text = "Scan Timeout(Seconds)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 102);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 41;
            this.label11.Text = "Test radio MAC";
            // 
            // tbTestRadioMAC
            // 
            this.tbTestRadioMAC.AutoSize = true;
            this.tbTestRadioMAC.Location = new System.Drawing.Point(123, 102);
            this.tbTestRadioMAC.Name = "tbTestRadioMAC";
            this.tbTestRadioMAC.Size = new System.Drawing.Size(16, 13);
            this.tbTestRadioMAC.TabIndex = 42;
            this.tbTestRadioMAC.Text = "---";
            // 
            // bTestRaio
            // 
            this.bTestRaio.Location = new System.Drawing.Point(171, 69);
            this.bTestRaio.Name = "bTestRaio";
            this.bTestRaio.Size = new System.Drawing.Size(75, 23);
            this.bTestRaio.TabIndex = 45;
            this.bTestRaio.Text = "Test Radio";
            this.bTestRaio.UseVisualStyleBackColor = true;
            this.bTestRaio.Click += new System.EventHandler(this.bTestRaio_Click);
            // 
            // nudLQIM
            // 
            this.nudLQIM.Location = new System.Drawing.Point(78, 34);
            this.nudLQIM.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLQIM.Name = "nudLQIM";
            this.nudLQIM.Size = new System.Drawing.Size(59, 20);
            this.nudLQIM.TabIndex = 46;
            this.nudLQIM.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // nudLQIT
            // 
            this.nudLQIT.Location = new System.Drawing.Point(78, 59);
            this.nudLQIT.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLQIT.Name = "nudLQIT";
            this.nudLQIT.Size = new System.Drawing.Size(59, 20);
            this.nudLQIT.TabIndex = 47;
            this.nudLQIT.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(39, 36);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 13);
            this.label13.TabIndex = 48;
            this.label13.Text = "LQIM";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(41, 62);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 49;
            this.label14.Text = "LQIT";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(27, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(45, 13);
            this.label15.TabIndex = 51;
            this.label15.Text = "channel";
            // 
            // lbTestChannel
            // 
            this.lbTestChannel.AutoSize = true;
            this.lbTestChannel.Location = new System.Drawing.Point(78, 12);
            this.lbTestChannel.Name = "lbTestChannel";
            this.lbTestChannel.Size = new System.Drawing.Size(16, 13);
            this.lbTestChannel.TabIndex = 52;
            this.lbTestChannel.Text = "---";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 67);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 13);
            this.label16.TabIndex = 53;
            this.label16.Text = "Control Voltage";
            // 
            // tControlVoltage
            // 
            this.tControlVoltage.AutoSize = true;
            this.tControlVoltage.Location = new System.Drawing.Point(123, 67);
            this.tControlVoltage.Name = "tControlVoltage";
            this.tControlVoltage.Size = new System.Drawing.Size(16, 13);
            this.tControlVoltage.TabIndex = 54;
            this.tControlVoltage.Text = "---";
            // 
            // RadioTest
            // 
            this.RadioTest.Controls.Add(this.label12);
            this.RadioTest.Controls.Add(this.nudPowerLevel);
            this.RadioTest.Controls.Add(this.lbTestChannel);
            this.RadioTest.Controls.Add(this.label15);
            this.RadioTest.Controls.Add(this.label14);
            this.RadioTest.Controls.Add(this.label13);
            this.RadioTest.Controls.Add(this.nudLQIT);
            this.RadioTest.Controls.Add(this.nudLQIM);
            this.RadioTest.Controls.Add(this.bTestRaio);
            this.RadioTest.Location = new System.Drawing.Point(260, 333);
            this.RadioTest.Name = "RadioTest";
            this.RadioTest.Size = new System.Drawing.Size(269, 98);
            this.RadioTest.TabIndex = 55;
            this.RadioTest.TabStop = false;
            this.RadioTest.Text = "Radio Test";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(171, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 13);
            this.label12.TabIndex = 54;
            this.label12.Text = "Power Level";
            // 
            // nudPowerLevel
            // 
            this.nudPowerLevel.Location = new System.Drawing.Point(171, 41);
            this.nudPowerLevel.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudPowerLevel.Name = "nudPowerLevel";
            this.nudPowerLevel.Size = new System.Drawing.Size(65, 20);
            this.nudPowerLevel.TabIndex = 53;
            this.DebugWindowtoolTip1.SetToolTip(this.nudPowerLevel, "Power level 0=max, 15=lowest");
            // 
            // bReftoDUT
            // 
            this.bReftoDUT.Location = new System.Drawing.Point(51, 37);
            this.bReftoDUT.Name = "bReftoDUT";
            this.bReftoDUT.Size = new System.Drawing.Size(75, 23);
            this.bReftoDUT.TabIndex = 40;
            this.bReftoDUT.Text = "Ref to DUT";
            this.bReftoDUT.UseVisualStyleBackColor = true;
            this.bReftoDUT.Click += new System.EventHandler(this.bReftoDUT_Click);
            // 
            // bDUTtoRef
            // 
            this.bDUTtoRef.Location = new System.Drawing.Point(51, 14);
            this.bDUTtoRef.Name = "bDUTtoRef";
            this.bDUTtoRef.Size = new System.Drawing.Size(75, 23);
            this.bDUTtoRef.TabIndex = 39;
            this.bDUTtoRef.Text = "DUT to Ref";
            this.bDUTtoRef.UseVisualStyleBackColor = true;
            this.bDUTtoRef.Click += new System.EventHandler(this.bDUTtoRef_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbOscStatus);
            this.groupBox3.Controls.Add(this.lbPIRMin);
            this.groupBox3.Controls.Add(this.lbPIRMax);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.bPIROscTest);
            this.groupBox3.Location = new System.Drawing.Point(257, 439);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(272, 68);
            this.groupBox3.TabIndex = 57;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "PIR Test";
            // 
            // lbOscStatus
            // 
            this.lbOscStatus.AutoSize = true;
            this.lbOscStatus.Location = new System.Drawing.Point(223, 19);
            this.lbOscStatus.Name = "lbOscStatus";
            this.lbOscStatus.Size = new System.Drawing.Size(16, 13);
            this.lbOscStatus.TabIndex = 5;
            this.lbOscStatus.Text = "---";
            // 
            // lbPIRMin
            // 
            this.lbPIRMin.AutoSize = true;
            this.lbPIRMin.Location = new System.Drawing.Point(171, 40);
            this.lbPIRMin.Name = "lbPIRMin";
            this.lbPIRMin.Size = new System.Drawing.Size(16, 13);
            this.lbPIRMin.TabIndex = 4;
            this.lbPIRMin.Text = "---";
            // 
            // lbPIRMax
            // 
            this.lbPIRMax.AutoSize = true;
            this.lbPIRMax.Location = new System.Drawing.Point(170, 20);
            this.lbPIRMax.Name = "lbPIRMax";
            this.lbPIRMax.Size = new System.Drawing.Size(16, 13);
            this.lbPIRMax.TabIndex = 3;
            this.lbPIRMax.Text = "---";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(140, 40);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(24, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Min";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(137, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(27, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Max";
            // 
            // bPIROscTest
            // 
            this.bPIROscTest.Location = new System.Drawing.Point(23, 31);
            this.bPIROscTest.Name = "bPIROscTest";
            this.bPIROscTest.Size = new System.Drawing.Size(90, 23);
            this.bPIROscTest.TabIndex = 0;
            this.bPIROscTest.Text = "Oscillation Test";
            this.bPIROscTest.UseVisualStyleBackColor = true;
            this.bPIROscTest.Click += new System.EventHandler(this.bPIROscTest_Click);
            // 
            // bPCBAConfig
            // 
            this.bPCBAConfig.Location = new System.Drawing.Point(13, 284);
            this.bPCBAConfig.Name = "bPCBAConfig";
            this.bPCBAConfig.Size = new System.Drawing.Size(90, 23);
            this.bPCBAConfig.TabIndex = 58;
            this.bPCBAConfig.Text = "PCBA Config";
            this.DebugWindowtoolTip1.SetToolTip(this.bPCBAConfig, "Program PCBA SN. Does not display\r\nin status window. Result in Bluetooth\r\nTest Wi" +
        "ndow.");
            this.bPCBAConfig.UseVisualStyleBackColor = true;
            this.bPCBAConfig.Click += new System.EventHandler(this.bPCBAConfig_Click);
            // 
            // bHLAConfig
            // 
            this.bHLAConfig.Location = new System.Drawing.Point(13, 307);
            this.bHLAConfig.Name = "bHLAConfig";
            this.bHLAConfig.Size = new System.Drawing.Size(90, 23);
            this.bHLAConfig.TabIndex = 59;
            this.bHLAConfig.Text = "HLA Config";
            this.DebugWindowtoolTip1.SetToolTip(this.bHLAConfig, "Program HLA data based on\r\nconfiguration.");
            this.bHLAConfig.UseVisualStyleBackColor = true;
            this.bHLAConfig.Click += new System.EventHandler(this.bHLAConfig_Click);
            // 
            // tControlVoltage2
            // 
            this.tControlVoltage2.AutoSize = true;
            this.tControlVoltage2.Location = new System.Drawing.Point(151, 67);
            this.tControlVoltage2.Name = "tControlVoltage2";
            this.tControlVoltage2.Size = new System.Drawing.Size(16, 13);
            this.tControlVoltage2.TabIndex = 60;
            this.tControlVoltage2.Text = "---";
            // 
            // bGetVersions
            // 
            this.bGetVersions.Location = new System.Drawing.Point(154, 400);
            this.bGetVersions.Name = "bGetVersions";
            this.bGetVersions.Size = new System.Drawing.Size(90, 23);
            this.bGetVersions.TabIndex = 66;
            this.bGetVersions.Text = "Get Versions";
            this.DebugWindowtoolTip1.SetToolTip(this.bGetVersions, "Program HLA data based on\r\nconfiguration.");
            this.bGetVersions.UseVisualStyleBackColor = true;
            this.bGetVersions.Click += new System.EventHandler(this.bGetVersions_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(15, 119);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(82, 13);
            this.label19.TabIndex = 62;
            this.label19.Text = "BLE Radio Mac";
            // 
            // lbTagRadioMac
            // 
            this.lbTagRadioMac.AutoSize = true;
            this.lbTagRadioMac.Location = new System.Drawing.Point(126, 118);
            this.lbTagRadioMac.Name = "lbTagRadioMac";
            this.lbTagRadioMac.Size = new System.Drawing.Size(16, 13);
            this.lbTagRadioMac.TabIndex = 63;
            this.lbTagRadioMac.Text = "---";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 268);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 65;
            this.label3.Text = "DUT Command";
            // 
            // bMACConfig
            // 
            this.bMACConfig.Location = new System.Drawing.Point(13, 330);
            this.bMACConfig.Name = "bMACConfig";
            this.bMACConfig.Size = new System.Drawing.Size(75, 23);
            this.bMACConfig.TabIndex = 67;
            this.bMACConfig.Text = "MAC Config";
            this.bMACConfig.UseVisualStyleBackColor = true;
            this.bMACConfig.Click += new System.EventHandler(this.bMACConfig_Click);
            // 
            // bReadManData
            // 
            this.bReadManData.Location = new System.Drawing.Point(109, 284);
            this.bReadManData.Name = "bReadManData";
            this.bReadManData.Size = new System.Drawing.Size(95, 23);
            this.bReadManData.TabIndex = 68;
            this.bReadManData.Text = "Read Man Data";
            this.bReadManData.UseVisualStyleBackColor = true;
            this.bReadManData.Click += new System.EventHandler(this.bReadManData_Click);
            // 
            // bClearManData
            // 
            this.bClearManData.Location = new System.Drawing.Point(109, 343);
            this.bClearManData.Name = "bClearManData";
            this.bClearManData.Size = new System.Drawing.Size(95, 23);
            this.bClearManData.TabIndex = 69;
            this.bClearManData.Text = "Clear Man Data";
            this.bClearManData.UseVisualStyleBackColor = true;
            this.bClearManData.Click += new System.EventHandler(this.bClearManData_Click);
            // 
            // bSetRadioParams
            // 
            this.bSetRadioParams.Location = new System.Drawing.Point(290, 281);
            this.bSetRadioParams.Name = "bSetRadioParams";
            this.bSetRadioParams.Size = new System.Drawing.Size(131, 23);
            this.bSetRadioParams.TabIndex = 70;
            this.bSetRadioParams.Text = "Set DUT Radio Params";
            this.bSetRadioParams.UseVisualStyleBackColor = true;
            this.bSetRadioParams.Click += new System.EventHandler(this.bSetRadioParams_Click);
            // 
            // bSetTestRadioParams
            // 
            this.bSetTestRadioParams.Location = new System.Drawing.Point(290, 304);
            this.bSetTestRadioParams.Name = "bSetTestRadioParams";
            this.bSetTestRadioParams.Size = new System.Drawing.Size(131, 23);
            this.bSetTestRadioParams.TabIndex = 71;
            this.bSetTestRadioParams.Text = "Set Test Radio Params";
            this.bSetTestRadioParams.UseVisualStyleBackColor = true;
            this.bSetTestRadioParams.Click += new System.EventHandler(this.bSetRefRadioParams_Click);
            // 
            // bReadDUTRadioParams
            // 
            this.bReadDUTRadioParams.Location = new System.Drawing.Point(427, 281);
            this.bReadDUTRadioParams.Name = "bReadDUTRadioParams";
            this.bReadDUTRadioParams.Size = new System.Drawing.Size(75, 23);
            this.bReadDUTRadioParams.TabIndex = 72;
            this.bReadDUTRadioParams.Text = "Read";
            this.bReadDUTRadioParams.UseVisualStyleBackColor = true;
            this.bReadDUTRadioParams.Click += new System.EventHandler(this.bReadDUTRadioParams_Click);
            // 
            // bReadTestRadioParams
            // 
            this.bReadTestRadioParams.Location = new System.Drawing.Point(427, 304);
            this.bReadTestRadioParams.Name = "bReadTestRadioParams";
            this.bReadTestRadioParams.Size = new System.Drawing.Size(75, 23);
            this.bReadTestRadioParams.TabIndex = 73;
            this.bReadTestRadioParams.Text = "Read";
            this.bReadTestRadioParams.UseVisualStyleBackColor = true;
            this.bReadTestRadioParams.Click += new System.EventHandler(this.bReadTestRadioParams_Click);
            // 
            // cbDisplayData
            // 
            this.cbDisplayData.AutoSize = true;
            this.cbDisplayData.Location = new System.Drawing.Point(109, 264);
            this.cbDisplayData.Name = "cbDisplayData";
            this.cbDisplayData.Size = new System.Drawing.Size(84, 17);
            this.cbDisplayData.TabIndex = 74;
            this.cbDisplayData.Text = "Display data";
            this.cbDisplayData.UseVisualStyleBackColor = true;
            this.cbDisplayData.CheckedChanged += new System.EventHandler(this.cbDisplayData_CheckedChanged);
            // 
            // bClearInputBuffers
            // 
            this.bClearInputBuffers.Location = new System.Drawing.Point(109, 314);
            this.bClearInputBuffers.Name = "bClearInputBuffers";
            this.bClearInputBuffers.Size = new System.Drawing.Size(116, 23);
            this.bClearInputBuffers.TabIndex = 75;
            this.bClearInputBuffers.Text = "Clear Input Buffers";
            this.bClearInputBuffers.UseVisualStyleBackColor = true;
            this.bClearInputBuffers.Click += new System.EventHandler(this.bClearInputBuffers_Click);
            // 
            // cbSetRefRadio
            // 
            this.cbSetRefRadio.AutoSize = true;
            this.cbSetRefRadio.Location = new System.Drawing.Point(199, 264);
            this.cbSetRefRadio.Name = "cbSetRefRadio";
            this.cbSetRefRadio.Size = new System.Drawing.Size(93, 17);
            this.cbSetRefRadio.TabIndex = 76;
            this.cbSetRefRadio.Text = "Set Ref Radio";
            this.cbSetRefRadio.UseVisualStyleBackColor = true;
            // 
            // bHwConfig
            // 
            this.bHwConfig.Location = new System.Drawing.Point(16, 353);
            this.bHwConfig.Name = "bHwConfig";
            this.bHwConfig.Size = new System.Drawing.Size(75, 23);
            this.bHwConfig.TabIndex = 77;
            this.bHwConfig.Text = "HW Config";
            this.bHwConfig.UseVisualStyleBackColor = true;
            this.bHwConfig.Click += new System.EventHandler(this.bHwConfig_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bDUTtoRef);
            this.groupBox1.Controls.Add(this.nBTtimeout);
            this.groupBox1.Controls.Add(this.bReftoDUT);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Location = new System.Drawing.Point(535, 323);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 110);
            this.groupBox1.TabIndex = 78;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bluetooth Test";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbNone);
            this.groupBox2.Controls.Add(this.rbBlue);
            this.groupBox2.Controls.Add(this.rbGreen);
            this.groupBox2.Controls.Add(this.rbRed);
            this.groupBox2.Location = new System.Drawing.Point(168, 513);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(94, 99);
            this.groupBox2.TabIndex = 79;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "LED Control";
            // 
            // rbNone
            // 
            this.rbNone.AutoSize = true;
            this.rbNone.Location = new System.Drawing.Point(6, 76);
            this.rbNone.Name = "rbNone";
            this.rbNone.Size = new System.Drawing.Size(51, 17);
            this.rbNone.TabIndex = 3;
            this.rbNone.TabStop = true;
            this.rbNone.Text = "None";
            this.rbNone.UseVisualStyleBackColor = true;
            this.rbNone.CheckedChanged += new System.EventHandler(this.cbRed_CheckedChanged);
            // 
            // rbBlue
            // 
            this.rbBlue.AutoSize = true;
            this.rbBlue.Location = new System.Drawing.Point(6, 53);
            this.rbBlue.Name = "rbBlue";
            this.rbBlue.Size = new System.Drawing.Size(46, 17);
            this.rbBlue.TabIndex = 2;
            this.rbBlue.TabStop = true;
            this.rbBlue.Text = "Blue";
            this.rbBlue.UseVisualStyleBackColor = true;
            this.rbBlue.CheckedChanged += new System.EventHandler(this.cbRed_CheckedChanged);
            // 
            // rbGreen
            // 
            this.rbGreen.AutoSize = true;
            this.rbGreen.Location = new System.Drawing.Point(6, 36);
            this.rbGreen.Name = "rbGreen";
            this.rbGreen.Size = new System.Drawing.Size(54, 17);
            this.rbGreen.TabIndex = 1;
            this.rbGreen.TabStop = true;
            this.rbGreen.Text = "Green";
            this.rbGreen.UseVisualStyleBackColor = true;
            this.rbGreen.CheckedChanged += new System.EventHandler(this.cbRed_CheckedChanged);
            // 
            // rbRed
            // 
            this.rbRed.AutoSize = true;
            this.rbRed.Location = new System.Drawing.Point(7, 19);
            this.rbRed.Name = "rbRed";
            this.rbRed.Size = new System.Drawing.Size(45, 17);
            this.rbRed.TabIndex = 0;
            this.rbRed.TabStop = true;
            this.rbRed.Text = "Red";
            this.rbRed.UseVisualStyleBackColor = true;
            this.rbRed.CheckedChanged += new System.EventHandler(this.cbRed_CheckedChanged);
            // 
            // bSetDim1
            // 
            this.bSetDim1.Location = new System.Drawing.Point(18, 71);
            this.bSetDim1.Name = "bSetDim1";
            this.bSetDim1.Size = new System.Drawing.Size(75, 23);
            this.bSetDim1.TabIndex = 80;
            this.bSetDim1.Text = "Set Dim";
            this.bSetDim1.UseVisualStyleBackColor = true;
            this.bSetDim1.Click += new System.EventHandler(this.bSetDim1_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.nudDim2Voltage);
            this.groupBox4.Controls.Add(this.nudDim1Voltage);
            this.groupBox4.Controls.Add(this.bSetDim1);
            this.groupBox4.Location = new System.Drawing.Point(18, 489);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(118, 100);
            this.groupBox4.TabIndex = 82;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Dim outputs";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(15, 48);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(31, 13);
            this.label21.TabIndex = 85;
            this.label21.Text = "Dim2";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(16, 24);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(31, 13);
            this.label20.TabIndex = 84;
            this.label20.Text = "Dim1";
            // 
            // nudDim2Voltage
            // 
            this.nudDim2Voltage.Location = new System.Drawing.Point(52, 45);
            this.nudDim2Voltage.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudDim2Voltage.Name = "nudDim2Voltage";
            this.nudDim2Voltage.Size = new System.Drawing.Size(41, 20);
            this.nudDim2Voltage.TabIndex = 83;
            // 
            // nudDim1Voltage
            // 
            this.nudDim1Voltage.Location = new System.Drawing.Point(52, 19);
            this.nudDim1Voltage.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudDim1Voltage.Name = "nudDim1Voltage";
            this.nudDim1Voltage.Size = new System.Drawing.Size(41, 20);
            this.nudDim1Voltage.TabIndex = 82;
            // 
            // bReadSensors
            // 
            this.bReadSensors.Location = new System.Drawing.Point(302, 513);
            this.bReadSensors.Name = "bReadSensors";
            this.bReadSensors.Size = new System.Drawing.Size(95, 23);
            this.bReadSensors.TabIndex = 83;
            this.bReadSensors.Text = "Read Sensors";
            this.bReadSensors.UseVisualStyleBackColor = true;
            this.bReadSensors.Click += new System.EventHandler(this.bReadSensors_Click);
            // 
            // tbAmbient
            // 
            this.tbAmbient.Location = new System.Drawing.Point(373, 562);
            this.tbAmbient.Name = "tbAmbient";
            this.tbAmbient.Size = new System.Drawing.Size(100, 20);
            this.tbAmbient.TabIndex = 84;
            // 
            // tbTemp
            // 
            this.tbTemp.Location = new System.Drawing.Point(373, 602);
            this.tbTemp.Name = "tbTemp";
            this.tbTemp.Size = new System.Drawing.Size(100, 20);
            this.tbTemp.TabIndex = 86;
            // 
            // tbPIR
            // 
            this.tbPIR.Location = new System.Drawing.Point(373, 542);
            this.tbPIR.Name = "tbPIR";
            this.tbPIR.Size = new System.Drawing.Size(100, 20);
            this.tbPIR.TabIndex = 87;
            // 
            // tbTempx
            // 
            this.tbTempx.AutoSize = true;
            this.tbTempx.Location = new System.Drawing.Point(287, 605);
            this.tbTempx.Name = "tbTempx";
            this.tbTempx.Size = new System.Drawing.Size(34, 13);
            this.tbTempx.TabIndex = 88;
            this.tbTempx.Text = "Temp";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(287, 565);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 13);
            this.label24.TabIndex = 90;
            this.label24.Text = "Ambient";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(287, 549);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(25, 13);
            this.label25.TabIndex = 91;
            this.label25.Text = "PIR";
            // 
            // bPOST
            // 
            this.bPOST.Location = new System.Drawing.Point(566, 479);
            this.bPOST.Name = "bPOST";
            this.bPOST.Size = new System.Drawing.Size(75, 23);
            this.bPOST.TabIndex = 92;
            this.bPOST.Text = "POST";
            this.bPOST.UseVisualStyleBackColor = true;
            this.bPOST.Click += new System.EventHandler(this.bPOST_Click);
            // 
            // bReboot
            // 
            this.bReboot.Location = new System.Drawing.Point(566, 513);
            this.bReboot.Name = "bReboot";
            this.bReboot.Size = new System.Drawing.Size(75, 23);
            this.bReboot.TabIndex = 93;
            this.bReboot.Text = "Reboot";
            this.bReboot.UseVisualStyleBackColor = true;
            this.bReboot.Click += new System.EventHandler(this.bReboot_Click);
            // 
            // nudRebootWaitMS
            // 
            this.nudRebootWaitMS.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudRebootWaitMS.Location = new System.Drawing.Point(648, 515);
            this.nudRebootWaitMS.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudRebootWaitMS.Name = "nudRebootWaitMS";
            this.nudRebootWaitMS.Size = new System.Drawing.Size(56, 20);
            this.nudRebootWaitMS.TabIndex = 94;
            this.nudRebootWaitMS.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(636, 499);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(86, 13);
            this.label22.TabIndex = 95;
            this.label22.Text = "max wait time ms";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(154, 371);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 23);
            this.button1.TabIndex = 96;
            this.button1.Text = "Change Over";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bCalTemp
            // 
            this.bCalTemp.Location = new System.Drawing.Point(22, 413);
            this.bCalTemp.Name = "bCalTemp";
            this.bCalTemp.Size = new System.Drawing.Size(75, 23);
            this.bCalTemp.TabIndex = 97;
            this.bCalTemp.Text = "Cal Temp";
            this.bCalTemp.UseVisualStyleBackColor = true;
            this.bCalTemp.Click += new System.EventHandler(this.bCalTemp_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(18, 384);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(106, 23);
            this.button2.TabIndex = 98;
            this.button2.Text = "Config all man data";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.bProgAllManData_Click);
            // 
            // bProgramCode
            // 
            this.bProgramCode.Location = new System.Drawing.Point(129, 439);
            this.bProgramCode.Name = "bProgramCode";
            this.bProgramCode.Size = new System.Drawing.Size(99, 23);
            this.bProgramCode.TabIndex = 99;
            this.bProgramCode.Text = "Program Code";
            this.bProgramCode.UseVisualStyleBackColor = true;
            this.bProgramCode.Click += new System.EventHandler(this.bProgramCode_Click);
            // 
            // bClearWindow
            // 
            this.bClearWindow.Location = new System.Drawing.Point(934, 254);
            this.bClearWindow.Name = "bClearWindow";
            this.bClearWindow.Size = new System.Drawing.Size(86, 23);
            this.bClearWindow.TabIndex = 100;
            this.bClearWindow.Text = "Clear Window";
            this.bClearWindow.UseVisualStyleBackColor = true;
            this.bClearWindow.Click += new System.EventHandler(this.bClearWindow_Click);
            // 
            // bStartbackground
            // 
            this.bStartbackground.Location = new System.Drawing.Point(196, 22);
            this.bStartbackground.Name = "bStartbackground";
            this.bStartbackground.Size = new System.Drawing.Size(75, 23);
            this.bStartbackground.TabIndex = 101;
            this.bStartbackground.Text = "start";
            this.bStartbackground.UseVisualStyleBackColor = true;
            this.bStartbackground.Click += new System.EventHandler(this.bStartbackground_Click);
            // 
            // FDSFixtureControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1054, 638);
            this.ControlBox = false;
            this.Controls.Add(this.bStartbackground);
            this.Controls.Add(this.bClearWindow);
            this.Controls.Add(this.bProgramCode);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.bCalTemp);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.nudRebootWaitMS);
            this.Controls.Add(this.bReboot);
            this.Controls.Add(this.bPOST);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.tbTempx);
            this.Controls.Add(this.tbPIR);
            this.Controls.Add(this.tbTemp);
            this.Controls.Add(this.tbAmbient);
            this.Controls.Add(this.bReadSensors);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.rtStatusWindow);
            this.Controls.Add(this.bHwConfig);
            this.Controls.Add(this.cbSetRefRadio);
            this.Controls.Add(this.bClearInputBuffers);
            this.Controls.Add(this.cbDisplayData);
            this.Controls.Add(this.bReadTestRadioParams);
            this.Controls.Add(this.bReadDUTRadioParams);
            this.Controls.Add(this.bSetTestRadioParams);
            this.Controls.Add(this.bSetRadioParams);
            this.Controls.Add(this.bClearManData);
            this.Controls.Add(this.bReadManData);
            this.Controls.Add(this.bMACConfig);
            this.Controls.Add(this.bGetVersions);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbTagRadioMac);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.tControlVoltage2);
            this.Controls.Add(this.bHLAConfig);
            this.Controls.Add(this.bPCBAConfig);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.RadioTest);
            this.Controls.Add(this.tControlVoltage);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.tbTestRadioMAC);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.LEDColor);
            this.Controls.Add(this.textLEDIntest);
            this.Controls.Add(this.textLEDFreq);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.checkBoxTestIndicator);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.numericUpDownLEDVoltage);
            this.Controls.Add(this.checkBoxTestLamp);
            this.Controls.Add(this.DUT3p3vReading);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TempProbeReading);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textPushButtonStatus);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textLidStatus);
            this.Name = "FDSFixtureControl";
            this.Text = "FDSFixtureControl";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FDSFixtureControl_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLEDVoltage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nBTtimeout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQIM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQIT)).EndInit();
            this.RadioTest.ResumeLayout(false);
            this.RadioTest.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPowerLevel)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDim2Voltage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDim1Voltage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRebootWaitMS)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label textLidStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label textPushButtonStatus;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label TempProbeReading;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label DUT3p3vReading;
        private System.Windows.Forms.CheckBox checkBoxTestLamp;
        private System.Windows.Forms.NumericUpDown numericUpDownLEDVoltage;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.CheckBox checkBoxTestIndicator;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label textLEDFreq;
        private System.Windows.Forms.Label textLEDIntest;
        private System.Windows.Forms.TextBox LEDColor;
        private System.Windows.Forms.RichTextBox rtStatusWindow;
        private System.Windows.Forms.NumericUpDown nBTtimeout;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label tbTestRadioMAC;
        private System.Windows.Forms.Button bTestRaio;
        private System.Windows.Forms.NumericUpDown nudLQIM;
        private System.Windows.Forms.NumericUpDown nudLQIT;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbTestChannel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label tControlVoltage;
        private System.Windows.Forms.GroupBox RadioTest;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbPIRMin;
        private System.Windows.Forms.Label lbPIRMax;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button bPIROscTest;
        private System.Windows.Forms.Label lbOscStatus;
        private System.Windows.Forms.Button bPCBAConfig;
        private System.Windows.Forms.Button bHLAConfig;
        private System.Windows.Forms.Label tControlVoltage2;
        private System.Windows.Forms.ToolTip DebugWindowtoolTip1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lbTagRadioMac;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bGetVersions;
        private System.Windows.Forms.Button bMACConfig;
        private System.Windows.Forms.Button bReadManData;
        private System.Windows.Forms.Button bClearManData;
        private System.Windows.Forms.Button bSetRadioParams;
        private System.Windows.Forms.Button bSetTestRadioParams;
        private System.Windows.Forms.Button bReadDUTRadioParams;
        private System.Windows.Forms.Button bReadTestRadioParams;
        private System.Windows.Forms.Button bReftoDUT;
        private System.Windows.Forms.Button bDUTtoRef;
        private System.Windows.Forms.CheckBox cbDisplayData;
        private System.Windows.Forms.Button bClearInputBuffers;
        private System.Windows.Forms.CheckBox cbSetRefRadio;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nudPowerLevel;
        private System.Windows.Forms.Button bHwConfig;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbBlue;
        private System.Windows.Forms.RadioButton rbGreen;
        private System.Windows.Forms.RadioButton rbRed;
        private System.Windows.Forms.RadioButton rbNone;
        private System.Windows.Forms.Button bSetDim1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown nudDim1Voltage;
        private System.Windows.Forms.NumericUpDown nudDim2Voltage;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button bReadSensors;
        private System.Windows.Forms.TextBox tbAmbient;
        private System.Windows.Forms.TextBox tbTemp;
        private System.Windows.Forms.TextBox tbPIR;
        private System.Windows.Forms.Label tbTempx;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button bPOST;
        private System.Windows.Forms.Button bReboot;
        private System.Windows.Forms.NumericUpDown nudRebootWaitMS;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button bCalTemp;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button bProgramCode;
        private System.Windows.Forms.Button bClearWindow;
        private System.Windows.Forms.Button bStartbackground;
    }
}