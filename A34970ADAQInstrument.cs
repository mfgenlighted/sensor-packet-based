﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;

using TestStationErrorCodes;
using SensorManufSY2;
using FixtureInstrumentBase;

namespace A34970ADriver
{
    public class A34970ADAQInstrument : FixtureInstrument, IFixtureInstrument
    {

        class Globls
        //"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        // Sets global variables to be used in different functions of the program.
        //"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        {
            public static int videfaultRM = 0;  // Resource manager session returned by viOpenDefaultRM(videfaultRM)
            public static int vi = 0;           // Session identifier of devices
            public static int errorStatus;      // VISA function status return code
            public static string instrumentName;      // Stores the Visa instrument name
            public static bool connected = false;  // Used to determine if there is connection with the instrument
            public static string addrtype = "ASRL";

            public static string LastFunctionErrorMessage = string.Empty;
            public static int LastFunctionStatus = 0;

            public static int measureMinChannel = 0;
            public static int measureMaxChannel = 0;

            public static int singleChannelNumber = 0;      // used to keep track of what channel that was set in the monitor mode
        }


        public A34970ADAQInstrument(string instrumentVisaName)
        {
            Globls.instrumentName = instrumentVisaName;
        }

        //---------------------------------------
        //---------- interface functions
        //---------------------------------------

        public bool Connect()
        {
            bool status = true;
            string rdr = string.Empty;

            Globls.LastFunctionErrorMessage = string.Empty;
            Globls.LastFunctionStatus = 0;

            if (!Globls.connected)
                if (!OpenPort())
                    return false;

            // Abort a scan, if one is in progress
            Globls.errorStatus = SendCmd("ABORt");      // SendCmd sets LastErrorxxx
            if (Globls.errorStatus != 0)                // only checking first command for errors.
            {
                status = false;
            }
            else
                // Reset instrument to turn-on condition
                SendCmd("*RST");

            return status;
        }

        public void Disconnect()
        {
            visa32.viClose(Globls.vi);
            visa32.viClose(Globls.videfaultRM);
            Globls.connected = false;
        }

        public bool AddChannel(string name, int channel, ChannelType type, int range)
        {
            throw new NotImplementedException();
        }

        public void ReadListOfChannels(ref Channel[] channelNames)
        {
            throw new NotImplementedException();
        }

        public double ReadChannel(string channelName)
        {
            throw new NotImplementedException();
        }

        public void SetChannel(string channelName, double value)
        {
            throw new NotImplementedException();
        }

        //------------------------------------
        // utility functions
        //-----------------------------------

        /// <summary>
        /// Check errors for last function that was called. true = error
        /// </summary>
        /// <param name="LastFunctionErrorMessage"></param>
        /// <param name="LastFunctionStatus"></param>
        /// <returns>true = there was a error</returns>
        public bool CheckForError(ref string LastFunctionErrorMessage, ref int LastFunctionStatus)
        {
            LastFunctionErrorMessage = Globls.LastFunctionErrorMessage;
            LastFunctionStatus = Globls.LastFunctionStatus;
            return (Globls.LastFunctionStatus != 0);

        }
        /// <summary>
        /// Will return readings for the list of measurements. It will set up a scan. All the 
        /// channels must be that same type of measurement. Returns empty list if error.
        /// </summary>
        /// <remarks>
        /// Globls.LastFunctionStatus
        ///   ErrorCodes.System.InstrumentNotSetUp
        ///   ErrorCodes.System.VisaSendError
        ///   ErrorCodes.System.VisaReadError
        /// </remarks>
        /// <param name="channels"></param>
        /// <param name="values"></param>
        public void ReadMeasurements(List<int> channels, List<InstrumentID> ids, List<ChannelType> type, List<int> range, ref List<double> values)
        {
            string data = string.Empty;
            string[] dataArray;
            string cmd = string.Empty;
            string channelList = string.Empty;
            string rdr = string.Empty;

            values.Clear();                     // clear the list of values
            if (!Globls.connected)
            {
                Globls.LastFunctionErrorMessage = "34970A not connected";
                Globls.LastFunctionStatus = ErrorCodes.System.InstrumentNotConnected;
                return;
            }

            // construct the scan list
            channelList = "(@";
            for (int i = 0; i < channels.Count; i++)
            {
                {
                    channelList += channels[i] + ",";
                    if (type[i] == ChannelType.DCInput)
                        cmd = "CONFigure:VOLTage:DC";
                    if (type[i] == ChannelType.ACInput)
                        cmd = "CONFigure:VOLTage:AC"; 
                    if (range[i] != 0)      // if not in auto range
                        cmd += " " + range[i].ToString() + ","; // add range
                    cmd += "(@" + channels[i] + ")";
                    SendCmd(cmd);
                    if (Globls.LastFunctionStatus != 0)         // if there was a error sending the command
                    {
                        Globls.LastFunctionErrorMessage = "ReadMeasurements: " + Globls.LastFunctionErrorMessage;
                        return;                                 // exit the test
                    }
                }

            }
            channelList.TrimEnd(',');           // get rid of trailing , in list
            channelList += ")";                 // now have the complete channel list

            cmd = "ROUT:SCAN " + channelList;
            SendCmd(cmd);
            if (Globls.LastFunctionStatus != 0)         // if there was a error sending the command
            {
                Globls.LastFunctionErrorMessage = "ReadMeasurements: " + Globls.LastFunctionErrorMessage;
                return;                                 // exit the test
            }

            // set up trigger
            SendCmd("TRIG:SOUR IMM");
            // get the values
            SendCmd("READ?");
            data = GetData();
            if (Globls.LastFunctionStatus != 0)         // if there was a error sending the command
            {
                Globls.LastFunctionErrorMessage = "ReadMeasurements: " + Globls.LastFunctionErrorMessage;
                return;                                 // exit the test
            }
            dataArray = Regex.Split(data, "(.+?)(?:,|$)");

            for (int i = 0; i < dataArray.Count(); i++)
            {
                // format of the data is value[,value]
                // pop one off at a time and put it in the correct place in the list
                if (dataArray[i] != "")
                {
                    values.Add(double.Parse(dataArray[i]));
                }
            }

            return;
        }

        /// <summary>
        /// Read a single measurement. It will put the channel as the monitor channel.
        /// </summary>
        /// <remarks>
        ///  Globls.LastFunctionStatus
        ///   ErrorCodes.System.InstrumentNotSetUp
        ///   ErrorCodes.System.VisaSendError
        ///   ErrorCodes.System.VisaReadError
        ///   ErrorCodes.System.InstrumentReturnedBadFormat
        /// </remarks>
        /// <param name="mesurmentName"></param>
        /// <returns></returns>
        public double ReadSingleMeasurement(int channel, ChannelType type, int range)
        {
            string data = string.Empty;
            double readValue = 0;
            string cmd = string.Empty;

            if (!Globls.connected)
            {
                Globls.LastFunctionErrorMessage = "34970A not connected";
                Globls.LastFunctionStatus = InsturmentError_CouldNotConnect;
                return 0;
            }

            // see if in monitor mode already
            SendCmd("ROUT:MON:STAT?");
            if (Globls.LastFunctionStatus != 0)         // if there was a error sending the command
            {
                Globls.LastFunctionErrorMessage = "ReadSingleMeasurement: " + Globls.LastFunctionErrorMessage;
                return 0;                                 // exit the test
            }
            data = GetData();
            if (Globls.LastFunctionStatus != 0)         // if there was a error sending the command
            {
                Globls.LastFunctionErrorMessage = "ReadSingleMeasurement: " + Globls.LastFunctionErrorMessage;
                return 0;                                 // exit the test
            }

            if (data.Contains("0"))         // if not in scan mode
            {
                if (type == ChannelType.DCInput)
                    cmd = "CONFigure:VOLTage:DC";
                if (type == ChannelType.ACInput)
                    cmd = "CONFigure:VOLTage:AC";
                if (range != 0)      // if not in auto range
                    cmd += " " + range.ToString() + ","; // add range
                cmd += "(@" + channel.ToString() + ")";
                SendCmd(cmd);
                SendCmd("ROUT:MON (@" + channel + ")");
                SendCmd("ROUT:MON:STAT ON");
                Globls.singleChannelNumber = channel;
            }
            else            // in scan mode.
            {
                if (Globls.singleChannelNumber != channel)    // if not the current monitor channel
                {
                    if (type == ChannelType.DCInput)
                        cmd = "CONFigure:VOLTage:DC";
                    if (type == ChannelType.ACInput)
                        cmd = "CONFigure:VOLTage:AC";
                    if (range != 0)      // if not in auto range
                        cmd += " " + range.ToString() + ","; // add range
                    cmd += "(@" + channel.ToString() + ")";
                    SendCmd(cmd);
                    SendCmd("ROUT:MON (@" + channel.ToString() + ")");
                    SendCmd("ROUT:MON:STAT ON");
                    Globls.singleChannelNumber = channel;
                }
            }
            // get the values
            SendCmd("ROUT:MON:DATA?");
            data = GetData();
            if (!double.TryParse(data, out readValue))
            {
                Globls.LastFunctionErrorMessage = "ReadSingleMeasurement: Data returned(" + data + ") not a number on channel " + channel;
                Globls.LastFunctionStatus = ErrorCodes.System.InstrumentReturnedBadFormat;
                readValue = 0;
            }

            return readValue;
        }

        /// <summary>
        /// Will set the output to the value passed. If it is a relay, 0=open !0=close.
        /// </summary>
        /// <remarks>
        /// Globls.LastFunctionStatus
        ///   ErrorCodes.System.InvalidOutputType
        /// </remarks>
        /// <param name="measurementName"></param>
        /// <param name="setValue"></param>
        public void SetOutput(string channel, double setValue)
        {
            string cmd = string.Empty;
            Channel current = new Channel();

            current = FindChannelData(channel);
            if (current.type == ChannelType.Contact)
            {
                if (setValue == 0)
                    cmd = "ROUT:OPEN (@" + current.channel + ")";
                else
                    cmd = "ROUT:CLOSE (@" + current.channel + ")";
                SendCmd(cmd);
                if (Globls.LastFunctionStatus != 0)         // if there was a error sending the command
                {
                    Globls.LastFunctionErrorMessage = "SetOutput: " + Globls.LastFunctionErrorMessage;
                    return;                                 // exit the test
                }

            }
            else if (current.type == ChannelType.DAC)
            { }
            else
            {
                Globls.LastFunctionErrorMessage = "SetOutput: Control " + current.channel + " not a contact or DAC";
                Globls.LastFunctionStatus = ErrorCodes.System.InvalidOutputType;
            }

        }

        //------------------------------
        // utilities
        //------------------------------

        /// <summary>
        /// Will open the port. If a error is found, the port will be closed.
        /// </summary>
        /// <returns></returns>
        bool OpenPort()
        //"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        // This function opens a port (the communication between the instrument and
        // computer).
        //"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        {
            string rdg;
            // Check for exceptions			
            Globls.LastFunctionErrorMessage = string.Empty;
            Globls.LastFunctionStatus = 0;

            // If port is open, close it
            if (Globls.connected)
            { 
                Globls.errorStatus = visa32.viClose(Globls.vi);
                visa32.viClose(Globls.videfaultRM);
            }

            // Open the Visa session
            Globls.errorStatus = visa32.viOpenDefaultRM(out Globls.videfaultRM);
            if (Globls.errorStatus != visa32.VI_SUCCESS)                // if issue opening the instrument
            {
                Globls.LastFunctionErrorMessage = "Error opening default RM. visa error code: " + Globls.errorStatus ;
                Globls.LastFunctionStatus = ErrorCodes.System.VisaOpenError;
                return false;
            }
            // Open communication to the instrument
            Globls.errorStatus = visa32.viOpen(Globls.videfaultRM, Globls.instrumentName, 0, 0, out Globls.vi);

            // If an error occurs, give a message
            if (Globls.errorStatus < visa32.VI_SUCCESS)
            {
                Globls.LastFunctionErrorMessage = "Error opening instrument " + Globls.instrumentName + " with error " + Globls.errorStatus.ToString();
                Globls.LastFunctionStatus = ErrorCodes.System.VisaOpenError;
                Globls.connected = false;
                return false;
            }

            // Set timeout in milliseconds; set the timeout for your requirements
            Globls.errorStatus = visa32.viSetAttribute(Globls.vi, visa32.VI_ATTR_TMO_VALUE, 2000);

            // Set RS-232 parameters, if address is for S-232 (ASRLx)
            //if (Globls.addrtype.CompareTo("ASRL") == 0)
            //{
                Globls.addrtype = "ASRL";

                // Set the RS-232 parameters; refer to the 34970A and VISA documentation
                // to change the settings. Make sure the instrument and the following
                // settings agree.
                Globls.errorStatus = visa32.viSetAttribute(Globls.vi, visa32.VI_ATTR_ASRL_BAUD, 115200);
                Globls.errorStatus = visa32.viSetAttribute(Globls.vi, visa32.VI_ATTR_ASRL_DATA_BITS, 8);
                Globls.errorStatus = visa32.viSetAttribute(Globls.vi, visa32.VI_ATTR_ASRL_PARITY, visa32.VI_ASRL_PAR_NONE);
                Globls.errorStatus = visa32.viSetAttribute(Globls.vi, visa32.VI_ATTR_ASRL_STOP_BITS, visa32.VI_ASRL_STOP_ONE);
                Globls.errorStatus = visa32.viSetAttribute(Globls.vi, visa32.VI_ATTR_ASRL_FLOW_CNTRL, visa32.VI_ASRL_FLOW_XON_XOFF);

                // Set the instrument to remote
            //}
            SendCmd("SYSTem:REMote");
            if (Globls.LastFunctionStatus != 0)         // if there was a error sending the command
            {
                visa32.viClose(Globls.vi);                  // close the port because there was failure.
                visa32.viClose(Globls.videfaultRM);
                Globls.connected = false;
                Globls.LastFunctionErrorMessage = "OpenPort: " + Globls.LastFunctionErrorMessage;
                return false;                                 // exit the test
            }

            // Check and make sure the correct instrument is addressed
            SendCmd("*IDN?");
            rdg = GetData();
            if (rdg.IndexOf("34970A") < 0)
            {
                Globls.LastFunctionErrorMessage = "Error opening instrument. It is not 34970A";
                Globls.LastFunctionStatus = ErrorCodes.System.NotCorrectInstrument;
                visa32.viClose(Globls.vi);
                visa32.viClose(Globls.videfaultRM);
                Globls.connected = false;
                return false;
            }

            // Check and make sure the 34901A Module is installed in slot 100;
            // Exit program if not correct
            SendCmd("SYSTem:CTYPe? 100");
            rdg = GetData();
            if (rdg.IndexOf("34901A") < 0)
            {
                Globls.LastFunctionErrorMessage = "Error opening instrument. It is not 34901A not installed in slot 1";
                Globls.LastFunctionStatus = ErrorCodes.System.NotCorrectCard;
                visa32.viClose(Globls.vi);
                visa32.viClose(Globls.videfaultRM);
                Globls.connected = false;
                return false;
            }

            // Check if the DMM is installed; convert returned ASCII string to number.
            // Exit program if not installed
            SendCmd("INSTrument:DMM:INSTalled?");
            if (Convert.ToInt16(GetData()) == 0)
            {
                Globls.LastFunctionErrorMessage = "Error opening instrument. DMM not installed; unable to make measurements.";
                Globls.LastFunctionStatus = ErrorCodes.System.MissingInstrumentOption;
                visa32.viClose(Globls.vi);
                visa32.viClose(Globls.videfaultRM);
                Globls.connected = false;
                return false;
            }

            // Check if the DMM is enabled;; convert returned ASCII string to number.
            // Enable the DMM, if not enabled
            SendCmd("INSTrument:DMM?");
            if (Convert.ToInt16(GetData()) == 0)
                SendCmd("INSTrument:DMM ON");

                Globls.connected = true;
                return true;
        }

        void Close()
        {
            visa32.viClose(Globls.vi);
            visa32.viClose(Globls.videfaultRM);
            Globls.connected = false;
        }

        /// <summary>
        /// Send the command. 
        /// </summary>
        /// <param name="SCPICmd"></param>
        /// <returns>vist</returns>
        int SendCmd(string SCPICmd)
        //"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        // This routine will send a SCPI command string to the instrument. If the
        // command contains a question mark (i.e., is a query command), you must
        // read the response with the 'GetData' function.
        //"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        {
            Globls.LastFunctionErrorMessage = string.Empty;
            Globls.LastFunctionStatus = 0;
            StringBuilder msg = new  StringBuilder();

            // Write the command to the instrument (terminated by a linefeed; vbLf is ASCII character 10)
            Globls.errorStatus = visa32.viPrintf(Globls.vi, SCPICmd + "\n");

            if (Globls.errorStatus < visa32.VI_SUCCESS)
            {
                visa32.viStatusDesc(Globls.vi, Globls.errorStatus, msg);
                Globls.LastFunctionErrorMessage = "Instrument error sending command: " + SCPICmd + " VISA error: " + msg.ToString();
                Globls.LastFunctionStatus = ErrorCodes.System.VisaSendError;
            }
            return Globls.errorStatus;
        }

        /// <summary>
        /// Read string from instrument
        /// </summary>
        /// <returns>
        /// Global.LastFunctionStatus
        ///  -15  ErrorCodes.System.FixtureError
        /// </returns>
        string GetData()
        //"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        // This function reads the string returned by the instrument
        //"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        {
            StringBuilder msg = new StringBuilder(2048);

            // Return the reading
            Globls.errorStatus = visa32.viScanf(Globls.vi, "%2048t", msg);

            if (Globls.errorStatus < visa32.VI_SUCCESS)
            {
                visa32.viStatusDesc(Globls.vi, Globls.errorStatus, msg);
                Globls.LastFunctionErrorMessage = "Instrument error getting data. " + msg.ToString();
                Globls.LastFunctionStatus = ErrorCodes.System.VisaReadError;
                return null;
            }
            // Remove carriage return or line feed and return data
//                if (Globls.addrtype == "ASRL")
                return (msg.ToString()).Remove((msg.ToString().Length - 2), 2);
//               else
//                    return (msg.ToString()).Remove((msg.ToString().Length - 1), 1);
        }

        private bool DidErrorOccured(string msg)
        //"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        // Checks for syntax and other errors.
        //"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        {
            int err_num;
            bool exit_code = false;
            string err_msg, show_msg;

            Globls.LastFunctionErrorMessage = string.Empty;
            Globls.LastFunctionStatus = 0;

            // Read error queue
            SendCmd("SYSTem:ERRor?");

            err_msg = GetData();

            // Get error number and message
            err_num = Convert.ToInt16(err_msg.Substring(0, err_msg.IndexOf(",")));
            err_msg = err_msg.Substring((err_msg.IndexOf(",") + 1), (err_msg.Length - (Convert.ToInt16(err_msg.IndexOf(","))) - 1));

            // If error found, check for more errors and exit program
            while (err_num != 0)
            {
                exit_code = true;

                show_msg = "Error in: \"" + msg + "\" function\n";
                show_msg += "Error Number: " + err_num.ToString() + "," + err_msg;

                // Read error queue
                SendCmd("SYSTem:ERRor?");
                err_msg = GetData();

                // Get error number and message
                err_num = Convert.ToInt16(err_msg.Substring(0, err_msg.IndexOf(",")));
                err_msg = err_msg.Substring((err_msg.IndexOf(",") + 1), (err_msg.Length - (Convert.ToInt16(err_msg.IndexOf(","))) - 1));
            }

            if (exit_code)
            {
                Globls.LastFunctionErrorMessage = err_msg;
                Globls.LastFunctionStatus = ErrorCodes.System.InstrumentErrorMsg;

                SendCmd("*CLS");

                // Close instrument session
                Globls.errorStatus = visa32.viClose(Globls.vi);

                // Close the session
                Globls.errorStatus = visa32.viClose(Globls.videfaultRM);

                // end the program
                return true;
            }

            return false;
        }

    }

}
