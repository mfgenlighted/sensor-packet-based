﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Diagnostics;

namespace SensorManufSY2
{

    //public class SensorConsole
    public class TextBasedConsole : IDisposable
    {
        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        // 
        // From the initializing routine, add
        //     sensor.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(xxxx);
        //  where xxxx is the routine to update the windows.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs> StatusUpdated;

        protected virtual void OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }

        //----------------------------------
        //  events for the BaseStream version of the serial handling
        //---------------------------------
        public delegate void RecieveDataHandler(byte[] data);
        public event RecieveDataHandler RecieveData;

        public delegate void ClosedHandler();
        public event ClosedHandler Closed;

        public delegate void SerialPortErrorHandler(IOException exception);
        public event SerialPortErrorHandler SerialPortError;


        //-------------------------
        //  properties
        //-------------------------
        public bool DisplaySendChar { get { return displaySendChar; } set { displaySendChar = value; } }
        public bool DisplayRcvChar { get { return displayRcvChar; } set { displayRcvChar = value; } }
        public string DVTMANPrompt = string.Empty;
        public string AppPrompt = string.Empty;
        public string LastErrorMessage = string.Empty;
        public string SerialBuffer { get { return serialBuffer; } }

        private bool disposed = true;
        //----------------------------------------
        // serial port values
        //----------------------------------------
        private SerialPort serialPort = null;            // dut serial port
        private string comPort = string.Empty;
        private string serialBuffer;                        // buffer of incoming serial data
        private string readBuffer = string.Empty;        // buffer read on one event
        private string dumpFileName = string.Empty;
        public bool UseOldMethod = true;                               // true=use serialDataReceived event handler
                                                                        // false=BaseStream method(default)

        private bool foundLF = true;                    // this gets set to true after a incoming LF is found.
        private byte[] buffer = new byte[4096];          // data buffer for BaseStream version
        private bool stopping = false;                  // BaseStream version, set to true to initiate a close

        private bool displaySendChar = false;
        private bool displayRcvChar = false;


        private object fileLock = new object();

        public void Dispose()
        {
            if (serialPort != null)
                serialPort.Dispose();
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (serialPort != null)
                        if (serialPort.IsOpen)
                            serialPort.Close();
                    serialPort.Dispose();
                    Close();
            disposed = true;
                }
            }
        }

        //------------------------------------------------------------
        // Open(string comPort)
        /// <summary>
        /// Open the DUT serial port. Exceptions are throw if errors.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="comPort"></param>
        public void Open(string comPort)
        {
            Trace.WriteLine("Opening sensor port");
            if (serialPort != null)                        // if already opne
            {
                if (serialPort.IsOpen)
                    serialPort.Close();                         // close it
            }


            if (CheckForSerialPort(comPort))
            {
                // port valid on this system, so try to open
                RecieveDataHandler rcvData = new RecieveDataHandler(processData);
                try
                {
                    serialPort = new SerialPort(comPort, 115200);
                    serialPort.ReadTimeout = 500;
                    serialPort.WriteTimeout = 500;
                    serialPort.DtrEnable = false;
                    serialPort.Handshake = Handshake.None;
                    serialPort.Parity = Parity.None;
                    serialPort.Parity = Parity.None;
                    if (UseOldMethod)
                        serialPort.DataReceived += new SerialDataReceivedEventHandler(serialDataReceived);
                    serialPort.Open();           // open the serial port
                    this.comPort = comPort;

                    if (!UseOldMethod)
                        serialPort.BaseStream.BeginRead(buffer, 0, buffer.Length, EndRead, null);
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Error opening {0}", comPort), ex);
                }
            }
            else
                throw new Exception("Dut serial port " + comPort + " is not a valid com port on this computer");
        }

        //--------------------------------------
        // Close()
        /// <summary>
        /// Close the DUT serial port
        /// </summary>
        public void Close()
        {
            //if (this.disposed)
            //{
            //    throw new ObjectDisposedException(this.GetType().Name, "Cannot use a disposed object.");
            //} 
            Trace.TraceInformation("Closing sensor port");
            if (UseOldMethod)
            {
                try
                {
                    if (serialPort != null)
                    {
                        Thread.Sleep(500);          // just in case something else is in the process of closing it
                        if (serialPort.IsOpen)
                            serialPort.Close();
                    }
                }
                catch (Exception ex)
                {
                    UpdateStatusWindow("Error while closing sensor console port. IsOpen = " + serialPort.IsOpen + "\n" + ex.Message + "\n", StatusType.failed);
                }
            }
            else
            {
                stopping = true;
                Thread.Sleep(100);
                if (serialPort.IsOpen)
                    serialPort.Close();
                Closed?.Invoke();
                serialPort.Dispose();
            }
        }


        public void SetDumpFileName(string directory, string baseTitle)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            dumpFileName = directory + @"\dut" + baseTitle;
        }

        public void EndDump()
        {
            dumpFileName = string.Empty;
        }

        //------------------------
        // ClearBuffers()
        /// <summary>
        /// Clear the serial buffers, both serial driver and serial in buffer.
        /// </summary>
        /// <remarks>
        ///      SerialPort.DiscardInBuffers construct exceptions
        ///      SerialPort.DiscardOutBuffers construct exceptions
        /// </remarks>
        public void ClearBuffers()
        {
            try
            {
                if ((serialPort != null) & (serialPort.IsOpen))
                {
                    serialPort.DiscardInBuffer();
                    serialPort.DiscardOutBuffer();
                    serialBuffer = string.Empty;
                    Thread.Sleep(500);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error clearing serial buffers", ex);
            }
        }

        //-------------------------------
        // WriteLine
        ///<summary>
        ///  Send a string to DUT
        ///  Will send the string to the dut. CR will be added to the end of the string.
        ///  After sending the string, it will wait for the echo.
        /// </summary> 
        /// <param name="cmdString">string cmdString - string to send. CR will be sent at end.</param>
        /// <param name="timeout">int - number of ms to wait for the echo</param>
        /// <returns>bool - true = command was sent and echo received</returns>
        public bool WriteLine(string cmdString, int timeout)
        {
            string rtnstring = string.Empty;
            bool status = true;
            bool echobad = false;
            int timesSent = 0;
            int x;
            char[] cmdBuf;
            string msg;

            cmdBuf = cmdString.ToCharArray();

            do
            {
                echobad = false;
                try
                {
                    msg = DateTime.Now.ToString("HH:mm:ss.fff") + "->" + cmdString + "\n";
                    if (displaySendChar)         // if display window is enabled
                        UpdateStatusWindow(timesSent.ToString() + ": " + msg, StatusType.serialSend);
                    if (dumpFileName != string.Empty)
                    {
                        lock (fileLock)
                        {
                            using (System.IO.StreamWriter file = new System.IO.StreamWriter(dumpFileName, true))
                            {
                                file.Write("\n" + msg);
                            }
                        }
                    }
                    for (x = 0; x < cmdString.Length; x++)
                    {
                        serialPort.Write(cmdBuf, x, 1);
                        
                        Thread.Sleep(5);
                    }
                    //serialPort.WriteLine(cmdString);
                    serialPort.Write("\n");
                    Thread.Sleep(100);          // give it time to echo
                }
                catch (Exception ex)
                {
                    LastErrorMessage = "WriteLine: Exception sending '" + cmdBuf + "'. Message: " + ex.Message;
                    Trace.TraceInformation(LastErrorMessage);
                    return false;
                }

                status = ReadLine(ref rtnstring, timeout);   // get the echo
                if (!rtnstring.Contains(cmdString))             // if the echo is bad
                {
                    Trace.WriteLine("echo bad. Expect:'" + cmdString + "' Got:'" + rtnstring + "'");
                    Thread.Sleep(500);
                    status = ReadLine(ref rtnstring, timeout);   // get the echo
                    if (!rtnstring.Contains(cmdString))             // if the echo is bad
                    {
                        Trace.WriteLine("Reread echo bad. Got:'" + rtnstring + "'");
                        echobad = true;
                        timesSent++;
                        //DutWaitForPrompt(2000, uutPrompt, out rtnstring);       // eat the prompt
                        //Thread.Sleep(500);
                    }
                }

            } while (echobad && (timesSent <= 1));

            return status;
        }

        //--------------------------------------------
        // Method: ReadLine
        /// <summary>
        ///  Reads the serial input buffer till a newline character is found.
        ///  If the newline character is not found, false is returned.
        /// </summary>
        ///
        ///<param name="rcvCount">int - Number of characters to look for</param>
        ///<param name="returnedLine">ref string - characters found. Will be a empty string
        ///if at least rcvCount characters are not found</param>
        ///<param name="timeoutms">int - Number of ms to wait for the characters</param>
        ///<returns>bool - true = at least rcvCount characters are found and returned in returnedLine
        ///                 false = not enough characters were found in time. Empty string returned.</returns>
        public bool ReadLine(ref string returnedLine, int timeoutms)
        {
            DateTime endTime;
            bool status = true;

            endTime = DateTime.Now.AddMilliseconds(timeoutms);
            while (!serialBuffer.Contains('\n') && (endTime > DateTime.Now))
            {
                Thread.Sleep(100);
            }

            if (serialBuffer.Contains('\n'))     // if line feed found
            {
                int nlIndex = serialBuffer.IndexOf('\n');
                returnedLine = serialBuffer.Substring(0, nlIndex);
                serialBuffer = serialBuffer.Substring(nlIndex + 1, serialBuffer.Length - nlIndex - 1);
            }
            else
            {
                returnedLine = string.Empty;
                status = false;
            }
            return status;
        }

        //public void WriteChars()
        //{
        //    throw new System.NotImplementedException();
        //}


        //public void ReadChars()
        //{
        //    throw new System.NotImplementedException();
        //}

        public bool WaitForDVTMANPrompt(int timeoutms, out string outputData)
        {
            outputData = string.Empty;

            if (DVTMANPrompt == string.Empty)
                return false;
            return WaitForPrompt(timeoutms, DVTMANPrompt, out outputData);     
        }

        /// <summary>
        /// Will wait for the app program prompt. The property AppPrompt must be set
        /// before using this method.
        /// If no prompt is found, or
        /// no serial data read, a empty string will be returned.
        /// </summary>
        /// <param name="timeoutms">timeout to wait for prompt in ms</param>
        /// <param name="outputData">Serial buffer from last read up till and including
        ///  prompt string. Will return empty string if prompt not found or no data
        ///  in the serial buffer</param>
        /// <returns></returns>
        public bool WaitForAppPrompt(int timeoutms, out string outputData)
        {
            outputData = string.Empty;

            if (AppPrompt == string.Empty)
            {
                UpdateStatusWindow("AppPrompt has not been set. Set the property AppPrompt.", StatusType.failed);
                return false;
            }
            return WaitForPrompt(timeoutms, AppPrompt, out outputData);
        }

        //-------------------------------------------
        // WaitForPrompt
        /// <summary>
        /// Read until a prompt is found from the DUT. If no prompt is found, or
        /// no serial data read, a empty string will be returned.
        /// </summary>
        /// 
        /// <param name="outputData">Serial buffer from last read up till and including
        ///  prompt string. Will return empty string if prompt not found or no data
        ///  in the serial buffer</param>
        ///  <param name="prompt">String to look for in the serial data</param>
        ///  <param name="timeoutms">Timeout in milliseconds to wait for the prompt string</param>
        ///  
        /// <returns>bool - true = prompt found
        ///                 false = timed out looking for prompt
        ///</returns>
        public bool WaitForPrompt(int timeoutms, string prompt, out string outputData)
        {
            DateTime endTime;

            endTime = DateTime.Now.AddMilliseconds(timeoutms);
            while (!serialBuffer.Contains(prompt) && (endTime > DateTime.Now))
            {
                Thread.Sleep(100);
            }

            if (serialBuffer.Contains(prompt))     // if prompt is found
            {
                int nlIndex = serialBuffer.IndexOf(prompt);                          // find where prompt is
                outputData = serialBuffer.Substring(0, nlIndex + prompt.Length);     // copy all data up to and including prompt
                if ((nlIndex + prompt.Length) == serialBuffer.Length)                // if prompt last thing in buffer
                    serialBuffer = string.Empty;                                     // empty the buffer
                else                                                                    // else move any data after prompt to buffer
                    serialBuffer = serialBuffer.Substring(nlIndex + prompt.Length,
                            serialBuffer.Length - nlIndex - prompt.Length);
                return true;
            }
            else                                    // prompt not found in time
            {
            outputData = string.Empty;       // so return what was read, but leave the buffer intact
            return false;
            }
        }

        public string GetCopyOfSerialBuffer()
        {
            return serialBuffer;
        }

        //---------------------
        //  utilities
        //-------------------
        public bool CheckForSerialPort(string comPort)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            // see if this port is in the list of ports on the computer
            foreach (string i in ports)
            {
                if (i.Equals(comPort))         // if found a match
                    return true;
            }
            return false;
        }

        

        //-------------------------------------
        // Method: DutSerialDataReceived
        //  StatusUpdated handler for the dut serial port
        //
        // If foundLF=true, means that the last character found was a line feed. So
        // if a non LF is found, it is a start of a new line, so add the time
        // to the dump file, if dumping.
        void serialDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            string smsg = string.Empty;         // corrected string removing '\0'
            string msg = string.Empty;          // raw string read
            string fmsg = string.Empty;         // string to write to file
            string rewritemsg = string.Empty;


            while (sp.BytesToRead != 0)
            {
                serialBuffer += (byte)sp.ReadByte();
            }


            msg = sp.ReadExisting();
            foreach (var item in msg)
            {
                if (foundLF)
                {
                    fmsg = fmsg + DateTime.Now.ToString("HH:mm:ss.fff") + "<-";
                    foundLF = false;                // wait for next lf
                }
                if (item != '\0')
                {
                    fmsg = fmsg + item;
                    smsg = smsg + item;
                    if (item == '\n')
                        foundLF = true;
                }
            }
            serialBuffer += smsg;
            if (displayRcvChar)         // if display window is enabled
            {
                UpdateStatusWindow(fmsg, StatusType.serialRcv);
            }
            if (dumpFileName != string.Empty)
            {
                lock (fileLock)
                {
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(dumpFileName, true))
                    {
                        file.Write(fmsg);
                    }
                }
            }
        }


        //----------------------------------------------
        // routines that suport the BaseStream method
        //---------------------------------------------
        void processData (byte[] data)
        {
            string smsg = string.Empty;         // corrected string removing '\0'
            string msg = string.Empty;          // raw string read
            string fmsg = string.Empty;         // string to write to file

            foreach (var item in data)
            {
                if (foundLF)
                {
                    fmsg = fmsg + DateTime.Now.ToString("HH:mm:ss.fff") + "<-";
                    foundLF = false;                // wait for next lf
                }
                if (item != '\0')
                {
                    fmsg = fmsg + item;
                    smsg = smsg + item;
                    if (item == '\n')
                        foundLF = true;
                }
            }
            serialBuffer += smsg;
            if (displayRcvChar)         // if display window is enabled
            {
                UpdateStatusWindow(fmsg, StatusType.serialRcv);
            }
            if (dumpFileName != string.Empty)
            {
                lock (fileLock)
                {
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(dumpFileName, true))
                    {
                        file.Write(fmsg);
                    }
                }
            }

        }

        void EndRead(IAsyncResult result)
        {
            string fmsg = string.Empty;
            string smsg = string.Empty;
            int count = 0;
            try
            {
                if (serialPort.IsOpen)
                    count = serialPort.BaseStream.EndRead(result);

                if (!stopping)
                {
                    byte[] data = new byte[count];
                    Buffer.BlockCopy(buffer, 0, data, 0, count);

                    RecieveData?.Invoke(data);
                    //foreach (var item in data)
                    //{
                    //    if (foundLF)
                    //    {
                    //        fmsg = fmsg + DateTime.Now.ToString("HH:mm:ss.fff") + "<-";
                    //        foundLF = false;                // wait for next lf
                    //    }
                    //    if (item != '\0')
                    //    {
                    //        fmsg = fmsg + (char)item;
                    //        smsg = smsg + (char)item;
                    //        if (item == '\n')
                    //            foundLF = true;
                    //    }
                    //}
                    ////            serialBuffer += smsg;
                    //updateSerialBuffer(bufferFunction.AddChars, ref smsg);
                    //if (displayRcvChar)         // if display window is enabled
                    //{
                    //    UpdateStatusWindow(fmsg, StatusType.serialRcv);
                    //}
                    //if (dumpFileName != string.Empty)
                    //{
                    //    lock (fileLock)
                    //    {
                    //        using (System.IO.StreamWriter file = new System.IO.StreamWriter(dumpFileName, true))
                    //        {
                    //            file.Write(fmsg);
                    //        }
                    //    }
                    //}

                }
            }
            catch (IOException exception)
            {
                SerialPortError?.Invoke(exception);
                Debug.WriteLine(exception.ToString());
            }

            // Init another async read or close port as appropriate
            if (!stopping)
            {
                serialPort.BaseStream.BeginRead(buffer, 0, buffer.Length, EndRead, null);
            }
            else
            {
                serialPort.Close();
                Closed?.Invoke();
            }
        }

    }
}
