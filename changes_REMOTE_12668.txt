﻿5.0.0
	packet based tests.
5.0.1
	Clear NV test was not adding test number to error code.
	added display message to serial handler to display if buffer overrun.
	added the packlist count to the serial handler screen dump.
	rewrote control output test.
	When PFS is rejected, but the result database has not been opened, do not try to
	 record PFS asked to record failure.
	Fixed problem with not reading the FixtureTypeDefs.xml file.
5.0.2
	fixed the clear NV command.
	Changed config file location to \programdata\enlighted\sensorV5.
	got test radio auto find working.
	many other improvements and bug fixes.
5.0.3
	increased debug screen refresh rate StartUpdateScreen from 100 to 500.
	removed printer from su-5s functional definition.
	added some tracing to debug the debug screen.
5.0.4
	fixed the debug screen issue.
5.0.5
	fixed missing printer in final
	fixed gain in fixture init from 2 to 3
	cleaned up fixture config screen
5.1.0
	added temp cal test.
	added the off delay to change over.
5.1.1
	Added option to program at power on.
5.1.3
	changeover off delay was after banner print instead of before.
	adjusted debug window led color ranges.
	made the firmware programming window work with fixture button.
5.1.4
	BLE tests status were not reporting right.
5.1.6
	added a 500ms delay after the clear NV command(ClearManData()) to see if it fixes
	 the occational 'port closed' issue when the reset function is called after it.
	TurnOnDut was not recording any results.
	blue tooth test fixes.
5.1.7
	added cr to bluetooth test messages.
	changed order of turn on so voltage is checked before programming.
	added database record of programming part.
	improved DUT turn on screen status messages.
	Changeover timeouts were not being * 1000.
5.1.8
	Added PFS ask to the ReadPCBASN function.
5.1.9
	ReadPCBASN did not return a fail status when PFS failed.
	added buffer clear before ReadPCBASN power on.
5.1.11
	Made the ReadPCBASN try a second time if it did not read the sn
	If PFS is enabled, but there is no pcba sn, it will not log
5.1.12
	changed the way sensor data math was done.
	record raw temperature reading of temp cal
5.1.13
	fixed error in TurnOnDut which was not reporting failures correctly.
	TestNetworks was not loging the pass/fail data.
	Changed the station config file location to SensorManufSY2_V5
<<<<<<< HEAD
=======
5.1.14
	Fake PFS was turned on. turned it off.
	
>>>>>>> 56955ce82fa52ed4e262f8feece9f7a1b54a8baa
5.1.15
	Improved Tunr on DUT routines
5.2.0
	Improved ReadPCBASN routine
	Test results now stored in production_results.
5.2.1
	fixed version tag.
	added ref radio console dump file
	