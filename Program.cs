﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Resources;
using System.Threading;
using System.Globalization;
using System.Configuration;

namespace SensorManufSY2
{

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)

        {
            string lang = Properties.Settings.Default.Language;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //if (Properties.Settings.Default.Language == "CHINESE")
            //{
            //    lang = "zh-Hans";
            //}
            //if (Properties.Settings.Default.Language == "ENGLISH")
            //{
            //    lang = "en";
            //}

            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.CreateSpecificCulture(lang);
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.CreateSpecificCulture(lang);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(lang);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(lang);

            Application.ThreadException += new ThreadExceptionEventHandler(MainWindow.MyExceptionHandler);

            Application.Run(new MainWindow());
        }

    }
}
