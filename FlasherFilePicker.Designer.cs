﻿namespace SensorManufSY2
{
    partial class FlasherFilePicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbPickAFile = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bDone = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbPickAFile
            // 
            this.cbPickAFile.FormattingEnabled = true;
            this.cbPickAFile.Location = new System.Drawing.Point(51, 56);
            this.cbPickAFile.Name = "cbPickAFile";
            this.cbPickAFile.Size = new System.Drawing.Size(121, 21);
            this.cbPickAFile.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pick a file";
            // 
            // bDone
            // 
            this.bDone.Location = new System.Drawing.Point(60, 105);
            this.bDone.Name = "bDone";
            this.bDone.Size = new System.Drawing.Size(75, 23);
            this.bDone.TabIndex = 2;
            this.bDone.Text = "Done";
            this.bDone.UseVisualStyleBackColor = true;
            this.bDone.Click += new System.EventHandler(this.bDone_Click);
            // 
            // FlasherFilePicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(290, 169);
            this.Controls.Add(this.bDone);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbPickAFile);
            this.Name = "FlasherFilePicker";
            this.Text = "FlasherFilePicker";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbPickAFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bDone;
    }
}