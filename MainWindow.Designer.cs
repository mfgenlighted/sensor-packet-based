﻿namespace SensorManufSY2
{
    partial class MainWindow
    {
        ///// <summary>
        ///// Required designer variable.
        ///// </summary>
        //private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            //if (disposing && (components != null))
            //{
            //    components.Dispose();
            //}
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.textLimitFileName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textLimitFileVersion = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textProgramVersion = new System.Windows.Forms.Label();
            this.buttonAbortTest = new System.Windows.Forms.Button();
            this.buttonStartTest = new System.Windows.Forms.Button();
            this.testStatusBox = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.configureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productToTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productSelection = new System.Windows.Forms.ToolStripComboBox();
            this.configureStationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configStationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configureFixtureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toggleSerialDisplayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pIRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fixtureControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utilitiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reprintLabelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearManufactureDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearResultWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearStatusWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetStationConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.turnOnDUTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testResults = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textDatabaseServer = new System.Windows.Forms.Label();
            this.textResultDatabase = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textTestChannel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lUnitsRun = new System.Windows.Forms.Label();
            this.lUnitsPassed = new System.Windows.Forms.Label();
            this.lUnitsFailed = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbStation = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lbStationFunction = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblComputerName = new System.Windows.Forms.Label();
            this.lDebugMode = new System.Windows.Forms.Label();
            this.lbDebugOptions = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textLimitFileName
            // 
            resources.ApplyResources(this.textLimitFileName, "textLimitFileName");
            this.textLimitFileName.Name = "textLimitFileName";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // textLimitFileVersion
            // 
            resources.ApplyResources(this.textLimitFileVersion, "textLimitFileVersion");
            this.textLimitFileVersion.Name = "textLimitFileVersion";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // textProgramVersion
            // 
            resources.ApplyResources(this.textProgramVersion, "textProgramVersion");
            this.textProgramVersion.Name = "textProgramVersion";
            // 
            // buttonAbortTest
            // 
            this.buttonAbortTest.BackColor = System.Drawing.Color.Red;
            resources.ApplyResources(this.buttonAbortTest, "buttonAbortTest");
            this.buttonAbortTest.Name = "buttonAbortTest";
            this.buttonAbortTest.UseVisualStyleBackColor = false;
            this.buttonAbortTest.Click += new System.EventHandler(this.AbortTestButton_Click);
            // 
            // buttonStartTest
            // 
            this.buttonStartTest.BackColor = System.Drawing.Color.Lime;
            resources.ApplyResources(this.buttonStartTest, "buttonStartTest");
            this.buttonStartTest.Name = "buttonStartTest";
            this.buttonStartTest.UseVisualStyleBackColor = false;
            this.buttonStartTest.Click += new System.EventHandler(this.StartTestButton_Click);
            // 
            // testStatusBox
            // 
            resources.ApplyResources(this.testStatusBox, "testStatusBox");
            this.testStatusBox.Name = "testStatusBox";
            this.testStatusBox.ReadOnly = true;
            this.testStatusBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.testStatusBox_KeyDown);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configureToolStripMenuItem,
            this.configureStationToolStripMenuItem,
            this.toggleSerialDisplayToolStripMenuItem,
            this.debugToolStripMenuItem,
            this.utilitiesToolStripMenuItem});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            // 
            // configureToolStripMenuItem
            // 
            this.configureToolStripMenuItem.AutoToolTip = true;
            this.configureToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.productToTestToolStripMenuItem});
            this.configureToolStripMenuItem.Name = "configureToolStripMenuItem";
            resources.ApplyResources(this.configureToolStripMenuItem, "configureToolStripMenuItem");
            // 
            // productToTestToolStripMenuItem
            // 
            this.productToTestToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.productSelection});
            this.productToTestToolStripMenuItem.Name = "productToTestToolStripMenuItem";
            resources.ApplyResources(this.productToTestToolStripMenuItem, "productToTestToolStripMenuItem");
            // 
            // productSelection
            // 
            this.productSelection.Name = "productSelection";
            resources.ApplyResources(this.productSelection, "productSelection");
            this.productSelection.SelectedIndexChanged += new System.EventHandler(this.productSelection_SelectedIndexChanged);
            // 
            // configureStationToolStripMenuItem
            // 
            this.configureStationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configStationToolStripMenuItem,
            this.configureFixtureToolStripMenuItem});
            this.configureStationToolStripMenuItem.Name = "configureStationToolStripMenuItem";
            resources.ApplyResources(this.configureStationToolStripMenuItem, "configureStationToolStripMenuItem");
            // 
            // configStationToolStripMenuItem
            // 
            this.configStationToolStripMenuItem.Name = "configStationToolStripMenuItem";
            resources.ApplyResources(this.configStationToolStripMenuItem, "configStationToolStripMenuItem");
            this.configStationToolStripMenuItem.Click += new System.EventHandler(this.configureStationToolStripMenuItem_Click);
            // 
            // configureFixtureToolStripMenuItem
            // 
            this.configureFixtureToolStripMenuItem.Name = "configureFixtureToolStripMenuItem";
            resources.ApplyResources(this.configureFixtureToolStripMenuItem, "configureFixtureToolStripMenuItem");
            this.configureFixtureToolStripMenuItem.Click += new System.EventHandler(this.configureFixtureToolStripMenuItem_Click);
            // 
            // toggleSerialDisplayToolStripMenuItem
            // 
            this.toggleSerialDisplayToolStripMenuItem.Name = "toggleSerialDisplayToolStripMenuItem";
            resources.ApplyResources(this.toggleSerialDisplayToolStripMenuItem, "toggleSerialDisplayToolStripMenuItem");
            this.toggleSerialDisplayToolStripMenuItem.Click += new System.EventHandler(this.toggleDUTDisplayToolStripMenuItem_Click);
            // 
            // debugToolStripMenuItem
            // 
            this.debugToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pIRToolStripMenuItem,
            this.fixtureControlToolStripMenuItem});
            this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
            resources.ApplyResources(this.debugToolStripMenuItem, "debugToolStripMenuItem");
            // 
            // pIRToolStripMenuItem
            // 
            this.pIRToolStripMenuItem.Name = "pIRToolStripMenuItem";
            resources.ApplyResources(this.pIRToolStripMenuItem, "pIRToolStripMenuItem");
            this.pIRToolStripMenuItem.Click += new System.EventHandler(this.pIRToolStripMenuItem_Click);
            // 
            // fixtureControlToolStripMenuItem
            // 
            this.fixtureControlToolStripMenuItem.Name = "fixtureControlToolStripMenuItem";
            resources.ApplyResources(this.fixtureControlToolStripMenuItem, "fixtureControlToolStripMenuItem");
            this.fixtureControlToolStripMenuItem.Click += new System.EventHandler(this.fixtureControlToolStripMenuItem_Click);
            // 
            // utilitiesToolStripMenuItem
            // 
            this.utilitiesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reprintLabelToolStripMenuItem,
            this.clearManufactureDataToolStripMenuItem,
            this.clearResultWindowToolStripMenuItem,
            this.clearStatusWindowToolStripMenuItem,
            this.resetStationConfigToolStripMenuItem,
            this.turnOnDUTToolStripMenuItem});
            this.utilitiesToolStripMenuItem.Name = "utilitiesToolStripMenuItem";
            resources.ApplyResources(this.utilitiesToolStripMenuItem, "utilitiesToolStripMenuItem");
            // 
            // reprintLabelToolStripMenuItem
            // 
            this.reprintLabelToolStripMenuItem.Name = "reprintLabelToolStripMenuItem";
            resources.ApplyResources(this.reprintLabelToolStripMenuItem, "reprintLabelToolStripMenuItem");
            this.reprintLabelToolStripMenuItem.Click += new System.EventHandler(this.ReprintLabel);
            // 
            // clearManufactureDataToolStripMenuItem
            // 
            this.clearManufactureDataToolStripMenuItem.Name = "clearManufactureDataToolStripMenuItem";
            resources.ApplyResources(this.clearManufactureDataToolStripMenuItem, "clearManufactureDataToolStripMenuItem");
            this.clearManufactureDataToolStripMenuItem.Click += new System.EventHandler(this.ClearManufacutureData);
            // 
            // clearResultWindowToolStripMenuItem
            // 
            this.clearResultWindowToolStripMenuItem.Name = "clearResultWindowToolStripMenuItem";
            resources.ApplyResources(this.clearResultWindowToolStripMenuItem, "clearResultWindowToolStripMenuItem");
            this.clearResultWindowToolStripMenuItem.Click += new System.EventHandler(this.clearResultWindowToolStripMenuItem_Click);
            // 
            // clearStatusWindowToolStripMenuItem
            // 
            this.clearStatusWindowToolStripMenuItem.Name = "clearStatusWindowToolStripMenuItem";
            resources.ApplyResources(this.clearStatusWindowToolStripMenuItem, "clearStatusWindowToolStripMenuItem");
            this.clearStatusWindowToolStripMenuItem.Click += new System.EventHandler(this.clearStatusWindowToolStripMenuItem_Click);
            // 
            // resetStationConfigToolStripMenuItem
            // 
            this.resetStationConfigToolStripMenuItem.Name = "resetStationConfigToolStripMenuItem";
            resources.ApplyResources(this.resetStationConfigToolStripMenuItem, "resetStationConfigToolStripMenuItem");
            this.resetStationConfigToolStripMenuItem.Click += new System.EventHandler(this.resetStationConfigToolStripMenuItem_Click);
            // 
            // turnOnDUTToolStripMenuItem
            // 
            this.turnOnDUTToolStripMenuItem.Name = "turnOnDUTToolStripMenuItem";
            resources.ApplyResources(this.turnOnDUTToolStripMenuItem, "turnOnDUTToolStripMenuItem");
            this.turnOnDUTToolStripMenuItem.Click += new System.EventHandler(this.TurnOnDUT);
            // 
            // testResults
            // 
            resources.ApplyResources(this.testResults, "testResults");
            this.testResults.Name = "testResults";
            this.testResults.ReadOnly = true;
            this.testResults.KeyDown += new System.Windows.Forms.KeyEventHandler(this.testResults_KeyDown);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // textDatabaseServer
            // 
            resources.ApplyResources(this.textDatabaseServer, "textDatabaseServer");
            this.textDatabaseServer.Name = "textDatabaseServer";
            // 
            // textResultDatabase
            // 
            resources.ApplyResources(this.textResultDatabase, "textResultDatabase");
            this.textResultDatabase.Name = "textResultDatabase";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // textTestChannel
            // 
            resources.ApplyResources(this.textTestChannel, "textTestChannel");
            this.textTestChannel.Name = "textTestChannel";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // lUnitsRun
            // 
            resources.ApplyResources(this.lUnitsRun, "lUnitsRun");
            this.lUnitsRun.Name = "lUnitsRun";
            // 
            // lUnitsPassed
            // 
            resources.ApplyResources(this.lUnitsPassed, "lUnitsPassed");
            this.lUnitsPassed.Name = "lUnitsPassed";
            // 
            // lUnitsFailed
            // 
            resources.ApplyResources(this.lUnitsFailed, "lUnitsFailed");
            this.lUnitsFailed.Name = "lUnitsFailed";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // lbStation
            // 
            resources.ApplyResources(this.lbStation, "lbStation");
            this.lbStation.Name = "lbStation";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // lbStationFunction
            // 
            resources.ApplyResources(this.lbStationFunction, "lbStationFunction");
            this.lbStationFunction.Name = "lbStationFunction";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // lblComputerName
            // 
            resources.ApplyResources(this.lblComputerName, "lblComputerName");
            this.lblComputerName.Name = "lblComputerName";
            // 
            // lDebugMode
            // 
            resources.ApplyResources(this.lDebugMode, "lDebugMode");
            this.lDebugMode.Name = "lDebugMode";
            // 
            // lbDebugOptions
            // 
            resources.ApplyResources(this.lbDebugOptions, "lbDebugOptions");
            this.lbDebugOptions.Name = "lbDebugOptions";
            // 
            // MainWindow
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lbDebugOptions);
            this.Controls.Add(this.lDebugMode);
            this.Controls.Add(this.lblComputerName);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lbStationFunction);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lbStation);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lUnitsFailed);
            this.Controls.Add(this.lUnitsPassed);
            this.Controls.Add(this.lUnitsRun);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textTestChannel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textResultDatabase);
            this.Controls.Add(this.textDatabaseServer);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.testResults);
            this.Controls.Add(this.textLimitFileName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textLimitFileVersion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textProgramVersion);
            this.Controls.Add(this.buttonAbortTest);
            this.Controls.Add(this.buttonStartTest);
            this.Controls.Add(this.testStatusBox);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CloseStation);
            this.Load += new System.EventHandler(this.InitStation);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label textLimitFileName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label textLimitFileVersion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label textProgramVersion;
        private System.Windows.Forms.Button buttonAbortTest;
        private System.Windows.Forms.Button buttonStartTest;
        private System.Windows.Forms.RichTextBox testStatusBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem configureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productToTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox productSelection;
        private System.Windows.Forms.ToolStripMenuItem toggleSerialDisplayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configureStationToolStripMenuItem;
        private System.Windows.Forms.RichTextBox testResults;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label textDatabaseServer;
        private System.Windows.Forms.Label textResultDatabase;
        private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pIRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem utilitiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reprintLabelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearManufactureDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fixtureControlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configStationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configureFixtureToolStripMenuItem;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label textTestChannel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lUnitsRun;
        private System.Windows.Forms.Label lUnitsPassed;
        private System.Windows.Forms.Label lUnitsFailed;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbStation;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lbStationFunction;
        private System.Windows.Forms.ToolStripMenuItem clearResultWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearStatusWindowToolStripMenuItem;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ToolStripMenuItem resetStationConfigToolStripMenuItem;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblComputerName;
        private System.Windows.Forms.ToolStripMenuItem turnOnDUTToolStripMenuItem;
        private System.Windows.Forms.Label lDebugMode;
        private System.Windows.Forms.Label lbDebugOptions;
        //private System.ComponentModel.IContainer components;
    }
}

