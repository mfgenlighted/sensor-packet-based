﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Xml.Linq;
using System.Management;

using TestStationErrorCodes;
using A34970ADriver;
using FixtureInstrumentBase;

namespace SensorManufSY2
{

    public class FixtureFunctions :  IDisposable
    {
        //----------------------
        // class properties
        //----------------------
        bool disposed = false;
        private StationConfigure stationConfigure = null;   // all the config data
        public StationConfigure StationConfigure { set { stationConfigure = value; } }
        public int DUTTurnOnDelayMs = 100;                 // time to let relays settle after commands
        public enum PowerControl { OFF, ON};

        public string LastFunctionErrorMessage = string.Empty;
        public int LastFunctionStatus = 0;

        public class PassData
        {
            public string channelName;
            public double returnedData;
        }

        //----------------------------------------------
        //  DAQ
        //---------------------------------------------
        // public enum ChannelType { AC, DC, Temperature, Contact, DCOutput };


        // instruments
        private LabJackDAQInstrument labjackDAQ;
        private A34970ADAQInstrument a34970ADAQ;
        private SwitchMateInstrument switchmateRelays;
        private OptomisticInstrument optomisticLEDDetector;

        private List<FixtureInstrument.Channel> ChannelList;        // full channel def


        //---------------------------------------
        //  Ref Radio data
        //---------------------------------------
        private string refRadioSerialPort = string.Empty;       // will be set from station config passed to init
        private string refRadioMAC = string.Empty;              // ref radio mac holder
        private string refBLEMAC = string.Empty;
            //---- default values
        public uint RadioDefaultChannel = 15;
        public uint RadioDefaultPanID = 0x6854;
        public uint RadioDefaultTxPower = 0;
        public string RadioDefaultEncryptKey = "manufacturetest1";
        public uint RadioDefaultTTL = 3;
        public uint RadioDeraultRate = 16;
            //----Properties
        public PacketBasedConsole RefRadio;                      // handle for ref radio   
        public string RefRadioMAC { get { return refRadioMAC; } } 
        public string RefBLEMAC { get { return refBLEMAC; } }


        //-------------------------------------
        //  DUT data
        //-------------------------------------
        private string dutSerialPort = string.Empty;
        public PacketBasedConsole SensorConsole;

        //------------------------------------
        //  fixture defines
        //  Defines the functions that the fixture has for each type
        //---------------------------------------
        public const string CH_DIM1_OUTPUT = "DIM1_DC";
        public const string CH_DIM2_OUTPUT = "DIM2_DC";
        public const string CH_V3P3_VOLTAGE = "3V3_DC";
        public const string CH_TEMPERTURE_PROBE = "TEMPERTURE_PROBE_TEMP";
        public const string CH_OPERATOR_BUTTON = "OPERATOR_BUTTON";
        public const string CH_FIXTURE_LID_SWITCH = "FIXTURE_CLOSE_DI";
        public const string CH_AMBIENT_DETECTOR_LED = "AMBIENT_DECTOR_DAC";
        public const string CH_DUT_POWER_CNTL = "DUT_POWER_CNTL";
        public const string CH_PIR_SOURCE_CNTL = "PIR_SOURCE_CNTL";
        public const string CH_LED_DETECTOR = "LED_DETECTOR";

        public List<FixtureInstrument.fixtureTypes> fixtureTypeList = new List<FixtureInstrument.fixtureTypes>
        {

                // Kona fixture
                new FixtureInstrument.fixtureTypes
                {
                    desc = "Functional SU-5S fixture",
                    fixtureType = "SU-5S-FCT",
                    hasEnlightedInterface = true,       // enlighted interface, CU and 0-10V output
                    hasConsolePort = true,              // has a console port to the DUT
                    usesPacketBasedConsole = true,      // uses packet based 
                    usesTextBaseConsole = false,        // not text based
                    usesCUPortForConsole = true,        // The console port is on the CU port.
                    hasBLE = true,                      // has bluetooth.
                    hasTestRadio = true,                // has a test radio(Enlighted only)
                    hasSniffer = false,                 // no sniffer
                    hasPCBAPrinter = false,             // no PCBA printer
                    hasHLAPrinter = false,              // no HLA printer
                    channelDefs = new List<FixtureInstrument.Channel>() {
                        new FixtureInstrument.Channel() { name = CH_DIM1_OUTPUT, type = FixtureInstrument.ChannelType.DCInput, channel = 2, lastReading =0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10 },
                        new FixtureInstrument.Channel() { name = CH_DIM2_OUTPUT, type = FixtureInstrument.ChannelType.DCInput, channel = 0, lastReading =0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10},
                        new FixtureInstrument.Channel() { name = CH_V3P3_VOLTAGE, type = FixtureInstrument.ChannelType.DCInput, channel = 1, lastReading =0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10},
                        new FixtureInstrument.Channel() { name = CH_TEMPERTURE_PROBE, type = FixtureInstrument.ChannelType.Temperature, channel = 3, lastReading =0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10},
                        new FixtureInstrument.Channel() { name = CH_OPERATOR_BUTTON, type = FixtureInstrument.ChannelType.DInput, channel = 4, lastReading =0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10},
                        new FixtureInstrument.Channel() { name = CH_FIXTURE_LID_SWITCH, type = FixtureInstrument.ChannelType.DInput, channel = 5, lastReading =0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10},
                        new FixtureInstrument.Channel() { name = CH_AMBIENT_DETECTOR_LED, type = FixtureInstrument.ChannelType.DAC, channel = 0, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10 },
                        new FixtureInstrument.Channel() { name = CH_DUT_POWER_CNTL, type = FixtureInstrument.ChannelType.Contact, channel = 1, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.SwitchMate, range = 0 },
                        new FixtureInstrument.Channel() { name = CH_PIR_SOURCE_CNTL, type = FixtureInstrument.ChannelType.Contact, channel = 2, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.SwitchMate, range = 0 },
                                                            // lastReading, range has no meaning for LED detector. Must get readings from properties freqency and intensity
                                                            // for Optomistic channelType and channel have no meaning.
                        new FixtureInstrument.Channel() { name =  CH_LED_DETECTOR, type = FixtureInstrument.ChannelType.DCInput, channel = 0, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.Optomistic, range = 0,}
                    },
                    hasFlasher = true,                  // has a flasher programmer
                 },

                new FixtureInstrument.fixtureTypes
                {
                    desc = "Functional SU-5E fixture",
                    fixtureType = "SU-5E-FCT",
                    hasEnlightedInterface = true,       // enlighted interface, CU and 0-10V output
                    hasConsolePort = true,              // has a console port to the DUT
                    usesCUPortForConsole = true,        // The console port is on the CU port.
                    usesPacketBasedConsole = true,      // uses packet based 
                    usesTextBaseConsole = false,        // not text based
                    hasBLE = true,                      // has bluetooth.
                    hasTestRadio = true,                // has a test radio(Enlighted only)
                    hasSniffer = false,                 // no sniffer
                    hasPCBAPrinter = false,             // no PCBA printer
                    hasHLAPrinter = false,              // no HLA printer
        //  AIN0    SO2
        //  AIN1    3.3
        //  AIN2    SO1
        //  AIN3    TP
        //  FIO4    OK
        //  FIO5    LS
        //  FIO6    --
        //  FIO7    --   
        //  DAC0    AS
        //
        //  TP = temperture probe
        //  3.3 = SU 3.3v
        //  OK = OK operator button
        //  LS = Lid switch
        //  SO1 = orignal sensor output voltage
        //  SO2 = second sensor output voltage
        //  AS = ambient LED source
                    channelDefs = new List<FixtureInstrument.Channel>() {
                        new FixtureInstrument.Channel() { name = CH_DIM1_OUTPUT, type = FixtureInstrument.ChannelType.DCInput, channel = 2, lastReading =0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10 },
                        new FixtureInstrument.Channel() { name = CH_DIM2_OUTPUT, type = FixtureInstrument.ChannelType.DCInput, channel = 0, lastReading =0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10},
                        new FixtureInstrument.Channel() { name = CH_V3P3_VOLTAGE, type = FixtureInstrument.ChannelType.DCInput, channel = 1, lastReading =0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10},
                        new FixtureInstrument.Channel() { name = CH_TEMPERTURE_PROBE, type = FixtureInstrument.ChannelType.Temperature, channel = 3, lastReading =0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10},
                        new FixtureInstrument.Channel() { name = CH_OPERATOR_BUTTON, type = FixtureInstrument.ChannelType.DInput, channel = 4, lastReading =0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10},
                        new FixtureInstrument.Channel() { name = CH_FIXTURE_LID_SWITCH, type = FixtureInstrument.ChannelType.DInput, channel = 5, lastReading =0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10},
                        new FixtureInstrument.Channel() { name = CH_AMBIENT_DETECTOR_LED, type = FixtureInstrument.ChannelType.DAC, channel = 0, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10 },
                        new FixtureInstrument.Channel() { name = CH_DUT_POWER_CNTL, type = FixtureInstrument.ChannelType.Contact, channel = 1, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.SwitchMate, range = 0 },
                        new FixtureInstrument.Channel() { name = CH_PIR_SOURCE_CNTL, type = FixtureInstrument.ChannelType.Contact, channel = 2, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.SwitchMate, range = 0 },
                                                            // lastReading, range has no meaning for LED detector. Must get readings from properties freqency and intensity
                                                            // for Optomistic channelType and channel have no meaning.
                        new FixtureInstrument.Channel() { name =  CH_LED_DETECTOR, type = FixtureInstrument.ChannelType.DCInput, channel = 0, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.Optomistic, range = 0,}
                    },
                    hasFlasher = true,                  // has a flasher programmer
                 },

                new FixtureInstrument.fixtureTypes
                {
                    desc = "SU-5 Final",
                    fixtureType = "SU-5-FNL",
                    hasEnlightedInterface = true,       // enlighted interface, CU and 0-10V output
                    hasConsolePort = true,              // has a console port to the DUT
                    usesPacketBasedConsole = true,      // uses packet based 
                    usesTextBaseConsole = false,        // not text based
                    usesCUPortForConsole = true,        // The console port is on the CU port.
                    hasBLE = false,                     // has bluetooth.
                    hasTestRadio = false,               // has a test radio(Enlighted only)
                    hasSniffer = false,                 // no sniffer
                    hasPCBAPrinter = false,              // no PCBA printer
                    hasHLAPrinter = true,               // has HLA printer
                },

                // old gateway fixture
                new FixtureInstrument.fixtureTypes
                {
                    desc = "GW-2 Functional",
                    fixtureType = "GW-2-FCT",
                    hasEnlightedInterface = false,       // enlighted interface, CU and 0-10V output
                    hasConsolePort = true,              // has a console port to the DUT
                    usesCUPortForConsole = false,        // The console port is on the CU port.
                    usesPacketBasedConsole = false,      // not packet based 
                    usesTextBaseConsole = true,         // uses text based
                    hasBLE = false,                     // has bluetooth.
                    hasTestRadio = true,               // has a test radio(Enlighted only)
                    usesAtmelRefRadio = true,           // uses old atmel ref radio
                    hasSniffer = false,                 // no sniffer
                    hasPCBAPrinter = true,              // has PCBA printer
                    hasHLAPrinter = true,               // has HLA printer
                    channelDefs = new List<FixtureInstrument.Channel>() {
                        new FixtureInstrument.Channel() { name = CH_V3P3_VOLTAGE, type = FixtureInstrument.ChannelType.DCInput, channel = 2, lastReading =0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10},
                        new FixtureInstrument.Channel() { name = CH_FIXTURE_LID_SWITCH, type = FixtureInstrument.ChannelType.DInput, channel = 4, lastReading =0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10},
                        new FixtureInstrument.Channel() { name = CH_LED_DETECTOR, type = FixtureInstrument.ChannelType.Frequency, channel = 5, instrumentID = FixtureInstrument.InstrumentID.LABJACK },
                    },
                }


                // new fixture
                //new fixtureTypes
                //{
                //    desc = "GW-2 Functional",
                //    fixtureType = "GW-2-FCT",
                //    hasEnlightedInterface = false,       // enlighted interface, CU and 0-10V output
                //    hasConsolePort = false,              // has a console port to the DUT
                //    usesCUPortForConsole = false,        // The console port is on the CU port.
                //    usesPacketBasedConsole = false,      // not packet based 
                //    usesTextBaseConsole = true,         // uses text based
                //    hasAmbientSensor = false,           // has ambient light sensor
                //    hasPirSensor = false,               // has PIR sensor
                //    hasBLE = false,                     // has bluetooth.
                //    hasTestRadio = false,               // has a test radio(Enlighted only)
                //    hasSniffer = false,                 // no sniffer
                //    hasTempProbe = false,               // has a temperature probe
                //    hasPCBAPrinter = false,              // has PCBA printer
                //    hasHLAPrinter = false,               // has HLA printer
                //    hasLEDDetector = false,             // has LED detector of some sort
                //    hasFixturePushButton = false,       // OK button on the fixture
                //    hasLidSwitch = false,               // has a lid down switch.
                //    hasLabJack = false,                 // has a LabJack for analog
                //    hasSwitchMate = false,              // has a Switch Mate for switching
                //    has34970A = true,                   // has 34970 for analog and switching
                //    channelDefs = new List<Channel>() {
                //        new Channel() { name = "VCC_5V_READ", type = FixtureInstrument.ChannelType.DCInput, channel = 101, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.A34970A, range = 10},
                //        new Channel() { name = "VDD_1V2_READ", type = FixtureInstrument.ChannelType.AC, channel = 102, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.A34970A, range = 10},
                //        new Channel() { name = "VDDIO_3V3_READ", type = FixtureInstrument.ChannelType.DCInput, channel = 103, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.A34970A, range = 10},
                //        new Channel() { name = "VDDA_1V8_READ", type = FixtureInstrument.ChannelType.DCInput, channel = 104, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.A34970A, range = 10},
                //        new Channel() { name = "3V3_BLE_READ", type = FixtureInstrument.ChannelType.DCInput, channel = 105, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.A34970A, range = 10},
                //        new Channel() { name = "3V3_Z_READ", type = FixtureInstrument.ChannelType.DCInput, channel = 106, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.A34970A, range = 10},
                //        new Channel() { name = "FIXTURE_CLOSE_READ", type = FixtureInstrument.ChannelType.DCInput, channel = 107, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.A34970A, range = 10},
                //        new Channel() { name = "OPERATOR_BUTTON_READ", type = FixtureInstrument.ChannelType.DCInput, channel = 108, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.A34970A, range = 10},
                //        new Channel() { name = "FIXTURE_PS_READ", type = FixtureInstrument.ChannelType.DCInput, channel = 109, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.A34970A, range = 10},
                //        new Channel() { name = "DUT_POWER_CONTROL", type = FixtureInstrument.ChannelType.Contact, channel = 201, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.A34970A, range = 10 },
                //        new Channel() { name = "PIR_SOURCE_CONTROL", type = FixtureInstrument.ChannelType.Contact, channel = 202, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.A34970A, range = 10 },
                //        new Channel() { name = "AMBIENT_SOURCE_CONTROL", type = FixtureInstrument.ChannelType.Contact, channel = 203, lastReading = 0, instrumentID = FixtureInstrument.InstrumentID.A34970A, range = 10 },
                //    },
                //}
        };


        /// <summary>
        /// Will open the class, but will not initialize the fixture. StationConfig
        /// must be setup before calling InitFixture.
        /// </summary>
        public FixtureFunctions()
        {
            // get any extentions of the fixtures
            GetFixtureDefFile(ref fixtureTypeList);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (RefRadio != null)
                    {
                        RefRadio.Close();
                        RefRadio = null;
                    }
                    if (SensorConsole != null)
                    {
                        SensorConsole.Close();
                        SensorConsole = null;
                    }
                    if (a34970ADAQ != null)
                    {
                        a34970ADAQ.Disconnect();
                        a34970ADAQ = null;
                    }
                    if (labjackDAQ != null)
                    {
                        labjackDAQ.Disconnect();
                        labjackDAQ = null;
                    }
                    if (switchmateRelays != null)
                    {
                        switchmateRelays.Disconnect();
                        switchmateRelays = null;
                    }
                    if (optomisticLEDDetector != null)
                    {
                        optomisticLEDDetector.Disconnect();
                        optomisticLEDDetector = null;
                    }
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Will intialize the fixture hardware. will return false if error.
        /// </summary>
        public bool InitFixture()
        {
            string msgs = string.Empty;

            // Variables to satisfy certain method signatures
            double[] dummyDoubleArray = { 0 };

            LastFunctionErrorMessage = string.Empty;
            LastFunctionStatus = 0;
            ChannelList = new List<FixtureInstrument.Channel>();

            if (stationConfigure == null)   // if it has not been set up yet
            {
                LastFunctionErrorMessage = "StationConfigure has not been set up before call InitFixture";
                LastFunctionStatus = (int)ErrorCodes.System.StationConfigNotRead;
                return false;
            }

            UpdateStatusWindow("Start Fixture init\n", StatusType.passed);



            // if ref radio is installed
            if (stationConfigure.FixtureEquipment.hasTestRadio)
            {
                UpdateStatusWindow("   Setting up Ref Radio - ", StatusType.text);
                RefRadio = new PacketBasedConsole();
                if (!RefRadio.Open(stationConfigure.ConfigTestRadioComPort, 115200))
                {
                    LastFunctionErrorMessage = RefRadio.LastErrorMessage;
                    LastFunctionStatus = RefRadio.LastErrorCode;
                    UpdateStatusWindow(" Failed - " + LastFunctionErrorMessage + "\n", StatusType.failed);
                    return false;
                }
                RefRadio.DisplayRcvChar = stationConfigure.ConfigDisplayRcvChar;
                RefRadio.DisplaySendChar = stationConfigure.ConfigDisplaySendChar;
                RefRadio.IsThisDUTConsole = false;
                if (!RefRadio.GetMACData(ref refRadioMAC, ref refBLEMAC))
                {
                    LastFunctionErrorMessage = RefRadio.LastErrorMessage;
                    LastFunctionStatus = RefRadio.LastErrorCode;
                    UpdateStatusWindow(" Failed Getting MAC - " + LastFunctionErrorMessage + "\n", StatusType.failed);
                    return false;
                }
                UpdateStatusWindow(" Passed - MAC: " + refRadioMAC + "\n", StatusType.passed);
            }

            // if has a console port
            if (stationConfigure.FixtureEquipment.hasConsolePort)
            {
                // init the dut serial port
                UpdateStatusWindow("    Checking DUT console port - ", StatusType.text);
                SensorConsole = new PacketBasedConsole();
                SensorConsole.CommandRecieveTimeoutMS = 100;
                if (!SensorConsole.Open(stationConfigure.ConfigDUTComPort, 115200))
                {
                    LastFunctionErrorMessage = SensorConsole.LastErrorMessage;
                    LastFunctionStatus = SensorConsole.LastErrorCode;
                    UpdateStatusWindow(" Failed - " + LastFunctionErrorMessage + "\n", StatusType.failed);
                    return false;
                }
                SensorConsole.IsThisDUTConsole = true;
                UpdateStatusWindow(" Passed\n", StatusType.passed);
            }

            // setup the instruments
            UpdateStatusWindow("   Setting up Instruments - ", StatusType.text);
            labjackDAQ = null;
            a34970ADAQ = null;
            switchmateRelays = null;
            optomisticLEDDetector = null;

            if (stationConfigure.FixtureEquipment.channelDefs != null)
            {

                foreach (var item in stationConfigure.FixtureEquipment.channelDefs)
                {
                    if (item.instrumentID == FixtureInstrument.InstrumentID.LABJACK)
                    {
                        if (labjackDAQ == null)
                            labjackDAQ = new LabJackDAQInstrument();
                        labjackDAQ.AddChannel(item.name, item.channel, item.type, item.range);
                        if (labjackDAQ.LastFunctionStatus != 0)
                        {
                            UpdateStatusWindow(labjackDAQ.LastFunctionErrorMessage + "\n", StatusType.failed);
                            LastFunctionErrorMessage = labjackDAQ.LastFunctionErrorMessage;
                            LastFunctionStatus = (int)ErrorCodes.System.DAQBase - labjackDAQ.LastFunctionStatus;       // system codes are negitive
                            return false;
                        }
                    }
                    if (item.instrumentID == FixtureInstrument.InstrumentID.A34970A)
                    {

                        if (a34970ADAQ == null)
                            a34970ADAQ = new A34970ADAQInstrument("A34970A");
                        a34970ADAQ.AddChannel(item.name, item.channel, item.type, item.range);
                        if (a34970ADAQ.LastFunctionStatus != 0)
                        {
                            UpdateStatusWindow(a34970ADAQ.LastFunctionErrorMessage + "\n", StatusType.failed);
                            LastFunctionErrorMessage = a34970ADAQ.LastFunctionErrorMessage;
                            LastFunctionStatus = (int)ErrorCodes.System.DAQBase - a34970ADAQ.LastFunctionStatus;       // system codes are negitive
                            return false;
                        }
                    }
                    if (item.instrumentID == FixtureInstrument.InstrumentID.SwitchMate)
                    {
                        if (switchmateRelays == null)
                            switchmateRelays = new SwitchMateInstrument(stationConfigure.ConfigSwitchMateComPort);
                        switchmateRelays.AddChannel(item.name, item.channel, item.type, item.range);
                        if (switchmateRelays.LastFunctionStatus != 0)
                        {
                            UpdateStatusWindow(switchmateRelays.LastFunctionErrorMessage + "\n", StatusType.failed);
                            LastFunctionErrorMessage = switchmateRelays.LastFunctionErrorMessage;
                            LastFunctionStatus = (int)ErrorCodes.System.DAQBase - switchmateRelays.LastFunctionStatus;       // system codes are negitive
                            return false;
                        }
                    }
                    if (item.instrumentID == FixtureInstrument.InstrumentID.Optomistic)
                        if (optomisticLEDDetector == null)
                            optomisticLEDDetector = new OptomisticInstrument(stationConfigure.ConfigLedDetector.ComPort);
                }
            }

            // now connect
            if (labjackDAQ != null)
            {
                if (!labjackDAQ.Connect())
                {
                    UpdateStatusWindow(labjackDAQ.LastFunctionErrorMessage + "\n", StatusType.failed);
                    LastFunctionErrorMessage = labjackDAQ.LastFunctionErrorMessage;
                    LastFunctionStatus = (int)ErrorCodes.System.DAQBase - labjackDAQ.LastFunctionStatus;       // system codes are negitive
                    return false;
                }
                UpdateStatusWindow(" LabJack connected.\n");
            }
            if (a34970ADAQ != null)
            {
                if (!a34970ADAQ.Connect())
                {
                    UpdateStatusWindow(a34970ADAQ.LastFunctionErrorMessage + "\n", StatusType.failed);
                    LastFunctionErrorMessage = a34970ADAQ.LastFunctionErrorMessage;
                    LastFunctionStatus = (int)ErrorCodes.System.DAQBase - a34970ADAQ.LastFunctionStatus;       // system codes are negitive
                    return false;
                }
                UpdateStatusWindow("  A3490A connected.\n");
            }
            if (switchmateRelays != null)
            {
                if (!switchmateRelays.Connect())
                {
                    UpdateStatusWindow(switchmateRelays.LastFunctionErrorMessage + "\n", StatusType.failed);
                    LastFunctionErrorMessage = switchmateRelays.LastFunctionErrorMessage;
                    LastFunctionStatus = (int)ErrorCodes.System.DAQBase - switchmateRelays.LastFunctionStatus;       // system codes are negitive
                    return false;
                }
                UpdateStatusWindow("  Switch-Mate connected.\n");
            }
            if (optomisticLEDDetector != null)
            {
                if (!optomisticLEDDetector.Connect())
                {
                    UpdateStatusWindow(optomisticLEDDetector.LastFunctionErrorMessage + "\n", StatusType.failed);
                    LastFunctionErrorMessage = optomisticLEDDetector.LastFunctionErrorMessage;
                    LastFunctionStatus = (int)ErrorCodes.System.DAQBase - optomisticLEDDetector.LastFunctionStatus;       // system codes are negitive
                    return false;
                }
                UpdateStatusWindow("  Optomistic LED detector connected.\n");
            }

            UpdateStatusWindow(" Passed\n", StatusType.passed);


            // if has code flasher
            if (stationConfigure.FixtureEquipment.hasFlasher)
            {
                if ((stationConfigure.ConfigFlasherComPort == "COM0") | (stationConfigure.ConfigFlasherComPort == string.Empty))
                {
                    UpdateStatusWindow("   Flasher COM port not set. Will run in manual mode.\n");
                }
                else
                {
                    UpdateStatusWindow("   Setting up Flasher - ", StatusType.text);
                    Flasher flasher = new Flasher(stationConfigure.ConfigFlasherComPort);
                    if (!flasher.Open())
                    {
                        LastFunctionErrorMessage = "Flasher error code: " + flasher.LastErrorCode + ", " + flasher.LastErrorMsg;
                        LastFunctionStatus = (int)ErrorCodes.System.FlasherBase - flasher.LastErrorCode;        // 
                        UpdateStatusWindow(" Failed - " + LastFunctionErrorMessage + "\n", StatusType.failed);
                        return false;
                    }
                    flasher.GetStatus();
                    flasher.Close();
                    if (!flasher.LastErrorStatus)
                    {
                        LastFunctionErrorMessage = "Flasher error code: " + flasher.LastErrorCode + ", " + flasher.LastErrorMsg;
                        LastFunctionStatus = (int)ErrorCodes.System.FlasherBase - flasher.LastErrorCode;        // system errors are negitive
                        UpdateStatusWindow(" Failed - " + LastFunctionErrorMessage + "\n", StatusType.failed);
                        return false;
                    }
                    UpdateStatusWindow(" Passed\n", StatusType.passed);
                }
            }


            // set up the instrument channels
            if (stationConfigure.FixtureEquipment.channelDefs != null)
            {
                foreach (var item in stationConfigure.FixtureEquipment.channelDefs)
                {
                    ChannelList.Add(item);
                }
            }

            return true;
        }


        /// <summary>
        /// Reads a list of measurements.
        /// </summary>
        /// <remarks>
        /// LastFunctionStatus
        /// </remarks>
        /// <param name="channelNames">string names of the measurement</param>
        /// <param name="readValues">Returned double values</param>
        public void ReadMeasurements(ref PassData[] channelNames)
        {
            FixtureInstrument.Channel[] ljList = new FixtureInstrument.Channel[channelNames.Length];
            FixtureInstrument.Channel[] aList = new FixtureInstrument.Channel[channelNames.Length];
            FixtureInstrument.Channel single = new FixtureInstrument.Channel();
            int ljindex = 0;
            int aindex = 0;

            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            // make list for each instrument
            foreach (var item in channelNames)
            {
                single = stationConfigure.FixtureEquipment.channelDefs.Find(x => x.name == item.channelName);
                if (single.instrumentID == FixtureInstrument.InstrumentID.LABJACK)
                    ljList[ljindex++] = single;
                if (single.instrumentID == FixtureInstrument.InstrumentID.A34970A)
                    aList[aindex++] = single;
            }

            if (aList[0] != null)  // if anything in the agilent list
            {
                if (a34970ADAQ != null)
                {
                    a34970ADAQ.ReadListOfChannels(ref aList);
                    if (a34970ADAQ.LastFunctionStatus != 0)
                    {
                        LastFunctionStatus = ErrorCodes.SystemGroups.FixtureErrorsBase - a34970ADAQ.LastFunctionStatus;  // any error will be a system error
                        LastFunctionErrorMessage = a34970ADAQ.LastFunctionErrorMessage;
                    }
                }
                else
                {
                    LastFunctionStatus = ErrorCodes.System.InstrumentNotConnected;  // any error will be a system error
                    LastFunctionErrorMessage = "Can not do measurements. A34970A not connected";
                }
            }

            if (ljList[0] != null) // if anything in the labjack list
            {
                if (labjackDAQ != null)
                {
                    labjackDAQ.ReadListOfChannels(ref ljList);
                    if (labjackDAQ.LastFunctionStatus != 0)
                    {
                        LastFunctionStatus = ErrorCodes.SystemGroups.FixtureErrorsBase - labjackDAQ.LastFunctionStatus;  // any error will be a system error
                        LastFunctionErrorMessage = labjackDAQ.LastFunctionErrorMessage;
                    }
                }
                else
                {
                    LastFunctionStatus = ErrorCodes.System.InstrumentNotConnected;  // any error will be a system error
                    LastFunctionErrorMessage = "Can not do measurements. Labjack not connected";
                }
            }

            // now have to get data back into orginal array
            foreach (var item in channelNames)
            {
                if (ljindex != 0)       // labjack read something
                {
                    foreach (var ljitem in ljList)
                    {
                        if (ljitem.name == item.channelName)
                        {
                            item.returnedData = ljitem.lastReading;
                            break;
                        }
                    }
                }
                if (aindex != 0)
                {
                    foreach (var aitem in aList)
                    {
                        if (aitem.name == item.channelName)
                        {
                            item.returnedData = aitem.lastReading;
                            break;
                        }
                    }
                }
            }
            return;
        }


        /// <summary>
        /// Read a single measurment. Pass the name of the measurement.
        /// </summary>
        /// <remarks>
        /// LastFunctionStatus
        /// </remarks>
        /// <param name="channel"></param>
        /// <returns></returns>
        public double ReadOneMeasurement(string measurement)
        {
            PassData[] channels = new PassData[1];

            channels[0] = new PassData();
            channels[0].channelName = measurement;
            ReadMeasurements(ref channels);
            return channels[0].returnedData;
        }

        public void SetOutput(string channel, double value)
        {
            FixtureInstrument.Channel single = new FixtureInstrument.Channel();
            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            if (stationConfigure.FixtureEquipment.channelDefs == null)
            {
                LastFunctionErrorMessage = "No output defined";
                LastFunctionStatus = (int)ErrorCodes.System.InstrumentNotConnected;
                return;
            }

            single = stationConfigure.FixtureEquipment.channelDefs.Find(x => x.name == channel);

            if (single.instrumentID == FixtureInstrument.InstrumentID.A34970A)
            {
                if (a34970ADAQ != null)
                {
                    a34970ADAQ.SetChannel(channel, value);
                    a34970ADAQ.GetLastError(ref LastFunctionErrorMessage, ref LastFunctionStatus);
                    if (LastFunctionStatus != 0)        // only generates system errors
                        LastFunctionStatus = (int)ErrorCodes.SystemGroups.FixtureErrorsBase - LastFunctionStatus;
                }
                else
                {
                    LastFunctionErrorMessage = "Can not set ouput. A34970A not connected.";
                    LastFunctionStatus = (int)ErrorCodes.System.InstrumentNotConnected;
                }
            }
            else if (single.instrumentID == FixtureInstrument.InstrumentID.LABJACK)
            {
                if (labjackDAQ != null)
                {
                    labjackDAQ.SetChannel(channel, value);
                    labjackDAQ.GetLastError(ref LastFunctionErrorMessage, ref LastFunctionStatus);
                    if (LastFunctionStatus != 0)        // only generates system errors
                        LastFunctionStatus = (int)ErrorCodes.SystemGroups.FixtureErrorsBase - LastFunctionStatus;
                }
                else
                {
                    LastFunctionErrorMessage = "Can not set ouput. Labjack not connected.";
                    LastFunctionStatus = (int)ErrorCodes.System.InstrumentNotConnected;
                }
            }
            else if (single.instrumentID == FixtureInstrument.InstrumentID.SwitchMate)
            {
                if (switchmateRelays != null)
                {
                    switchmateRelays.SetChannel(channel, value);
                    switchmateRelays.GetLastError(ref LastFunctionErrorMessage, ref LastFunctionStatus);
                    if (LastFunctionStatus != 0)        // only generates system errors
                        LastFunctionStatus = (int)ErrorCodes.SystemGroups.FixtureErrorsBase - LastFunctionStatus;
                }
                else
                {
                    LastFunctionErrorMessage = "Can not set ouput. Switch-Mate not connected.";
                    LastFunctionStatus = (int)ErrorCodes.System.InstrumentNotConnected;
                }
            }
            else
            {
                LastFunctionStatus = (int)ErrorCodes.System.InvalidInstrumentDefined;
                LastFunctionErrorMessage = "Instrument ID(" + single.instrumentID + ") not valid for SetOutput Function.";
            }
        }

        //--------------------------------------
        // specialized function calls
        //-------------------------------------

        /// <summary>
        /// Returns the lid status
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>true = cover closed</returns>
        public bool IsCoverClosed()
        {
            double value;
            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            value = ReadOneMeasurement(CH_FIXTURE_LID_SWITCH);


            return (value == 1);
        }

        /// <summary>
        /// Returns the box button status
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>true = button pushed</returns>
        public bool IsBoxButtonPushed()
        {
            double value;

            value = ReadOneMeasurement(CH_OPERATOR_BUTTON);


            return (value == 1);
        }



        //--------------------------------------
        // ControlPIRSource
        /// <summary>
        /// Control the Test source for the PIR. Will delay ConfigPIRDetectorLightSource.onDelayMs
        /// when turned on and ConfigPIRDetectorLightSource.OffDelayMs when turned off.
        /// </summary>
        /// <param name="control"></param>
        /// <returns>Last error status</returns>
        public int ControlPIRSource(PowerControl control)
        {
            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            SetOutput(CH_PIR_SOURCE_CNTL, (double)control);

            return LastFunctionStatus;
        }


        /// <summary>
        /// Will turn power on and off to the dut. Will delay after set.
        /// </summary>
        /// <remarks>
        /// <param name="control 1=on 0=off"></param>
        /// <param name="aftersetdelayms">ms to wait after control</param>
        /// <returns>
        /// last error code
        /// </returns>
        public int ControlDUTPower(PowerControl control, int aftersetdelayms)
        {
            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            SetOutput(CH_DUT_POWER_CNTL, (double)control);
            Thread.Sleep(aftersetdelayms);

            return LastFunctionStatus;
        }


        //---------------------------------------
        // SetAmbientSensorLED
        /// <summary>
        /// Set the Ambient to the voltage.
        /// </summary>
        /// <returns></returns>
        public int SetAmbientSensorLED(double voltage)
        {

            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            SetOutput(CH_AMBIENT_DETECTOR_LED, voltage);

            return LastFunctionStatus;
        }


        //----------------------------------------
        // ReadTemperature
        //
        //  Read the temperature probe in C
        public double ReadTemperatureProbe()
        {
            double value = 0;

            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            value = ReadOneMeasurement(CH_TEMPERTURE_PROBE);
            if (stationConfigure.ConfigTempSensorOutput == "C")
                value *= 10;
            return value;
        }


        //----------------------------------
        // Reads the LED Detector.
        //  Exceptions
        //      LabJack exceptions
        //
        // The following properties are set:
        //      double value - frequency, average of 10 period readings
        public int ReadLEDDetector(ref double freq, ref double dutyCycle)
        {
            bool usesLabJack = false;
            bool usesOptomistic = false;

            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            // Variables to satisfy certain method signatures
            double[] valuearray = new double[10];
            double[] valuearraydc = new double[10];

            // see if LED Detector is on the Labjack
            if (labjackDAQ != null)
            {
                if (labjackDAQ.FindChannelData(FixtureFunctions.CH_LED_DETECTOR) != null)
                    usesLabJack = true;
            }

            // see if it is a Optomistic
            if (optomisticLEDDetector != null)
            {
                usesOptomistic = true;
            }

            if (!usesOptomistic & !usesLabJack)
            {
                LastFunctionErrorMessage = "LED Detector does not exist.";
                LastFunctionStatus = ErrorCodes.System.InvalidChannelData;
                return LastFunctionStatus;
            }


            //            Thread.Sleep(100);
            freq = 0;
            for (int i = 0; i < 10; i++)
            {
                if (usesOptomistic)
                {
                    int retrycount = 0;
                    do      // this loop is to try to catch when a 0 comes back from the read.
                            // assuming that for some reason no reads are being done, so give a some 
                            // time.
                    {
                        if (optomisticLEDDetector.wavelength == 0)
                            Thread.Sleep(100);
                        valuearray[i] = optomisticLEDDetector.wavelength;
                        valuearraydc[i] = optomisticLEDDetector.intensity;
                    } while ((freq == 0) & (retrycount++ < 5));
                }
                else
                {
                    valuearray[i] = labjackDAQ.ReadChannel("LED_DETECTOR");
                }
            }

            freq = valuearray.Average();
            dutyCycle = valuearraydc.Average();
            return 0;
        }

        /// <summary>
        /// Will read the LED Detector and see if it is in the color range.
        /// </summary>
        /// <param name="ledColor"></param>
        /// <param name="readFreq"></param>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        public int CheckLedColor(string checkColor, string freqRange, ref int readFreq, ref int readIntensity, ref string outmsg)
        {
            int status = 0;
            double freq = 0;
            int checkFreqLow = 0;
            int checkFreqHigh = 0;
            double intensity = 0;

            outmsg = string.Empty;
            readFreq = 0;
            if (freqRange == string.Empty)
            {
                status = (int)ErrorCodes.System.ParameterFormatError;
                outmsg = "LED color range for " + checkColor + " can not be empty.";
                return status;
            }
            checkFreqLow = Convert.ToInt32(freqRange.Substring(0, freqRange.IndexOf(',')));
            checkFreqHigh = Convert.ToInt32(freqRange.Substring(freqRange.IndexOf(',') + 1));

            if (status == 0)
                status = ReadLEDDetector(ref freq, ref intensity);

            if (status == 0)
            {
                readFreq = (int)freq;
                readIntensity = (int)intensity;
                if (((int)freq < checkFreqLow) || ((int)freq > checkFreqHigh))
                {
                    status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                    outmsg = string.Format("Reading({0:N0}) for color {1} is outside the limits({2}-{3})",
                                            freq, checkColor, checkFreqHigh, checkFreqLow);
                }
            }
            return status;
        }

        /// <summary>
        /// Will read the LED Detector and see if it is in the color/int range.
        /// </summary>
        /// <param name="ledColor"></param>
        /// <param name="readFreq"></param>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        public int CheckLedValues(int checkFreqLow, int checkFreqHigh, int checkIntLow, int checkIntHigh,
                    ref int freqStatus, ref int intensityStatus, ref int readFreq, ref int readIntensity, ref string outmsg)
        {
            int status = 0;
            double freq = 0;
            double intensity = 0;

            outmsg = string.Empty;
            readFreq = 0;
            freqStatus = 0;
            intensityStatus = 0;

            if (status == 0)
                status = ReadLEDDetector(ref freq, ref intensity);

            if (status == 0)
            {
                readFreq = (int)freq;
                readIntensity = (int)intensity;
                if (checkFreqLow != -1)         // -1 indicates that it is not to be tested
                {
                    if (((int)freq < checkFreqLow) || ((int)freq > checkFreqHigh))
                    {
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        freqStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        outmsg = string.Format("Reading({0:N0}) is outside the limits({1}-{2})",
                                                freq, checkFreqHigh, checkFreqLow);
                    }
                }
                if (checkIntLow != -1)         // -1 indicates that it is not to be tested
                {
                    if (((int)intensity < checkIntLow) || ((int)intensity > checkIntHigh))
                    {
                        status = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        intensityStatus = (int)ErrorCodes.Program.ValueOutsideOfLimit;
                        outmsg = string.Format("Reading({0:N0}) is outside the limits({1}-{2})",
                                                intensity, checkIntHigh, checkIntLow);
                    }
                }
            }
            return status;
        }


        //-------------------------------------------------------
        // Class utilities
        //-------------------------------------------------------

        /// <summary>
        /// Find the channel number for a named channel
        /// </summary>
        /// <param name="channelName">Text name of channel</param>
        /// <returns>channel data. null if not found</returns>
        public FixtureInstrument.Channel FindChannelData(string channelName)
        {
            foreach (var item in ChannelList)
            {
                if (channelName.ToUpper().Equals(item.name))
                {
                    return item;
                }
            }
            return null;
        }


        /// <summary>
        /// Will look for fixtureTypeDefs.xml file. If found, will add the defs
        /// to the fixDefs list. Will replace predefined if type matches predefined.
        /// </summary>
        /// <param name="fixDefs">current list of fixture definitions</param>
        public void GetFixtureDefFile (ref List<FixtureInstrument.fixtureTypes> fixDefs)
        {
            var path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            path = Path.Combine(path, "FixtureTypeDefs.xml");
            path = path.Substring(6);   // get rid of the 'file://' at the begining
            if (File.Exists(path))
            {
                FixtureInstrument.fixtureTypes readTypes = new FixtureInstrument.fixtureTypes();
                XDocument xdoc;
                xdoc = XDocument.Load(path);

                var xx = xdoc.Elements();

                foreach (var typeDefs in xx.Descendants("Fixture"))
                {
                    readTypes.desc = string.Empty;
                    readTypes.hasEnlightedInterface = false;     // enlighted interface, CU and 0-10V output
                    readTypes.hasConsolePort = false;             // has a console port to the DUT
                    readTypes.usesCUPortForConsole = false;       // The console port is on the CU port.
                    readTypes.hasBLE = false;                    // has bluetooth
                    readTypes.hasPCBAPrinter = false;             // has a PCBA printer
                    readTypes.hasHLAPrinter = false;              // has a HLA printer
                    readTypes.hasTestRadio = false;               // has a test radio
                    readTypes.hasSniffer = false;                 // has a sniffer

                    foreach (var item in typeDefs.Elements())
                    {
                        if (item.Name.ToString().ToUpper() == "DESC")
                            readTypes.desc = item.Value.ToString();
                        if (item.Name.ToString().ToUpper() == "TYPE")
                            readTypes.fixtureType = item.Value.ToString();
                        if (item.Name.ToString().ToUpper() == "HASENLIGHTEDINTERFACE")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasEnlightedInterface = true;
                        if (item.Name.ToString().ToUpper() == "HASBLE")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasBLE = true;
                        if (item.Name.ToString().ToUpper() == "HASPCBAPRINTER")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasPCBAPrinter = true;
                        if (item.Name.ToString().ToUpper() == "HASHLAPRINTER")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasHLAPrinter = true;
                        if (item.Name.ToString().ToUpper() == "HASCONSOLEPORT")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasConsolePort = true;
                        if (item.Name.ToString().ToUpper() == "USESCUPORTFORCONSOLE")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.usesCUPortForConsole = true;
                        if (item.Name.ToString().ToUpper() == "HASTESTRADIO")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasTestRadio = true;
                        if (item.Name.ToString().ToUpper() == "HASSNIFFER")
                            if (item.Value.ToUpper() == "YES")
                                readTypes.hasSniffer = true;
                    }

                    // see if this def is in the list alread
                    bool found = false;
                    for (int i = 0; i < fixDefs.Count; i++)
                    {
                        if (fixDefs[i].fixtureType.ToUpper() == readTypes.fixtureType.ToUpper())
                            fixDefs[i] = readTypes;         // replace with what is in the file
                    }
                    if (!found)     // if not found in the list, it is new
                        fixDefs.Add(readTypes);
                }
            }
        }

        public bool IsInstrumentUsed(FixtureInstrument.InstrumentID id)
        {
            if (stationConfigure.FixtureEquipment.channelDefs == null)
                return false;
            return stationConfigure.FixtureEquipment.channelDefs.Exists(x => x.instrumentID == id);
        }

        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs>
            StatusUpdated;

        protected virtual void
            OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }


    }
}

