﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorManufSY2
{
    public partial class ReloadBanner : Form
    {
        private FixtureFunctions lclFixture;

        public ReloadBanner(FixtureFunctions fixture)
        {
            InitializeComponent();
            timer1.Enabled = true;
            lclFixture = fixture;
            while (fixture.IsBoxButtonPushed()) ;     // wait for box button up, if it is pushed
        }

        private void checkBoxButton(object sender, EventArgs e)
        {
            if (lclFixture.IsBoxButtonPushed())
            {
                timer1.Enabled = false;
                DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

    }
}
