﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

using FixtureInstrumentBase;
using System.Threading;

namespace SensorManufSY2
{
    class SwitchMateInstrument : FixtureInstrument, IFixtureInstrument
    {
        public enum RelayStates { Open, Close };

        //------------------
        // Properties
        //----------------


        //--------------------------------------------
        //  Switch-Mate data
        //--------------------------------------------
        private string comPort = string.Empty;
        private SerialPort serialPort;                          // place to save the Switch-Mate serial port handle
        private const int baud = 19200;                         // default baud rate
        private const string sm_prompt = "-> ";                 // prompt
        private const string sm_newline = "\r";                 // new line character
        private const string switchMateID = "SWITCH-MATE";      // leading part of the board ID. Read with SM_ID?
        private const string sm_dutPowerRelay = "01";              // relay number for DUT power
        private const string sm_pirSource = "02";                  // relay number for the PIR source

        private const string sm_select_relay = "SM_SR";         // select specific relay, rrs - rr= relay 01-08, s=0/1
        private const string sm_get_rly_status = "SM_RS?";      // get relay status
        private const string sm_master_reset = "SM_MR";         // master reset
        private const string sm_get_device_id = "SM_ID?";       // get device ID

        public SwitchMateInstrument (string port)
        {
            currentInstrument = InstrumentID.SwitchMate;
            comPort = port;
        }

        /// <summary>
        /// Adds channels to channel list. Passing a blank name will clear the list.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="channel"></param>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        public bool AddChannel(string name, int channel, ChannelType type, int range)
        {
            if (ChannelList == null)
                ChannelList = new List<Channel>();
            if (name == string.Empty)
                ChannelList.Clear();

            Channel temp = new Channel();
            temp.name = name;
            temp.channel = channel;
            temp.type = type;
            temp.lastReading = 0;
            temp.instrumentID = currentInstrument;
            temp.range = range;
            ChannelList.Add(temp);
            return true;
        }

        /// <summary>
        /// Connect to the switch mate
        /// </summary>
        /// <returns>true = connected</returns>
        public bool Connect()
        {
            string readData = string.Empty;

            LastFunctionErrorMessage = string.Empty; ;
            LastFunctionStatus = 0;

            // setup the serial port
            serialPort = new SerialPort();
            serialPort.PortName = comPort;
            serialPort.BaudRate = baud;
            serialPort.ReadTimeout = 100;
            serialPort.NewLine = sm_newline;
            try
            {
                serialPort.Open();
                serialPort.WriteLine(sm_get_device_id);
                readData = serialPort.ReadTo(sm_prompt);
                Thread.Sleep(500);
                serialPort.WriteLine(sm_get_device_id);  // Do a second time just incase garbage
                readData = serialPort.ReadTo(sm_prompt);
                Thread.Sleep(500);
                if (!readData.Contains(switchMateID))
                {
                    LastFunctionErrorMessage = "Switch-Mate ID is wrong. " + readData;
                    LastFunctionStatus = InsturmentError_CouldNotConnect;
                    return false;
                }
            }
            catch (Exception ex)        // something went wrong getting the port open or reading the ID
            {
                LastFunctionErrorMessage = "Switch-Mate ID is wrong. " + readData + " : " + ex.Message;
                LastFunctionStatus = InsturmentError_CouldNotConnect;
                return false;
            }
            return true;
        }

        public void Disconnect()
        {
            LastFunctionErrorMessage = string.Empty; ;
            LastFunctionStatus = 0;

            if (serialPort.IsOpen)
                serialPort.Close();
            if (serialPort != null)
                serialPort.Dispose();
        }

        /// <summary>
        /// Will read the status of the relay channel. 0=open, 1=closed
        /// </summary>
        /// <param name="channelName"></param>
        /// <returns></returns>
        public double ReadChannel(string channelName)
        {
            Channel[] channelData = new Channel[1];

            channelData[0] = new Channel();

            channelData[0].name = channelName;
            ReadListOfChannels(ref channelData); // do not have to do errorcheck. LastErrorxxx set by ReadListOfChannels
            return channelData[0].lastReading;
        }

        /// <summary>
        /// Will read a list of relay channel status. 0=open, 1=closed
        /// </summary>
        /// <param name="channelsData">Channel struct</param>
        public void ReadListOfChannels(ref Channel[] channelsData)
        {
            string responseString = string.Empty;
            int channelNumber = 0;
            Channel channelData = new Channel();

            LastFunctionErrorMessage = string.Empty; ;
            LastFunctionStatus = 0;

            if (serialPort == null)
            {
                LastFunctionErrorMessage = "Switch Mate is not port is not connected.";
                LastFunctionStatus = InstrumentError_NotConnected;
                return;
            }
            if (!serialPort.IsOpen)
            {
                LastFunctionErrorMessage = "Switch Mate is not port is not connected.";
                LastFunctionStatus = InstrumentError_NotConnected;
                return;
            }

            for (int i = 0; i < channelsData.Length; i++)
            {

                if ((channelData = FindChannelData(channelsData[i].name)) == null)       // if the name was not found
                {
                    LastFunctionErrorMessage = "Channel " + channelsData[i].name + " is not defined.";
                    LastFunctionStatus = InstrumentError_RequestedChannelNotFound;
                    return;
                }

                if (!Int32.TryParse(channelData.name, out channelNumber))
                {
                    LastFunctionErrorMessage = "channelName must be a interger value.  Found " + channelData.name;
                    LastFunctionStatus = InstrumentError_InvalidParameter;
                    return;
                }
                if ((channelNumber < 1) | (channelNumber > 8))
                {
                    LastFunctionErrorMessage = "channelName must be a interger value 1 to 8.  Found " + channelData.name;
                    LastFunctionStatus = InstrumentError_InvalidParameter;
                    return;
                }

                try
                {
                    serialPort.Write(sm_get_rly_status + "\r");
                    responseString = serialPort.ReadTo(sm_prompt);
                    if (responseString.Substring(responseString.IndexOf('<') + channelNumber, 1) == "1") // if showing closed
                    {
                        channelsData[i].lastReading = 1;
                    }
                    else
                    {
                        channelsData[i].lastReading = 0;
                    }
                }
                catch (Exception ex)
                {
                    LastFunctionErrorMessage = "Error getting relay status. " + ex.Message;
                    LastFunctionStatus = InstrumentError_ReadError;
                    return;
                }
            }
        }

        /// <summary>
        /// Set a named channel
        /// </summary>
        /// <param name="channelName">name of channel</param>
        /// <param name="value">value to set to</param>
        public void SetChannel(string channelName, double value)
        {
            string responseString = string.Empty;
            RelayStates state = RelayStates.Open;
            Channel channelData = new Channel();

            LastFunctionErrorMessage = string.Empty; ;
            LastFunctionStatus = 0;

            if (serialPort == null)
            {
                LastFunctionErrorMessage = "Switch Mate is not port is not connected.";
                LastFunctionStatus = InstrumentError_NotConnected;
                return;
            }
            if (!serialPort.IsOpen)
            {
                LastFunctionErrorMessage = "Switch Mate is not port is not connected.";
                LastFunctionStatus = InstrumentError_NotConnected;
                return;
            }

            if ((channelData = FindChannelData(channelName)) == null)       // if the name was not found
            {
                LastFunctionErrorMessage = "Channel " + channelName + " is not defined.";
                LastFunctionStatus = InstrumentError_RequestedChannelNotFound;
                return;
            }

            if (value == 0) // if open
                state = RelayStates.Open;
            else
                state = RelayStates.Close;

            try
            {
                serialPort.DiscardInBuffer();
                serialPort.DiscardOutBuffer();
                if (state == RelayStates.Open)
                {
                    serialPort.Write(sm_select_relay + channelData.channel.ToString("D2") + "0\r");
                }
                else
                {
                    serialPort.Write(sm_select_relay + channelData.channel.ToString("D2") + "1\r");
                }
                serialPort.ReadTo(sm_prompt);            // wait for the command response prompt
                // verify that it was set
                serialPort.Write(sm_get_rly_status + "\r");
                responseString = serialPort.ReadTo(sm_prompt);
                if (responseString.Substring(responseString.IndexOf('<') + channelData.channel, 1) == "1") // if showing closed
                {
                    if (state == RelayStates.Open)      // if showing closed when it should be open
                    {
                        LastFunctionStatus = InsterumentError_ContactStatusReadbackWrong;
                        LastFunctionErrorMessage = "Relay not in correct state";
                        return;
                    }
                }
                Thread.Sleep(500);                              // so next does not come to quick
            }
            catch (Exception ex)
            {
                LastFunctionStatus = InstrumentError_ReadError;
                LastFunctionErrorMessage = "Error issuing relay set command. " + ex.Message;
                return;
            }
            return;
        }
    }
}
