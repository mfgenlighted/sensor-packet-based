﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorManufSY2
{
    public partial class PickFixture : Form
    {
        public string SelectedFixture = string.Empty;

        public PickFixture(List<string> fixtures)
        {
            InitializeComponent();
            cbFixturePick.DataSource = fixtures;
        }

        private void bDone_Click(object sender, EventArgs e)
        {
            SelectedFixture = cbFixturePick.Text;
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
