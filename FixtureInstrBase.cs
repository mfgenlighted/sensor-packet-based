﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FixtureInstrumentBase
{
    public abstract class FixtureInstrument
    {
        public InstrumentID currentInstrument;
        public enum InstrumentID { NotDefined, LABJACK, A34970A, SwitchMate, Optomistic };
        public enum ChannelType { DInput, DOutput, ACInput, DCInput, Temperature, Contact, Frequency, DAC};

        public string LastFunctionErrorMessage = string.Empty;              // if a function had a error, contains error message
        public int LastFunctionStatus = 0;    // status of the last function called

        public const int InsturmentError_NoError = 0;                       // 0
        public const int InsturmentError_CouldNotConnect = 1;               // could not connect
        public const int InstrumentError_ChannelAddInstOpen = 2;            // can not add a channel if the instrument is connected
        public const int InstrumentError_ChannelAdd = 3;                    // error trying to add channels
        public const int InstrumentError_RequestedChannelNotFound = 4;      // the channel name was not found in the setup list
        public const int InstrumentError_ReadError = 5;                     // error reading the channel
        public const int InstrumentError_WrongChannelType = 6;              // requested channel does not support type assked for
        public const int InstrumentError_NoChannelList = 7;                 // must have a channel list before connecting to instrument
        public const int InstrumentError_InvalidFunction = 8;               // instrument does not support this function
        public const int InstrumentError_InvalidParameter = 9;              // a parameter passed is invalid for this function
        public const int InstrumentError_NotConnected = 10;                 // instrurment is not connected
        public const int InsterumentError_ContactStatusReadbackWrong = 11;  // the readback of a set contact was wrong value

        public class fixtureTypes
        {
            public string desc = string.Empty;
            public string fixtureType = string.Empty;       // defines the config of a fixture

            // interfaces
            public bool hasEnlightedInterface = false;      // enlighted interface, CU and 0-10V output
            public bool hasDaliInterface = false;           // has 2 wire dali interface
            public bool hasConsolePort = false;             // has a console port to the DUT.
            public bool usesCUPortForConsole = false;       // The console port is on the CU port.
            public bool usesPacketBasedConsole = false;     // decoded packet base commuications
            public bool usesTextBaseConsole = false;        // text based commuications

            // DUT radios
            public bool hasBLE = false;                     // has bluetooth.
            public bool hasEnlighteRadio = false;           // has Enlighted radio

            // test radios
            public bool hasTestRadio = false;               // has a test radio(Enlighted only)
            public bool usesAtmelRefRadio = false;          // used the old Atmel ref radio for enlighted radio test
            public bool usesKalapanaRefRadio = false;       // use Kalapana radio for enlighted radio test
            public bool hasSniffer = false;                 // has a sniffer
            public bool hasPCBAPrinter = false;             // has a PCBA printer
            public bool hasHLAPrinter = false;              // has a HLA printer
            public bool hasFlasher = false;                 // has Flasher programmer

            // instruments
            public List<FixtureInstrument.Channel> channelDefs;               // definition of the channels on DAQ
        };

        public FixtureInstrument()
        {
            if (ChannelList != null)
                ChannelList = null;
        }


        public class Channel
        {
            public string name;
            public int channel;
            public ChannelType type;
            public double lastReading;
            public InstrumentID instrumentID;
            public int range;                   // if set to 0, it will autorange if instrument can
        }

        public List<Channel> ChannelList = null;
        public Channel ChannelData;

        /// <summary>
        /// Get the last error status. They only errors that these functions throw are system errors. 
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="code"></param>
        public void GetLastError(ref string msg, ref int code)
        {
            msg = LastFunctionErrorMessage;
            code = LastFunctionStatus;
        }


        //----------  utils
        /// <summary>
        /// Find the channel number for a named channel
        /// </summary>
        /// <param name="channelName">Text name of channel</param>
        /// <returns>channel data. null if not found</returns>
        public Channel FindChannelData(string channelName)
        {
            foreach (var item in ChannelList)
            {
                if (channelName.ToUpper().Equals(item.name))
                {
                    return item;
                }
            }
            return null;
        }

    }
}
