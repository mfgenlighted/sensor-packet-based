﻿namespace SensorManufSY2
{
    partial class GetMAC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbMAC1 = new System.Windows.Forms.TextBox();
            this.abortButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbMAC2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(43, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "MAC1";
            // 
            // tbMAC1
            // 
            this.tbMAC1.Location = new System.Drawing.Point(43, 45);
            this.tbMAC1.Name = "tbMAC1";
            this.tbMAC1.Size = new System.Drawing.Size(130, 20);
            this.tbMAC1.TabIndex = 0;
            this.tbMAC1.Text = "6854F5";
            // 
            // abortButton
            // 
            this.abortButton.BackColor = System.Drawing.Color.Red;
            this.abortButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.abortButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.abortButton.Location = new System.Drawing.Point(140, 123);
            this.abortButton.Name = "abortButton";
            this.abortButton.Size = new System.Drawing.Size(87, 41);
            this.abortButton.TabIndex = 3;
            this.abortButton.Text = "Exit Test";
            this.abortButton.UseVisualStyleBackColor = false;
            // 
            // okButton
            // 
            this.okButton.BackColor = System.Drawing.Color.Lime;
            this.okButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.okButton.Location = new System.Drawing.Point(22, 123);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(100, 41);
            this.okButton.TabIndex = 2;
            this.okButton.Text = "Start Test";
            this.okButton.UseVisualStyleBackColor = false;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(43, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "MAC2";
            // 
            // tbMAC2
            // 
            this.tbMAC2.Location = new System.Drawing.Point(43, 85);
            this.tbMAC2.Name = "tbMAC2";
            this.tbMAC2.Size = new System.Drawing.Size(130, 20);
            this.tbMAC2.TabIndex = 1;
            this.tbMAC2.Text = "6854F5";
            // 
            // GetMAC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 196);
            this.ControlBox = false;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbMAC2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbMAC1);
            this.Controls.Add(this.abortButton);
            this.Controls.Add(this.okButton);
            this.Name = "GetMAC";
            this.Text = "Get MAC";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbMAC1;
        private System.Windows.Forms.Button abortButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbMAC2;
    }
}